/**
 * This file is part of vessel.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt).
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#ifndef VESSEL_LIB_AUTHOR_H
#define VESSEL_LIB_AUTHOR_H

#include <build-config.h>

#include <memory>

#include <liberate/types/byte.h>
#include <liberate/logging.h>
#include <liberate/sys/memory.h>

#include <s3kr1t/keys.h>
#include <s3kr1t/signatures.h>

#include <vessel/error.h>
#include <vessel/author.h>

#include "algorithms.h"

namespace vessel {

/**
 * Create a default author id
 */
inline byte_sequence
create_default_author_id(algorithm_options const & opts)
{
  byte_sequence author_id;
  author_id.resize(opts.author_identifier_size);
  liberate::sys::secure_memzero(&author_id[0], author_id.size());
  return author_id;
}


/**
 * Check a byte sequence is a valid author id; this only checks the
 * size.
 */
inline bool
is_valid_author_id(algorithm_options const & opts,
    byte_sequence const & author_id)
{
  return author_id.size() > 0 && author_id.size() == opts.author_identifier_size;
}


/**
 * Generic function for deriving an author id from a key. This is used
 * below, and specialized for the above.
 */
inline byte_sequence
extract_author_id(algorithm_options const & opts,
    s3kr1t::key const & key)
{
  byte_sequence authid;

  if (!opts.author_identifier_size) {
    LIBLOG_ERROR("Need an author identifier size!");
    return authid;
  }

  if (!key.is_public()) {
    LIBLOG_ERROR("Need a public key to derive an identifier from.");
    return authid;
  }

  // If no hashing is to be done, we need to check if that's valid.
  if (s3kr1t::DT_NONE == opts.author_identifier_hash_type) {
    if (s3kr1t::KT_ED != key.type()) {
      LIBLOG_ERROR("Invalid hash algorithm for key type!");
      return authid;
    }

    authid.resize(opts.author_identifier_size);
    size_t written = 0;
    auto err = key.write(static_cast<void *>(&authid[0]), authid.size(),
        s3kr1t::KE_RAW, written);
    if (s3kr1t::ERR_SUCCESS != err) {
      LIBLOG_ERROR("Could not encode key: " << s3kr1t::error_name(err));
      liberate::sys::secure_memzero(&authid[0], authid.size());
      return authid;
    }

    authid.resize(written);
  }
  else {
    // Otherwise hash the key according to the hash type.
    authid = key.hash(opts.author_identifier_hash_type);
  }

  if (authid.size() != opts.author_identifier_size) {
    LIBLOG_DEBUG("Bad author identifer size");
    liberate::sys::secure_memzero(&authid[0], authid.size());
  }
  return authid;
}


/**
 * The author class is parametrized by the author id type, but defaults to the
 * above. It takes a public key or key pair, and provides an abstraction for
 * authoring related functions. These are *very* thin wrappers around s3kr1t
 * keys, and mainly provide some kind of common frame of reference.
 *
 * However, the author class also keeps track of a counter value to aid in the
 * creation of new extents.
 *
 * Authors are meant to be copied in a lightweight fashion, so pimpl their
 * data.
 */
template <
  typename counterT // FIXME can default?
>
struct author
{
public:
  using counter_t = counterT;
  using author_id_t = byte_sequence;

  static constexpr char const * const KEY_PARAMS = "ed25519";
  // FIXME align this with algorithms.h
  static constexpr size_t ED25519_PUBKEY_DER_SIZE = 44;
  static constexpr size_t ED25519_PRIVKEY_DER_SIZE = 48;

  inline explicit author(
      algorithm_options const & opts,
      counter_t const & initial_counter = {})
    : m_data{std::make_shared<data>()}
  {
    m_data->opts = opts;
    m_data->counter = initial_counter;
  }

  inline explicit author(
      algorithm_options const & opts,
      s3kr1t::key const & key,
      counter_t const & initial_counter = {})
    : m_data{std::make_shared<data>()}
  {
    m_data->opts = opts;
    m_data->counter = initial_counter;

    m_data->key_pair = key.create_pair();
    if (!m_data->key_pair.is_valid()) {
      m_data->key_pair.pubkey = std::make_shared<s3kr1t::key>(key);
    }

    m_data->id = extract_author_id(m_data->opts, *(m_data->key_pair.pubkey));
    LIBLOG_DEBUG("Author initialized with ID " << m_data->id);
  }

  inline author(algorithm_options const & opts, author_id_t const & id)
    : m_data{std::make_shared<data>()}
  {
    m_data->opts = opts;
    m_data->id = id;
    LIBLOG_DEBUG("Author initialized with ID " << m_data->id);
  }

  inline author(author const &) = default;
  inline author(author &&) = default;
  inline author & operator=(author const &) = default;

  inline bool operator==(author const & other) const
  {
    return m_data->id == other.m_data->id;
  }

  inline bool operator<(author const & other) const
  {
    return m_data->id < other.m_data->id;
  }

  inline author_id_t author_id() const
  {
    return m_data->id;
  }

  // Authors can sign/encrypt/author/create if they have a private key,
  // otherwise only verify.
  inline bool can_create() const
  {
    return static_cast<bool>(m_data->key_pair.privkey);
  }

  inline bool can_verify() const
  {
    return static_cast<bool>(m_data->key_pair.pubkey);
  }

  inline vessel_error_t
  sign(void * sigbuf, size_t & sigbuf_size,
      void const * text, size_t text_size)
  {
    if (!m_data->key_pair.privkey) {
      LIBLOG_ERROR("Need a private key to sign.");
      return VESSEL_ERR_INVALID_VALUE;
    }

    size_t required = 0;
    auto err = s3kr1t::sign(sigbuf, sigbuf_size, required,
        text, text_size, *(m_data->key_pair.privkey),
        s3kr1t::DT_NONE);

    sigbuf_size = required;
    if (s3kr1t::ERR_SUCCESS != err) {
      // TODO we could distinguish between a few different error cases here,
      //      but to be honest, that seems redundant.
      return VESSEL_ERR_AUTHORING;
    }
    return VESSEL_ERR_SUCCESS;
  }

  inline vessel_error_t
  verify(void const * text, size_t text_size,
      void const * sigbuf, size_t sigbuf_size)
  {
    if (!m_data->key_pair.pubkey) {
      LIBLOG_ERROR("Need a public key to verify.");
      return VESSEL_ERR_INVALID_VALUE;
    }

    auto err = s3kr1t::verify(text, text_size, sigbuf, sigbuf_size,
        *(m_data->key_pair.pubkey),
        s3kr1t::DT_NONE);
    if (s3kr1t::ERR_SUCCESS != err) {
      // TODO we could distinguish between a few different error cases here,
      //      but to be honest, that seems redundant.
      return VESSEL_ERR_VERIFICATION;
    }
    return VESSEL_ERR_SUCCESS;
  }

  // Next to the above, authors maintain a counter. That is, they can increment
  // and return their current counter value. But for initializing the currently
  // held counter, they can also be updated.
  inline bool update_counter(counter_t const & encountered)
  {
    if (encountered > m_data->counter) {
      m_data->counter = encountered;
      return true;
    }
    return false;
  }

  inline counter_t reserve_counter()
  {
    return ++(m_data->counter);
  }

  inline counter_t current_counter()
  {
    return m_data->counter;
  }

  inline vessel_error_t
  generate_keypair()
  {
    auto privkey = s3kr1t::key::generate(s3kr1t::KT_ED, KEY_PARAMS);
    if (!privkey) {
      LIBLOG_ERROR("Could not generate key pair!");
      return VESSEL_ERR_KEY_GENERATION;
    }

    m_data->key_pair = privkey.create_pair();
    if (!m_data->key_pair) {
      LIBLOG_ERROR("Could not extract key pair from private key!");
      return VESSEL_ERR_KEY_GENERATION;
    }

    m_data->id = extract_author_id(m_data->opts, *(m_data->key_pair.pubkey));
    return VESSEL_ERR_SUCCESS;
  }


  static inline constexpr size_t
  key_size(int key_part)
  {
    switch (key_part) {
      case VESSEL_KP_PUBLIC:
        return ED25519_PUBKEY_DER_SIZE;

      case VESSEL_KP_PRIVATE:
        return ED25519_PRIVKEY_DER_SIZE;

      default:
        // XXX not in constexpr
        // LIBLOG_ERROR("Cannot specify keysize for unknown part: " << key_part);
        return 0;
    }
  }


  inline vessel_error_t
  deserialize(void const * buffer, size_t bufsize)
  {
    try {
      m_data->key_pair = s3kr1t::key::read_keys(buffer, bufsize);
      if (!m_data->key_pair) {
        LIBLOG_ERROR("Failed to deserialize author key(s)!");
        return VESSEL_ERR_KEY_SERIALIZATION;
      }

      m_data->id = extract_author_id(m_data->opts, *(m_data->key_pair.pubkey));
    } catch (s3kr1t::exception const & ex) {
      LIBLOG_ERROR("Failed to deserialize author key(s): "
          << ex.what());
      return VESSEL_ERR_KEY_SERIALIZATION;
    }
    return VESSEL_ERR_SUCCESS;
  }



  inline vessel_error_t
  serialize(void * buffer, size_t bufsize, int key_part) const
  {
    s3kr1t::key * the_key = nullptr;

    switch (key_part) {
      case VESSEL_KP_PUBLIC:
        if (!can_verify()) {
          LIBLOG_ERROR("Cannot serialize requested part: this author cannot verify.");
          return VESSEL_ERR_INVALID_VALUE;
        }
        the_key = m_data->key_pair.pubkey.get();
        break;

      case VESSEL_KP_PRIVATE:
        if (!can_create()) {
          LIBLOG_ERROR("Cannot serialize requested part: this author cannot sign.");
          return VESSEL_ERR_INVALID_VALUE;
        }
        the_key = m_data->key_pair.privkey.get();
        break;

      default:
        LIBLOG_ERROR("Cannot serialize invalid key part: " << key_part);
        return VESSEL_ERR_INVALID_VALUE;
    }

    // Ok, write to the buffer
    size_t written = 0;
    auto err = the_key->write(buffer, bufsize, s3kr1t::KE_DER, written);
    if (s3kr1t::ERR_SUCCESS != err) {
      LIBLOG_ERROR("Error serializing author!");
      return VESSEL_ERR_KEY_SERIALIZATION;
    }

    return VESSEL_ERR_SUCCESS;
  }

private:
  struct data
  {
    algorithm_options opts = {};
    s3kr1t::key::pair key_pair = {};
    counter_t         counter = {};
    author_id_t       id = {};
  };

  std::shared_ptr<data> m_data;
};


using default_author = author<uint64_t>;

} // namespace vessel


extern "C" {

/**
 * The public author ID structure contains an author id, as well as a reference
 * to algorithm options.
 */
struct vessel_author_id
{
  vessel::algorithm_options   opts{};
  vessel::byte_sequence       author_id{};

  inline explicit vessel_author_id(vessel::algorithm_options const & _opts)
    : opts{_opts}
    , author_id{create_default_author_id(opts)}
  {
  }

  inline vessel_author_id()
  {
    // Author id is zero length and uninitialized
  }
};

/**
 * Similarly, define the author struct.
 */
struct vessel_author
{
  using author_type = vessel::default_author;
  author_type author;

  inline explicit vessel_author(vessel::algorithm_options const & _opts)
    : author{_opts}
  {
  }
};


} // extern "C"

namespace vessel {

inline std::ostream &
operator<<(std::ostream & os, struct vessel_author_id const & ext_id)
{
  os << ext_id.author_id;
  return os;
}

} // namespace vessel

#endif // guard
