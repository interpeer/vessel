/**
 * This file is part of vessel.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt).
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#ifndef VESSEL_LIB_SECTION_H
#define VESSEL_LIB_SECTION_H

#include <build-config.h>

#include <vessel/section.h>

#include <map>
#include <functional>

#include "algorithms.h"
// FIXME including payload.h produces errors, but the map
//       of section type to callbacks best lives here, not in
//       resource's static member
namespace vessel {

/**
 * Forward declarations
 */
struct resource;
struct section;
struct payload;
struct extent;

using integrity_creation_callback = std::function<bool (resource const &, extent const &, section &, payload const &)>;
using integrity_validation_callback = std::function<bool (resource const &, extent const &, section const &, payload const &)>;

struct integrity_callbacks
{
  integrity_creation_callback   create{};
  integrity_validation_callback validate{};
};

} // namespace vessel

extern "C" {

/**
 * Definition of the opaque struct
 */
struct vessel_section_context
{
  vessel_algorithm_context const *  algos = nullptr;

  using section_size_map = std::map<vessel_section_t, int>;
  section_size_map                  section_sizes = {};

  using integrity_callback_map = std::map<vessel_section_t, vessel::integrity_callbacks>;
  integrity_callback_map            integrity_callbacks = {};

  vessel_section_context();
};


} // extern "C"

namespace vessel {

static constexpr uint16_t const SEC_MAX_SINGLE_WORD = 0b0111'1111'1111'1111;
static constexpr uint32_t const SEC_MAX_SIZE        = 0b0111'1111'1111'1111'1111'1111'1111'1111;
static constexpr uint16_t const SEC_DOUBLE_WORD_BIT = ~SEC_MAX_SINGLE_WORD;

/**
 * Returns the size of a variable section based on the data size.
 */
inline ssize_t
variable_section_size(std::size_t const & data_size)
{
  // Cannot determine the size if no data is given; it's 6 or 8 plus data size.
  if (!data_size) {
    return 0;
  }

  std::size_t sec_size = 4 + 2 + data_size;
  if (sec_size > SEC_MAX_SINGLE_WORD) {
    sec_size += 2;
  }
  if (sec_size > SEC_MAX_SIZE) {
    sec_size = SEC_MAX_SIZE;
  }
  return sec_size;
}


/**
 * Given a section_size_map and algorithm_options, return a section size.
 * Returns:
 * - the section size, or 0, or <0
 *    - the section size if it's a fixed size, or if for a variable
 *      sized section, a data size is provided.
 *    - 0 if it's a variable sized section and no data size is given
 *    - < 0 on errors
 * - if the section size is fixed (true) or variable (false)
 *
 * If {0, false} is returned, it cannot be determined what the variable
 * section size would be - but it is going to be at least 6 bytes, possibly
 * 8 bytes, plus the data size.
 */
inline std::tuple<ssize_t, bool>
section_size(
    vessel_section_context::section_size_map const & section_sizes,
    vessel::algorithm_options const & opts,
    vessel_section_t section,
    size_t variable_data_size)
{
  switch (section) {
    case VESSEL_SECTION_CRC32:
      // Simple, fixed size:
      return {8, true};

    case VESSEL_SECTION_MAC:
      // Need the message authentication size from the choices.
      return {4 + opts.message_authentication_size, true};

    case VESSEL_SECTION_SIGNATURE:
      // Need the signature size from the choices.
      return {4 + opts.signature_size, true};

    case VESSEL_SECTION_CONTENT_TYPE:
    case VESSEL_SECTION_BLOB:
      // Variable sized sections
      return {variable_section_size(variable_data_size), false};

    default:
      // Unknown section.
      auto iter = section_sizes.find(section);
      if (iter == section_sizes.end()) {
        // Not in registry, unknown.
        return {-1, true};
      }
      if (iter->second) {
        // Fixed size
        return {iter->second, true};
      }
      return {variable_section_size(variable_data_size), false};
  }

  // Shouldn't reach here.
  return {-2, true};
}

} // namespace vessel

#endif // guard
