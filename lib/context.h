/**
 * This file is part of vessel.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt).
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#ifndef VESSEL_LIB_CONTEXT_H
#define VESSEL_LIB_CONTEXT_H

#include <build-config.h>

#include <vessel/context.h>

#include "section.h"

extern "C" {

/**
 * Definition of the opaque struct
 */
struct vessel_context
{
  vessel_algorithm_context const *        algos = nullptr;
  std::unique_ptr<vessel_section_context> sections = {};
};

} // extern "C"

#endif // guard
