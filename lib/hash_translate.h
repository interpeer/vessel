/**
 * This file is part of vessel.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt).
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#ifndef VESSEL_LIB_HASH_TRANSLATE_H
#define VESSEL_LIB_HASH_TRANSLATE_H

#include <build-config.h>

#include <vessel/algorithms.h>
#include <s3kr1t/digests.h>

namespace vessel {

inline s3kr1t::digest_type
hash_translate(vessel_hash_algorithms const & algo)
{
  switch (algo) {
    case VESSEL_HA_SHA3_512:
      return s3kr1t::DT_SHA3_512;
    case VESSEL_HA_SHA3_384:
      return s3kr1t::DT_SHA3_384;
    case VESSEL_HA_SHA3_256:
      return s3kr1t::DT_SHA3_256;
    case VESSEL_HA_SHA3_224:
      return s3kr1t::DT_SHA3_224;
    case VESSEL_HA_SHA2_512:
      return s3kr1t::DT_SHA2_512;
    case VESSEL_HA_SHA2_384:
      return s3kr1t::DT_SHA2_384;
    case VESSEL_HA_SHA2_256:
      return s3kr1t::DT_SHA2_256;
    case VESSEL_HA_SHA2_224:
      return s3kr1t::DT_SHA2_224;

    default:
      break;
  }
  return s3kr1t::DT_NONE;
}

} // namespace vessel

#endif // guard

