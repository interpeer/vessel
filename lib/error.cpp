/**
 * This file is part of vessel.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt).
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#include <vessel/error.h>

/**
 * Stringify the symbol passed to VESSEL_SPRINGIFY()
 **/
#define VESSEL_STRINGIFY(n) VESSEL_STRINGIFY_HELPER(n)
#define VESSEL_STRINGIFY_HELPER(n) #n

/**
 * (Un-)define macros to ensure error defintions are being generated.
 **/
#define VESSEL_START_ERRORS \
  static const struct { \
    char const * const name; \
    vessel_error_t code; \
    char const * const message; \
  } _vessel_error_table[] = {
#define VESSEL_ERRDEF(name, code, desc) \
  { "VESSEL_" VESSEL_STRINGIFY(name), code, desc },
#define VESSEL_END_ERRORS { nullptr, 0, nullptr } };

#undef VESSEL_ERROR_H
#include <vessel/error.h>



/*****************************************************************************
 * Functions
 **/

extern "C" {

char const *
vessel_error_message(vessel_error_t code)
{
  if (code >= VESSEL_ERROR_LAST) {
    return "unidentified error";
  }

  for (size_t i = 0
      ; i < sizeof(_vessel_error_table) / sizeof(_vessel_error_table[0])
      ; ++i)
  {
    if (code == _vessel_error_table[i].code) {
      return _vessel_error_table[i].message;
    }
  }

  return "unidentified error";
}



char const *
vessel_error_name(vessel_error_t code)
{
  if (code >= VESSEL_ERROR_LAST) {
    return "unidentified error";
  }

  for (size_t i = 0
      ; i < sizeof(_vessel_error_table) / sizeof(_vessel_error_table[0])
      ; ++i)
  {
    if (code == _vessel_error_table[i].code) {
      return _vessel_error_table[i].name;
    }
  }

  return "unidentified error";
}

} // extern "C"
