/**
 * This file is part of vessel.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt).
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#ifndef VESSEL_LIB_ERRNO_TRANSLATE_H
#define VESSEL_LIB_ERRNO_TRANSLATE_H


#include <errno.h>

#include <vessel/error.h>

namespace vessel {

inline vessel_error_t
translate_errno()
{
  switch (errno) {
    case EACCES:
    case EDQUOT:
    case EISDIR:
    case EMFILE:
    case ENFILE:
    case ENODEV:
    case ENOENT:
    case ELOOP:
    case ENAMETOOLONG:
    case ENOSPC:
    case ENOTDIR:
    case ENXIO:
    case EOPNOTSUPP:
    case EOVERFLOW:
    case EPERM:
    case EROFS:
    case ETXTBSY:
    case EBADF:
    case ESPIPE:
    case EFBIG:
    case EIO:
    case EBUSY:
      LIBLOG_ERRNO("I/O error");
      return VESSEL_ERR_IO;

    case EFAULT:
      LIBLOG_ERRNO("Memory access error");
      return VESSEL_ERR_MEMORY_ACCESS;

    case EINTR:
      LIBLOG_ERRNO("I/O interrupted");
      return VESSEL_ERR_INTERRUPTED;

    case EINVAL:
      LIBLOG_ERRNO("Invalid value");
      return VESSEL_ERR_INVALID_VALUE;

    case ENOMEM:
      LIBLOG_ERRNO("Out of memory");
      return VESSEL_ERR_OUT_OF_MEMORY;

    case EWOULDBLOCK:
    default:
      break;
  }

  LIBLOG_ERRNO("Unexpected error");
  return VESSEL_ERR_UNEXPECTED;
}

} // namespace vessel

#endif // guard
