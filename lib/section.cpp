/**
 * This file is part of vessel.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt).
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#include <build-config.h>

#include <map>

#include <vessel/section.h>

#include <s3kr1t/macs.h>

#include <liberate/checksum/crc32.h>
#include <liberate/logging.h>

#include "section.h"
#include "algorithms.h"
#include "content_type.h"
#include "serialization.h"
#include "context.h"
#include "payload.h"
#include "resource.h"
#include "crypto.h"

vessel_section_context::vessel_section_context()
{
  integrity_callbacks = {
    // *** CRC32 *********************************************************
    {
      VESSEL_SECTION_CRC32,
      vessel::integrity_callbacks{
        [](vessel::resource const & res [[maybe_unused]],
          vessel::extent const & ext [[maybe_unused]],
          vessel::section & section,
          vessel::payload const & payload) -> bool
        {
          using namespace liberate::checksum;

          auto crc = crc32<CRC32>(payload.buffer + section.section_size,
              payload.buffer + payload.buffer_size);

          crc32_serialize s = crc;
          auto used = liberate::serialization::serialize_int(
              static_cast<char *>(section.data), section.data_size, s);
          if (used != sizeof(s)) {
            LIBLOG_ERROR("Unable to enccode checksum.");
            return false;
          }

          return true;
        },
        [](vessel::resource const & res [[maybe_unused]],
          vessel::extent const & ext [[maybe_unused]],
          vessel::section const & section,
          vessel::payload const & payload) -> bool
        {
          using namespace liberate::checksum;

          auto crc = crc32<CRC32>(payload.buffer + section.section_size,
              payload.buffer + payload.buffer_size);

          crc32_serialize s;
          auto used = liberate::serialization::deserialize_int(
              s, static_cast<char const *>(section.data), section.data_size);
          if (used != sizeof(s)) {
            LIBLOG_ERROR("Unable to deccode checksum.");
            return false;
          }

          if (s != crc) {
            LIBLOG_ERROR("Checksum validation failed.");
            return false;
          }

          return true;
        },
      },
    },

    // *** MAC ***********************************************************
    {
      VESSEL_SECTION_MAC,
      vessel::integrity_callbacks{
        [](vessel::resource const & res [[maybe_unused]],
          vessel::extent const & ext [[maybe_unused]],
          vessel::section & section,
          vessel::payload const & payload) -> bool
        {
          if (section.data_size != ext.opts.message_authentication_size) {
            LIBLOG_ERROR("Invalid section size; need "
                << ext.opts.message_authentication_size << " Bytes of data.");
            return false;
          }

          if (res.m_symmetric_key.empty()) {
            LIBLOG_ERROR("Need a symmetric key for generating MACs, aborting!");
            return false;
          }

          // Generate salt/nonce
          namespace vc = vessel::crypto;
          auto nonce = vc::generate_nonce(ext.opts, ext.header);
          if (nonce.empty()) {
            LIBLOG_ERROR("Cannot create MAC without nonce.");
            return false;
          }

          // Generate MAC. Currently, all alggorithms we have for the MAC use
          // a nonce, but let's be prepared for the ones that might come.
          size_t required = 0;
          auto err = s3kr1t::mac(section.data, section.data_size, required,
              payload.buffer + section.section_size,
              payload.buffer_size - section.section_size,
              res.m_symmetric_key.data(), res.m_symmetric_key.size(),
              nonce.data(), nonce.size(),
              ext.opts.message_authentication_type);
          if (s3kr1t::ERR_SUCCESS != err) {
            LIBLOG_ERROR("Error computing MAC.");
            return false;
          }

          return true;
        },
        [](vessel::resource const & res [[maybe_unused]],
          vessel::extent const & ext [[maybe_unused]],
          vessel::section const & section,
          vessel::payload const & payload) -> bool
        {
          if (section.data_size != ext.opts.message_authentication_size) {
            LIBLOG_ERROR("Invalid section size; need "
                << ext.opts.message_authentication_size << " Bytes of data.");
            return false;
          }

          if (res.m_symmetric_key.empty()) {
            LIBLOG_ERROR("Need a symmetric key for validating MACs, aborting!");
            return false;
          }

          // Generate salt/nonce
          namespace vc = vessel::crypto;
          auto nonce = vc::generate_nonce(ext.opts, ext.header);
          if (nonce.empty()) {
            LIBLOG_ERROR("Cannot validate MAC without nonce.");
            return false;
          }

          // Generate MAC. Currently, all alggorithms we have for the MAC use
          // a nonce, but let's be prepared for the ones that might come.
          auto err = s3kr1t::check(
              payload.buffer + section.section_size,
              payload.buffer_size - section.section_size,
              section.data, section.data_size,
              res.m_symmetric_key.data(), res.m_symmetric_key.size(),
              nonce.data(), nonce.size(),
              ext.opts.message_authentication_type);
          if (s3kr1t::ERR_SUCCESS != err) {
            LIBLOG_ERROR("Error validating MAC.");
            return false;
          }

          return true;

        },
      },
    },

    // *** Signature *****************************************************
    {
      VESSEL_SECTION_SIGNATURE,
      vessel::integrity_callbacks{
        [](vessel::resource const & res [[maybe_unused]],
          vessel::extent const & ext [[maybe_unused]],
          vessel::section & section,
          vessel::payload const & payload) -> bool
        {
          if (section.data_size != ext.opts.signature_size) {
            LIBLOG_ERROR("Invalid section size; need "
                << ext.opts.signature_size << " Bytes of data.");
            return false;
          }

          // The resource keeps authors in its authors map from creating
          // extents. We have to find the author that corresponds to the
          // author id of this extent.
          auto author_iter = res.m_authors.find(ext.header.author_identifier);
          if (author_iter == res.m_authors.end()) {
            LIBLOG_ERROR("No author found for author ID of the extent.");
            return false;
          }
          auto author = author_iter->second;

          // Check that the author can create (a signature)
          if (!author.can_create()) {
            LIBLOG_ERROR("We have an author, but this author cannot sign.");
            return false;
          }

          // Create the signature
          auto err = author.sign(section.data, section.data_size,
              payload.buffer + section.section_size,
              payload.buffer_size - section.section_size);
          if (s3kr1t::ERR_SUCCESS != err) {
            LIBLOG_ERROR("Error computing signature.");
            return false;
          }

          return true;
        },
        [](vessel::resource const & res [[maybe_unused]],
          vessel::extent const & ext [[maybe_unused]],
          vessel::section const & section,
          vessel::payload const & payload) -> bool
        {
          if (section.data_size != ext.opts.signature_size) {
            LIBLOG_ERROR("Invalid section size; need "
                << ext.opts.signature_size << " Bytes of data.");
            return false;
          }

          // The resource keeps authors in its authors map from creating
          // extents. We have to find the author that corresponds to the
          // author id of this extent.
          auto author_iter = res.m_authors.find(ext.header.author_identifier);
          if (author_iter == res.m_authors.end()) {
            LIBLOG_ERROR("No author found for author ID of the extent.");
            return false;
          }
          auto author = author_iter->second;

          // Check that the author can create (a signature)
          if (!author.can_verify()) {
            LIBLOG_ERROR("We have an author, but this author cannot verify.");
            return false;
          }

          // Verify the signature
          auto err = author.verify(
              payload.buffer + section.section_size,
              payload.buffer_size - section.section_size,
              section.data, section.data_size);
          if (s3kr1t::ERR_SUCCESS != err) {
            LIBLOG_ERROR("Error verifying signature.");
            return false;
          }

          return true;
        },
      },
    },


  };
  // LIBLOG_DEBUG("=============== CTOR ==============");
}


extern "C" {

VESSEL_API vessel_error_t
vessel_section_context_create(
    struct vessel_section_context ** context,
    struct vessel_algorithm_context const * algorithm_context)
{
  if (!context || !algorithm_context) {
    return VESSEL_ERR_INVALID_VALUE;
  }

  if (*context) {
    auto err = vessel_section_context_free(context);
    if (VESSEL_ERR_SUCCESS != err) {
      return err;
    }
  }

  auto ctx = new vessel_section_context{};
  ctx->algos = algorithm_context;

  *context = ctx;
  return VESSEL_ERR_SUCCESS;
}



VESSEL_API vessel_error_t
vessel_section_context_free(struct vessel_section_context ** context)
{
  if (!context) {
    return VESSEL_ERR_INVALID_VALUE;
  }

  if (*context) {
    delete *context;
    *context = nullptr;
  }

  return VESSEL_ERR_SUCCESS;
}



VESSEL_API size_t
vessel_section_size(struct vessel_section_context const * context,
    struct vessel_algorithm_choices const * choices,
    vessel_section_t section, size_t data_size)
{
  if (!context || !choices) {
    LIBLOG_ERROR("Cannot proceed without context/choices.");
    return -2;
  }

  auto opts = vessel::options_from_choices(*choices);
  auto [sec_size, fixed] = vessel::section_size(context->section_sizes, opts, section, data_size);
  return sec_size;
}



VESSEL_API vessel_error_t
vessel_set_section(struct vessel_section_context * context,
    vessel_section_t section, int section_size)
{
  if (!context) {
    return VESSEL_ERR_INVALID_VALUE;
  }

  if (section < VESSEL_SECTION_CUSTOM_START) {
    return VESSEL_ERR_BAD_SECTION;
  }

  if (section_size < 0) {
    return VESSEL_ERR_INVALID_VALUE;
  }

  context->section_sizes[section] = section_size;

  return VESSEL_ERR_SUCCESS;
}



struct vessel_content_type
{
  std::shared_ptr<vessel::content_type> type = std::make_shared<vessel::content_type>();
};



VESSEL_API vessel_error_t
vessel_content_type_create(struct vessel_content_type ** ct)
{
  if (!ct) {
    return VESSEL_ERR_INVALID_VALUE;
  }

  if (!*ct) {
    delete *ct;
  }

  *ct = new vessel_content_type{};

  return VESSEL_ERR_SUCCESS;
}



VESSEL_API vessel_error_t
vessel_content_type_free(struct vessel_content_type ** ct)
{
  if (!ct) {
    return VESSEL_ERR_INVALID_VALUE;
  }

  if (*ct) {
    delete *ct;
    *ct = nullptr;
  }

  return VESSEL_ERR_SUCCESS;
}


VESSEL_API vessel_error_t
vessel_content_type_from_utf8(struct vessel_content_type ** ct,
    char const * input, size_t input_size)
{
  if (!input) {
    return VESSEL_ERR_INVALID_VALUE;
  }

  auto tmp = std::make_shared<vessel::content_type>();
  auto err = vessel::parse_content_type(*tmp, input,
      input_size ? input_size : -1);
  if (VESSEL_ERR_SUCCESS != err) {
    return err;
  }

  err = vessel_content_type_create(ct);
  if (VESSEL_ERR_SUCCESS != err) {
    return err;
  }

  (*ct)->type = tmp;

  return VESSEL_ERR_SUCCESS;
}



VESSEL_API vessel_error_t
vessel_content_type_set(struct vessel_content_type * ct,
    char const * key, size_t key_size,
    char const * value, size_t value_size)
{
  if (!ct || !key || !value) {
    return VESSEL_ERR_INVALID_VALUE;
  }

  icu::UnicodeString u_key;
  auto err = vessel::util::from_utf8(u_key, key, key_size ? key_size : -1);
  if (VESSEL_ERR_SUCCESS != err) {
    LIBLOG_ERROR("Could not validate key as UTF-8");
    return err;
  }

  // FIXME nope, key *cannot* contain separator characters.
  err = vessel::unescape_value(u_key);
  if (VESSEL_ERR_SUCCESS != err) {
    LIBLOG_ERROR("Bad escape sequences in key");
    return err;
  }

  icu::UnicodeString u_value;
  err = vessel::util::from_utf8(u_value, value, value_size ? value_size : -1);
  if (VESSEL_ERR_SUCCESS != err) {
    LIBLOG_ERROR("Could not validate value as UTF-8");
    return err;
  }

  // FIXME perhaps we need to take this verbatim, and escape it later?
  err = vessel::unescape_value(u_value);
  if (VESSEL_ERR_SUCCESS != err) {
    LIBLOG_ERROR("Bad escape sequences in value");
    return err;
  }

  ct->type->values[u_key] = u_value;

  return VESSEL_ERR_SUCCESS;
}



VESSEL_API vessel_error_t
vessel_content_type_get(char * value, size_t * value_size,
    struct vessel_content_type const * ct,
    char const * key, size_t key_size)
{
  if (!ct || !value || !value_size || !*value_size || !key || !key_size) {
    return VESSEL_ERR_INVALID_VALUE;
  }

  icu::UnicodeString u_key;
  auto err = vessel::util::from_utf8(u_key, key, key_size ? key_size : -1);
  if (VESSEL_ERR_SUCCESS != err) {
    LIBLOG_ERROR("Could not validate key as UTF-8");
    return err;
  }

  // FIXME nope, key *cannot* contain separator characters.
  err = vessel::unescape_value(u_key);
  if (VESSEL_ERR_SUCCESS != err) {
    LIBLOG_ERROR("Bad escape sequences in key");
    return err;
  }

  auto iter = ct->type->values.find(u_key);
  if (iter == ct->type->values.end()) {
    return VESSEL_ERR_KEY_NOT_FOUND;
  }

  auto & u_value = iter->second;
  auto val = vessel::util::to_utf8(u_value);

  if (val.size() > *value_size) {
    *value_size = val.size();
    return VESSEL_ERR_OUT_OF_MEMORY;
  }

  std::memcpy(value, val.c_str(), val.size());
  *value_size = val.size();

  return VESSEL_ERR_SUCCESS;
}





VESSEL_API vessel_error_t
vessel_content_type_remove(struct vessel_content_type * ct,
    char const * key, size_t key_size)
{
  if (!ct || !key) {
    return VESSEL_ERR_INVALID_VALUE;
  }

  icu::UnicodeString u_key;
  auto err = vessel::util::from_utf8(u_key, key, key_size ? key_size : -1);
  if (VESSEL_ERR_SUCCESS != err) {
    LIBLOG_ERROR("Could not validate key as UTF-8");
    return err;
  }

  // FIXME nope, key *cannot* contain separator characters.
  err = vessel::unescape_value(u_key);
  if (VESSEL_ERR_SUCCESS != err) {
    LIBLOG_ERROR("Bad escape sequences in key");
    return err;
  }

  // Success or not is irrelevant; the key is removed
  ct->type->values.erase(u_key);

  auto end = ct->type->positions.end();
  for (auto iter = ct->type->positions.begin() ; iter != end ; ++iter) {
    if (iter->second == u_key) {
      ct->type->positions.erase(iter);
      break;
    }
  }

  return VESSEL_ERR_SUCCESS;
}



VESSEL_API vessel_error_t
vessel_content_type_to_utf8(char * output, size_t * output_size,
    struct vessel_content_type const * ct)
{
  if (!output || !output_size || !*output_size || !ct) {
    return VESSEL_ERR_INVALID_VALUE;
  }

  icu::UnicodeString tmp;
  auto err = vessel::serialize_content_type(tmp, *(ct->type));
  if (VESSEL_ERR_SUCCESS != err) {
    return err;
  }

  auto serialized = vessel::util::to_utf8(tmp);
  if (serialized.size() > *output_size) {
    *output_size = serialized.size();
    return VESSEL_ERR_OUT_OF_MEMORY;
  }

  std::memcpy(output, serialized.c_str(), serialized.size());
  *output_size = serialized.size();
  return VESSEL_ERR_SUCCESS;
}



VESSEL_API vessel_error_t
vessel_section_write_header(
    void ** data, size_t * data_size,
    struct vessel_context const * context,
    uint32_t version_tag,
    void * buffer, size_t buffer_size,
    vessel_section_t section, vessel_topic_t topic,
    size_t requested_size)
{
  if (!data || !data_size || !context || !buffer || !buffer_size) {
    return VESSEL_ERR_INVALID_VALUE;
  }

  auto algo_iter = context->algos->algorithms.find(version_tag);
  if (algo_iter == context->algos->algorithms.end()) {
    LIBLOG_ERROR("Unknown version tag!");
    return VESSEL_ERR_INVALID_VALUE;
  }

  // Serialize section
  vessel::section sec{};
  sec.type = section;
  sec.topic = topic;
  sec.data_size = requested_size;

  auto used = vessel::serialization::serialize_section(
      context->sections->section_sizes, algo_iter->second.options,
      buffer, buffer_size,
      sec, true);
  if (!used) {
    LIBLOG_ERROR("Serialization failed.");
    return VESSEL_ERR_IO;
  }

  // Deserialize again to get the data offset.
  // XXX This could be improved by passing a mutable section and updating it.
  auto used2 = vessel::serialization::deserialize_section(
      context->sections->section_sizes, algo_iter->second.options,
      sec,
      buffer, used);
  if (used2 != used) {
    LIBLOG_ERROR("Verification failed.");
    return VESSEL_ERR_IO;
  }

  *data = sec.data;
  *data_size = sec.data_size;
  return VESSEL_ERR_SUCCESS;
}


} // extern "C"
