/**
 * This file is part of vessel.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt).
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#ifndef VESSEL_LIB_RESOURCE_H
#define VESSEL_LIB_RESOURCE_H

#include <build-config.h>

#include <memory>
#include <functional>
#include <atomic>
#include <condition_variable>

#include <vessel/context.h>

#include "algorithms.h"
#include "extent.h"
#include "author.h"
#include "content_type.h"
#include "context.h"
#include "payload.h"

namespace vessel {

// Forward declarations
struct resource;

inline vessel_error_t
section_request_trampoline(struct vessel_backing_store const * store,
    vessel_operation_id opid, void * baton);

template <typename funcT>
inline vessel_error_t
store_trampoline(struct vessel_backing_store const * store,
    vessel_operation_id opid, void * baton);


// Resource
struct resource
{
  vessel_context const *          m_ctx = nullptr;
  vessel_backing_store const *    m_store = nullptr;

  vessel::extent_id_dag           m_dag = {};
  byte_sequence                   m_origin_extent = {};

  using ct_map = std::map<vessel_topic_t, content_type>;
  ct_map                          m_content_types = {};

  using authors_map = std::map<byte_sequence, default_author>;
  authors_map                     m_authors = {};

  // FIXME why is this not m_prefixed? should it be? be consistent, please
  vessel_section_t                integrity_section = 0;

  // A symmetric key for MACs and encryption.
  std::vector<liberate::types::byte>  m_symmetric_key;

  struct section_result
  {
    struct vessel_extent *    ext = nullptr;
    std::unique_ptr<payload>  extent_payload = {};
    size_t                    section_data_size = 0;
  };


  using section_result_callback = std::function<void (vessel_error_t, section_result & result)>;


  struct section_request
  {
    uint32_t            version_tag;        // The version tag of extents the
                                            // section may live in.
    vessel_section_t    section;            // Section type
    vessel_topic_t      topic;              // Section topic
    size_t              min_data_size;      // Minimum data size for the section;
                                            // unused for fixed sized sections.
    size_t              desired_data_size;  // The desired data size, if
                                            // different from the minimum.
    size_t              max_extent_size;    // When creating an extent, the
                                            // maximum extent size.
    vessel_author_id *  author_id;          // Author ID
    vessel_author *     author;             // Author
    uint32_t            counter;            // Authoring counter
    section_result_callback   callback;     // Callback to invoke with results.
    vessel_section_t    integrity_section = 0;  // If given, the type of
                                                // integrity section to
                                                // add/require in an extent.
  };

  enum section_request_phase
  {
    SRP_NONE            = 0,
    SRP_CHECK_DAG       = 1,
    SRP_CREATE_EXTENT   = 2,
    SRP_ENSURE_SECTIONS = 3,
    SRP_FINALIZE        = 100,
  };

  struct section_request_context
  {
    resource *        res;

    section_request   request;
    algorithm_options algo_opts{};

    size_t            max_num_blocks = 0;

    size_t            checksum_size = 0;
    size_t            ct_size = 0;
    size_t            required_size_section_min = 0;
    size_t            required_size_section_ideal = 0;
    size_t            required_size_total_min = 0;
    size_t            required_size_total_ideal = 0;

    section_request_phase         phase = SRP_NONE;
    extent_id_dag::const_iterator dag_iter = {};
    struct vessel_extent *        ext = nullptr;

    vessel_error_t    return_value  = VESSEL_ERR_SUCCESS;

    bool              author_id_owned = false;

    // Reference counter, used in the store trampoline and SRP_FINALIZE.
    // This is an ugly artifact, because we're not handling the boundary
    // between the asynchronous store API and synchronous resource APIs
    // well; technically, the resource should sleep in synchronous calls
    // until woken up by the trampoline. Instead, the trampoline goes
    // straight into the progress() function, then loops back through
    // various phases, and may so end up in SRP_FINALIZE multiple times -
    // once for each store call that yielded a result, but not the
    // result where e.g. a section can fit or some such.
    // This needs a revamp, and the reference counter needs to go.
    // See: https://codeberg.org/interpeer/vessel/issues/12
    size_t refcount = 0;

    inline section_request_context(
        resource * _res,
        section_request const & _req,
        algorithm_options const & _algo_opts)
      : res{_res}
      , request{_req}
      , algo_opts{_algo_opts}
    {
      if (request.author) {
        auto id = request.author->author.author_id();
        res->m_authors.insert({id, request.author->author});

        if (request.author_id) {
          // Compare
          throw std::runtime_error{"implement me!"};
        }
        else {
          // Set; ownership goes to request object
          request.author_id = new vessel_author_id{algo_opts};
          request.author_id->author_id = id;
          author_id_owned = true;
        }
      }
    }

    inline ~section_request_context()
    {
      res->release(&ext);
      if (author_id_owned) {
        delete request.author_id;
        request.author_id = nullptr;
      }
    }
  };


  inline resource(vessel_context const * ctx,
      vessel_backing_store const * store)
    : m_ctx{ctx}
    , m_store{store}
  {
    if (!m_ctx || !m_ctx->sections || !m_ctx->algos) {
      throw vessel::exception{VESSEL_ERR_INVALID_VALUE, "Bad context!"};
    }

    if (!m_store) {
      throw vessel::exception{VESSEL_ERR_INVALID_VALUE, "Bad store!"};
    }

    // Try to build the index without a callback. This isn't really how
    // this should be done; it relies on this to be relatively fast. But
    // On the other hand, that just means stores should either do this
    // fast, or return something that says it can't be done quickly...
    // XXX this may need improvement.
    auto opid = m_store->try_build_index(m_store, nullptr, nullptr);
    if (opid) {
      struct vessel_store_index_iterator * iter = nullptr;
      auto err = m_store->index_result(store, &iter, opid);
      if (VESSEL_ERR_SUCCESS == err) {
        struct vessel_extent_id * curid = nullptr;
        struct vessel_extent_id * previd = nullptr;
        struct vessel_author_id * authorid = nullptr;
        while (VESSEL_ERR_SUCCESS == vessel_store_index_iterator_forward(m_store,
              iter, &curid, &authorid, &previd))
        {
          if (!m_dag.add(curid->extent_id,
                authorid->author_id,
                previd->extent_id)) {
            LIBLOG_ERROR("TODO: got to handle inability to add DAG node");
          }
        }

        m_origin_extent = m_dag.root();

        vessel_extent_id_free(&curid);
        vessel_author_id_free(&authorid);
        vessel_extent_id_free(&previd);
        err = store->index_iterator_free(&iter);
      }
    }
  }


  inline void set_content_type(vessel_topic_t const & topic,
      content_type const & ct)
  {
    m_content_types[topic] = ct;
  }


  inline void clear_content_type(vessel_topic_t const & topic)
  {
    m_content_types.erase(topic);
  }


  inline void set_symmetric_key(std::string const & key)
  {
    m_symmetric_key.resize(key.size());
    std::memcpy(&m_symmetric_key[0], key.c_str(), key.size());
  }

  inline void clear_symmetric_key()
  {
    m_symmetric_key.clear();
  }


  inline bool set_validation(vessel_section_t const & section)
  {
    switch (section) {
      case 0:
        LIBLOG_DEBUG("Clearing need for validation section.");
        integrity_section = section;
        return true;

      case VESSEL_SECTION_CRC32:
      case VESSEL_SECTION_MAC:
      case VESSEL_SECTION_SIGNATURE:
        LIBLOG_DEBUG("Setting validation section type: "
            << static_cast<int>(section));
        integrity_section = section;
        return true;

      default:
        break;
    }

    LIBLOG_DEBUG("Invalid valdiation section: "
        << static_cast<int>(section));
    return false;
  }



  inline vessel_error_t
  make_space_for_section(section_request const & request)
  {
    auto algo_iter = m_ctx->algos->algorithms.find(request.version_tag);
    if (algo_iter == m_ctx->algos->algorithms.end()) {
      LIBLOG_ERROR("Unknown version tag!");
      return VESSEL_ERR_INVALID_VALUE;
    }

    auto req = new section_request_context{this, request,
        algo_iter->second.options};

    // Check max size is feasible
    req->max_num_blocks = (request.max_extent_size / VESSEL_NOMINAL_BLOCK_SIZE);
    if (!req->max_num_blocks) {
      LIBLOG_ERROR("Maximum extent size is too small; need at least "
          << VESSEL_NOMINAL_BLOCK_SIZE);
      return finalize_result(VESSEL_ERR_INVALID_VALUE, req);
    }

    // Calculate needed checksum size.
    if (request.integrity_section) {
      auto [size, fixed] = section_size(m_ctx->sections->section_sizes,
          req->algo_opts, request.integrity_section, 0);
      req->checksum_size = size;
    }
    LIBLOG_DEBUG("Integrity section has size: " << req->checksum_size);

    // Similarly, if the topic requires a content type section, we
    // need to consider it here.
    auto ct_iter = m_content_types.find(request.topic);
    if (ct_iter != m_content_types.end()) {
      // TODO calculcate ct section size and add it here.
      // FIXME this needs to be added to the resource from an API call
      LIBLOG_ERROR("FIXME: " << __FILE__ << ":" << __LINE__);
    }
    LIBLOG_DEBUG("Content type section has size: " << req->ct_size);

    // Calculate the required section size based on the minimum data size.
    auto [min, min_fixed] = section_size(m_ctx->sections->section_sizes,
          req->algo_opts, request.section, request.min_data_size);
    req->required_size_section_min = min;
    auto [ideal, ideal_fixed] = section_size(m_ctx->sections->section_sizes,
          req->algo_opts, request.section, request.desired_data_size);
    req->required_size_section_ideal = ideal;

    LIBLOG_DEBUG("Section to insert has size: " << req->required_size_section_min
        << " to " << req->required_size_section_ideal);

    // Total required payload size
    req->required_size_total_min = req->required_size_section_min + req->checksum_size +
      req->ct_size;
    req->required_size_total_ideal = req->required_size_section_ideal + req->checksum_size +
      req->ct_size;

    size_t paged_size = req->max_num_blocks * VESSEL_NOMINAL_BLOCK_SIZE;
    auto payload_size = extent::payload_size(paged_size,
        req->algo_opts);

    if (req->required_size_total_min > payload_size) {
      LIBLOG_ERROR("Required payload size of " << req->required_size_total_min
          << " exceeds the payload size of " << payload_size
          << " that the given maximum extent size allows.");
      return finalize_result(VESSEL_ERR_INVALID_VALUE, req);
    }
    LIBLOG_DEBUG("Require payload of at least: " << req->required_size_total_min);

    // Check if there is space in any of the known extents.
    req->dag_iter = m_dag.begin();
    if (req->dag_iter != m_dag.end()) {
      req->phase = SRP_CHECK_DAG;

      vessel_extent_id ext_id{req->algo_opts, req->dag_iter->extent_id};
      auto opid = m_store->fetch_extent_latest(m_store, &ext_id, {},
          section_request_trampoline, req);
      if (!opid) {
        LIBLOG_ERROR("Internal error; can't fetch extent.");
        return finalize_result(VESSEL_ERR_IO, req);
      }
    }
    else {
      // If we reached here, there was no space to be found that fits the
      // given criteria. It's best we create a new extent. At this point,
      // we can be creative about the size - it's perfectly reasonable to
      // create a single extent that fits <= max_size.
      req->phase = SRP_CREATE_EXTENT;
    }
    return progress(m_store, 0, req);
  }



  inline vessel_error_t
  fetch_latest_blocking(vessel_extent_id const & ext_id, vessel_extent ** extent)
  {
    std::condition_variable cv{};
    std::atomic<bool> io_completed{false};
    auto func = [&io_completed, &cv](vessel_backing_store const *, vessel_operation_id opid, void *) -> vessel_error_t
    {
      io_completed = true;
      cv.notify_all();
      return VESSEL_ERR_SUCCESS;
    };

    auto opid = m_store->fetch_extent_latest(m_store, &ext_id, {},
        store_trampoline<decltype(func)>,
        &func
    );
    if (!opid) {
      LIBLOG_ERROR("Internal error; can't fetch extent.");
      return VESSEL_ERR_IO;
    }

    // Since we're *blocking*, we wait for the condition variable now.
    std::mutex m;
    std::unique_lock l{m};
    while (!io_completed) {
      cv.wait(l);
    }

    // Grab extent result
    auto err = m_store->extent_result(m_store, extent, opid);
    return err;
  }


  inline vessel_error_t
  release(vessel_extent ** extent)
  {
    // Ignore opid, just go with it.
    // XXX this produces a small memory leak, we may want to fix this
    // eventually.
    m_store->release_extent(m_store, extent, nullptr, nullptr /* FIXME */);
    return VESSEL_ERR_SUCCESS;
  }



  inline vessel_error_t
  commit(vessel_extent * extent)
  {
    // Update validity section, if we have any.
    auto pl = std::make_unique<payload>(
        m_ctx->sections->section_sizes, extent->ext->opts,
        static_cast<char *>(extent->ext->payload()),
        extent->ext->payload_size()
    );

    integrity_creation_callback iccb;

    auto section_iter = pl->begin();
    if (section_iter != pl->end()) {
      auto iter = m_ctx->sections->integrity_callbacks.find(section_iter->type);
      if (iter != m_ctx->sections->integrity_callbacks.end()) {
        // Found an integrity section
        iccb = iter->second.create;
      }
    }

    if (!pl->finalize(*this, *(extent->ext), iccb)) {
      LIBLOG_DEBUG("Could not finalize payload before committing extent.");
      return VESSEL_ERR_IO;
    }

    // Payload is finalized, so we can commit.
    m_store->commit_extent(m_store, extent, nullptr, nullptr /* FIXME */);
    return VESSEL_ERR_SUCCESS;
  }


  inline vessel_operation_id
  add_extent(uint32_t version_tag, size_t size_in_blocks, struct vessel_author & author)
  {
    // TODO fix this here.
//    std::condition_variable cv{};
//    std::atomic<bool> io_completed{false};
//    auto func = [&io_completed, &cv](vessel_backing_store const *, vessel_operation_id opid, void *) -> vessel_error_t
//    {
//      io_completed = true;
//      cv.notify_all();
//      return VESSEL_ERR_SUCCESS;
//    };
//
//    auto opid = m_store->fetch_extent_latest(m_store, &ext_id, {},
//        store_trampoline<decltype(func)>,
//        &func
//    );
//    if (!opid) {
//      LIBLOG_ERROR("Internal error; can't fetch extent.");
//      return VESSEL_ERR_IO;
//    }
  }



private:

  inline vessel_error_t
  extent_result(vessel_operation_id opid, vessel_extent ** extent)
  {
    auto err = m_store->extent_result(m_store, extent, opid);

    // Don't bail out immediately on error - we *could* have an extent
    // nonetheless.
    if ((*extent) && (*extent)->ext) {
      // For every extent we retrieved, we add it to the DAG. This way, we
      // keep the resource ordered.
      if (empty_extent_id((*extent)->ext->header.previous_extent_identifier)) {
        // This is a root, let's try to add it to the dag.
        if (!m_dag.add((*extent)->ext->header.current_extent_identifier,
            (*extent)->ext->header.author_identifier))
        {
          LIBLOG_ERROR("Secondary root found, returning error.");
          err = VESSEL_ERR_EXTENT_NOT_FOUND;
        }
        else {
          // We successfully added a root; this means we can set our
          // m_origin_extent.
          m_origin_extent = m_dag.root();
        }
      }
      else {
        // This is not a root, add this to the dag
        if (!m_dag.add((*extent)->ext->header.current_extent_identifier,
            (*extent)->ext->header.author_identifier,
            (*extent)->ext->header.previous_extent_identifier))
        {
          LIBLOG_ERROR("Conflicting extent encountered, retunring error.");
          err = VESSEL_ERR_EXTENT_NOT_FOUND;
        }
      }
    }

    // Return error code from m_store, though.
    return err;
  }



  inline vessel_error_t
  progress(struct vessel_backing_store const * store,
      vessel_operation_id opid, section_request_context * req)
  {
    switch (req->phase) {
      case SRP_CHECK_DAG:
        LIBLOG_DEBUG("Phase: checking DAG...");
        return phase_check_dag(store, opid, req);

      case SRP_CREATE_EXTENT:
        LIBLOG_DEBUG("Phase: creating extent...");
        return phase_create_extent(store, opid, req);

      case SRP_ENSURE_SECTIONS:
        LIBLOG_DEBUG("Phase: ensuring required sections exist...");
        return phase_ensure_sections(store, opid, req);

      case SRP_FINALIZE:
        LIBLOG_DEBUG("Phase: done.");
        {
          // FIXME do all the finalization in a function here, then
          //       do less in finalize().
          auto ret = req->return_value;
          if (req->refcount > 1) {
            (req->refcount)--;
          }
          else {
            delete req;
          }
          return ret;
        }
        break;

      case SRP_NONE:
      default:
        break;
    }

    LIBLOG_ERROR("Unknown or unsupported phase, aborting.");
    return finalize_result(VESSEL_ERR_UNEXPECTED, req);
  }


  inline vessel_error_t
  next_dag_entry(struct vessel_backing_store const * store,
      section_request_context * req)
  {
    // Release any extent that may be fetched
    if (req->ext) {
      release(&(req->ext));
    }

    ++(req->dag_iter);
    if (req->dag_iter == m_dag.end()) {
      req->phase = SRP_CREATE_EXTENT;
      return progress(store, 0, req);
    }

    // Fetch next entry
    vessel_extent_id ext_id{req->algo_opts, req->dag_iter->extent_id};
    auto opid = store->fetch_extent_latest(store, &ext_id, {},
        section_request_trampoline, req);
    if (!opid) {
      LIBLOG_ERROR("Internal error; can't fetch extent.");
      return finalize_result(VESSEL_ERR_IO, req);
    }

    return progress(store, 0, req);
  }


  inline vessel_error_t
  phase_check_dag(struct vessel_backing_store const * store,
      vessel_operation_id opid, section_request_context * req)
  {
    // Release any extent that may be fetched
    if (req->ext) {
      release(&(req->ext));
    }

    // If there is no extent fetched yet, do so now - this *should* be the case
    // if the store triggered this callback.
    if (!req->ext) {
      auto err = extent_result(opid, &(req->ext));
      if (VESSEL_ERR_SUCCESS != err) {
        LIBLOG_DEBUG("Could not get extent result from store.");
        return finalize_result(err, req);
      }
    }

    // Check the version tag; if it doesn't fit, move on to the next.
    if (req->ext->ext->envelope.version_tag != req->request.version_tag) {
      LIBLOG_DEBUG("Skipping extent because of mismatching version tag.");
      return next_dag_entry(store, req);
    }

    // Similar, check the author id.
    if (req->ext->ext->header.author_identifier != req->request.author_id->author_id) {
      LIBLOG_DEBUG("Skipping extent because of mismatching version author.");
      return next_dag_entry(store, req);
    }

    // TODO
//    // If the extent has a reliability section, and that section matches the
//    // above, we're halfway there.
//    payload pl{m_ctx->sections->section_sizes, algo_iter->second.options,
//      static_cast<char *>(ext->ext->payload()), ext->ext->payload_size()
//    };
//
//    if (integrity_section) {
//      auto section_iter = pl.begin();
//      if (section_iter != pl.end()) {
//        if (section_iter->type != integrity_section) {
//          LIBLOG_DEBUG("Payload has a mismatching integrity section");
//          release(&ext);
//          continue;
//        }
//      }
//    }
//
//  XXX The content type does *not* have to be checked as such.

    // We need to ensure the payload actually has enough space for the requested
    // section.
    payload pl{m_ctx->sections->section_sizes, req->algo_opts,
      static_cast<char *>(req->ext->ext->payload()), req->ext->ext->payload_size()
    };
    auto [has_space, found_size] = pl.can_add_section(
        req->request.section,
        req->request.min_data_size, req->request.desired_data_size);
    if (!has_space) {
      LIBLOG_DEBUG("Extent does not have space for the section, trying next.");
      return next_dag_entry(store, req);
    }

    // We've found a suitable extent, let's add sections!
    req->phase = SRP_ENSURE_SECTIONS;
    return progress(store, opid, req);
  }



  inline vessel_error_t
  phase_create_extent(struct vessel_backing_store const * store,
      vessel_operation_id opid, section_request_context * req)
  {
    // We enter this phase with a zero opid, which means we have to create an
    // extent. Which means if the opid is non-zero, we need to progress to a
    // later phase.
    if (opid) {
      req->phase = SRP_ENSURE_SECTIONS;
      return progress(store, opid, req);
    }

    // A this point, we need to create an extent.
    // We have no origin extent? Great, then create it.
    if (m_origin_extent.empty()) {
      auto next_opid = store->create_extent_origin(store,
          req->request.version_tag, req->max_num_blocks,
          req->request.author_id, req->request.counter,
          section_request_trampoline, req);
      if (!next_opid) {
        LIBLOG_ERROR("Could not create origin extent!");
        return finalize_result(VESSEL_ERR_IO, req);
      }

      return progress(store, next_opid, req);
    }

    // Query the dag for the best parent.
    auto parent = m_dag.best_parent();
    if (empty_extent_id(parent)) {
      LIBLOG_ERROR("No best parent to determine, aborting.");
      return finalize_result(VESSEL_ERR_INTERNAL_LOGIC, req);
    }

    // Because we're calling a public API, we have to create an extent
    // identifier. XXX we're faking the options here; that's ok for this
    // use.
    vessel_extent_id parent_id{{}};
    parent_id.extent_id = parent;

    auto next_opid = store->create_extent(store,
        req->request.version_tag, req->max_num_blocks,
        req->request.author_id, req->request.counter,
        &parent_id,
        section_request_trampoline, req);
    if (!next_opid) {
      LIBLOG_ERROR("Could not create extent!");
      return finalize_result(VESSEL_ERR_IO, req);
    }

    // Progress
    return progress(store, opid, req);
  }



  inline vessel_error_t
  phase_ensure_sections(struct vessel_backing_store const * store [[maybe_unused]],
      vessel_operation_id opid, section_request_context * req)
  {
    // If there is no extent fetched yet, do so now - this *should* be the case
    // if the store triggered this callback.
    if (!req->ext) {
      auto err = extent_result(opid, &(req->ext));
      if (VESSEL_ERR_SUCCESS != err) {
        return finalize_result(err, req);
      }
    }

    // At this point, the extent is loaded and checked. Now we *may* have to
    // create integrity and content type sections. For this, we need to create
    // a payload.
    auto pl = std::make_unique<payload>(
        m_ctx->sections->section_sizes, req->algo_opts,
        static_cast<char *>(req->ext->ext->payload()),
        req->ext->ext->payload_size()
    );
    bool do_commit = false;

    // See what to do about integrity.
    if (req->request.integrity_section) {
      auto section_iter = pl->begin();
      if (section_iter != pl->end()) {
        if (section_iter->type != req->request.integrity_section) {
          LIBLOG_DEBUG("Payload has a mismatching integrity section");
          return finalize_result(VESSEL_ERR_INTERNAL_LOGIC, req);
        }

        if (!validate_integrity(*section_iter, *(req->ext->ext), *pl)) {
          LIBLOG_DEBUG("Payload has the correct integrity section, but it is "
              "invalid.");
          return finalize_result(VESSEL_ERR_VERIFICATION, req);
        }

        // Pass through, all good
      }
      else {
        // Here, the payload is empty and we have to add an integrity
        // section.
        auto [res, section] = pl->add_section(
            req->request.integrity_section, VESSEL_TOPIC_METADATA,
            0);
        if (!res) {
          // FIXME maybe a different code?
          return finalize_result(VESSEL_ERR_IO, req);
        }

        do_commit = true;
      }
    }

    // See what to do about content type.
    auto ct_iter = m_content_types.find(req->request.topic);
    if (ct_iter != m_content_types.end()) {
      LIBLOG_ERROR("TODO: check if the last content type section found matches this ct");
      // TODO
      do_commit = true;
    }

    // Commit the extent before returning
    if (do_commit) {
      LIBLOG_ERROR("TODO: should commit extent - or should we?");
      // TODO
    }

    // At this point, we not only have an extent but also sections that were
    // required. This is success!
    return finalize_result(VESSEL_ERR_SUCCESS, req, std::move(pl));
  }


  static inline vessel_error_t
  finalize_result(vessel_error_t err, section_request_context * req,
      std::unique_ptr<payload> payload = {})
  {
    // Final check & determine the section size.
    size_t data_size = 0;
    if (payload) {
      auto [has_space, found_size] = payload->can_add_section(
          req->request.section,
          req->request.min_data_size, req->request.desired_data_size);
      if (has_space) {
        data_size = found_size;
      }
      else {
        LIBLOG_ERROR("After creating/fetching extent, there is not enough space after all: "
            << found_size << " for [" << req->request.min_data_size << ","
            << req->request.desired_data_size << "].");
        err = VESSEL_ERR_OUT_OF_MEMORY;
      }
    }

    if (req->request.callback) {
      section_result res{};
      res.ext = req->ext;
      req->ext = nullptr;
      res.extent_payload = std::move(payload);
      res.section_data_size = data_size;
      req->request.callback(err, res);
    }

    // FIXME this is an issue if there are more progress calls
    // delete req;
    req->phase = SRP_FINALIZE;
    req->return_value = err;
    return err;
  }


  inline bool
  validate_integrity(section const & _section, extent const & ext, payload const & _payload)
  {
    auto iter = m_ctx->sections->integrity_callbacks.find(_section.type);
    if (iter == m_ctx->sections->integrity_callbacks.end()) {
      LIBLOG_ERROR("Integrity section type unknown: " << _section.type);
      return false;
    }

    return _payload.validate(*this, ext, _section.type, iter->second.validate);
  }


  friend vessel_error_t
  section_request_trampoline(struct vessel_backing_store const * store,
      vessel_operation_id opid, void * baton);

  template <typename funcT>
  friend vessel_error_t
  store_trampoline(struct vessel_backing_store const * store,
      vessel_operation_id opid, void * baton);


};


inline vessel_error_t
section_request_trampoline(struct vessel_backing_store const * store,
    vessel_operation_id opid, void * baton)
{
  if (!store || !baton) {
    LIBLOG_ERROR("Internal error; store and/or baton are not set.");
    return VESSEL_ERR_INTERNAL_LOGIC;
  }
  auto req = static_cast<resource::section_request_context *>(baton);

  if (!req->res) {
    LIBLOG_ERROR("Internal error; resource in request not set.");
    return resource::finalize_result(VESSEL_ERR_INTERNAL_LOGIC, req);
  }
  LIBLOG_DEBUG("Progress from operation id: " << opid);
  (req->refcount)++;
  return req->res->progress(store, opid, req);
}


template <typename funcT>
inline vessel_error_t
store_trampoline(struct vessel_backing_store const * store,
    vessel_operation_id opid, void * baton)
{
  if (!store || !baton) {
    LIBLOG_ERROR("Internal error; store and/or baton are not set.");
    return VESSEL_ERR_INTERNAL_LOGIC;
  }
  auto func = *static_cast<funcT *>(baton);

  return func(store, opid, baton);
}



} // namespace vessel



extern "C" {

/**
 * Definition of the opaque struct
 */
struct vessel_resource
{
  std::shared_ptr<vessel::resource> resource;
};

} // extern "C"

#endif // guard
