/**
 * This file is part of vessel.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt).
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#ifndef VESSEL_LIB_PAGER_H
#define VESSEL_LIB_PAGER_H

#include <build-config.h>

#include <memory>

#include <vessel/error.h>

namespace vessel {

/**
 * Forward; see below
 */
struct pager;

/**
 * Paged memory. This is a multiple of the system-defined page size; the number
 * of pages is returned in the "count" member, while the number of Bytes is in
 * the "size" member.
 */
struct paged_memory
{
  void *  data;   // Data pointer.
  size_t  size;   // Size in Bytes.
  size_t  count;  // Number of pages.

  std::weak_ptr<pager>  responsible_pager;
};


/**
 * Types of pager
 */
enum pager_kind : uint8_t
{
  PK_MMAP = 0,
  // TODO
};


/**
 * The pager interface. All pagers carry a a kind, and an implementation
 * pointer.
 */
struct pager
{
public:
  inline pager(pager_kind kind)
    : m_kind{kind}
  {
  }

  virtual ~pager() = default;


  inline pager_kind kind() const
  {
    return m_kind;
  }

  /**
   * Returns the (static) page size for this pager.
   */
  virtual size_t page_size() const = 0;

  /**
   * Returns the number of pages currently available. This is equivalent to
   * the mapping size expressed as a number of pages. It also means that
   * valid page offsets are in the range of [0,pages_available()).
   */
  virtual size_t pages_available() const = 0;

  /**
   * Returns how many pages are currently in use (via fetch); note that
   * pages fetched multiple times also count multiple times here.
   */
  virtual size_t pages_in_use() const = 0;

  /**
   * Tries to fetch paged memory from the pager.
   *
   * The number of pages to fetch is given via the amount parameter. The offset
   * is a page index into the managed area, i.e. an offset of 1 means
   * page_size() bytes into the backing buffer.
   */
  virtual vessel_error_t fetch(paged_memory & out, size_t offset, size_t amount) = 0;

  /**
   * Synchronize either the memory region specified by the given page. If the
   * page is not given, synchronize the entire memory region of the pager.
   */
  virtual vessel_error_t sync(paged_memory const * mem) = 0;

  /**
   * Release an allocated page structure.
   */
  virtual vessel_error_t release(paged_memory & mem) = 0;

private:
  pager_kind  m_kind;
};

} // namespace vessel


#ifdef VESSEL_HAVE_SYS_MMAN_H
#include "pagers/mmap.h"
#endif

#endif // guard
