/**
 * This file is part of vessel.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt).
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#include <build-config.h>

#include <string>
#include <memory>
#include <limits>

#include <vessel/store.h>

#include "pager.h"
#include "extent.h"
#include "author.h"
#include "serialization.h"
#include "algorithms.h"

extern "C" {

struct vessel_store_index_iterator
{
  void * data = nullptr;
};



VESSEL_API vessel_error_t
vessel_store_index_iterator_forward(
    struct vessel_backing_store const * store,
    struct vessel_store_index_iterator * iter,
    struct vessel_extent_id ** current_id,
    struct vessel_author_id ** author_id,
    struct vessel_extent_id ** previous_id)
{
  if (!store || !store->data || !iter) {
    return VESSEL_ERR_INVALID_VALUE;
  }

  auto err = store->index_iterator_value(iter, current_id, author_id, previous_id);
  if (VESSEL_ERR_SUCCESS != err) {
    return err;
  }

  return store->index_iterator_next(iter);
}

} // extern "C"


namespace vessel {
namespace {

/**
 * The pager_backed_store uses a pager to match memory pages onto extents. It
 * does not really process extents, other than to determine their size, and
 * match that to multiple pages.
 *
 * Pagers work on page offsets; stores work on extent identifiers. The store
 * therefore needs to scan pages for containing extents.
 *
 * The store will *never* move extents. That is, once an extent has been
 * committed to the pageer, only its contents may change, not its identifier,
 * size or position.
 */
struct pager_backed_store
{
public:

  enum op_type
  {
    OT_EXTENT = 0,
    OT_INDEX  = 1,
  };

  struct op_context
  {
    vessel_store_operation_completed  callback = nullptr;
    void *                            baton = nullptr;
    op_type                           type = OT_EXTENT;

    inline op_context(vessel_store_operation_completed _callback = nullptr,
        void * _baton = nullptr,
        op_type _type = OT_EXTENT)
      : callback{_callback}
      , baton{_baton}
      , type{_type}
    {
    }

    inline op_context(op_context const &) = default;
    inline op_context(op_context &&) = default;

    inline op_context & operator=(op_context const &) = default;
    inline op_context & operator=(op_context &&) = default;
  };


private:
  inline pager_backed_store(
      struct vessel_backing_store * store,
      struct vessel_algorithm_context * algo_ctx,
      char const * filename)
    : m_store{store}
    , m_algo_ctx{algo_ctx}
  {
    if (filename) {
      m_filename = filename;
    }

#ifdef VESSEL_HAVE_SYS_MMAN_H
    m_pager = std::make_shared<pagers::mmap_pager>(m_filename);
#endif

    if (!m_pager) {
      throw std::runtime_error{"No available pager implementation!"};
    }

    LIBLOG_INFO("Pages available in store: " << m_pager->pages_available());

    // Ensure that the pager's page size divides cleanly by the extent block
    // size, otherwise the pager is no good.
    auto remainder = VESSEL_NOMINAL_BLOCK_SIZE % m_pager->page_size();
    if (remainder) {
      throw std::runtime_error{"Nominal vessel block size does not divide by pager's page size."};
    }
  }



  inline bool
  build_index_if_required()
  {
    // Already have an index? Nothing to do.
    if (!m_index.empty()) {
      return true;
    }

    if (!m_algo_ctx) {
      LIBLOG_ERROR("Cannot use this operation without an algorithm context.");
      return false;
    }

    index idx;
    size_t offset = 0;
    while (offset < m_pager->pages_available()) {
      // Fetch single page at the offset
      paged_memory tmp;
      auto err = m_pager->fetch(tmp, offset, 1);
      if (VESSEL_ERR_SUCCESS != err) {
        LIBLOG_ERROR("Error fetching page, aborting.");
        m_pager->release(tmp);
        break;
      }

      char const * current = static_cast<char const *>(tmp.data);
      size_t remaining = tmp.size;

      // Deserialize an envelope. See if the version tag is known.
      extent_envelope_00 env;
      auto used = serialization::deserialize_envelope_00(env, current, remaining);
      if (!used) {
        LIBLOG_ERROR("Unable to parse envelope, aborting.");
        m_pager->release(tmp);
        break;
      }
      current += used;
      remaining -= used;

      // Fetch options for the version tag here.
      auto algo_iter = m_algo_ctx->algorithms.find(env.version_tag);
      if (algo_iter == m_algo_ctx->algorithms.end()) {
        LIBLOG_WARN("Unsupported version tag found, aborting at "
            "offset " << offset);
        m_pager->release(tmp);
        break;
      }

      // If this is ok, get the extent header - from that we can deduce the
      // size.
      extent_header_00 head;
      used = serialization::deserialize_header_00(algo_iter->second.options, head,
          current, remaining);
      if (!used) {
        LIBLOG_ERROR("Unable to parse header, arborting.");
        m_pager->release(tmp);
        break;
      }

      // Add to index and advance
      m_pager->release(tmp);
      size_t num_pages = (head.extent_size_multiplier * VESSEL_NOMINAL_BLOCK_SIZE)
        / m_pager->page_size();

      idx[head.current_extent_identifier] = {
        {offset, num_pages},
        {head.author_identifier, head.previous_extent_identifier},
      };
      offset += num_pages;
    }

    m_index = idx;

    return true;
  }



  inline vessel_operation_id
  op_completed(vessel_operation_id opid, op_context & ctx)
  {
    auto iter = m_operations.find(opid);
    if (iter == m_operations.end()) {
      return 0;
    }

    // Invoke callback if it's set, otherwise just return the opid
    if (ctx.callback) {
      auto err = ctx.callback(m_store, opid, ctx.baton);
      if (VESSEL_ERR_SUCCESS != err) {
        iter->second.cb_status = err;
        return opid;
      }
    }
    return opid;
  }

public:

  static inline pager_backed_store *
  create(
      struct vessel_backing_store * store,
      struct vessel_algorithm_context * algo_ctx,
      char const * filename)
  {
    try {
      return new pager_backed_store{store, algo_ctx, filename};
    } catch (std::exception const & ex) {
      LIBLOG_ERROR("Could not create store: " << ex.what());
      return nullptr;
    }
  }


  inline vessel_operation_id
  try_build_index(op_context const & ctx)
  {
   // Create opid and register result
    auto opid = m_next_id++;
    auto [iter, success] = m_operations.insert({opid, {ctx}});
    if (!success) {
      LIBLOG_ERROR("Could not schedule operation, aborting.");
      return 0;
    }

    if (!build_index_if_required()) {
      return 0;
    }

    iter->second.status = VESSEL_ERR_SUCCESS;
    return opid;
  }



  inline vessel_operation_id
  fetch_latest(vessel_extent_id const & id, vessel_authoring_counter version,
      op_context const & ctx)
  {
    if (!build_index_if_required()) {
      return 0;
    }

    // Create opid and register result
    auto opid = m_next_id++;
    auto [iter, success] = m_operations.insert({opid, {ctx}});
    if (!success) {
      LIBLOG_ERROR("Could not schedule operation, aborting.");
      return 0;
    }

    // Fetch
    auto ext = fetch_internal(iter->second, id.extent_id);
    if (!ext) {
      return op_completed(opid, iter->second.ctx);
    }

    // We're supposed to find something later than version. We'll always return
    // what we found, but perhaps we don't set a status code.
    iter->second.ext = ext;
    if (ext->header.authoring_counter >= version) {
      iter->second.status = VESSEL_ERR_SUCCESS;
    }
    else {
      iter->second.status = VESSEL_ERR_EXTENT_NOT_FOUND;
    }

    return op_completed(opid, iter->second.ctx);
  }


  inline vessel_operation_id
  fetch_specific(vessel_extent_id const & id, vessel_authoring_counter version,
      op_context const & ctx)
  {
    if (!build_index_if_required()) {
      return 0;
    }

    // Create opid and register result
    auto opid = m_next_id++;
    auto [iter, success] = m_operations.insert({opid, {ctx}});
    if (!success) {
      LIBLOG_ERROR("Could not schedule operation, aborting.");
      return 0;
    }

    // Fetch
    auto ext = fetch_internal(iter->second, id.extent_id);
    if (!ext) {
      return op_completed(opid, iter->second.ctx);
    }

    // We're supposed to find a precise match. We're still delivering the
    // results, though, just so callers can decide a bit more.
    iter->second.ext = ext;
    if (ext->header.authoring_counter == version) {
      iter->second.status = VESSEL_ERR_SUCCESS;
    }
    else {
      iter->second.status = VESSEL_ERR_EXTENT_NOT_FOUND;
    }

    return op_completed(opid, iter->second.ctx);
  }


  inline vessel_operation_id
  create(uint32_t version_tag, size_t size_in_blocks,
    struct vessel_author_id const & author,
    vessel_authoring_counter counter,
    struct vessel_extent_id const & previous_extent,
    op_context const & ctx)
  {
    if (!build_index_if_required()) {
      return 0;
    }

    // Create opid and register result
    auto opid = m_next_id++;
    auto [iter, success] = m_operations.insert({opid, {ctx}});
    if (!success) {
      LIBLOG_ERROR("Could not schedule operation, aborting.");
      return 0;
    }

    auto ext = create_internal(iter->second,
        version_tag, size_in_blocks, author, counter,
        &previous_extent);
    if (!ext) {
      return op_completed(opid, iter->second.ctx);
    }

    // Nope, all good
    iter->second.status = VESSEL_ERR_SUCCESS;
    iter->second.ext = ext;
    return op_completed(opid, iter->second.ctx);
  }


  inline vessel_operation_id
  create_origin(uint32_t version_tag, size_t size_in_blocks,
    struct vessel_author_id const & author,
    vessel_authoring_counter counter,
    op_context const & ctx)
  {
    if (!build_index_if_required()) {
      return 0;
    }

    // Create opid and register result
    auto opid = m_next_id++;
    auto [iter, success] = m_operations.insert({opid, {ctx}});
    if (!success) {
      LIBLOG_ERROR("Could not schedule operation, aborting.");
      return 0;
    }

    auto ext = create_internal(iter->second,
        version_tag, size_in_blocks, author, counter, nullptr);
    if (!ext) {
      return op_completed(opid, iter->second.ctx);
    }

    // Nope, all good
    iter->second.status = VESSEL_ERR_SUCCESS;
    iter->second.ext = ext;
    return op_completed(opid, iter->second.ctx);
  }


  inline vessel_operation_id
  release(struct vessel_extent ** extent,
    op_context const & ctx)
  {
    // Create opid and register result
    auto opid = m_next_id++;
    auto [iter, success] = m_operations.insert({opid, {ctx}});

    // Nothing to do? Treat it as success. We're collapsing a few cases here
    // into one.
    if (extent) {
      // Only delete struct if we find it.
      if (*extent) {
        // Release pages from pager
        if ((*extent)->ext) {
          m_pager->release((*extent)->ext->paged);
        }

        delete *extent;
        *extent = nullptr;
      }
    }

    // Always success
    iter->second.status = VESSEL_ERR_SUCCESS;
    return op_completed(opid, iter->second.ctx);
  }



  inline vessel_operation_id
  commit(struct vessel_extent * extent,
    op_context const & ctx)
  {
    // Create opid and register result
    auto opid = m_next_id++;
    auto [iter, success] = m_operations.insert({opid, {ctx}});

    if (!extent || !extent->ext) {
      iter->second.status = VESSEL_ERR_INVALID_VALUE;
      return op_completed(opid, iter->second.ctx);
    }

    // We have to serialize the header and footer if they're dirty. This should
    // only happen if the extent is newly created.
    if (extent->ext->dirty) {
      char * offset = static_cast<char *>(extent->ext->paged.data);
      auto remaining = extent->ext->paged.size;

      // Serialize envelope
      auto used = serialization::serialize_envelope_00(offset, remaining,
          extent->ext->envelope);
      if (!used) {
        iter->second.status = VESSEL_ERR_IO;
        return op_completed(opid, iter->second.ctx);
      }
      offset += used;
      remaining -= used;

      // Serialize header
      used = serialization::serialize_header_00(extent->ext->opts, offset,
          remaining, extent->ext->header);
      if (!used) {
        iter->second.status = VESSEL_ERR_IO;
        return op_completed(opid, iter->second.ctx);
      }
      offset += used;
      remaining -= used;

      // Done
      extent->ext->dirty = false;
    }

    // We don't do very much here at all - we just ask the pager to sync the
    // extent's paged memory.
    m_pager->sync(&(extent->ext->paged));

    iter->second.status = VESSEL_ERR_SUCCESS;
    return op_completed(opid, iter->second.ctx);
  }



  inline vessel_error_t
  extent_result(struct vessel_extent ** extent,
      vessel_operation_id opid)
  {
    auto iter = m_operations.find(opid);
    if (iter == m_operations.end()) {
      return VESSEL_ERR_OPERATION_NOT_FOUND;
    }

    if (OT_EXTENT != iter->second.ctx.type) {
      return VESSEL_ERR_INVALID_VALUE;
    }

    if (VESSEL_ERR_SUCCESS != iter->second.cb_status) {
      LIBLOG_ERROR("Callback returned an error, this is an application problem.");
      auto ret = iter->second.cb_status;
      m_operations.erase(iter);
      return ret;
    }

    auto ret = iter->second.status;

    // If there is no data, we can ignore extent. But if there is data,
    // it needs to be non-null, otherwise we can't return the data.
    if (iter->second.ext && extent) {
      // No need to free the extent if it already exists; we just overwrite
      // the data (if any).
      if (!*extent) {
        *extent = new vessel_extent{};
      }
      (*extent)->ext = iter->second.ext;
    }

    // Erase only if the data was passed.
    m_operations.erase(iter);
    return ret;
  }


  inline vessel_error_t
  index_result(struct vessel_store_index_iterator ** out_iter,
      vessel_operation_id opid)
  {
    auto iter = m_operations.find(opid);
    if (iter == m_operations.end()) {
      return VESSEL_ERR_OPERATION_NOT_FOUND;
    }

    if (OT_INDEX != iter->second.ctx.type) {
      return VESSEL_ERR_INVALID_VALUE;
    }

    if (VESSEL_ERR_SUCCESS != iter->second.cb_status) {
      LIBLOG_ERROR("Callback returned an error, this is an application problem.");
      auto ret = iter->second.cb_status;
      m_operations.erase(iter);
      return ret;
    }

    auto ret = iter->second.status;

    if (out_iter) {
      if (!*out_iter) {
        *out_iter = new vessel_store_index_iterator{};
      }

      if ((*out_iter)->data) {
        auto data = static_cast<iter_data *>((*out_iter)->data);
        delete data;
      }

      (*out_iter)->data = new iter_data{this, m_index.begin()};
    }

    m_operations.erase(iter);
    return ret;
  }



  inline vessel_error_t
  index_iterator_value(
      struct vessel_store_index_iterator const * iter,
      struct vessel_extent_id ** current_id,
      struct vessel_author_id ** author_id,
      struct vessel_extent_id ** previous_id)
  {
    auto data = static_cast<iter_data *>(iter->data);
    if (this != data->store) {
      return VESSEL_ERR_INVALID_VALUE;
    }

    if (data->iter == m_index.end()) {
      return VESSEL_ERR_END_ITERATION;
    }

    if (current_id) {
      if (!*current_id) {
        *current_id = new vessel_extent_id{};
      }
      (*current_id)->extent_id = data->iter->first;
    }

    if (author_id) {
      if (!*author_id) {
        *author_id = new vessel_author_id{};
      }
      (*author_id)->author_id = data->iter->second.meta.author_id;
    }

    if (previous_id) {
      if (!*previous_id) {
        *previous_id = new vessel_extent_id{};
      }
      if (vessel_extent_id::is_empty_id(data->iter->second.meta.previous_id)) {
        (*previous_id)->extent_id.clear();
      }
      else {
        (*previous_id)->extent_id = data->iter->second.meta.previous_id;
      }
    }

    return VESSEL_ERR_SUCCESS;
  }



  inline vessel_error_t
  index_iterator_next(
      struct vessel_store_index_iterator * iter)
  {
    auto data = static_cast<iter_data *>(iter->data);
    if (this != data->store) {
      return VESSEL_ERR_INVALID_VALUE;
    }

    ++(data->iter);
    return VESSEL_ERR_SUCCESS;
  }



  inline vessel_error_t
  index_iterator_free(
      struct vessel_store_index_iterator ** iter)
  {
    // The trampline guarantees that this is a fully initialized
    // iterator, so we're not going to check that again.
    auto data = static_cast<iter_data *>((*iter)->data);
    (*iter)->data = nullptr;
    delete data;

    delete *iter;
    *iter = nullptr;

    return VESSEL_ERR_SUCCESS;
  }



private:
  struct op_result
  {
    op_context                      ctx;
    vessel_error_t                  status = VESSEL_ERR_NOT_IMPLEMENTED;
    vessel_error_t                  cb_status = VESSEL_ERR_SUCCESS;
    std::shared_ptr<vessel::extent> ext = {};
  };

  using operation_map = std::map<vessel_operation_id, op_result>;

  // Paged range is page offset, number of pages
  using paged_range = std::tuple<size_t, size_t>;
  // Extent meta is some meta information, required to build a DAG. For anything
  // else, the extent needs to be retrieved.
  struct extent_meta
  {
    byte_sequence author_id;
    byte_sequence previous_id;
  };

  // The index value consists of the paged range and the extent meta.
  struct index_value
  {
    paged_range paged;
    extent_meta meta;
  };

  using index = std::map<byte_sequence, index_value>;

public:

  struct iter_data
  {
    pager_backed_store *  store;
    index::const_iterator iter;
  };

private:

  inline std::shared_ptr<extent>
  fetch_internal(op_result & result, byte_sequence const & extent_id)
  {
    // Look up extent in index.
    auto e_iter = m_index.find(extent_id);
    if (e_iter == m_index.end()) {
      result.status = VESSEL_ERR_EXTENT_NOT_FOUND;
      return {};
    }
    auto [offset, num_pages] = e_iter->second.paged;

    // If we found the extent, fetch the pages for it.
    paged_memory mem;
    auto err = m_pager->fetch(mem, offset, num_pages);
    if (VESSEL_ERR_SUCCESS != err) {
      result.status = err;
      return {};
    }

    char const * current = static_cast<char const *>(mem.data);
    size_t remaining = mem.size;

    // Envelope
    extent_envelope_00 env;
    auto used = serialization::deserialize_envelope_00(env, current, remaining);
    if (!used) {
      LIBLOG_ERROR("Unable to parse envelope, aborting.");
      m_pager->release(mem);
      result.status = VESSEL_ERR_IO;
      return {};
    }
    current += used;
    remaining -= used;

    // Fetch options for the version tag here.
    auto algo_iter = m_algo_ctx->algorithms.find(env.version_tag);
    if (algo_iter == m_algo_ctx->algorithms.end()) {
      LIBLOG_WARN("Unsupported version tag found, aborting at "
          "offset " << offset);
      m_pager->release(mem);
      result.status = VESSEL_ERR_BAD_ALGORITHMS;
      return {};
    }

    // Create extent; we're feeling confident.
    auto ext = std::make_shared<extent>(m_store, algo_iter->second.options);
    ext->paged = mem;
    ext->envelope = env;
    ext->dirty = false;

    // Read the header.
    used = serialization::deserialize_header_00(algo_iter->second.options,
        ext->header,
        current, remaining);
    if (!used) {
      LIBLOG_ERROR("Unable to parse header, arborting.");
      m_pager->release(mem);
      result.status = VESSEL_ERR_IO;
      return {};
    }

    return ext;
  }


  inline std::shared_ptr<extent>
  create_internal(op_result & result,
      uint32_t version_tag, size_t size_in_blocks,
      struct vessel_author_id const & author,
      vessel_authoring_counter counter,
      struct vessel_extent_id const * previous_extent)
  {
    // Check size
    if (size_in_blocks > std::numeric_limits<uint16_t>::max()) {
      // Bad size
      result.status = VESSEL_ERR_INVALID_VALUE;
      return {};
    }

    // Find matching algorithms
    auto algo_iter = m_algo_ctx->algorithms.find(version_tag);
    if (algo_iter == m_algo_ctx->algorithms.end()) {
      result.status = VESSEL_ERR_BAD_ALGORITHMS;
      return {};
    }

    // Create extent; or at least the data.
    auto ext = std::make_shared<extent>(m_store, algo_iter->second.options);

    // Get paged memory for it. This requires calculating the number of pages.
    auto num_pages = size_in_blocks * (VESSEL_NOMINAL_BLOCK_SIZE / m_pager->page_size());
    auto offset = m_pager->pages_available();
    auto err = m_pager->fetch(ext->paged, offset, num_pages);
    if (VESSEL_ERR_SUCCESS != err) {
      result.status = err;
      return {};
    }

    // Generate extent identifier
    struct vessel_extent_id * id = nullptr;
    err = vessel_extent_id_generate(&id, &(algo_iter->second.choices),
        &author, previous_extent);
    if (VESSEL_ERR_SUCCESS != err) {
      vessel_extent_id_free(&id);

      result.status = err;
      return {};
    }

    // On success, copy data to envelope and header
    ext->envelope.envelope_version = EV_DEFAULT;
    ext->envelope.version_tag = version_tag;

    ext->header.extent_size_multiplier = size_in_blocks;
    ext->header.current_extent_identifier = id->extent_id;
    ext->header.authoring_counter = counter;
    if (!previous_extent) {
      // FIXME encapsulate
      ext->header.previous_extent_identifier.resize(id->extent_id.size());
      liberate::sys::secure_memzero(&(ext->header.previous_extent_identifier)[0], id->extent_id.size());
    }
    else {
      // Copy previous extent over.
      ext->header.previous_extent_identifier = previous_extent->extent_id;
    }
    ext->header.author_identifier = author.author_id;

    ext->dirty = true;

    // Free the temporary ID
    vessel_extent_id_free(&id);

    // Done successfully
    result.status = VESSEL_ERR_SUCCESS;

    // Add extent to index.
    m_index[ext->header.current_extent_identifier] = {
      {offset, num_pages},
      {ext->header.author_identifier, ext->header.previous_extent_identifier},
    };

    return ext;
  }


  std::string                       m_filename = {};
  std::shared_ptr<pager>            m_pager = {};

  vessel_operation_id               m_next_id = 1;

  operation_map                     m_operations = {};

  index                             m_index = {};

  struct vessel_backing_store *     m_store;
  struct vessel_algorithm_context * m_algo_ctx;
};


/**
 * Trampolines for pager_backed_store
 */

static vessel_operation_id
pbs_fetch_latest(struct vessel_backing_store const * store,
    vessel_extent_id const * id,
    vessel_authoring_counter version,
    vessel_store_operation_completed callback,
    void * baton
)
{
  if (!store || !store->data || !id) {
    return VESSEL_ERR_INVALID_VALUE;
  }
  auto pbs = static_cast<vessel::pager_backed_store *>(store->data);
  return pbs->fetch_latest(*id, version, {callback, baton});
}



static vessel_operation_id
pbs_fetch_specific(struct vessel_backing_store const * store,
    vessel_extent_id const * id,
    vessel_authoring_counter version,
    vessel_store_operation_completed callback,
    void * baton
)
{
  if (!store || !store->data || !id) {
    return VESSEL_ERR_INVALID_VALUE;
  }
  auto pbs = static_cast<vessel::pager_backed_store *>(store->data);
  return pbs->fetch_specific(*id, version, {callback, baton});
}



static vessel_operation_id
pbs_release(struct vessel_backing_store const * store,
    struct vessel_extent ** extent,
    vessel_store_operation_completed callback,
    void * baton
)
{
  if (!store || !store->data) {
    return VESSEL_ERR_INVALID_VALUE;
  }
  auto pbs = static_cast<vessel::pager_backed_store *>(store->data);
  return pbs->release(extent, {callback, baton});
}



static vessel_operation_id
pbs_commit(struct vessel_backing_store const * store,
    struct vessel_extent * extent,
    vessel_store_operation_completed callback,
    void * baton
)
{
  if (!store || !store->data) {
    return VESSEL_ERR_INVALID_VALUE;
  }
  auto pbs = static_cast<vessel::pager_backed_store *>(store->data);
  return pbs->commit(extent, {callback, baton});
}



static vessel_error_t
pbs_extent_result(struct vessel_backing_store const * store,
    struct vessel_extent ** extent,
    vessel_operation_id opid)
{
  if (!store || !store->data) {
    return VESSEL_ERR_INVALID_VALUE;
  }
  auto pbs = static_cast<vessel::pager_backed_store *>(store->data);
  return pbs->extent_result(extent, opid);
}



static vessel_operation_id
pbs_create(struct vessel_backing_store const * store,
    uint32_t version_tag, size_t size_in_blocks,
    struct vessel_author_id const * author,
    vessel_authoring_counter counter,
    struct vessel_extent_id const * previous_extent,
    vessel_store_operation_completed callback,
    void * baton
)
{
  if (!store || !store->data || !author || !previous_extent) {
    return VESSEL_ERR_INVALID_VALUE;
  }
  auto pbs = static_cast<vessel::pager_backed_store *>(store->data);
  return pbs->create(version_tag, size_in_blocks, *author, counter,
      *previous_extent, {callback, baton});
}



static vessel_operation_id
pbs_create_origin(struct vessel_backing_store const * store,
    uint32_t version_tag, size_t size_in_blocks,
    struct vessel_author_id const * author,
    vessel_authoring_counter counter,
    vessel_store_operation_completed callback,
    void * baton
)
{
  if (!store || !store->data || !author) {
    return VESSEL_ERR_INVALID_VALUE;
  }
  auto pbs = static_cast<vessel::pager_backed_store *>(store->data);
  return pbs->create_origin(version_tag, size_in_blocks, *author, counter, {callback, baton});
}


static vessel_operation_id
pbs_try_build_index(struct vessel_backing_store const * store,
    vessel_store_operation_completed callback,
    void * baton
)
{
  if (!store || !store->data) {
    return VESSEL_ERR_INVALID_VALUE;
  }
  auto pbs = static_cast<vessel::pager_backed_store *>(store->data);
  return pbs->try_build_index({callback, baton,
      vessel::pager_backed_store::OT_INDEX});
}



static vessel_error_t
pbs_index_result(struct vessel_backing_store const * store,
    struct vessel_store_index_iterator ** iter,
    vessel_operation_id opid)
{
  if (!store || !store->data) {
    return VESSEL_ERR_INVALID_VALUE;
  }
  auto pbs = static_cast<vessel::pager_backed_store *>(store->data);
  return pbs->index_result(iter, opid);
}



static vessel_error_t
pbs_index_iterator_value(
    struct vessel_store_index_iterator const * iter,
    struct vessel_extent_id ** current_id,
    struct vessel_author_id ** author_id,
    struct vessel_extent_id ** previous_id)
{
  if (!iter || !iter->data) {
    return VESSEL_ERR_INVALID_VALUE;
  }

  auto iter_data = static_cast<vessel::pager_backed_store::iter_data *>(iter->data);
  if (!iter_data->store) {
    return VESSEL_ERR_INVALID_VALUE;
  }

  return iter_data->store->index_iterator_value(iter, current_id, author_id,
      previous_id);
}



static vessel_error_t
pbs_index_iterator_next(
    struct vessel_store_index_iterator * iter)
{
  if (!iter || !iter->data) {
    return VESSEL_ERR_INVALID_VALUE;
  }

  auto iter_data = static_cast<vessel::pager_backed_store::iter_data *>(iter->data);
  if (!iter_data->store) {
    return VESSEL_ERR_INVALID_VALUE;
  }

  return iter_data->store->index_iterator_next(iter);
}



static vessel_error_t
pbs_index_iterator_free(
    struct vessel_store_index_iterator ** iter)
{
  if (!iter) {
    return VESSEL_ERR_INVALID_VALUE;
  }

  if (!*iter) {
    return VESSEL_ERR_SUCCESS;
  }

  if (!(*iter)->data) {
    delete *iter;
    *iter = nullptr;
    return VESSEL_ERR_SUCCESS;
  }

  auto iter_data = static_cast<vessel::pager_backed_store::iter_data *>((*iter)->data);
  if (!iter_data->store) {
    return VESSEL_ERR_INVALID_VALUE;
  }

  return iter_data->store->index_iterator_free(iter);
}



static vessel_error_t
pbs_free(struct vessel_backing_store * store)
{
  if (!store) {
    return VESSEL_ERR_INVALID_VALUE;
  }
  if (!store->data) {
    return VESSEL_ERR_SUCCESS;
  }

  auto pbs = static_cast<vessel::pager_backed_store *>(store->data);
  delete pbs;
  store->data = nullptr;

  return VESSEL_ERR_SUCCESS;
}



inline vessel_error_t
create_pbs(struct vessel_backing_store ** store,
    struct vessel_algorithm_context * algo_ctx,
    char const * filename = nullptr)
{
  if (!store) {
    return VESSEL_ERR_INVALID_VALUE;
  }

  if (*store) {
    auto err = vessel_backing_store_free(store);
    if (VESSEL_ERR_SUCCESS != err) {
      return err;
    }
  }

  vessel_backing_store * ret = new vessel_backing_store{};
  ret->data = vessel::pager_backed_store::create(ret, algo_ctx, filename);
  if (!ret->data) {
    delete ret;
    return VESSEL_ERR_FUNC_NOT_AVAILABLE;
  }

  ret->data_free = vessel::pbs_free;

  ret->fetch_extent_latest = vessel::pbs_fetch_latest;
  ret->fetch_extent_specific = vessel::pbs_fetch_specific;

  ret->create_extent = vessel::pbs_create;
  ret->create_extent_origin = vessel::pbs_create_origin;

  ret->release_extent = vessel::pbs_release;
  ret->commit_extent = vessel::pbs_commit;

  ret->extent_result = vessel::pbs_extent_result;

  ret->try_build_index = vessel::pbs_try_build_index;
  ret->index_result = vessel::pbs_index_result;

  ret->index_iterator_value = vessel::pbs_index_iterator_value;
  ret->index_iterator_next = vessel::pbs_index_iterator_next;
  ret->index_iterator_free = vessel::pbs_index_iterator_free;

  *store = ret;
  return VESSEL_ERR_SUCCESS;
}

} // anonymous namespace
} // namespace vessel


extern "C" {

VESSEL_API vessel_error_t
vessel_backing_store_create_memory(struct vessel_backing_store ** store,
    struct vessel_algorithm_context * algo_ctx)
{
  return vessel::create_pbs(store, algo_ctx);
}



VESSEL_API vessel_error_t
vessel_backing_store_create_file(struct vessel_backing_store ** store,
    struct vessel_algorithm_context * algo_ctx,
    char const * filename)
{
  return vessel::create_pbs(store, algo_ctx, filename);
}



VESSEL_API vessel_error_t
vessel_backing_store_free(struct vessel_backing_store ** store)
{
  if (!store) {
    return VESSEL_ERR_INVALID_VALUE;
  }

  if (*store) {
    if ((*store)->data_free && (*store)->data) {
      auto err = (*store)->data_free(*store);
      if (VESSEL_ERR_SUCCESS != err) {
        return err;
      }
    }

    delete *store;
    *store = nullptr;
  }

  return VESSEL_ERR_SUCCESS;
}



} // extern "C"
