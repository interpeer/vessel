/**
 * This file is part of vessel.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt).
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#include <build-config.h>

#include <vessel/author.h>

#include <cstring>

#include "author.h"
#include "algorithms.h"

extern "C" {



/*****************************************************************************
 * vessel_author
 */


VESSEL_API vessel_error_t
vessel_author_generate(struct vessel_author ** author,
    struct vessel_algorithm_choices const * choices)
{
  if (!author || !choices) {
    return VESSEL_ERR_INVALID_VALUE;
  }

  if (*author) {
    auto err = vessel_author_free(author);
    if (VESSEL_ERR_SUCCESS != err) {
      return err;
    }
  }

  auto opts = vessel::options_from_choices(*choices);

  *author = new vessel_author{opts};

  auto err = (*author)->author.generate_keypair();
  if (VESSEL_ERR_SUCCESS != err) {
    vessel_author_free(author);
  }
  return err;
}



VESSEL_API vessel_error_t
vessel_author_free(struct vessel_author ** author)
{
  if (!author) {
    return VESSEL_ERR_INVALID_VALUE;
  }

  if (*author) {
    delete *author;
    *author = nullptr;
  }

  return VESSEL_ERR_SUCCESS;
}


VESSEL_API size_t
vessel_author_size(int key_part)
{
  switch (key_part) {
    case VESSEL_KP_PUBLIC:
    case VESSEL_KP_BOTH:
      return vessel_author::author_type::key_size(key_part);

    default:
      LIBLOG_ERROR("Invalid key part specified.");
      return 0;
  }
}




VESSEL_API vessel_error_t
vessel_author_from_buffer(struct vessel_author ** author,
    struct vessel_algorithm_choices const * choices,
    void const * buffer, size_t bufsize)
{
  if (!author || !buffer || !choices) {
    LIBLOG_DEBUG("Don't know what to do with NULL pointers.");
    return VESSEL_ERR_INVALID_VALUE;
  }

  if (*author) {
    auto err = vessel_author_free(author);
    if (VESSEL_ERR_SUCCESS != err) {
      return err;
    }
  }

  auto opts = vessel::options_from_choices(*choices);

  *author = new vessel_author{opts};

  auto err = (*author)->author.deserialize(buffer, bufsize);
  if (VESSEL_ERR_SUCCESS != err) {
    vessel_author_free(author);
  }
  return err;
}


VESSEL_API vessel_error_t
vessel_author_to_buffer(void * buffer, size_t bufsize,
    struct vessel_algorithm_choices const * choices,
    struct vessel_author const * author,
    int key_part)
{
  if (!author || !buffer || !choices) {
    LIBLOG_DEBUG("Don't know what to do with NULL pointers.");
    return VESSEL_ERR_INVALID_VALUE;
  }
  auto required = vessel_author_size(key_part);
  if (!required) {
    LIBLOG_DEBUG("Invalid key parts specified.");
    return VESSEL_ERR_INVALID_VALUE;
  }
  if (bufsize < required) {
    LIBLOG_DEBUG("Input buffer too small. Got " << bufsize << " but need "
        << required);
    return VESSEL_ERR_INVALID_VALUE;
  }

  auto err = author->author.serialize(buffer, bufsize, key_part);
  return err;
}



VESSEL_API int
vessel_author_compare(struct vessel_author const * author1,
    struct vessel_author const * author2)
{
  if (author1 == author2) {
    return 0;
  }

  if (!author1) {
    return -1;
  }
  if (!author2) {
    return 1;
  }


  if (author1->author < author2->author) {
    return -1;
  }

  return (author1->author == author2->author)
    ? 0
    : 1;
}





/*****************************************************************************
 * vessel_author_id
 */


VESSEL_API vessel_error_t
vessel_author_id_generate(struct vessel_author_id ** author_id,
    struct vessel_algorithm_choices const * choices)
{
  if (!author_id || !choices) {
    return VESSEL_ERR_INVALID_VALUE;
  }

  if (*author_id) {
    auto err = vessel_author_id_free(author_id);
    if (VESSEL_ERR_SUCCESS != err) {
      return err;
    }
  }

  auto opts = vessel::options_from_choices(*choices);

  *author_id = new vessel_author_id{opts};
  // TODO fill with random?
  return VESSEL_ERR_SUCCESS;
}



VESSEL_API vessel_error_t
vessel_author_id_from_author(struct vessel_author_id ** author_id,
    struct vessel_algorithm_choices const * choices,
    struct vessel_author const * author)
{
  if (!author || !author_id || !choices) {
    return VESSEL_ERR_INVALID_VALUE;
  }

  if (*author_id) {
    auto err = vessel_author_id_free(author_id);
    if (VESSEL_ERR_SUCCESS != err) {
      return err;
    }
  }

  auto opts = vessel::options_from_choices(*choices);

  *author_id = new vessel_author_id{opts};
  // TODO
  // auto id = author->author.author_id(opts);
  // if (id.size() != opts.author_identifier_size) {
  // ...
  // }
  (*author_id)->author_id = author->author.author_id();

  return VESSEL_ERR_SUCCESS;
}




VESSEL_API vessel_error_t
vessel_author_id_free(struct vessel_author_id ** author_id)
{
  if (!author_id) {
    return VESSEL_ERR_INVALID_VALUE;
  }

  if (*author_id) {
    delete *author_id;
    *author_id = nullptr;
  }

  return VESSEL_ERR_SUCCESS;
}



VESSEL_API size_t
vessel_author_id_size(struct vessel_algorithm_choices const * choices)
{
  if (!choices) {
    LIBLOG_ERROR("Bad algorithm choices.");
    return 0;
  }

  auto opts = vessel::options_from_choices(*choices);
  return opts.author_identifier_size;
}



VESSEL_API vessel_error_t
vessel_author_id_from_buffer(struct vessel_author_id ** author_id,
    struct vessel_algorithm_choices const * choices,
    void const * buffer, size_t bufsize)
{
  if (!author_id || !buffer || !choices) {
    LIBLOG_DEBUG("Don't know what to do with NULL pointers.");
    return VESSEL_ERR_INVALID_VALUE;
  }
  auto opts = vessel::options_from_choices(*choices);

  if (bufsize < opts.author_identifier_size) {
    LIBLOG_DEBUG("Input buffer too small. Got " << bufsize << " but need "
        << opts.author_identifier_size);
    return VESSEL_ERR_INVALID_VALUE;
  }

  if (!*author_id) {
    *author_id = new vessel_author_id{opts};
  }

  std::memcpy(&((*author_id)->author_id[0]), buffer, opts.author_identifier_size);

  return VESSEL_ERR_SUCCESS;
}



VESSEL_API vessel_error_t
vessel_author_id_to_buffer(void * buffer, size_t * bufsize,
    struct vessel_algorithm_choices const * choices,
    struct vessel_author_id const * author_id)
{
  if (!author_id || !buffer || !bufsize || !choices) {
    LIBLOG_DEBUG("Don't know what to do with NULL pointers.");
    return VESSEL_ERR_INVALID_VALUE;
  }
  auto opts = vessel::options_from_choices(*choices);

  if (author_id->author_id.size() != opts.author_identifier_size) {
    LIBLOG_DEBUG("Author ID does not match choices; is "
        << author_id->author_id.size() << " bytes, but expect "
        << opts.author_identifier_size);
    return VESSEL_ERR_INVALID_VALUE;
  }

  if (*bufsize < opts.author_identifier_size) {
    LIBLOG_DEBUG("Output buffer too small. Got " << bufsize << " but need "
        << opts.author_identifier_size);
    *bufsize = opts.author_identifier_size;
    return VESSEL_ERR_OUT_OF_MEMORY;
  }

  std::memcpy(buffer, author_id->author_id.data(), opts.author_identifier_size);
  *bufsize = opts.author_identifier_size;

  return VESSEL_ERR_SUCCESS;
}


VESSEL_API int
vessel_author_id_compare(struct vessel_author_id const * author_id1,
    struct vessel_author_id const * author_id2)
{
  if (author_id1 == author_id2) {
    return 0;
  }

  if (!author_id1) {
    return -1;
  }
  if (!author_id2) {
    return 1;
  }


  if (author_id1->author_id < author_id2->author_id) {
    return -1;
  }

  return (author_id1->author_id == author_id2->author_id)
    ? 0
    : 1;
}



} // extern "C"
