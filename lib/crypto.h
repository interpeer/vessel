/**
 * This file is part of vessel.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2023 Interpeer gUG (haftungsbeschränkt).
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#ifndef VESSEL_LIB_CRYPTO_H
#define VESSEL_LIB_CRYPTO_H

#include <build-config.h>

#include <vector>

#include <vessel/context.h>

#include <liberate/serialization/integer.h>

#include "context.h"
#include "serialization.h"

// This file/namespace contains cryptographic helper functions, such as
// - for example - the nonce generation algorithm from the specs.
namespace vessel::crypto {

/**
 * Generate a nonce from extent identifier, authoring counter and the
 * nonce hasing algorithm.
 */
inline byte_sequence
generate_nonce(algorithm_options const & algo_opts, extent_header_00 const & header)
{
  // Allocate enough buffer for the concatenated inputs
  byte_sequence tmp;
  tmp.resize(header.current_extent_identifier.size()
      + sizeof(header.authoring_counter));

  // Copy extent identifier
  auto offset = &(tmp[0]);
  size_t remaining = tmp.size();
  std::memcpy(offset, &(header.current_extent_identifier[0]),
      header.current_extent_identifier.size());
  offset += header.current_extent_identifier.size();
  remaining -= header.current_extent_identifier.size();

  // Copy authoring counter
  auto used = liberate::serialization::serialize_int(offset, remaining,
      header.authoring_counter);
  if (used != sizeof(header.authoring_counter)) {
    LIBLOG_ERROR("Could not add counter to nonce, aborting");
    return {};
  }

  // Hash the inputs
  byte_sequence hash;
  hash.resize(algo_opts.nonce_size);

  size_t required = 0;
  auto err = s3kr1t::digest(&hash[0], hash.size(), required,
      tmp.data(), tmp.size(), algo_opts.nonce_hash_type);
  if (s3kr1t::ERR_SUCCESS != err) {
    LIBLOG_ERROR("Could not generate nonce hash!");
    return {};
  }

  // Truncate the hash and return. The size comes from the specs
  hash.resize(12);
  return hash;
}


} // namespace vessel::crypto

#endif // guard
