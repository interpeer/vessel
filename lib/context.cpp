/**
 * This file is part of vessel.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt).
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#include <build-config.h>

#include <vessel/context.h>

#include <liberate/logging.h>

#include "context.h"

extern "C" {

VESSEL_API vessel_error_t
vessel_context_create(struct vessel_context ** context,
    struct vessel_algorithm_context const * algo_context)
{
  if (!context || !algo_context) {
    return VESSEL_ERR_INVALID_VALUE;
  }

  if (*context) {
    auto err = vessel_context_free(context);
    if (VESSEL_ERR_SUCCESS != err) {
      return err;
    }
  }

  vessel_error_t err = VESSEL_ERR_UNEXPECTED;

  *context = new vessel_context{};

  // Just keep a pointer
  (*context)->algos = algo_context;

  // For the section context, however, we'll own stuff. That makes teardown much
  // simpler.
  vessel_section_context * sec_ctx = nullptr;
  err = vessel_section_context_create(&sec_ctx, algo_context);
  if (VESSEL_ERR_SUCCESS != err) {
    goto cleanup;
  }
  (*context)->sections = std::unique_ptr<vessel_section_context>(sec_ctx);

  return VESSEL_ERR_SUCCESS;
cleanup:
  vessel_context_free(context);
  return err;
}



VESSEL_API struct vessel_section_context *
vessel_context_get_section_ctx(struct vessel_context const * context)
{
  if (!context || !context->algos) {
    return nullptr;
  }
  return context->sections.get();
}



VESSEL_API vessel_error_t
vessel_context_free(struct vessel_context ** context)
{
  if (!context) {
    return VESSEL_ERR_INVALID_VALUE;
  }

  if (*context) {
    delete *context;
    *context = nullptr;
  }

  return VESSEL_ERR_SUCCESS;
}

} // extern "C"
