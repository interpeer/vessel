/**
 * This file is part of vessel.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt).
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#ifndef VESSEL_LIB_EXTENT_H
#define VESSEL_LIB_EXTENT_H

#include <build-config.h>

#include <cstring>

#include <memory>
#include <map>
#include <set>
#include <unordered_map>
#include <unordered_set>
#include <deque>
#include <optional>

#include <liberate/types/byte.h>
#include <liberate/logging.h>
#include <liberate/sys/memory.h>
#include <liberate/cpp/hash.h>
#include <liberate/string/hexencode.h>

#include <s3kr1t/random.h>
#include <s3kr1t/digests.h>

#include <vessel/store.h>

#include "pager.h"
#include "serialization.h"

namespace vessel {

/**
 * The extent contains all extent data; it's a separate structure so that
 * the public vessel_extent can be more reasonaby used as an opaque handle.
 */
struct extent
{
  vessel_backing_store *        store = nullptr;
  algorithm_options             opts = {};

  paged_memory                  paged = {};

  extent_envelope_00            envelope = {};
  extent_header_00              header = {};
  extent_footer_00              footer = {};

  // Dirty means that envelope and header need to be written. We're handling
  // the footer more directly in the resource abstraction.
  bool                          dirty = true;


  inline extent(vessel_backing_store * _store,
      algorithm_options const & _opts)
    : store{_store}
    , opts{_opts}
  {
  }


  inline size_t block_size() const
  {
    return paged.size / vessel::VESSEL_NOMINAL_BLOCK_SIZE;
  }


  inline constexpr size_t payload_size() const
  {
    return payload_size(paged.size, opts);
  }

  static inline constexpr size_t payload_size(size_t const & paged_size, algorithm_options const & _opts)
  {
    size_t ret = paged_size;

    // Envelope + header
    ret -= payload_offset(_opts);

    // Footer (signature)
    ret -= _opts.signature_size;

    return ret;
  }


  inline void * payload() const
  {
    return static_cast<char *>(paged.data) + payload_offset();
  }


  inline constexpr size_t payload_offset() const
  {
    return payload_offset(opts);
  }

  static inline constexpr size_t payload_offset(algorithm_options const & _opts)
  {
    size_t ret = 0;

    // Envelope - this is fixed in this draft
    ret += 8;

    // Header - this very much depends on the version_tag
    ret +=
        2 // extent_size
      + _opts.extent_identifier_size // current
      + 3 // counter, 24 bits
      + _opts.extent_identifier_size // previous
      + _opts.author_identifier_size // author
    ;

    return ret;
  }
};



inline bool
empty_extent_id(byte_sequence const & id)
{
  for (auto b : id) {
    if (b) {
      return false;
    }
  }
  return true;
}




inline vessel_error_t
create_combined_hash(
    algorithm_options const & opts,
    byte_sequence & out,
    byte_sequence const * extent_id,
    byte_sequence const & author_id)
{
  // Alrighty, let's do this thing. Create a buffer large enough for the author
  // ID and previous extent ID.
  auto author_id_size = opts.author_identifier_size;
  auto extent_id_size = opts.extent_identifier_size;
  if (!author_id_size || !extent_id_size) {
    LIBLOG_DEBUG("Bad algorithm choices.");
    return VESSEL_ERR_BAD_ALGORITHMS;
  }
  if (author_id.size() != author_id_size) {
    LIBLOG_DEBUG("Bad author id (need " << author_id_size << " but got "
      << author_id.size() << ")");
    return VESSEL_ERR_INVALID_VALUE;
  }
  if (extent_id && extent_id->size() != extent_id_size) {
    LIBLOG_DEBUG("Bad extent id!");
    return VESSEL_ERR_INVALID_VALUE;
  }

  std::vector<char> buf;
  buf.resize(author_id_size + extent_id_size);
  liberate::sys::secure_memzero(&buf[0], buf.size());

  // Copy the author ID over.
  char * offset = &buf[0];
  std::memcpy(offset, author_id.data(), author_id_size);
  offset += author_id_size;

  // Copy the extent ID, if present
  if (extent_id) {
    std::memcpy(offset, extent_id->data(), extent_id_size);
    offset += extent_id_size;
  }

  // Now hash this out; we'll then copy a sufficient number of Bytes into the
  // out value.
  if (!opts.extent_identifier_hash) {
    LIBLOG_DEBUG("No hash function selected for extent identifier.");
    return VESSEL_ERR_UNEXPECTED;
  }

  // Generate hash
  out.resize(extent_id_size);
  std::size_t used = 0;
  auto err = opts.extent_identifier_hash(&out[0], out.size(),
      used, &buf[0], buf.size());
  if (VESSEL_ERR_SUCCESS != err) {
    return err;
  }
  if (used != extent_id_size) {
    LIBLOG_DEBUG("Didn't use all the hash buffer we wanted!");
    return VESSEL_ERR_UNEXPECTED;
  }

  return VESSEL_ERR_SUCCESS;
}


/**
 * Create a derived extent ID.
 */
template <
  typename extent_idT,
  typename author_idT
>
inline vessel_error_t
extent_id_create_derived(
    struct vessel::algorithm_options const & opts,
    extent_idT & out, author_idT const & author_id, extent_idT const & parent)
{
  return create_combined_hash(opts, out, &parent, author_id);
}


/**
 * Create a new extent ID.
 */
inline vessel_error_t
extent_id_create_new(
    struct vessel::algorithm_options const & opts,
    byte_sequence & out)
{
  size_t required = opts.extent_identifier_size;
  out.resize(required);
  auto err = s3kr1t::get_random_bytes(&out[0], required);

  // We can log errors here, but not do much else.
  if (s3kr1t::ERR_SUCCESS != err) {
    LIBLOG_ERROR("Could not generate random bytes!");
    return VESSEL_ERR_UNEXPECTED;
  }
  if (required != opts.extent_identifier_size) {
    LIBLOG_ERROR("Could not generate sufficient amounts of random bytes!");
    return VESSEL_ERR_UNEXPECTED;
  }

  return VESSEL_ERR_SUCCESS;
}


/**
 * The class creates a DAG from extent IDs, based on the parent/child
 * relationship of IDs. In order to establish these relationships, the
 * extent author ID is also required.
 *
 * As a user of the structure, you add extent IDs, and the DAG gets
 * constructed from that. You must have exactly one root extent ID.
 * A further restriction is that in this DAG, every node other than this
 * root has exactly one parent.
 *
 * This does not mean that every other ID added will have a parent in the DAG,
 * though that is the ideal. It is possible for an extent to be added with
 * a parent ID, but the parent itself not being present. This creates
 * orphan nodes. Orphans become de-oprhaned and linked back into the rest of
 * the DAG once the parent is added (provided the parent's parent is
 * present; if not the parent becomes an orphan, etc.)
 *
 * Retrieving nodes from the DAG is in itself a stable operation. We use
 * C++'s notion of lexicographic ordering (see e.g.
 * https://en.cppreference.com/w/cpp/algorithm/lexicographical_compare )
 * to effectively sort extent IDs bitwise. As described in docs/design.md,
 * we retrieve nodes breadth first by extent ID from the root. After that,
 * we do the same for each orphan.
 *
 * This does mean, however, that by unorphaning an orphan, the next time
 * the extents are iterated over, the order may be different - linking the
 * orphan back into the DAG's root's descendants will produce it earlier.
 * There is not much to be done about that.
 */
struct extent_id_dag
{
  using extent_id_type = byte_sequence;
  using author_id_type = byte_sequence;

  struct value_type
  {
    using extent_id_type = typename extent_id_dag::extent_id_type;
    using author_id_type = typename extent_id_dag::author_id_type;

    extent_id_type                extent_id = {};
    std::optional<extent_id_type> parent_id = {};
    std::optional<author_id_type> author_id = {};

    inline bool operator==(value_type const & other) const
    {
      if (extent_id != other.extent_id) {
        return false;
      }

      if (parent_id.has_value() != other.parent_id.has_value()) {
        return false;
      }
      if (parent_id.has_value()) {
        if (parent_id.value() != other.parent_id.value()) {
          return false;
        }
      }

      if (author_id.has_value() != other.author_id.has_value()) {
        return false;
      }
      if (author_id.has_value()) {
        if (author_id.value() != other.author_id.value()) {
          return false;
        }
      }

      return true;
    }
  };


  inline extent_id_dag()
  {
  }


  inline explicit extent_id_dag(extent_id_type const & extent_id,
      std::optional<author_id_type> const & author_id = {})
  {
    add(extent_id, author_id);
  }


  /**
   * Add the root extent ID. This function will only accept one call.
   */
  inline bool add(extent_id_type const & extent_id,
      std::optional<author_id_type> const & author_id = {})
  {
    auto node_value = std::make_unique<value_type>();
    node_value->extent_id = extent_id;

    if (author_id.has_value()) {
      node_value->author_id = author_id.value();
    }

    if (m_root_node.value) {
      if (*m_root_node.value == *node_value) {
        // No change, just accept it silently
        return true;
      }
      LIBLOG_DEBUG("DAG already has a root extent ID.");
      return false;
    }

    m_root_node.value = std::move(node_value);
    return true;
  }

  /**
   * Return the number of orphans.
   */
  inline std::size_t num_orphans() const
  {
    std::size_t num_orphans = 0;
    for (auto & [parent, orphans] : m_missing_parents) {
      num_orphans += orphans.size();
    }
    return num_orphans;
  }

  /**
   * Return the number of siblings for the given node, including itself - i.e.
   * the minimum value is always 1. A value of zero indicates the node could
   * not be found.
   */
  inline std::size_t num_siblings(extent_id_type const & extent_id) const
  {
    if (extent_id == m_root_node.value->extent_id) {
      return 1;
    }

    auto iter = m_index.find(extent_id);
    if (iter == m_index.end()) {
      return 0;
    }

    if (!iter->second->parent) {
      // This is an orphan. Find all nodes with the same parent ID
      // insted.
      auto iter2 = m_missing_parents.find(iter->second->value->parent_id.value());
      if (iter2 == m_missing_parents.end()) {
        LIBLOG_ERROR("Internal DAG inconsistency detected, aborting!");
        return 0;
      }
      return iter2->second.size();
    }

    return iter->second->parent->children.size();
  }

  /**
   * Return all missing parents
   */
  inline std::set<extent_id_type> missing_parents() const
  {
    std::set<extent_id_type> ret;
    for (auto & [parent, orphans] : m_missing_parents) {
      ret.insert(parent);
    }
    return ret;
  }

  /**
   * Return all orphans
   */
  inline std::set<extent_id_type> orphans() const
  {
    std::set<extent_id_type> ret;
    for (auto & [parent, orphans] : m_missing_parents) {
      for (auto & orphan : orphans) {
        ret.insert(orphan);
      }
    }
    return ret;
  }


  /**
   * Raturn root ID
   */
  inline extent_id_type root() const
  {
    if (m_root_node.value) {
      return m_root_node.value->extent_id;
    }
    return {};
  }


  /**
   * Extent IDs other than the root must have an author and a parent.
   */
  inline bool add(extent_id_type const & extent_id,
      author_id_type const & author_id,
      extent_id_type const & parent_id)
  {
    // Convencience for when the parent id is empty
    if (parent_id.empty()) {
      return add(extent_id, author_id);
    }

    // It's possible that we already know this extent_id. If that's the
    // case, we don't really need to do much, other than provide some kind
    // of sanity check.
    auto iter = m_index.find(extent_id);
    if (iter != m_index.end()) {
      value_type const & val = *(iter->second->value.get());
      if (author_id == val.author_id
          && parent_id == val.parent_id)
      {
        // If we're asked to add *exactly* the same value, we may as well
        // pretend it worked.
        return true;
      }
      // Otherwise, however, we have to bail out.
      return false;
    }

    // We can create a new node.
    auto new_node = std::make_unique<node>();
    new_node->value = std::make_unique<value_type>();
    new_node->value->extent_id = extent_id;
    new_node->value->author_id = author_id;
    new_node->value->parent_id = parent_id;
    auto new_node_raw = new_node.get();

    // First, add them to the container.
    m_index[extent_id] = std::move(new_node);

    // Next, we try to find the parent extent. If that does not exist,
    // we're adding an orphan. Otherwise, we know where to put this
    // node.
    iter = m_index.find(parent_id);
    if (iter == m_index.end()) {
      // The parent could be the root.
      bool in_root = false;
      if (m_root_node.value) {
        if (parent_id == m_root_node.value->extent_id) {
          in_root = true;
          new_node_raw->parent = &m_root_node;
          new_node_raw->parent->children[extent_id] = new_node_raw;
        }
      }

      // The parent is unknown, which makes this an orphan.
      // Record that the parent has a child.
      if (!in_root) {
        m_missing_parents[parent_id].insert(extent_id);
      }
    }
    else {
      // If the parent is known, we can add the node to the parent. For
      // this, we've kept the raw node pointer.
      new_node_raw->parent = iter->second.get();
      new_node_raw->parent->children[extent_id] = new_node_raw;
    }

    // If the newly added node unorphans current orphans, we should
    // fix those.
    auto iter2 = m_missing_parents.find(extent_id);
    if (iter2 != m_missing_parents.end()) {
      for (auto & orphan_id : iter2->second) {
        // Find the child node.
        iter = m_index.find(orphan_id);
        if (iter == m_index.end() || iter->second->parent) {
          throw std::logic_error("This cannot happen if the code is correct.");
        }

        // The child node's parent is the raw node.
        iter->second->parent = new_node_raw;
        new_node_raw->children[orphan_id] = iter->second.get();
      }

      // Erase this extent ID from the parent index.
      m_missing_parents.erase(iter2);
    }

    // Success!
    return true;
  }


  /**
   * Find the optimal parent extent identifier for creating a new extent. This
   * is based on the path traversal algorithm defined in the specs. That is,
   * this always reflects a current best effort based on the synchronization
   * state of the resource, i.e. the number of nodes in the dag.
   */
  inline extent_id_type best_parent() const
  {
    // We first create a map of IDs to path values for all nodes. Using the
    // breadth first iterator here helps, as it covers all orphaned sub-trees
    // *and* allows us to accumulate the path value for each node by also
    // considering the parent node's value.
    std::map<extent_id_type, double>  path_weights;
    for (auto & value : *this) {
      double cur = 1.0 / num_siblings(value.extent_id);
      if (value.parent_id.has_value()) {
        cur += path_weights[value.parent_id.value()];
      }
      path_weights[value.extent_id] = cur;
    }

    // Now find the weightiest paths. There may be several.
    std::set<extent_id_type> current_best;
    double current_best_weight = 0;
    for (auto & [extent_id, weight] : path_weights) {
      if (weight > current_best_weight) {
        current_best_weight = weight;
        current_best.clear();
        current_best.insert(extent_id);
      }
      else if (weight == current_best_weight) {
        current_best.insert(extent_id);
      }
    }

    // The current_best set is ordered, so the last entry wins.
    return *(current_best.rbegin());
  }


  /**
   * Return true if the given extent ID is contained within.
   */
  inline bool contains(extent_id_type const & extent_id)
  {
    // True, and contained at the root
    if (m_root_node.value && m_root_node.value->extent_id == extent_id) {
      return true;
    }

    auto iter = m_index.find(extent_id);
    return iter != m_index.end();
  }


  /**
   * Lookup operator is const only.
   */
  inline value_type const &
  operator[](extent_id_type const & extent_id) const
  {
    if (m_root_node.value && m_root_node.value->extent_id == extent_id) {
      return *m_root_node.value;
    }

    auto iter = m_index.find(extent_id);
    if (iter != m_index.end()) {
      return *(iter->second->value.get());
    }

    return invalid_value();
  }



  inline friend std::ostream &
  operator<<(std::ostream & os, extent_id_dag const & dag);


  struct node;

  using node_map = std::map<extent_id_type, node *>;

  struct node
  {
    std::unique_ptr<value_type> value = {};
    node *                      parent = nullptr;
    node_map                    children = {};
  };


  struct extent_id_hasher
  {
    std::size_t operator()(extent_id_type const & extent_id) const noexcept
    {
      return liberate::cpp::range_hash(extent_id.data(), extent_id.data() + extent_id.size());
    }
  };

  // We use a global index of known extent IDs; this helps with some sanity
  // checks. It's also our main container.
  // In the global index, we use an *unordered* map, because we'll use ordered
  // maps where we need the ordering to be stable.
  using global_index = std::unordered_map<
    extent_id_type,
    std::unique_ptr<node>,
    extent_id_hasher
  >;

  // We use a secondary index to record parent IDs where the parent itself
  // is not in the map. These effectively represent orphans.
  using orphan_set = std::set<
    extent_id_type
  >;
  using parent_index = std::map<
    extent_id_type,
    orphan_set
  >;


  inline static value_type const & invalid_value()
  {
    static value_type ret;
    return ret;
  }


  inline bool operator==(extent_id_dag const & other) const
  {
    // Compare identity
    return this == &other;
  }

  inline bool operator!=(extent_id_dag const & other) const
  {
    // Compare identity
    return this != &other;
  }

  // The const_iterator is a breadth first iterator, ordering children by
  // extent id.
  struct const_iterator
  {
  public:
    inline value_type const & operator*() const
    {
      if (todo.empty()) {
        return invalid_value();
      }
      auto cur = todo.begin();
      if (!(*cur)->value) {
        return invalid_value();
      }
      return *((*cur)->value.get());
    }

    inline value_type const * operator->() const
    {
      if (todo.empty()) {
        return nullptr;
      }
      auto cur = todo.begin();
      if (!(*cur)->value) {
        return nullptr;
      }
      return (*cur)->value.get();
    }

    inline bool operator==(const_iterator const & other) const
    {
      if (dag != other.dag) {
        return false;
      }
      // Whole list comparison may be long, let's compare size first.
      if (todo.size() != other.todo.size()) {
        return false;
      }
      if (todo != other.todo) {
        return false;
      }
      return true;
    }

    inline bool operator!=(const_iterator const & other) const
    {
      return !(*this == other);
    }

    // Prefix
    inline const_iterator & operator++()
    {
      if (!dag) {
        return *this;
      }

      auto cur = todo.begin();

      // First, add all the current node's children to the deque (if any).
      for (auto & [child_id, child] : (*cur)->children) {
        todo.push_back(child);
      }

      // Next, pop the front.
      todo.pop_front();

      // If the queue is now empty, we may add a new root. That will be any root
      // from the dag's orphans. This part is not super efficient, but it only
      // occurs when a new orphaned sub-tree is to be found.
      if (todo.empty() && visit_orphans) {
        for (auto & [parent, orphans] : dag->m_missing_parents) {
          for (auto & orphan : orphans) {
            auto iter = visited_roots.find(orphan);
            if (iter == visited_roots.end()) {
              auto node_iter = dag->m_index.find(orphan);
              if (node_iter == dag->m_index.end()) {
                LIBLOG_ERROR("Internal DAG inconsistency detected, aborting!");
                return *this;
              }

              todo.push_back(node_iter->second.get());
              visited_roots.insert(orphan);
            }
          }
        }
      }

      return *this;
    }

    // Postfix
    inline const_iterator operator++(int) const
    {
      if (!dag) {
        return *this;
      }

      const_iterator copy{*this};
      ++copy;
      return copy;
    }

    inline const_iterator() = default;

    inline const_iterator & operator=(const_iterator const &) = default;
    inline const_iterator & operator=(const_iterator &&) = default;


  private:
    friend struct extent_id_dag;

    inline explicit const_iterator(extent_id_dag const * _dag,
        node const * _node, bool _visit_orphans = true)
      : dag{_dag}
      , visit_orphans{_visit_orphans}
    {
      if (!_node || !_node->value) {
        return;
      }

      todo.push_back(_node);
      visited_roots.insert(_node->value->extent_id);
    }

    inline const_iterator(const_iterator const &) = default;
    inline const_iterator(const_iterator &&) = default;

    extent_id_dag const *     dag = nullptr;
    bool                      visit_orphans = false;

    using todo_queue = std::deque<node const *>;
    todo_queue                todo = {};

    using visited_root_set = std::set<extent_id_type>;
    visited_root_set          visited_roots = {};
  };


  inline const_iterator begin() const
  {
    return const_iterator{this, &m_root_node};
  }

  inline const_iterator end() const
  {
    return const_iterator{this, nullptr};
  }


  /**
   * Find a node with the given extent id. You can use this node to as a root
   * for a partial DAG, and iterate to the end. Note, however, that orphans
   * will *NOT* be automatically visisted.
   *
   * However, using find() and orphans() above, it is possible to iterate e.g.
   * an orphaned sub-tree.
   */
  inline const_iterator find(extent_id_type const & extent_id) const
  {
    if (extent_id == m_root_node.value->extent_id) {
      return const_iterator{this, &m_root_node, false};
    }

    auto iter = m_index.find(extent_id);
    if (iter == m_index.end()) {
      return end();
    }

    return const_iterator{this, iter->second.get(), false};
  }


private:
  global_index  m_index;
  parent_index  m_missing_parents;

  // There can be only one root node, however.
  node          m_root_node;
};


namespace {

template <typename bytesT>
inline void
hexdump(std::ostream & os,
    bytesT const * arr,
    char first, char last,
    bool verbose)
{
  namespace ls = liberate::string;

  static constexpr size_t SHORT_SIZE = 8;
  size_t display_size = sizeof(bytesT);

  if (!verbose) {
    display_size = std::min(display_size, SHORT_SIZE);
  }

  os << first << ls::hexencode(reinterpret_cast<liberate::types::byte const *>(arr),
      display_size);
  if (display_size < sizeof(bytesT) && !verbose) {
    os << "...";
  }
  os << last;
}


inline void
dump_node_value(std::ostream & os, typename extent_id_dag::value_type const & value, size_t indent, bool verbose)
{
  namespace ls = liberate::string;

  for (size_t i = 0 ; i < indent ; ++i) {
    os << "  ";
  }

  hexdump(os, &value.extent_id, '<', '>', verbose);

  if (value.parent_id.has_value()) {
    os << " (from: ";
    hexdump(os, &(value.parent_id.value()), '<', '>', verbose);
    os << ")";
  }

  if (value.author_id.has_value()) {
    os << ' ';
    hexdump(os, &(value.author_id.value()), '[', ']', verbose);
  }
  else {
    os << " [unknown author]";
  }
}


inline void
dump_node(std::ostream & os, typename extent_id_dag::node const & node, size_t indent, bool verbose)
{
  if (!node.value) {
    os << "<empty>";
  }
  else {
    dump_node_value(os, *node.value, indent, verbose);
  }
  os << std::endl;

  auto size = node.children.size();
  for (auto & [child_id, child] : node.children) {
    dump_node(os, *child, indent + 1, verbose);
    if (--size > 1) {
      os << std::endl;
    }
  }
}


} // anonymous namespace


inline std::ostream &
operator<<(std::ostream & os, typename extent_id_dag::value_type const & value)
{
  dump_node_value(os, value, 0, false);
  return os;
}



inline std::ostream &
operator<<(std::ostream & os, extent_id_dag const & dag)
{
  bool verbose = false;

  os << "ROOT:" << std::endl;;
  dump_node(os, dag.m_root_node, 1, verbose);

  // Collect orphans
  typename extent_id_dag::orphan_set orphans;
  for (auto & [parent_id, p_orphans] : dag.m_missing_parents) {
    for (auto & item : p_orphans) {
      orphans.insert(item);
    }
  }

  auto size = orphans.size();
  os << "ORPHANS: " << size << std::endl;
  for (auto & orphan_id : orphans) {
    auto iter = dag.m_index.find(orphan_id);
    dump_node(os, *(iter->second), 1, verbose);
  }

  return os;
}

} // namespace vessel

extern "C" {

struct vessel_extent_id
{
  vessel::algorithm_options   opts{};
  vessel::byte_sequence       extent_id{};

  inline explicit vessel_extent_id(vessel::algorithm_options const & _opts)
    : opts{_opts}
  {
    extent_id.resize(opts.extent_identifier_size);
    liberate::sys::secure_memzero(&extent_id[0], extent_id.size());
  }

  inline vessel_extent_id(vessel::algorithm_options const & _opts,
      vessel::byte_sequence const & _extent_id)
    : opts{_opts}
    , extent_id{_extent_id}
  {
    if (extent_id.size() != opts.extent_identifier_size) {
      throw vessel::exception{VESSEL_ERR_INVALID_VALUE, "Extent identifier has bad size!"};
    }
  }

  inline vessel_extent_id()
  {
    // Extent id is zero length and uninitalized
  }

  inline bool is_empty_id() const
  {
    return is_empty_id(extent_id);
  }

  static inline bool is_empty_id(vessel::byte_sequence const & id)
  {
    return id.empty() || is_zero(id);
  }

  static inline bool is_zero(vessel::byte_sequence const & id)
  {
    for (auto & b : id) {
      if (b) {
        return false;
      }
    }
    return true;
  }
};


struct vessel_extent
{
  std::shared_ptr<vessel::extent> ext{};
};


} // extern "C"


namespace vessel {

inline std::ostream &
operator<<(std::ostream & os, struct vessel_extent_id const & ext_id)
{
  os << ext_id.extent_id;
  return os;
}

} // namespace vessel


#endif // guard
