/**
 * This file is part of vessel.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt).
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#ifndef VESSEL_LIB_EXCEPTION_H
#define VESSEL_LIB_EXCEPTION_H

#include <vessel/error.h>

namespace vessel {

namespace {

// Helper function for trying to create a verbose error message. This may fail
// if there can't be any more allocations, for example, so be extra careful.
inline void
combine_error(std::string & result, vessel_error_t code, int errnum,
    std::string const & details)
{
  try {
    result = "[" + std::string{vessel_error_name(code)} + "] ";
    result += std::string{vessel_error_message(code)};
    if (errnum) {
      result += " // ";
      result += liberate::sys::error_message(errnum);
    }
    if (!details.empty()) {
      result += " // ";
      result += details;
    }
  } catch (...) {
    result = "Error copying error message.";
  }
}

} // anonymous namespace

/**
 * Exception class. Constructed with an error code and optional message.
 **/
class exception : public std::runtime_error
{
public:
  /**
   * Constructor/destructor
   **/
  inline explicit exception(vessel_error_t code, std::string const & details = {}) throw()
    : std::runtime_error{""}
    , m_code{code}
  {
    combine_error(m_message, m_code, 0, details);
  }


  inline exception(vessel_error_t code, int errnum, std::string const & details = {}) throw()
    : std::runtime_error{""}
    , m_code{code}
  {
    combine_error(m_message, m_code, errnum, details);
  }

  virtual ~exception() throw() = default;

  /**
   * std::exception interface
   **/
  virtual char const * what() const throw()
  {
    if (m_message.empty()) {
      return vessel_error_message(m_code);
    }
    return m_message.c_str();
  }


  /**
   * Additional functions
   **/
  char const * name() const throw()
  {
    return vessel_error_name(m_code);
  }

  vessel_error_t code() const throw()
  {
    return m_code;
  }

private:
  vessel_error_t  m_code;
  std::string     m_message;
};

} // namespace vessel

#endif // guard
