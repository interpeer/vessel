/**
 * This file is part of vessel.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt).
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#ifndef VESSEL_LIB_ALGORITHMS_H
#define VESSEL_LIB_ALGORITHMS_H

#include <build-config.h>

#include <unordered_map>
#include <vector>
#include <functional>
#include <ostream>
#include <iomanip>
#include <cstring>

#include <vessel/algorithms.h>

#include <liberate/types/byte.h>
#include <liberate/cpp/operators/comparison.h>
#include <liberate/logging.h>

#include <s3kr1t/digests.h>
#include <s3kr1t/macs.h>

namespace vessel {

/**
 * We need a variable length byte container for various identifiers; This
 * unfortunately means we have no compile-time check on the length of the
 * identifier, but otherwise it would be difficult to support a variable
 * number of version tags.
 */
using byte_sequence = std::vector<liberate::types::byte>;

inline std::ostream &
operator<<(std::ostream & os, byte_sequence const & bytes)
{
  os << "<" << std::hex;
  for (size_t i = 0 ; i < bytes.size() ; ++i) {
    if (i && !(i % 2)) {
      os << ':';
    }
    os << std::setw(2) << std::setfill('0') << static_cast<uint16_t>(bytes[i]);
  }
  os << ">" << std::dec;
  return os;
}



/**
 * Hash functions; they're quite lightweight, but mean more information on
 * the s3kr1t library is bundled in one place, making this easier to change.
 */
// XXX We're not using std::function here, because apparently that does not
//     initialize all its members, leading to valgrind errors. Luckily a simple
//     function pointer suffices.
typedef vessel_error_t (*hash_function)(void *, size_t, size_t &, void const *, size_t);

template <s3kr1t::digest_type TYPE>
inline vessel_error_t
hash_function_wrapper(void * out, size_t outsize, size_t & used,
    void const * in, size_t insize)
{
  auto err = s3kr1t::digest(out, outsize, used, in, insize, TYPE);
  if (s3kr1t::ERR_SUCCESS != err) {
    // TODO log s3kr1t error
    return VESSEL_ERR_HASH_GENERATION;
  }
  return VESSEL_ERR_SUCCESS;
}


/**
 * While the public vessel_algorithm_choices provides algorithm *selections*,
 * in practice, we need more concrete information to work with them - such as
 * hash output lengths, and hash functions, etc.
 *
 * This structure collects a set of these corresponding to some choices and/or
 * a version tag.
 */
struct algorithm_options
  : public liberate::cpp::comparison_operators<algorithm_options>
{
  std::size_t         extent_identifier_size = 0;
  hash_function       extent_identifier_hash = nullptr;

  std::size_t         author_identifier_size = 0;
  s3kr1t::digest_type author_identifier_hash_type = s3kr1t::DT_NONE;

  // TODO
  std::size_t         nonce_size = 0;
  s3kr1t::digest_type nonce_hash_type = s3kr1t::DT_NONE;

  std::size_t         message_authentication_size = 0;
  s3kr1t::mac_type    message_authentication_type = s3kr1t::MT_NONE;

  std::size_t         signature_size = 0;

  bool                private_header = false;

  // POD struct; let's keep tihs simple.
  inline algorithm_options() = default;
  inline algorithm_options(algorithm_options const &) = default;
  inline algorithm_options(algorithm_options &&) = default;

  inline algorithm_options & operator=(algorithm_options const &) = default;
  inline algorithm_options & operator=(algorithm_options &&) = default;

  // XXX For a simple struct, memory-based comparison is ok. In particular,
  //     we're not really concerned with the semantics here, just care about
  //     making it not-equal comparable.
  inline bool is_equal_to(algorithm_options const & other) const
  {
    return 0 == std::memcmp(this, &other, sizeof(algorithm_options));
  }

  inline bool operator<(algorithm_options const & other) const
  {
    return 0 > std::memcmp(this, &other, sizeof(algorithm_options));
  }
};


inline algorithm_options
options_from_choices(vessel_algorithm_choices const & choices)
{
  algorithm_options ret{};

  // *** Version tag hash
  switch (choices.version_tag_hash) {
    // TODO
    default:
      break;
  }


  // *** Extent identifier hash
  switch (choices.extent_identifier_hash) {
    case VESSEL_EIH_SHA2_224:
      ret.extent_identifier_size = 224 / 8;
      ret.extent_identifier_hash = hash_function_wrapper<s3kr1t::DT_SHA2_224>;
      break;

    case VESSEL_EIH_SHA2_256:
      ret.extent_identifier_size = 256 / 8;
      ret.extent_identifier_hash = hash_function_wrapper<s3kr1t::DT_SHA2_256>;
      break;

    case VESSEL_EIH_SHA2_384:
      ret.extent_identifier_size = 384 / 8;
      ret.extent_identifier_hash = hash_function_wrapper<s3kr1t::DT_SHA2_384>;
      break;

    case VESSEL_EIH_SHA2_512:
      ret.extent_identifier_size = 512 / 8;
      ret.extent_identifier_hash = hash_function_wrapper<s3kr1t::DT_SHA2_512>;
      break;

    case VESSEL_EIH_SHA3_224:
      ret.extent_identifier_size = 224 / 8;
      ret.extent_identifier_hash = hash_function_wrapper<s3kr1t::DT_SHA3_224>;
      break;

    case VESSEL_EIH_SHA3_256:
      ret.extent_identifier_size = 256 / 8;
      ret.extent_identifier_hash = hash_function_wrapper<s3kr1t::DT_SHA3_256>;
      break;

    case VESSEL_EIH_SHA3_384:
      ret.extent_identifier_size = 384 / 8;
      ret.extent_identifier_hash = hash_function_wrapper<s3kr1t::DT_SHA3_384>;
      break;

    case VESSEL_EIH_SHA3_512:
      ret.extent_identifier_size = 512 / 8;
      ret.extent_identifier_hash = hash_function_wrapper<s3kr1t::DT_SHA3_512>;
      break;

    default:
      break;
  }


  // *** Author identifier hash
  switch (choices.author_identifier_hash) {
    case VESSEL_AIH_SHA2_224:
      ret.author_identifier_size = 224 / 8;
      ret.author_identifier_hash_type = s3kr1t::DT_SHA2_224;
      break;

    case VESSEL_AIH_SHA2_256:
      ret.author_identifier_size = 256 / 8;
      ret.author_identifier_hash_type = s3kr1t::DT_SHA2_256;
      break;

    case VESSEL_AIH_SHA2_384:
      ret.author_identifier_size = 384 / 8;
      ret.author_identifier_hash_type = s3kr1t::DT_SHA2_384;
      break;

    case VESSEL_AIH_SHA2_512:
      ret.author_identifier_size = 512 / 8;
      ret.author_identifier_hash_type = s3kr1t::DT_SHA2_512;
      break;

    case VESSEL_AIH_SHA3_224:
      ret.author_identifier_size = 224 / 8;
      ret.author_identifier_hash_type = s3kr1t::DT_SHA3_224;
      break;

    case VESSEL_AIH_SHA3_256:
      ret.author_identifier_size = 256 / 8;
      ret.author_identifier_hash_type = s3kr1t::DT_SHA3_256;
      break;

    case VESSEL_AIH_SHA3_384:
      ret.author_identifier_size = 384 / 8;
      ret.author_identifier_hash_type = s3kr1t::DT_SHA3_384;
      break;

    case VESSEL_AIH_SHA3_512:
      ret.author_identifier_size = 512 / 8;
      ret.author_identifier_hash_type = s3kr1t::DT_SHA3_512;
      break;

    case VESSEL_AIH_NONE:
      if (VESSEL_AS_ED25519 == choices.asymmetric_signature) {
        ret.author_identifier_size = 32;
      }
      else if (VESSEL_AS_ED448 == choices.asymmetric_signature) {
        ret.author_identifier_size = 57;
      }
      else {
        LIBLOG_ERROR("'none' author identifier hash is only permitted with EdDSA");
      }
      ret.author_identifier_hash_type = s3kr1t::DT_NONE;
      break;

    default:
      break;
  }


  // *** Nonce hash
  switch (choices.nonce_hash) {
    case VESSEL_NH_SHA2_224:
      ret.nonce_size = 224 / 8;
      ret.nonce_hash_type = s3kr1t::DT_SHA2_224;
      break;

    case VESSEL_NH_SHA2_256:
      ret.nonce_size = 256 / 8;
      ret.nonce_hash_type = s3kr1t::DT_SHA2_256;
      break;

    case VESSEL_NH_SHA2_384:
      ret.nonce_size = 384 / 8;
      ret.nonce_hash_type = s3kr1t::DT_SHA2_384;
      break;

    case VESSEL_NH_SHA2_512:
      ret.nonce_size = 512 / 8;
      ret.nonce_hash_type = s3kr1t::DT_SHA2_512;
      break;

    case VESSEL_NH_SHA3_224:
      ret.nonce_size = 224 / 8;
      ret.nonce_hash_type = s3kr1t::DT_SHA3_224;
      break;

    case VESSEL_NH_SHA3_256:
      ret.nonce_size = 256 / 8;
      ret.nonce_hash_type = s3kr1t::DT_SHA3_256;
      break;

    case VESSEL_NH_SHA3_384:
      ret.nonce_size = 384 / 8;
      ret.nonce_hash_type = s3kr1t::DT_SHA3_384;
      break;

    case VESSEL_NH_SHA3_512:
      ret.nonce_size = 512 / 8;
      ret.nonce_hash_type = s3kr1t::DT_SHA3_512;
      break;

    default:
      break;
  }


  // *** Signature hash
  switch (choices.signature_hash) {
    case VESSEL_SH_SHA2_224:
      ret.signature_size = 224 / 8;
      break;

    case VESSEL_SH_SHA2_256:
      ret.signature_size = 256 / 8;
      break;

    case VESSEL_SH_SHA2_384:
      ret.signature_size = 384 / 8;
      break;

    case VESSEL_SH_SHA2_512:
      ret.signature_size = 512 / 8;
      break;

    case VESSEL_SH_SHA3_224:
      ret.signature_size = 224 / 8;
      break;

    case VESSEL_SH_SHA3_256:
      ret.signature_size = 256 / 8;
      break;

    case VESSEL_SH_SHA3_384:
      ret.signature_size = 384 / 8;
      break;

    case VESSEL_SH_SHA3_512:
      ret.signature_size = 512 / 8;
      break;

    case VESSEL_SH_EDDSA:
      // For EdDSA we have to consult the algorithm choice. We'll do that
      // in the section below.
      break;

    default:
      break;
  }


  // *** Asymmetric signature
  switch (choices.asymmetric_signature) {
    case VESSEL_AS_DSA:
      // TODO
      break;

    case VESSEL_AS_RSA:
      // TODO
      break;

    case VESSEL_AS_ED25519:
      // TODO
      ret.signature_size = 64;
      break;

    case VESSEL_AS_ED448:
      // TODO
      ret.signature_size = 114;
      break;

    default:
      break;
  }


  // *** Message authentication
  switch (choices.message_authentication) {
    case VESSEL_MA_KMAC128:
      ret.message_authentication_size = 16; // selected by s3kr1t
      ret.message_authentication_type = s3kr1t::MT_KMAC_128;
      break;

    case VESSEL_MA_KMAC256:
      ret.message_authentication_size = 32; // selected by s3kr1t
      ret.message_authentication_type = s3kr1t::MT_KMAC_256;
      break;

    case VESSEL_MA_POLY1305:
      ret.message_authentication_size = 16; // 128 bits according to RFC8439
      ret.message_authentication_type = s3kr1t::MT_POLY1305;
      break;

    // TODO
    default:
      break;
  }


  // *** Symmetric encryption
  switch (choices.symmetric_encryption) {
    // TODO
    default:
      break;
  }


  // *** Signature algorithm
  switch (choices.signature_algorithm) {
    // TODO
    default:
      break;
  }


  // *** Private header flag
  switch (choices.private_header_flag) {
    case VESSEL_PHF_PH_EQ_0:
      ret.private_header = false;
      break;

    case VESSEL_PHF_PH_EQ_1:
      ret.private_header = true;
      break;

    default:
      break;
  }


  return ret;
}

} // namespace vessel


/**
 * The vessel algorithm context
 */
struct vessel_algorithm_context
{
  struct algorithm
  {
    inline algorithm() = default;

    inline algorithm(vessel_algorithm_choices const & _choices)
      : choices{_choices}
      , options{vessel::options_from_choices(_choices)}
    {}

    vessel_algorithm_choices  choices = {};
    vessel::algorithm_options options = {};
  };

  using algorithm_map = std::unordered_map<uint32_t, algorithm>;
  algorithm_map algorithms;
};


#endif // guard
