/**
 * This file is part of vessel.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt).
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#ifndef VESSEL_LIB_PAYLOAD_H
#define VESSEL_LIB_PAYLOAD_H

#include <build-config.h>

#include <functional>

#include <liberate/string/hexencode.h>

#include "algorithms.h"
#include "section.h"
#include "serialization.h"

namespace vessel {

struct resource; // FIXME

/**
 * A context for iterating an extent payload section by section.
 */
struct payload
{
  vessel_section_context::section_size_map const &  section_sizes;
  algorithm_options const &                         algo_opts;
  char *                                            buffer;
  size_t                                            buffer_size;

  inline payload(
        vessel_section_context::section_size_map const & _section_sizes,
        algorithm_options const & _algo_opts,
        char * _buffer,
        size_t _buffer_size
      )
    : section_sizes{_section_sizes}
    , algo_opts{_algo_opts}
    , buffer{_buffer}
    , buffer_size{_buffer_size}
  {
  }

  using value_type = vessel::section;

  struct iterator
  {
    value_type        value = {};
    size_t            size = 0;

    payload &         ctx;
    size_t            offset;

    inline iterator(payload & _ctx,
        size_t _offset)
      : ctx{_ctx}
      , offset{_offset}

    {
      if (offset < ctx.buffer_size) {
        auto used = serialization::deserialize_section(
            ctx.section_sizes, ctx.algo_opts,
            value,
            ctx.buffer + offset,
            ctx.buffer_size - offset);
        if (!used) {
          offset = ctx.buffer_size;
        }
      }
    }

    inline bool operator==(iterator const & other) const
    {
      return offset == other.offset;
    }

    inline bool operator!=(iterator const & other) const
    {
      return !(*this == other);
    }

    inline value_type & operator*()
    {
      return value;
    }

    inline value_type * operator->()
    {
      return &value;
    }

    inline iterator & operator++()
    {
      // If no section could be parsed, set to end-of-buffer
      if (!value.section_size) {
        offset = ctx.buffer_size;
      }
      else {
        offset += value.section_size;
        if (offset > ctx.buffer_size) {
          LIBLOG_ERROR("Section is larger than payload end, this isn't right!");
          offset = ctx.buffer_size;
        }
        else if (offset < ctx.buffer_size) {
          // Otherwise, parse the next section after the current one.
          value = value_type{};

          auto used = serialization::deserialize_section(
              ctx.section_sizes, ctx.algo_opts,
              value,
              ctx.buffer + offset,
              ctx.buffer_size - offset);
          if (!used) {
            LIBLOG_DEBUG("Looks like the last section.");
            offset = ctx.buffer_size;
          }
        }
      }

      return *this;
    }
  };


  inline iterator begin()
  {
    return {*this, 0};
  }


  inline iterator end()
  {
    return {*this, buffer_size};
  }


  inline size_t size()
  {
    size_t count = 0;
    for (auto & _ [[maybe_unused]] : *this) {
      ++count;
    }
    return count;
  }



  inline size_t used()
  {
    size_t total = 0;
    for (auto & section : *this) {
      total += section.section_size;
    }
    return total;
  }


  inline size_t available()
  {
    return buffer_size - used();
  }


  inline std::tuple<bool, size_t>
  can_add_section(vessel_section_t const & section,
      size_t min_data_size, size_t desired_data_size)
  {
    auto [min_sec_size, min_fixed] = section_size(section_sizes, algo_opts, section,
        min_data_size);
    auto [desired_sec_size, desired_fixed] = section_size(section_sizes, algo_opts, section,
        desired_data_size);
    if (min_sec_size < 0 || desired_sec_size < 0) {
      LIBLOG_ERROR("Could not calculate section size.");
      return {false, 0};
    }
    if (static_cast<size_t>(min_sec_size) > available()) {
      return {false, 0};
    }

    // If we can provide the desired size, that'd be great!
    if (static_cast<size_t>(desired_sec_size) <= available()) {
      return {true, desired_data_size};
    }

    // Ok, now we have to calculate the section size *backwards* from the
    // available size to report the maximum amount of data we can give. But
    // the header size depends on the section type and data size, so this
    // gets a little complicated.

    // If we were to request SEC_MAX_SINGLE_WORD that'd give us the largest
    // section header.
    auto [two_word_header, fixed] = section_size(section_sizes, algo_opts, section,
        SEC_MAX_SINGLE_WORD);
    two_word_header -= SEC_MAX_SINGLE_WORD;

    // If we were to request SEC_MAX_SINGLE_WORD minus that header size,
    // we would definitely get the smaller header.
    auto smaller = SEC_MAX_SINGLE_WORD - two_word_header;

    // So now, if the available buffer is larger than this smaller size
    // (to err on the size of caution), we can return the available buffer
    // minus the two word header.
    if (available() > static_cast<size_t>(smaller)) {
      return {true, available() - two_word_header};
    }

    // Otherwise, we can grant two more bytes due to the smaller header
    return {true, available() - two_word_header + 2};
  }


  inline std::tuple<bool, section>
  add_section(vessel_section_t const & section,
      vessel_topic_t const & topic,
      size_t data_size = 0,
      char const * data = nullptr)
  {
    vessel::section sec;
    sec.type = section;
    sec.topic = topic;
    sec.data = const_cast<char *>(data);
    sec.data_size = data_size;

    size_t offset = used();

    auto res = serialization::serialize_section(
        section_sizes, algo_opts,
        buffer + offset, buffer_size - offset,
        sec, data ? false : true);
    if (!res) {
      LIBLOG_ERROR("Not enough buffer or some other error.");
      return {false, {}};
    }

    // Deserialize the section we just wrote to get the real data pointer.
    res = serialization::deserialize_section(
        section_sizes, algo_opts,
        sec,
        buffer + offset, buffer_size - offset);
    if (!res) {
      LIBLOG_ERROR("Deserialization error");
      return {false, {}};
    }

    return {true, sec};
  }


  inline bool validate(resource const & res, extent const & ext,
      vessel_section_t type, integrity_validation_callback func) const
  {
    if (!type) {
      LIBLOG_DEBUG("No validation section expected; automatic success!");
      return true;
    }

    // If there is a MAC, signature or CRC32 section, we should fill it in.
    // However, since we know the spec says they have to be in the beginning of
    // the payload, we'll just try the start.
    section sec{};
    auto used = serialization::deserialize_section(
          section_sizes, algo_opts,
          sec,
          buffer, buffer_size);
    if (!used) {
      LIBLOG_DEBUG("No sections found, aborting.");
      return false;
    }

    if (type != sec.type) {
      LIBLOG_DEBUG("Payload has a mismatching integrity section");
      return false;
    }

    switch (type) {
      case VESSEL_SECTION_CRC32:
      case VESSEL_SECTION_MAC:
      case VESSEL_SECTION_SIGNATURE:
        return func(res, ext, sec, *this);

      default:
        LIBLOG_ERROR("Unsupported validation section type: " << type);
        break;
    }

    // Default to failure
    return false;
  }


  inline bool finalize(resource const & res, extent const & ext,
      integrity_creation_callback func)
  {
    // The first thing we have to do is add padding to the unused part of the
    // payload.
    auto offset = used();
    auto padding_size = buffer_size - offset;
    uint8_t padding_value = padding_size % 256;
    std::memset(buffer + offset, padding_value, padding_size);

    if (!func) {
      LIBLOG_DEBUG("No integrity function; automatic success.");
      return true;
    }

    // If there is a MAC, signature or CRC32 section, we should fill it in.
    // However, since we know the spec says they have to be in the beginning of
    // the payload, we'll just try the start.
    section sec{};
    auto used = serialization::deserialize_section(
          section_sizes, algo_opts,
          sec,
          buffer, buffer_size);
    if (!used) {
      LIBLOG_DEBUG("No sections found, aborting.");
      return true;
    }

    switch (sec.type) {
      case VESSEL_SECTION_CRC32:
      case VESSEL_SECTION_MAC:
      case VESSEL_SECTION_SIGNATURE:
        if (!func(res, ext, sec, *this)) {
          LIBLOG_DEBUG("Error writing integrity information.");
          return false;
        }
        return true;

      default:
        LIBLOG_DEBUG("Unknown validation section: " << sec.type);
        break;
    }

    // Default to failure
    return false;
  }


  inline void dump(std::ostream & os) const
  {
    liberate::string::canonical_hexdump hd;
    os << hd(buffer, buffer_size) << std::endl;
  }
};

} // namespace vessel

#endif // guard
