/**
 * This file is part of vessel.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt).
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#include <build-config.h>

#include <vector>
#include <cstring>

#include <vessel/extent.h>
#include <vessel/author.h>

#include <s3kr1t/digests.h>

#include <liberate/sys/memory.h>

#include "extent.h"
#include "author.h"
#include "context.h"
#include "payload.h"

extern "C" {

/*****************************************************************************
 * extent_id
 */

VESSEL_API vessel_error_t
vessel_extent_id_new(struct vessel_extent_id ** extent_id)
{
  if (!extent_id) {
    return VESSEL_ERR_INVALID_VALUE;
  }

  if (*extent_id) {
    auto err = vessel_extent_id_free(extent_id);
    if (VESSEL_ERR_SUCCESS != err) {
      return err;
    }
  }

  *extent_id = new vessel_extent_id{};
  return VESSEL_ERR_SUCCESS;
}



VESSEL_API vessel_error_t
vessel_extent_id_free(struct vessel_extent_id ** extent_id)
{
  if (!extent_id) {
    return VESSEL_ERR_INVALID_VALUE;
  }

  if (*extent_id) {
    delete *extent_id;
    *extent_id = nullptr;
  }

  return VESSEL_ERR_SUCCESS;
}



VESSEL_API size_t
vessel_extent_id_size(struct vessel_algorithm_choices const * choices)
{
  if (!choices) {
    LIBLOG_ERROR("Bad algorithm choices.");
    return 0;
  }

  auto opts = vessel::options_from_choices(*choices);
  return opts.extent_identifier_size;
}



VESSEL_API vessel_error_t
vessel_extent_id_create_from_buffer(struct vessel_extent_id ** extent_id,
    struct vessel_algorithm_choices const * choices,
    void const * buffer, size_t bufsize)
{
  auto err = vessel_extent_id_new(extent_id);
  if (VESSEL_ERR_SUCCESS != err) {
    return err;
  }
  return vessel_extent_id_from_buffer(*extent_id, choices, buffer, bufsize);
}





VESSEL_API vessel_error_t
vessel_extent_id_from_buffer(struct vessel_extent_id * extent_id,
    struct vessel_algorithm_choices const * choices,
    void const * buffer, size_t bufsize)
{
  if (!extent_id || !buffer || !choices) {
    LIBLOG_DEBUG("Don't know what to do with NULL pointers.");
    return VESSEL_ERR_INVALID_VALUE;
  }

  auto opts = vessel::options_from_choices(*choices);

  if (bufsize < opts.extent_identifier_size) {
    LIBLOG_DEBUG("Input buffer too small. Got " << bufsize << " but need "
        << opts.extent_identifier_size);
    return VESSEL_ERR_INVALID_VALUE;
  }

  extent_id->extent_id.resize(opts.extent_identifier_size);

  std::memcpy(&(extent_id->extent_id[0]), buffer, bufsize);

  return VESSEL_ERR_SUCCESS;
}



VESSEL_API vessel_error_t
vessel_extent_id_to_buffer(void * buffer, size_t * bufsize,
    struct vessel_algorithm_choices const * choices,
    struct vessel_extent_id const * extent_id)
{
  if (!extent_id || !buffer || !bufsize) {
    LIBLOG_DEBUG("Don't know what to do with NULL pointers.");
    return VESSEL_ERR_INVALID_VALUE;
  }
  auto size = vessel_extent_id_size(choices);
  if (!extent_id->extent_id.empty()) {
    if (size != extent_id->extent_id.size()) {
      LIBLOG_DEBUG("The extent ID data is not the same size as expected!");
      return VESSEL_ERR_INVALID_VALUE;
    }
  }
  if (*bufsize < size) {
    LIBLOG_DEBUG("Input buffer too small. Got " << bufsize << " but need "
        << vessel_extent_id_size(choices));
    *bufsize = size;
    return VESSEL_ERR_OUT_OF_MEMORY;
  }

  // Copy, or as a fallback set to zero of the desired size.
  if (extent_id->extent_id.empty()) {
    liberate::sys::secure_memzero(buffer, size);
  }
  else {
    std::memcpy(buffer, extent_id->extent_id.data(), size);
  }
  *bufsize = size;

  return VESSEL_ERR_SUCCESS;
}



VESSEL_API vessel_error_t
vessel_extent_id_compare(int * result,
    struct vessel_extent_id const * extent_id1,
    struct vessel_extent_id const * extent_id2)
{
  if (!result || !extent_id1 || !extent_id2) {
    LIBLOG_DEBUG("Some parameters were NULL, can't proceed.");
    return VESSEL_ERR_INVALID_VALUE;
  }

  if (extent_id1->extent_id.size() < extent_id2->extent_id.size()) {
    *result = -1;
    return VESSEL_ERR_SUCCESS;
  }

  if (extent_id1->extent_id.size() > extent_id2->extent_id.size()) {
    *result = -1;
    return VESSEL_ERR_SUCCESS;
  }

  // Note: memcmp() is safe enough for extent IDs, I suppose, because they do
  // not contain sensitive data. But perhaps this needs a secure (constant time)
  // variant.
  *result = std::memcmp(extent_id1->extent_id.data(),
      extent_id2->extent_id.data(),
      extent_id1->extent_id.size());

  return VESSEL_ERR_SUCCESS;
}



VESSEL_API int
vessel_extent_id_cmp(
    struct vessel_extent_id const * extent_id1,
    struct vessel_extent_id const * extent_id2)
{
  int result = 0;
  auto err = vessel_extent_id_compare(&result, extent_id1, extent_id2);
  if (VESSEL_ERR_SUCCESS != err) {
    LIBLOG_DEBUG("Comparison failed: " << vessel_error_name(err) << " // "
        << vessel_error_message(err));
    result = INT_MAX;
  }
  return result;
}



VESSEL_API vessel_error_t
vessel_extent_id_generate(struct vessel_extent_id ** extent_id,
    struct vessel_algorithm_choices const * choices,
    struct vessel_author_id const * author_id,
    struct vessel_extent_id const * previous_extent_id)
{
  if (!extent_id || !author_id || !choices) {
    LIBLOG_DEBUG("Mandatory parameters missing.");
    return VESSEL_ERR_INVALID_VALUE;
  }
  if (!*extent_id) {
    auto err = vessel_extent_id_new(extent_id);
    if (VESSEL_ERR_SUCCESS != err) {
      return err;
    }
  }

  auto opts = vessel::options_from_choices(*choices);

  vessel_error_t err = VESSEL_ERR_SUCCESS;
  if (previous_extent_id) {
    err = vessel::extent_id_create_derived(
        opts,
        (*extent_id)->extent_id,
        author_id->author_id, previous_extent_id->extent_id);
  }
  else {
    err = vessel::extent_id_create_new(opts, (*extent_id)->extent_id);
  }

  if (VESSEL_ERR_SUCCESS != err) {
    vessel_extent_id_free(extent_id);
  }
  return err;
}



VESSEL_API vessel_error_t
vessel_extent_id_verify(struct vessel_extent_id const * extent_id,
    struct vessel_algorithm_choices const * choices,
    struct vessel_author_id const * author_id,
    struct vessel_extent_id const * previous_extent_id)
{
  if (!extent_id || !author_id || !choices) {
    LIBLOG_DEBUG("Mandatory parameters missing.");
    return VESSEL_ERR_INVALID_VALUE;
  }

  if (!previous_extent_id) {
    // Due to the random root ID generation process, we can't
    // verify anything here - this is by design!
    return VESSEL_ERR_SUCCESS;
  }

  auto opts = vessel::options_from_choices(*choices);

  // Create a combined hash
  vessel::byte_sequence generated_id;
  // FIXME

  auto err = vessel::create_combined_hash(opts,
      generated_id,
      &(previous_extent_id->extent_id),
      author_id->author_id);
  if (VESSEL_ERR_SUCCESS != err) {
    return err;
  }

  if (extent_id->extent_id != generated_id) {
    LIBLOG_ERROR("Could not verify extent ID.");
    return VESSEL_ERR_VERIFICATION;
  }
  return VESSEL_ERR_SUCCESS;
}


/*****************************************************************************
 * extent
 */
VESSEL_API vessel_error_t
vessel_extent_identifier(struct vessel_extent_id ** id,
    struct vessel_extent const * extent)
{
  if (!id || !extent || !extent->ext) {
    return VESSEL_ERR_INVALID_VALUE;
  }

  if (!*id) {
    *id = new vessel_extent_id{};
  }

  (*id)->extent_id = extent->ext->header.current_extent_identifier;
  return VESSEL_ERR_SUCCESS;
}



VESSEL_API vessel_error_t
vessel_extent_previous_identifier(struct vessel_extent_id ** id,
    struct vessel_extent const * extent)
{
  if (!id || !extent || !extent->ext) {
    return VESSEL_ERR_INVALID_VALUE;
  }

  if (!*id) {
    *id = new vessel_extent_id{};
  }

  (*id)->extent_id = extent->ext->header.previous_extent_identifier;
  return VESSEL_ERR_SUCCESS;
}



VESSEL_API vessel_error_t
vessel_extent_author_identifier(struct vessel_author_id ** id,
    struct vessel_extent const * extent)
{
  if (!id || !extent || !extent->ext) {
    return VESSEL_ERR_INVALID_VALUE;
  }

  if (!*id) {
    *id = new vessel_author_id{};
  }

  (*id)->author_id = extent->ext->header.author_identifier;
  return VESSEL_ERR_SUCCESS;
}



VESSEL_API size_t
vessel_extent_default_size_in_blocks()
{
  return VESSEL_DEFAULT_EXTENT_SIZE_MULTIPLIER;
}


VESSEL_API size_t
vessel_extent_block_size()
{
  return vessel::VESSEL_NOMINAL_BLOCK_SIZE;
}


VESSEL_API vessel_error_t
vessel_extent_size_in_blocks(size_t * size,
    struct vessel_extent const * extent)
{
  if (!size || !extent || !extent->ext) {
    return VESSEL_ERR_INVALID_VALUE;
  }

  *size = extent->ext->block_size();
  return VESSEL_ERR_SUCCESS;
}



VESSEL_API vessel_error_t
vessel_extent_payload_size(size_t * size,
    struct vessel_extent const * extent)
{
  if (!size || !extent || !extent->ext) {
    return VESSEL_ERR_INVALID_VALUE;
  }

  *size = extent->ext->payload_size();
  return VESSEL_ERR_SUCCESS;
}



VESSEL_API vessel_error_t
vessel_extent_payload(void ** payload,
    struct vessel_extent const * extent)
{
  if (!payload || !extent || !extent->ext) {
    return VESSEL_ERR_INVALID_VALUE;
  }

  *payload = extent->ext->payload();
  return VESSEL_ERR_SUCCESS;
}


VESSEL_API vessel_error_t
vessel_extent_version_tag(void * version_tag, size_t tag_size,
    struct vessel_extent const * extent)
{
  if (!version_tag || !extent || !extent->ext) {
    return VESSEL_ERR_INVALID_VALUE;
  }

  if (tag_size < sizeof(extent->ext->envelope.version_tag)) {
    return VESSEL_ERR_OUT_OF_MEMORY;
  }

  *static_cast<decltype(extent->ext->envelope.version_tag) *>(version_tag) = extent->ext->envelope.version_tag;
  return VESSEL_ERR_SUCCESS;
}


VESSEL_API vessel_error_t
vessel_extent_algorithm_choices(struct vessel_algorithm_choices * choices,
    struct vessel_algorithm_context const * ctx,
    struct vessel_extent const * extent)
{
  if (!choices || !ctx || !extent || !extent->ext) {
    return VESSEL_ERR_INVALID_VALUE;
  }

  auto info_iter = ctx->algorithms.find(extent->ext->envelope.version_tag);
  if (info_iter == ctx->algorithms.end()) {
    LIBLOG_ERROR("Version tag not known: 0x" << std::hex
        << extent->ext->envelope.version_tag << std::dec);
    return VESSEL_ERR_INVALID_VALUE;
  }

  *choices = info_iter->second.choices;

  return VESSEL_ERR_SUCCESS;
}



VESSEL_API vessel_error_t
vessel_extent_payload_unused_offset(void ** offset,
    size_t * available,
    struct vessel_context const * ctx,
    struct vessel_extent const * extent)
{
  if (!offset || !extent || !extent->ext) {
    return VESSEL_ERR_INVALID_VALUE;
  }

  auto buffer = static_cast<char *>(extent->ext->payload());

  vessel::payload pl{ctx->sections->section_sizes, extent->ext->opts,
    buffer, extent->ext->payload_size()
  };

  // TODO add integrity section here or when finding extent?

  auto used = pl.used();
  *offset = buffer + used;
  *available = pl.buffer_size - used;
  return VESSEL_ERR_SUCCESS;
}



struct vessel_section_iterator
{
  std::shared_ptr<vessel::extent> extent;
  vessel::payload                 payload;
  vessel::payload::iterator       iter;

  inline vessel_section_iterator(
      struct vessel_section_context * sec_ctx,
      struct vessel_extent * _extent)
    : extent{_extent->ext}
    , payload{
        sec_ctx->section_sizes, extent->opts,
        static_cast<char *>(extent->payload()),
        extent->payload_size()
      }
    , iter{payload.begin()}
  {
  }
};


VESSEL_API vessel_error_t
vessel_section_iterator_create(struct vessel_section_iterator ** iterator,
    struct vessel_section_context * ctx,
    struct vessel_extent * extent)
{
  if (!iterator || !ctx || !extent || !extent->ext) {
    return VESSEL_ERR_INVALID_VALUE;
  }

  if (*iterator) {
    auto err = vessel_section_iterator_free(iterator);
    if (VESSEL_ERR_SUCCESS != err) {
      return err;
    }
  }

  *iterator = new vessel_section_iterator{ctx, extent};

  return VESSEL_ERR_SUCCESS;
}



VESSEL_API vessel_error_t
vessel_section_iterator_free(struct vessel_section_iterator ** iterator)
{
  if (!iterator) {
    return VESSEL_ERR_INVALID_VALUE;
  }

  if (*iterator) {
    delete *iterator;
    *iterator = nullptr;
  }

  return VESSEL_ERR_SUCCESS;
}



VESSEL_API vessel_error_t
vessel_section_iterator_next(struct vessel_section * section,
    struct vessel_section_iterator * iterator)
{
  if (!section || !iterator || !iterator->extent) {
    return VESSEL_ERR_INVALID_VALUE;
  }

  if (iterator->iter == iterator->payload.end()) {
    section->type = 0;
    section->topic = 0;
    section->section_size = 0;
    section->payload_size = 0;
    section->payload = nullptr;
    return VESSEL_ERR_END_ITERATION;
  }

  auto sec = *(iterator->iter);

  section->type = sec.type;
  section->topic = sec.topic;
  section->section_size = sec.section_size;
  section->payload_size = sec.data_size;
  section->payload = sec.data;

  // After the copying value, increment.
  ++(iterator->iter);

  return VESSEL_ERR_SUCCESS;
}


} // extern "C"
