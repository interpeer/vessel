/**
 * This file is part of vessel.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt).
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#ifndef VESSEL_SERIALIZATION_H
#define VESSEL_SERIALIZATION_H

#include <build-config.h>

#include <cstring>

#include <liberate/types/byte.h>
#include <liberate/logging.h>
#include <liberate/serialization/integer.h>

#include <vessel/section.h>
#include <vessel/author.h>

#include "algorithms.h"
#include "section.h"


/**
 * This file defines serialization and deserialization primitives for
 * extents and sections as described by the specs. As these primitives
 * translate from byte arrays to data structures and vice versa, the
 * data structures themselves are also defined here.
 */

namespace vessel {

static constexpr std::size_t VESSEL_NOMINAL_BLOCK_SIZE = 4096;

/**
 * Extents consist of an envelope, a header, a payload section, and a footer at
 * the end. The envelope is versioned, and its version determines the general
 * layout of the other sections.
 */
enum ev_version : char
{
  EV_DRAFT_00     = '\x00', // Version -00 of draft on https://specs.interpeer.io
  EV_DEFAULT      = EV_DRAFT_00, // Latest supported
};


/**
 * The envelope consists of a magic three octet sequence that is always the
 * same, so does not require an entry in a data structure. The version tag
 * is in version EV_DRAFT_00 a four octet byte string, which therefore fits into
 * a 32 bit integer value for easier processing.
 */
using version_tag_00_type = uint32_t;

struct extent_envelope_00
{
  ev_version          envelope_version;
  version_tag_00_type version_tag;
};


/**
 * The header contains the following fields; the size of each field depends
 * on the value of the version tag.
 */
struct extent_header_00
{
  uint16_t                  extent_size_multiplier;
  byte_sequence             current_extent_identifier;
  vessel_authoring_counter  authoring_counter;
  byte_sequence             previous_extent_identifier;
  byte_sequence             author_identifier;

  inline std::size_t extent_size() const
  {
    return extent_size_multiplier * VESSEL_NOMINAL_BLOCK_SIZE;
  }
};


/**
 * The footer is particularly simple.
 */
struct extent_footer_00
{
  byte_sequence signature;
};


/**
 * Sections can either be fixed length or variable length. Applications must
 * define the size of each section type they support, or deserialization will
 * fail. The section and data sizes are computed when deserializing a section.
 * When serializing a section, the data_size *must* be set appropriately.
 */
struct section
{
  vessel_section_t    type = 0;
  vessel_topic_t      topic = 0;
  std::size_t         section_size = 0;

  void *              data = nullptr;
  std::size_t         data_size = 0;
};



namespace serialization {

/**
 * serialize_* functions have the same general prototype:
 *  - A void * buffer to serialize into
 *  - A size_t that tells the size of the buffer to the function.
 *  - An element to seralize, as a const &
 *  - Return a size_t with the bytes consumed; this is 0 on errors.
 *
 * deserialize_* functions also have the same general prototype:
 *  - An element to deseralize into, as a &
 *  - A void const * buffer to deserialize from
 *  - A size_t that tells the size of this buffer to the function
 *  - Return a size_t with the bytes consumed; this is 0 on errors.
 *
 * Depending on whether all sizes are fixed, a first argument may provide
 * algorithm options. This does not apply to all serialization functions.
 */

/**
 * Serialize and deserialize envelopes. The first four bytes are always fixed as
 * per the spec, which means there are functions for the envelope prefix, and a
 * combined function for the complete envelope. This, of course, is always
 * specific to the version.
 */
inline std::size_t
serialize_envelope_prefix(void * buffer,
    std::size_t const & bufsize,
    ev_version const & version)
{
  if (!buffer || bufsize < 4) {
    LIBLOG_ERROR("Buffer too small, aborting.");
    return 0;
  }

  auto out = static_cast<liberate::types::byte *>(buffer);

  // EV Version
  switch (version) {
    case EV_DRAFT_00:
      out[3] = version;
      break;

    default:
      LIBLOG_ERROR("Unsupported version: 0x" << std::hex <<
          static_cast<int>(version) << std::dec);
      return 0;
  }

  // "XEV"
  out[0] = 'X';
  out[1] = 'E';
  out[2] = 'V';

  return 4;
}



inline std::size_t
deserialize_envelope_prefix(ev_version & version,
    void const * buffer,
    std::size_t const & bufsize)
{
  if (!buffer || bufsize < 4) {
    LIBLOG_ERROR("Buffer too small, aborting.");
    return 0;
  }

  auto in = static_cast<liberate::types::byte const *>(buffer);

  // XEV
  if (in[0] != 'X' || in[1] != 'E' || in[2] != 'V') {
    LIBLOG_ERROR("Bad magic sequence, aborting.");
    return 0;
  }

  // EV Version
  switch (in[3]) {
    case EV_DRAFT_00:
      version = static_cast<ev_version>(in[3]);
      break;

    default:
      LIBLOG_ERROR("Unsupported version: 0x" << std::hex <<
          static_cast<int>(in[3]) << std::dec);
      return 0;
  }

  return 4;
}



inline std::size_t
serialize_envelope_00(void * buffer,
    std::size_t const & bufsize,
    extent_envelope_00 const & envelope)
{
  if (!buffer || bufsize < 8) {
    LIBLOG_ERROR("Buffer too small, aborting.");
    return 0;
  }

  auto out = static_cast<liberate::types::byte *>(buffer);
  auto remaining = bufsize;

  // Prefix
  auto used = serialize_envelope_prefix(out, remaining,
      envelope.envelope_version);
  if (!used) {
    return 0;
  }
  out += used;
  remaining -= used;

  // Version tag
  used = liberate::serialization::serialize_int(out, remaining,
      envelope.version_tag);
  if (used != sizeof(envelope.version_tag)) {
    return 0;
  }

  return 4 + sizeof(envelope.version_tag);
}



inline std::size_t
deserialize_envelope_00(extent_envelope_00 & envelope,
    void const * buffer,
    std::size_t const & bufsize)
{
  if (!buffer || bufsize < 8) {
    LIBLOG_ERROR("Buffer too small, aborting.");
    return 0;
  }

  auto in = static_cast<liberate::types::byte const *>(buffer);
  auto remaining = bufsize;

  // Prefix
  auto used = deserialize_envelope_prefix(envelope.envelope_version,
      in, remaining);
  if (!used) {
    return 0;
  }
  in += used;
  remaining -= used;

  // Version tag
  used = liberate::serialization::deserialize_int(envelope.version_tag,
      in, remaining);
  if (used != sizeof(envelope.version_tag)) {
    return 0;
  }

  return 4 + sizeof(envelope.version_tag);
}


/**
 * Serialize and deserialize extent headers.
 */
inline std::size_t
serialize_header_00(algorithm_options const & algos,
    void * buffer, std::size_t const & bufsize,
    extent_header_00 const & header)
{
  if (!buffer || bufsize < 8) {
    LIBLOG_ERROR("Buffer too small, aborting.");
    return 0;
  }

  auto out = static_cast<liberate::types::byte *>(buffer);
  auto remaining = bufsize;

  // Extent size. This one is easy, because it's just a uint16_t.
  auto used = liberate::serialization::serialize_int(out, remaining,
      header.extent_size_multiplier);
  if (used != sizeof(header.extent_size_multiplier)) {
    LIBLOG_ERROR("Could not encode extent size multiplier, aborting.");
    return 0;
  }
  out += used;
  remaining -= used;

  // Current extent identifier. It makes sense to check that it's of the
  // expected size here.
  if (header.current_extent_identifier.size() != algos.extent_identifier_size) {
    LIBLOG_ERROR("Current extent identifier is not of expected size "
        << algos.extent_identifier_size << " - got: "
        << header.current_extent_identifier.size());
    return 0;
  }
  if (remaining < header.current_extent_identifier.size()) {
    LIBLOG_ERROR("Not enough space for current extent identifier, aborting.");
    return 0;
  }
  std::memcpy(out, &header.current_extent_identifier[0],
      header.current_extent_identifier.size());
  out += header.current_extent_identifier.size();
  remaining -= header.current_extent_identifier.size();

  // The authoring counter represents a small issue; it's a uint32_t in the
  // struct, but we want to encode it as 24 bits. So we'll have to serialize
  // it into a temporary buffer and copy that over.
  liberate::types::byte tmp[4];
  if (remaining < 3) {
    LIBLOG_ERROR("Not enough space for authoring counter, aborting");
    return 0;
  }
  auto tmp_used = liberate::serialization::serialize_int(tmp, sizeof(tmp),
      header.authoring_counter);
  if (tmp_used != sizeof(tmp)) {
    LIBLOG_ERROR("Error encoding authoring counter, aborting.");
    return 0;
  }
  std::memcpy(out, tmp + 1, 3);
  out += 3;
  remaining -= 3;

  // Previous extent identifier
  if (header.previous_extent_identifier.size() != algos.extent_identifier_size) {
    LIBLOG_ERROR("Previous extent identifier is not of expected size "
        << algos.extent_identifier_size << " - got: "
        << header.previous_extent_identifier.size());
    return 0;
  }
  if (remaining < header.previous_extent_identifier.size()) {
    LIBLOG_ERROR("Not enough space for previous extent identifier, aborting.");
    return 0;
  }
  std::memcpy(out, &header.previous_extent_identifier[0],
      header.previous_extent_identifier.size());
  out += header.previous_extent_identifier.size();
  remaining -= header.previous_extent_identifier.size();

  // Author identifier
  if (header.author_identifier.size() != algos.author_identifier_size) {
    LIBLOG_ERROR("Author identifier is not of expected size "
        << algos.author_identifier_size << " - got: "
        << header.author_identifier.size());
    return 0;
  }
  if (remaining < header.author_identifier.size()) {
    LIBLOG_ERROR("Not enough space for author identifier, aborting.");
    return 0;
  }
  std::memcpy(out, &header.author_identifier[0],
      header.author_identifier.size());
  out += header.author_identifier.size();
  remaining -= header.author_identifier.size();

  return out - static_cast<liberate::types::byte *>(buffer);
}



inline std::size_t
deserialize_header_00(algorithm_options const & algos,
    extent_header_00 & header,
    void const * buffer, std::size_t const & bufsize)
{
  if (!buffer || bufsize < 8) {
    LIBLOG_ERROR("Buffer too small, aborting.");
    return 0;
  }

  auto in = static_cast<liberate::types::byte const *>(buffer);
  auto remaining = bufsize;

  // Extent size. This one is easy, because it's just a uint16_t.
  auto used = liberate::serialization::deserialize_int(
      header.extent_size_multiplier, in, remaining);
  if (used != sizeof(header.extent_size_multiplier)) {
    LIBLOG_ERROR("Could not decode extent size multiplier, aborting.");
    return 0;
  }
  in += used;
  remaining -= used;

  if (!header.extent_size_multiplier) {
    LIBLOG_ERROR("Must have a positive size multiplier!");
    return 0;
  }

  // Current extent identifier. It makes sense to check that it's of the
  // expected size here.
  header.current_extent_identifier.resize(algos.extent_identifier_size);
  if (remaining < header.current_extent_identifier.size()) {
    LIBLOG_ERROR("Not enough space for current extent identifier, aborting.");
    return 0;
  }
  std::memcpy(&header.current_extent_identifier[0], in,
      header.current_extent_identifier.size());
  in += header.current_extent_identifier.size();
  remaining -= header.current_extent_identifier.size();

  // The authoring counter represents a small issue; it's a uint32_t in the
  // struct, but we want to encode it as 24 bits. We'll deserialize too much,
  // and mask the byte we don't want.
  if (remaining < 3) {
    LIBLOG_ERROR("Not enough space for authoring counter, aborting");
    return 0;
  }
  auto tmp_used = liberate::serialization::deserialize_int(
      header.authoring_counter, in - 1, remaining + 1);
  if (tmp_used != sizeof(header.authoring_counter)) {
    LIBLOG_ERROR("Error encoding authoring counter, aborting.");
    return 0;
  }
  header.authoring_counter &= 0x00ffffff;
  in += 3;
  remaining -= 3;

  // Previous extent identifier.
  header.previous_extent_identifier.resize(algos.extent_identifier_size);
  if (remaining < header.previous_extent_identifier.size()) {
    LIBLOG_ERROR("Not enough space for previous extent identifier, aborting.");
    return 0;
  }
  std::memcpy(&header.previous_extent_identifier[0], in,
      header.previous_extent_identifier.size());
  in += header.previous_extent_identifier.size();
  remaining -= header.previous_extent_identifier.size();

  // Author identifier.
  header.author_identifier.resize(algos.author_identifier_size);
  if (remaining < header.author_identifier.size()) {
    LIBLOG_ERROR("Not enough space for author identifier, aborting.");
    return 0;
  }
  std::memcpy(&header.author_identifier[0], in,
      header.author_identifier.size());
  in += header.author_identifier.size();
  remaining -= header.author_identifier.size();

  return in - static_cast<liberate::types::byte const *>(buffer);
}


/**
 * Serialize and deserialize extent footers.
 */
inline std::size_t
serialize_footer_00(algorithm_options const & algos,
    void * buffer, std::size_t const & bufsize,
    extent_footer_00 const & footer)
{
  if (!buffer || bufsize < 8) {
    LIBLOG_ERROR("Buffer too small, aborting.");
    return 0;
  }

  auto out = static_cast<liberate::types::byte *>(buffer);
  auto remaining = bufsize;

  // Signature. Check if it's of the expected size.
  if (footer.signature.size() != algos.signature_size) {
    LIBLOG_ERROR("Signature is not of expected size "
        << algos.signature_size << " - got: "
        << footer.signature.size());
    return 0;
  }
  if (remaining < footer.signature.size()) {
    LIBLOG_ERROR("Not enough space for signature, aborting.");
    return 0;
  }
  std::memcpy(out, &footer.signature[0],
      footer.signature.size());
  out += footer.signature.size();
  remaining -= footer.signature.size();

  return out - static_cast<liberate::types::byte *>(buffer);
}



inline std::size_t
deserialize_footer_00(algorithm_options const & algos,
    extent_footer_00 & footer,
    void const * buffer, std::size_t const & bufsize)
{
  if (!buffer || bufsize < 8) {
    LIBLOG_ERROR("Buffer too small, aborting.");
    return 0;
  }

  auto in = static_cast<liberate::types::byte const *>(buffer);
  auto remaining = bufsize;

  // Signature.
  footer.signature.resize(algos.signature_size);
  if (remaining < footer.signature.size()) {
    LIBLOG_ERROR("Not enough space for signature, aborting.");
    return 0;
  }
  std::memcpy(&footer.signature[0], in,
      footer.signature.size());
  in += footer.signature.size();
  remaining -= footer.signature.size();

  return in - static_cast<liberate::types::byte const *>(buffer);
}


/**
 * Serialize and deserialize sections; this requires passing some extra data
 * from the calling context.
 */
inline std::size_t
serialize_section(
    vessel_section_context::section_size_map const & section_sizes,
    algorithm_options const & algos,
    void * buffer, std::size_t const & bufsize,
    section const & section, bool no_data = false)
{
  if (!buffer || bufsize < 6) {
    LIBLOG_ERROR("Buffer too small, aborting.");
    return 0;
  }

  // Determine section size. This may yield 0 to indicate a variable size
  // section, in which case the section size must be determined from the data
  // size of the passed section.
  auto [sec_size, fixed] = section_size(section_sizes, algos, section.type,
      section.data_size);
  if (sec_size < 0) {
    LIBLOG_ERROR("Error determining section size, aborting.");
    return 0;
  }

  if (bufsize < static_cast<std::size_t>(sec_size)) {
    LIBLOG_ERROR("Buffer too small, aborting. Need " << sec_size << " but only "
        "have " << bufsize);
    return 0;
  }

  if (section.data_size && !section.data && !no_data) {
    LIBLOG_ERROR("Require " << section.data_size << " bytes of data, but got nothing.");
    return 0;
  }

  auto out = static_cast<liberate::types::byte *>(buffer);
  auto remaining = bufsize;

  // Encode type
  uint16_t tmp = section.type;
  auto used = liberate::serialization::serialize_int(out, remaining, tmp);
  if (used != sizeof(tmp)) {
    LIBLOG_ERROR("Could not encode section type, aborting.");
    return 0;
  }
  out += used;
  remaining -= used;

  // Encode topic
  tmp = section.topic;
  used = liberate::serialization::serialize_int(out, remaining, tmp);
  if (used != sizeof(tmp)) {
    LIBLOG_ERROR("Could not encode section topic, aborting.");
    return 0;
  }
  out += used;
  remaining -= used;

  // Size
  if (!fixed) {
    uint32_t tmp_size = sec_size;
    uint16_t first_word = (tmp_size >> 16);
    uint16_t second_word = tmp_size & 0xffff;

    // First word, optionally
    if (first_word) {
      // If the first word has any value, we'll set the MSB
      tmp = first_word | SEC_DOUBLE_WORD_BIT;
      used = liberate::serialization::serialize_int(out, remaining, tmp);
      if (used != sizeof(tmp)) {
        LIBLOG_ERROR("Could not encode section size, aborting.");
        return 0;
      }
      out += used;
      remaining -= used;
    }

    // Second word
    tmp = second_word;
    used = liberate::serialization::serialize_int(out, remaining, tmp);
    if (used != sizeof(tmp)) {
      LIBLOG_ERROR("Could not encode section size, aborting.");
      return 0;
    }
    out += used;
    remaining -= used;
  }

  // Section data
  if (remaining < section.data_size) {
    // This check should be superflous; see the sec_size check above.
    LIBLOG_ERROR("Not enough buffer for section data, aborting.");
    return 0;
  }
  if (!no_data) {
    std::memcpy(out, section.data, section.data_size);
  }
  out += section.data_size;
  remaining -= section.data_size;

  return out - static_cast<liberate::types::byte *>(buffer);
}



inline std::size_t
deserialize_section(
    vessel_section_context::section_size_map const & section_sizes,
    algorithm_options const & algos,
    section & section,
    void * buffer, std::size_t const & bufsize)
{
  if (!buffer || bufsize < 6) {
    // XXX This is technically an error, but occurs often, so best to keep
    //     it silent.
    // LIBLOG_ERROR("Buffer too small, aborting.");
    return 0;
  }

  auto in = static_cast<liberate::types::byte *>(buffer);
  auto remaining = bufsize;

  // Decode type
  uint16_t tmp;
  auto used = liberate::serialization::deserialize_int(tmp, in, remaining);
  if (used != sizeof(tmp)) {
    LIBLOG_ERROR("Could not decode section type, aborting.");
    return 0;
  }
  section.type = static_cast<vessel_section_t>(tmp);
  in += used;
  remaining -= used;

  // Decode topic - this is always safe.
  used = liberate::serialization::deserialize_int(tmp, in, remaining);
  if (used != sizeof(tmp)) {
    LIBLOG_ERROR("Could not decode section topic, aborting.");
    return 0;
  }
  section.topic = static_cast<vessel_topic_t>(tmp);
  in += used;
  remaining -= used;

  // Determine section size.
  auto [sec_size, fixed] = section_size(section_sizes, algos, section.type, 0);
  if (sec_size < 0) {
    LIBLOG_ERROR("Error determining section size, aborting.");
    return 0;
  }

  if (fixed) {
    // Fixed size - cannot be 0
    if (!sec_size) {
    LIBLOG_ERROR("Invalid fixed section size of 0, aborting.");
    return 0;
    }

    if (bufsize < static_cast<std::size_t>(sec_size)) {
      LIBLOG_ERROR("Buffer is " << bufsize << ", but section needs " << sec_size);
      return 0;
    }

    section.section_size = sec_size;
    section.data_size = sec_size - 4;
  }
  else {
    // Variable size - we'll have to determine the data size from the section
    // size we're reading.
    uint32_t tmp_size = 0;

    // First word
    used = liberate::serialization::deserialize_int(tmp, in, remaining);
    if (used != sizeof(tmp)) {
      LIBLOG_ERROR("Could not decode section size, aborting.");
      return 0;
    }
    tmp_size = static_cast<uint32_t>(tmp & SEC_MAX_SINGLE_WORD);
    in += used;
    remaining -= used;

    // Second word, if necessary
    if (tmp & SEC_DOUBLE_WORD_BIT) {
      tmp_size <<= 16;
      used = liberate::serialization::deserialize_int(tmp, in, remaining);
      if (used != sizeof(tmp)) {
        LIBLOG_ERROR("Could not decode section size, aborting.");
        return 0;
      }
      tmp_size += tmp;
      in += used;
      remaining -= used;
    }

    if (bufsize < tmp_size) {
      LIBLOG_ERROR("Buffer is " << bufsize << " bytes, but section size is "
          "encoded to be " << tmp_size);
      return 0;
    }

    section.section_size = tmp_size;
    section.data_size = tmp_size - (in - static_cast<liberate::types::byte *>(buffer));
  }

  // Data - we do NOT copy it, because vessel uses the extent's backing store
  // here.
  if (section.data_size) {
    section.data = in;
    in += section.data_size;
    remaining -= section.data_size;
  }
  else {
    section.data = nullptr;
  }

  return in - static_cast<liberate::types::byte *>(buffer);
}

} // namespace serialization


} // namespace vessel

#endif // guard
