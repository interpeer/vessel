/**
 * This file is part of vessel.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt).
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#include <build-config.h>

#include <vessel/algorithms.h>

#include <cstring>
#include <regex>
#include <vector>
#include <numeric>

#include <liberate/string/util.h>
#include <liberate/string/hexencode.h>
#include <liberate/logging.h>

#include "hash_translate.h"

#include "algorithms.h"

namespace vessel {
namespace {

// Maximum input length per specification we accept.
static constexpr size_t MAX_SPEC_LEN = 200;

static constexpr vessel_algorithm_choices EMPTY_ALGOS = {
  0x0,
  static_cast<vessel_version_tag_hash>(0),
  static_cast<vessel_extent_identifier_hash>(0),
  static_cast<vessel_author_identifier_hash>(0),
  static_cast<vessel_nonce_hash>(0),
  static_cast<vessel_signature_hash>(0),
  static_cast<vessel_asymmetric_signature>(0),
  static_cast<vessel_message_authentication>(0),
  static_cast<vessel_symmetric_encryption>(0),
  static_cast<vessel_signature_algorithm>(0),
  static_cast<vessel_private_header_flag>(0),
};

static constexpr std::size_t DRAFT_00_ALGORITHMS = 10;

#define _VESSEL_HASH_MAPPING_00(prefix, name) \
  if (name == "sha3-512") { \
    return VESSEL_ ## prefix ## _SHA3_512; \
  } \
  else if (name == "sha3-384") { \
    return VESSEL_ ## prefix ## _SHA3_384; \
  } \
  else if (name == "sha3-256") { \
    return VESSEL_ ## prefix ## _SHA3_256; \
  } \
  else if (name == "sha3-224") { \
    return VESSEL_ ## prefix ## _SHA3_224; \
  } \
  else if (name == "sha2-512") { \
    return VESSEL_ ## prefix ## _SHA2_512; \
  } \
  else if (name == "sha2-384") { \
    return VESSEL_ ## prefix ## _SHA2_384; \
  } \
  else if (name == "sha2-256") { \
    return VESSEL_ ## prefix ## _SHA2_256; \
  } \
  else if (name == "sha2-224") { \
    return VESSEL_ ## prefix ## _SHA2_224; \
  }

#define _VESSEL_HASH_MAPPING_INV_00(prefix, value) \
  if (value == VESSEL_ ## prefix ## _SHA3_512) { \
    return "sha3-512"; \
  } \
  else if (value == VESSEL_ ## prefix ## _SHA3_384) { \
    return "sha3-384"; \
  } \
  else if (value == VESSEL_ ## prefix ## _SHA3_256) { \
    return "sha3-256"; \
  } \
  else if (value == VESSEL_ ## prefix ## _SHA3_224) { \
    return "sha3-224"; \
  } \
  else if (value == VESSEL_ ## prefix ## _SHA2_512) { \
    return "sha2-512"; \
  } \
  else if (value == VESSEL_ ## prefix ## _SHA2_384) { \
    return "sha2-384"; \
  } \
  else if (value == VESSEL_ ## prefix ## _SHA2_256) { \
    return "sha2-256"; \
  } \
  else if (value == VESSEL_ ## prefix ## _SHA2_224) { \
    return "sha2-224"; \
  }


inline vessel_version_tag_hash
valid_version_tag_hash_algo_00(std::string const & name)
{
  _VESSEL_HASH_MAPPING_00(VTH, name);
  return VESSEL_VTH_UNSUPPORTED;
}



inline vessel_extent_identifier_hash
valid_extent_identifier_hash_algo_00(std::string const & name)
{
  _VESSEL_HASH_MAPPING_00(EIH, name);
  return VESSEL_EIH_UNSUPPORTED;
}



inline vessel_author_identifier_hash
valid_author_identifier_hash_algo_00(std::string const & name)
{
  _VESSEL_HASH_MAPPING_00(AIH, name);
  if (name == "none") {
    return VESSEL_AIH_NONE;
  }
  return VESSEL_AIH_UNSUPPORTED;
}



inline vessel_nonce_hash
valid_nonce_hash_algo_00(std::string const & name)
{
  _VESSEL_HASH_MAPPING_00(NH, name);
  return VESSEL_NH_UNSUPPORTED;
}



inline vessel_signature_hash
valid_signature_hash_algo_00(std::string const & name)
{
  _VESSEL_HASH_MAPPING_00(SH, name);
  if (name == "eddsa") {
    return VESSEL_SH_EDDSA;
  }
  return VESSEL_SH_UNSUPPORTED;
}



inline vessel_asymmetric_signature
valid_asymmetric_signature_algo_00(std::string const & name)
{
  if (name == "dsa") {
    return VESSEL_AS_DSA;
  }
  else if (name == "ed25519") {
    return VESSEL_AS_ED25519;
  }
  else if (name == "ed448") {
    return VESSEL_AS_ED448;
  }
  else if (name == "rsa") {
    return VESSEL_AS_RSA;
  }
  else if (name == "dsa") {
    return VESSEL_AS_DSA;
  }

  return VESSEL_AS_UNSUPPORTED;
}



inline vessel_message_authentication
valid_message_authentication_algo_00(std::string const & name)
{
  if (name == "kmac128") {
    return VESSEL_MA_KMAC128;
  }
  else if (name == "kmac256") {
    return VESSEL_MA_KMAC256;
  }
  else if (name == "poly1305") {
    return VESSEL_MA_POLY1305;
  }

  return VESSEL_MA_UNSUPPORTED;
}



inline vessel_symmetric_encryption
valid_symmetric_encryption_algo_00(std::string const & name)
{
  if (name == "aead_aes_128_gcm") {
    return VESSEL_SE_AEAD_AES_128_GCM;
  }
  else if (name == "aead_aes_256_gcm") {
    return VESSEL_SE_AEAD_AES_256_GCM;
  }
  else if (name == "aead_aes_128_ccm") {
    return VESSEL_SE_AEAD_AES_128_CCM;
  }
  else if (name == "aead_aes_256_ccm") {
    return VESSEL_SE_AEAD_AES_256_CCM;
  }
  else if (name == "chacha20") {
    return VESSEL_SE_CHACHA20;
  }
  else if (name == "none") {
    return VESSEL_SE_NONE;
  }

  return VESSEL_SE_UNSUPPORTED;
}



inline vessel_signature_algorithm
valid_signature_algorithm_algo_00(std::string const & name)
{
  if (name == "aead") {
    return VESSEL_SA_AEAD;
  }
  else if (name == "keypair") {
    return VESSEL_SA_KEYPAIR;
  }
  else if (name == "mac") {
    return VESSEL_SA_MAC;
  }

  return VESSEL_SA_UNSUPPORTED;
}



inline vessel_private_header_flag
valid_private_header_flag_algo_00(std::string const & name)
{
  if (name == "ph=0") {
    return VESSEL_PHF_PH_EQ_0;
  }
  else if (name == "ph=1") {
    return VESSEL_PHF_PH_EQ_1;
  }

  return VESSEL_PHF_UNSUPPORTED;
}


inline bool
validate_00(vessel_algorithm_choices const & choices)
{
  // Simple tests.
  if (choices.version_tag_hash == VESSEL_VTH_UNSUPPORTED) {
    LIBLOG_DEBUG("Unsupported version tag hash.");
    return false;
  }
  if (choices.extent_identifier_hash == VESSEL_EIH_UNSUPPORTED) {
    LIBLOG_DEBUG("Unsupported extent identifier hash.");
    return false;
  }
  if (choices.author_identifier_hash == VESSEL_AIH_UNSUPPORTED) {
    LIBLOG_DEBUG("Unsupported author identifier hash.");
    return false;
  }
  if (choices.nonce_hash == VESSEL_NH_UNSUPPORTED) {
    LIBLOG_DEBUG("Unsupported nonce hash.");
    return false;
  }
  if (choices.signature_hash == VESSEL_SH_UNSUPPORTED) {
    LIBLOG_DEBUG("Unsupported signature hash.");
    return false;
  }
  if (choices.asymmetric_signature == VESSEL_AS_UNSUPPORTED) {
    LIBLOG_DEBUG("Unsupported asymmetric signature.");
    return false;
  }
  if (choices.message_authentication == VESSEL_MA_UNSUPPORTED) {
    LIBLOG_DEBUG("Unsupported message authentication.");
    return false;
  }
  if (choices.symmetric_encryption == VESSEL_SE_UNSUPPORTED) {
    LIBLOG_DEBUG("Unsupported symmetric encryption.");
    return false;
  }
  if (choices.signature_algorithm == VESSEL_SA_UNSUPPORTED) {
    LIBLOG_DEBUG("Unsupported signature algorithm.");
    return false;
  }
  if (choices.private_header_flag == VESSEL_PHF_UNSUPPORTED) {
    LIBLOG_DEBUG("Unsupported private header flag.");
    return false;
  }

  // Bad combinations
  if (choices.symmetric_encryption == VESSEL_SE_NONE) {
    if (choices.signature_algorithm == VESSEL_SA_AEAD) {
      LIBLOG_DEBUG("Without AEAD (symmetric_encryption), signature_algorithm MUST NOT be VESSEL_SA_AEAD!");
      return false;
    }
  }
  else {
    if (choices.signature_algorithm != VESSEL_SA_AEAD) {
      LIBLOG_DEBUG("With AEAD (symmetric_encryption), signature_algorithm MUST be VESSEL_SA_AEAD!");
      return false;
    }
  }

  if (choices.signature_hash == VESSEL_SH_EDDSA) {
    if (choices.asymmetric_signature != VESSEL_AS_ED25519
        && choices.asymmetric_signature != VESSEL_AS_ED448)
    {
      LIBLOG_DEBUG("The VESSEL_SH_EDDSA signature hash is only valid for edwards curve asymmetric_signature choices!");
      return false;
    }
  }

  return true;
}




inline vessel_algorithm_choices
parse_and_validate_00(char const * spec, size_t len)
{
  // Parse into words.
  std::regex delim("\\s*;\\s*");
  std::vector<std::string> words;
  auto iter = std::cregex_token_iterator(spec, spec + len, delim, -1);
  for ( ; iter != std::cregex_token_iterator() ; ++iter) {
    words.push_back(liberate::string::to_lower(*iter));
  }

  if (words.size() != DRAFT_00_ALGORITHMS) {
    return EMPTY_ALGOS;
  }

  // Create algorithm choice from spec
  auto ret = EMPTY_ALGOS;
  ret.version_tag_hash = valid_version_tag_hash_algo_00(words[0]);
  ret.extent_identifier_hash = valid_extent_identifier_hash_algo_00(words[1]);
  ret.author_identifier_hash = valid_author_identifier_hash_algo_00(words[2]);
  ret.nonce_hash = valid_nonce_hash_algo_00(words[3]);
  ret.signature_hash = valid_signature_hash_algo_00(words[4]);
  ret.asymmetric_signature = valid_asymmetric_signature_algo_00(words[5]);
  ret.message_authentication = valid_message_authentication_algo_00(words[6]);
  ret.symmetric_encryption = valid_symmetric_encryption_algo_00(words[7]);
  ret.signature_algorithm = valid_signature_algorithm_algo_00(words[8]);
  ret.private_header_flag = valid_private_header_flag_algo_00(words[9]);

  // Validate that the chosen elements work together.
  if (!validate_00(ret)) {
    return EMPTY_ALGOS;
  }

  // Reassmble words. They're normalized, so this should be the right input
  // string.
  auto input = std::accumulate(std::begin(words) + 1, std::end(words),
      *std::begin(words),
      [](std::string & cur, std::string & elem)
      {
        return cur + ';' + elem;
      });

  // Hash. We concatenate the prehash with the prefix by writing the prehash
  // to a buffer that already containts the prefix and is long enough (hashes
  // are at most 64 bytes).
  char concatenated[80] = "Vessel";
  char * offset = concatenated + 6;
  size_t size = sizeof(concatenated) - 6;
  size_t used = 0;

  auto dig = hash_translate(static_cast<vessel_hash_algorithms>(ret.version_tag_hash));
  auto err = s3kr1t::digest(offset, size, used, input.c_str(), input.size(), dig);
  if (s3kr1t::ERR_SUCCESS != err) {
    LIBLOG_DEBUG("Unable to create prehash: " << s3kr1t::error_name(err)
        << " // " << s3kr1t::error_message(err));
    return EMPTY_ALGOS;
  }

  char hash[64];
  err = s3kr1t::digest(hash, sizeof(hash), used, concatenated, used + 6, dig);
  if (s3kr1t::ERR_SUCCESS != err) {
    LIBLOG_DEBUG("Unable to create hash: " << s3kr1t::error_name(err)
        << " // " << s3kr1t::error_message(err));
    return EMPTY_ALGOS;
  }

  // Truncating the result means shifting the first four bytes into the version
  // tag.
#if defined(DEBUG)
  liberate::string::canonical_hexdump hd;
#endif
  LIBLOG_DEBUG("Full hash is:" << std::endl << hd(hash, used));
  ret.version_tag =
      ((static_cast<uint32_t>(hash[0]) & 0xff) << 24)
    | ((static_cast<uint32_t>(hash[1]) & 0xff) << 16)
    | ((static_cast<uint32_t>(hash[2]) & 0xff) <<  8)
    |  (static_cast<uint32_t>(hash[3]) & 0xff);

  return ret;
}



inline std::string
serialize_version_tag_hash_algo_00(vessel_version_tag_hash value)
{
  _VESSEL_HASH_MAPPING_INV_00(VTH, value);
  return {};
}



inline std::string
serialize_extent_identifier_hash_algo_00(vessel_extent_identifier_hash value)
{
  _VESSEL_HASH_MAPPING_INV_00(EIH, value);
  return {};
}



inline std::string
serialize_author_identifier_hash_algo_00(vessel_author_identifier_hash value)
{
  _VESSEL_HASH_MAPPING_INV_00(AIH, value);
  if (value == VESSEL_AIH_NONE) {
    return "none";
  }
  return {};
}



inline std::string
serialize_nonce_hash_algo_00(vessel_nonce_hash value)
{
  _VESSEL_HASH_MAPPING_INV_00(NH, value);
  return {};
}



inline std::string
serialize_signature_hash_algo_00(vessel_signature_hash value)
{
  _VESSEL_HASH_MAPPING_INV_00(SH, value);
  if (value == VESSEL_SH_EDDSA) {
    return "eddsa";
  }
  return {};
}



inline std::string
serialize_asymmetric_signature_algo_00(vessel_asymmetric_signature value)
{
  switch (value) {
    case VESSEL_AS_DSA:
      return "dsa";
    case VESSEL_AS_ED25519:
      return "ed25519";
    case VESSEL_AS_ED448:
      return "ed448";
    case VESSEL_AS_RSA:
      return "rsa";
    case VESSEL_AS_UNSUPPORTED:
    default:
      break;
  }
  return {};
}



inline std::string
serialize_message_authentication_algo_00(vessel_message_authentication value)
{
  switch (value) {
    case VESSEL_MA_KMAC128:
      return "kmac128";
    case VESSEL_MA_KMAC256:
      return "kmac256";
    case VESSEL_MA_POLY1305:
      return "poly1305";
    case VESSEL_MA_UNSUPPORTED:
    default:
      break;
  }
  return {};
}



inline std::string
serialize_symmetric_encryption_algo_00(vessel_symmetric_encryption value)
{
  switch (value) {
    case VESSEL_SE_AEAD_AES_128_GCM:
      return "aead_aes_128_gcm";
    case VESSEL_SE_AEAD_AES_256_GCM:
      return "aead_aes_256_gcm";
    case VESSEL_SE_AEAD_AES_128_CCM:
      return "aead_aes_128_ccm";
    case VESSEL_SE_AEAD_AES_256_CCM:
      return "aead_aes_256_ccm";
    case VESSEL_SE_CHACHA20:
      return "chacha20";
    case VESSEL_SE_NONE:
      return "none";
    case VESSEL_SE_UNSUPPORTED:
    default:
      break;
  }
  return {};
}



inline std::string
serialize_signature_algorithm_algo_00(vessel_signature_algorithm value)
{
  switch (value) {
    case VESSEL_SA_AEAD:
      return "aead";
    case VESSEL_SA_KEYPAIR:
      return "keypair";
    case VESSEL_SA_MAC:
      return "mac";
    case VESSEL_SA_UNSUPPORTED:
    default:
      break;
  }
  return {};
}



inline std::string
serialize_private_header_flag_algo_00(vessel_private_header_flag value)
{
  switch (value) {
    case VESSEL_PHF_PH_EQ_0:
      return "ph=0";
    case VESSEL_PHF_PH_EQ_1:
      return "ph=1";
    case VESSEL_PHF_UNSUPPORTED:
    default:
      break;
  }
  return {};
}



inline std::string
assemble_00(vessel_algorithm_choices const & choices)
{
  std::vector<std::string> words;

  words.push_back(serialize_version_tag_hash_algo_00(choices.version_tag_hash));
  words.push_back(serialize_extent_identifier_hash_algo_00(choices.extent_identifier_hash));
  words.push_back(serialize_author_identifier_hash_algo_00(choices.author_identifier_hash));
  words.push_back(serialize_nonce_hash_algo_00(choices.nonce_hash));
  words.push_back(serialize_signature_hash_algo_00(choices.signature_hash));
  words.push_back(serialize_asymmetric_signature_algo_00(choices.asymmetric_signature));
  words.push_back(serialize_message_authentication_algo_00(choices.message_authentication));
  words.push_back(serialize_symmetric_encryption_algo_00(choices.symmetric_encryption));
  words.push_back(serialize_signature_algorithm_algo_00(choices.signature_algorithm));
  words.push_back(serialize_private_header_flag_algo_00(choices.private_header_flag));

  auto res = std::accumulate(std::begin(words) + 1, std::end(words),
      *std::begin(words),
      [](std::string & cur, std::string & elem)
      {
        return cur + ';' + elem;
      });

  return res;
}



inline bool
add_validated_choice(vessel_algorithm_context & ctx,
    struct vessel_algorithm_choices const & choice)
{
  LIBLOG_DEBUG("Adding version tag: 0x" << std::hex << choice.version_tag << std::dec);

  ctx.algorithms[choice.version_tag] = vessel_algorithm_context::algorithm{choice};

  return true;
}




inline bool
add_choices(vessel_algorithm_context & ctx,
    struct vessel_algorithm_choices const * choices,
    size_t amount)
{
  for (size_t i = 0 ; i < amount ; ++i) {
    if (!validate_00(choices[i])) {
      LIBLOG_ERROR("Choices at index " << i << " did not validate to draft-00 "
          "specs");
      return false;
    }
    if (!add_validated_choice(ctx, choices[i])) {
      LIBLOG_ERROR("Could not add validated choice to context.");
      return false;
    }
  }
  return true;
}


inline bool
add_specs(vessel_algorithm_context & ctx,
    char const ** specs,
    size_t amount)
{
  for (size_t i = 0 ; i < amount ; ++i) {
    if (!specs[i]) {
      return false;
    }
    size_t len = strnlen(specs[i], MAX_SPEC_LEN);

    auto choices = parse_and_validate_00(specs[i], len);
    if (!choices.version_tag) {
      return false;
    }

    if (!add_validated_choice(ctx, choices)) {
      return false;
    }
  }
  return true;
}

} // anonymous namespace
} // namespace vessel

extern "C" {


struct vessel_algorithm_choices
vessel_algorithm_choices_create(char const * spec)
{
  if (!spec) {
    LIBLOG_DEBUG("NULL spec, aborting.");
    return vessel::EMPTY_ALGOS;
  }
  auto len = strnlen(spec, vessel::MAX_SPEC_LEN);
  if (!len) {
    LIBLOG_DEBUG("Zero length spec, aborting.");
    return vessel::EMPTY_ALGOS;
  }

  auto ret = vessel::parse_and_validate_00(spec, len);
  if (!ret.version_tag) {
    return vessel::EMPTY_ALGOS;
  }

  return ret;
}



VESSEL_API vessel_error_t
vessel_algorithm_choices_serialize(char * out, size_t * out_size,
    struct vessel_algorithm_choices const * choices)
{
  if (!out || !out_size || !*out_size || !choices) {
    return VESSEL_ERR_INVALID_VALUE;
  }

  auto res = vessel::assemble_00(*choices);

  if (res.empty()) {
    LIBLOG_ERROR("The combination of algorithms chosen is not supported.");
    return VESSEL_ERR_BAD_ALGORITHMS;
  }
  *out_size = res.size() + 1;

  if (res.size() >= *out_size) {
    LIBLOG_ERROR("Buffer too small; need " << *out_size);
    return VESSEL_ERR_OUT_OF_MEMORY;
  }

  // Copy if we have enough buffer
  std::memcpy(out, res.c_str(), res.size());
  out[res.size()] = '\0';
  return VESSEL_ERR_SUCCESS;
}



VESSEL_API vessel_error_t
vessel_algorithm_context_create_from_choices(
    struct vessel_algorithm_context ** context,
    struct vessel_algorithm_choices const * choices,
    size_t amount)
{
  if (!context || !choices || !amount) {
    return VESSEL_ERR_INVALID_VALUE;
  }

  if (*context) {
    auto err = vessel_algorithm_context_free(context);
    if (VESSEL_ERR_SUCCESS != err) {
      return err;
    }
  }

  auto ctx = new vessel_algorithm_context{};
  if (!vessel::add_choices(*ctx, choices, amount)) {
    delete ctx;
    return VESSEL_ERR_BAD_ALGORITHMS;
  }

  *context = ctx;
  return VESSEL_ERR_SUCCESS;
}



VESSEL_API vessel_error_t
vessel_algorithm_context_create_from_specs(
    struct vessel_algorithm_context ** context,
    char const ** specs,
    size_t amount)
{
  if (!context || !specs || !amount) {
    return VESSEL_ERR_INVALID_VALUE;
  }

  if (*context) {
    auto err = vessel_algorithm_context_free(context);
    if (VESSEL_ERR_SUCCESS != err) {
      return err;
    }
  }

  vessel_algorithm_context * ctx = new vessel_algorithm_context{};
  if (!vessel::add_specs(*ctx, specs, amount)) {
    delete ctx;
    return VESSEL_ERR_BAD_ALGORITHMS;
  }

  *context = ctx;
  return VESSEL_ERR_SUCCESS;
}


VESSEL_API vessel_error_t
vessel_algorithm_context_free(struct vessel_algorithm_context ** context)
{
  if (!context) {
    return VESSEL_ERR_INVALID_VALUE;
  }

  if (*context) {
    delete *context;
    *context = nullptr;
  }

  return VESSEL_ERR_SUCCESS;
}


VESSEL_API size_t
vessel_algorithm_context_choices(struct vessel_algorithm_context * context)
{
  if (!context) {
    return 0;
  }

  return context->algorithms.size();
}


} // extern "C"
