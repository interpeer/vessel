/**
 * This file is part of vessel.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt).
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#ifndef VESSEL_PAGERS_MMAP_H
#define VESSEL_PAGERS_MMAP_H

#include <build-config.h>

#ifndef VESSEL_HAVE_SYS_MMAN_H
#error Cannot build this without <sys/mman.h>
#endif

#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>

#include <liberate/logging.h>
#include <liberate/sys/memory.h>

#include "../pager.h"
#include "../exception.h"
#include "../errno_translate.h"


namespace vessel::pagers {

/**
 * The mmap_pager struct uses POSIX mmap() to map a file into memory and provide
 * access to pages.
 */
struct mmap_pager
  : public ::vessel::pager
  , std::enable_shared_from_this<mmap_pager>
{
public:
  inline explicit mmap_pager(std::string const & filename = {})
    : pager{PK_MMAP}
    , m_data{std::make_shared<data>()}
  {
    m_data->m_filename = filename;

    size_t initial_size = 0;
    if (!m_data->m_filename.empty()) {
      initial_size = open_mmapped_file();
    }

    auto err = grow_and_remap(initial_size);
    if (VESSEL_ERR_SUCCESS != err) {
      throw ::vessel::exception{err, "Failed to initialize mmap_pager!"};
    }
  }


  virtual ~mmap_pager()
  {
    sync(nullptr);
    do_munmap();
    if (m_data->m_fd > 0) {
      ::close(m_data->m_fd);
    }
  }


  virtual size_t page_size() const override final
  {
    if (!m_data->m_system_page_size) {
      // Determine system page size on first use
      long pagesize = sysconf(_SC_PAGESIZE);
      if (pagesize < 0) {
        LIBLOG_ERRNO("Could not determine system page size.");
        return 0;
      }
      m_data->m_system_page_size = pagesize;
    }
    return m_data->m_system_page_size;
  }


  virtual size_t pages_available() const override final
  {
    return m_data->m_mapping_size / page_size();
  }


  virtual size_t pages_in_use() const override final
  {
    return m_data->m_pages_in_use;
  }


  virtual vessel_error_t fetch(paged_memory & out, size_t offset, size_t amount) override final
  {
    if (!amount) {
      LIBLOG_ERROR("Invalid amount specified.")
      return VESSEL_ERR_INVALID_VALUE;
    }

    // Initialite the page for the error case
    out.data = nullptr;
    out.count = 0;
    out.size = 0;
    out.responsible_pager = shared_from_this();

    // Check total amount fits in memory
    size_t offset_bytes = offset * page_size();
    size_t total_requested = amount * page_size();
    if (!m_data->m_mapping_size
        || total_requested > m_data->m_mapping_size - offset_bytes)
    {
      // Not enough space - maybe grow?
      size_t grow_size = offset_bytes + total_requested;
      LIBLOG_DEBUG("Need to grow mapping from " << m_data->m_mapping_size
          << " by " << total_requested << " to " << grow_size);
      vessel_error_t err = grow_and_remap(grow_size);
      if (VESSEL_ERR_SUCCESS != err) {
        LIBLOG_ERROR("Grow on OOM failed.");
        return err;
      }
    }

    // It does, so let's provide the data.
    out.count = amount;
    out.size = total_requested;
    out.data = static_cast<char *>(m_data->m_mapping) + offset_bytes;

    // Increment the use count for the mapping
    m_data->m_pages_in_use += amount;

    return VESSEL_ERR_SUCCESS;
  }


  virtual vessel_error_t sync(paged_memory const * mem) override final
  {
    // We sync either the entire mapping or the page, whichever is given.
    void * addr = m_data->m_mapping;
    size_t amount = m_data->m_mapping_size;
    if (mem) {
      auto resp = mem->responsible_pager.lock();
      if (resp.get() != this) {
        LIBLOG_ERROR("Cannot sync a memory region we're not responsible for.");
        return VESSEL_ERR_INVALID_VALUE;
      }
      addr = mem->data;
      amount = mem->size;
    }

    // Sync
    int err = msync(addr, amount, MS_SYNC | MS_INVALIDATE);
    if (err < 0) {
      LIBLOG_ERROR("msync() failed.");
      return translate_errno();
    }

    return VESSEL_ERR_SUCCESS;
  }


  virtual vessel_error_t release(paged_memory & mem) override final
  {
    auto resp = mem.responsible_pager.lock();
    if (resp.get() != this) {
      LIBLOG_ERROR("Cannot free a memory region we're not responsible for.");
      return VESSEL_ERR_INVALID_VALUE;
    }

    if (!mem.data) {
      // Nothing to do
      return VESSEL_ERR_SUCCESS;
    }

    // Decrement the page count
    m_data->m_pages_in_use -= mem.count;

    // Releasing is nothing but freeing this structure's data.
    mem.data = nullptr;
    mem.count = 0;
    mem.size = 0;
    mem.responsible_pager.reset();

    return VESSEL_ERR_SUCCESS;
  }

private:

  inline size_t
  open_mmapped_file()
  {
    // Try to open file
    int res = open(m_data->m_filename.c_str(),
        O_RDWR | O_CREAT | O_CLOEXEC,
        S_IRUSR | S_IWUSR);
    if (res < 0) {
      LIBLOG_ERROR("Could not open file.");
      throw ::vessel::exception{VESSEL_ERR_IO,
        "File not found."};
    }
    m_data->m_fd = res;

    // Determine file size
    struct stat statbuf;
    liberate::sys::secure_memzero(&statbuf, sizeof(statbuf));
    res = fstat(m_data->m_fd, &statbuf);
    if (res < 0) {
      LIBLOG_ERROR("Could not determine file size.");
      throw ::vessel::exception{VESSEL_ERR_IO,
        "Could not determine file size.."};
    }
    return statbuf.st_size;
  }



  inline vessel_error_t
  grow_mmapped_file(size_t target_size)
  {
    if (!target_size) {
      return VESSEL_ERR_SUCCESS;
    }

    // Seek and write
    int err = lseek(m_data->m_fd, target_size - 1, SEEK_SET);
    if (err < 0) {
      LIBLOG_ERROR("Could not lseek file.");
      return translate_errno();
    }

    char buf = 0;
    err = write(m_data->m_fd, &buf, sizeof(buf));
    if (err < 0) {
      LIBLOG_ERROR("Could not grow file.");
      return translate_errno();
    }

    return VESSEL_ERR_SUCCESS;
  }



  inline vessel_error_t
  do_mmap(size_t target_size)
  {
    if (!target_size) {
      return VESSEL_ERR_SUCCESS;
    }

    int fd = -1;
    int flags = MAP_ANONYMOUS | MAP_PRIVATE;
    if (m_data->m_fd > -1) {
      fd = m_data->m_fd;
      flags = MAP_SHARED;
    }

    // Create mapping
    void * mapping = mmap(NULL, target_size, PROT_READ | PROT_WRITE,
        flags, fd, 0);
    if (MAP_FAILED == mapping) {
      LIBLOG_ERROR("mmap() failed.");
      return translate_errno();
    }

    // Worked, return valid
    m_data->m_mapping = mapping;
    m_data->m_mapping_size = target_size;

    return VESSEL_ERR_SUCCESS;
  }



  inline vessel_error_t
  do_munmap()
  {
    // Unmap
    if (m_data->m_mapping) {
      int err = munmap(m_data->m_mapping, m_data->m_mapping_size);
      if (err < 0) {
        LIBLOG_ERROR("munmap() failed.");
        return translate_errno();
      }
      m_data->m_mapping = nullptr;
      m_data->m_mapping_size = 0;
    }

    return VESSEL_ERR_SUCCESS;
  }



  inline vessel_error_t
  grow_and_remap(size_t target_size)
  {
    if (m_data->m_pages_in_use) {
      LIBLOG_ERROR("Cannot modify mapping with pages in use. Release them and "
          "try again. (used: " << m_data->m_pages_in_use << ")");
      return VESSEL_ERR_OUT_OF_MEMORY;
    }

    vessel_error_t err = VESSEL_ERR_SUCCESS;

    // Sync everything
    if (m_data->m_mapping) {
      err = sync(nullptr);
      if (VESSEL_ERR_SUCCESS != err) {
        LIBLOG_ERROR("Synchronizing mapping failed.");
        return err;
      }

      // Unmap
      err = do_munmap();
      if (VESSEL_ERR_SUCCESS != err) {
        LIBLOG_ERROR("Unmapping failed.");
        return err;
      }
    }

    // Grow file (if that exists)
    if (m_data->m_fd > -1) {
      err = grow_mmapped_file(target_size);
      if (VESSEL_ERR_SUCCESS != err) {
        LIBLOG_ERROR("Could not grow file.");
        return err;
      }
    }

    // Map again
    err = do_mmap(target_size);
    if (VESSEL_ERR_SUCCESS != err) {
      LIBLOG_ERROR("Mapping failed.");
      return err;
    }

    return VESSEL_ERR_SUCCESS;
  }

  struct data
  {
    std::string m_filename = {};
    size_t      m_system_page_size = 0;
    size_t      m_pages_in_use = 0;

    int         m_fd =  -1;
    void *      m_mapping = nullptr;
    size_t      m_mapping_size = 0;
  };

  std::shared_ptr<data> m_data;
};


} // namespace vessel::pagers

#endif // guard
