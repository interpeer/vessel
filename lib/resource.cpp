/**
 * This file is part of vessel.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt).
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#include <build-config.h>

#include <vessel/resource.h>

#include <liberate/logging.h>

#include "resource.h"

extern "C" {


VESSEL_API vessel_error_t
vessel_resource_create(struct vessel_resource ** resource,
    struct vessel_context const * context,
    struct vessel_backing_store const * store,
    struct vessel_extent_id const * origin_extent_id)
{
  if (!resource || !context || !store) {
    return VESSEL_ERR_INVALID_VALUE;
  }

  if (*resource) {
    auto err = vessel_resource_free(resource);
    if (VESSEL_ERR_SUCCESS != err) {
      return err;
    }
  }

  *resource = new vessel_resource{};
  (*resource)->resource = std::make_shared<vessel::resource>(context, store);
  if (origin_extent_id && !origin_extent_id->extent_id.empty()) {
    (*resource)->resource->m_origin_extent = origin_extent_id->extent_id;
  }

  return VESSEL_ERR_SUCCESS;
}



VESSEL_API vessel_error_t
vessel_resource_free(struct vessel_resource ** resource)
{
  if (!resource) {
    return VESSEL_ERR_INVALID_VALUE;
  }

  if (*resource) {
    delete *resource;
    *resource = nullptr;
  }

  return VESSEL_ERR_SUCCESS;
}



VESSEL_API vessel_operation_id
vessel_resource_add_extent(struct vessel_resource * resource,
    uint32_t version_tag, size_t size_in_blocks,
    struct vessel_author_id const * author,
    vessel_authoring_counter counter)
{
  if (!resource || !resource->resource || !version_tag || !size_in_blocks || !author) {
    return VESSEL_ERR_INVALID_VALUE;
  }

//  return resource->resource->add_extent(version_tag, size_in_blocks,
//      *author, counter);
}



VESSEL_API vessel_error_t
vessel_resource_result(struct vessel_resource * resource,
    vessel_operation_id opid,
    struct vessel_extent ** extent)
{
  if (!resource || !resource->resource || !extent) {
    return VESSEL_ERR_INVALID_VALUE;
  }

  // FIXME return resource->resource->result(opid, extent);
  return VESSEL_ERR_NOT_IMPLEMENTED;
}



VESSEL_API vessel_error_t
vessel_resource_release(struct vessel_resource * resource,
    struct vessel_extent ** extent)
{
  if (!resource || !resource->resource || !extent) {
    return VESSEL_ERR_INVALID_VALUE;
  }

  return resource->resource->release(extent);
}



VESSEL_API vessel_error_t
vessel_resource_commit(struct vessel_resource * resource,
    struct vessel_extent * extent)
{
  if (!resource || !resource->resource || !extent) {
    return VESSEL_ERR_INVALID_VALUE;
  }

  return resource->resource->commit(extent);
}

namespace {

inline std::tuple<bool, size_t>
can_use_extent(
    struct vessel_resource & resource,
    struct vessel_extent & extent,
    struct vessel_algorithm_choices const & algos,
    vessel_section_t section,
    size_t min_data_size,
    size_t desired_data_size,
    struct vessel_author & author)
{
  // Checks first - the extent must be by the given author and use the given
  // algorithms, otherwise it's not usable.
  auto opts = vessel::options_from_choices(algos);
  if (opts != extent.ext->opts) {
    return {false, 0};
  }

  if (extent.ext->header.author_identifier != author.author.author_id()) {
    return {false, 0};
  }

  // Check available payload
  vessel::payload payload{resource.resource->m_ctx->sections->section_sizes,
    extent.ext->opts,
    static_cast<char *>(extent.ext->payload()),
    extent.ext->payload_size()};

  auto [has_space, found_size] = payload.can_add_section(
      section, min_data_size, desired_data_size);
  if (!has_space) {
    return {false, 0};
  }

  return {true, found_size};
}

} // anonymous namespace


VESSEL_API vessel_error_t
vessel_resource_make_space_for_section(struct vessel_resource * resource,
    struct vessel_extent ** extent,
    struct vessel_algorithm_choices const * algos,
    vessel_section_t section, vessel_topic_t topic,
    size_t min_data_size,
    size_t * data_size,
    size_t max_extent_multiplier,
    struct vessel_author * author,
    int flags)
{
  if (!resource || !resource->resource || !extent || !algos || !data_size || !author) {
    return VESSEL_ERR_INVALID_VALUE;
  }

  // First things first - if we have an extent supplied, check whether it's
  // usable for the section in question.
  if (*extent) {
    auto [success, dsize] = can_use_extent(*resource, **extent, *algos, section,
        min_data_size, *data_size, *author);
    if (success) {
      // If we can use the extent, just return it!
      *data_size = dsize;
      return VESSEL_ERR_SUCCESS;
    }

    if (flags & VESSEL_MF_ERROR_ON_UNUSABLE) {
      return VESSEL_ERR_OUT_OF_MEMORY;
    }

    if (!(flags & VESSEL_MF_NO_COMMIT)) {
      auto err = vessel_resource_commit(resource, *extent);
      if (VESSEL_ERR_SUCCESS != err) {
        return err;
      }
    }

    auto err = vessel_resource_release(resource, extent);
    if (VESSEL_ERR_SUCCESS != err) {
      return err;
    }
  }

  // Ok, so if that extent wasn't usable, fetch extents from the store until
  // we have a match, or create a new one.
  vessel::resource::section_request req{};
  req.version_tag = algos->version_tag;
  req.section = section;
  req.topic = topic;
  req.min_data_size = min_data_size;
  req.desired_data_size = *data_size;
  req.max_extent_size = vessel_extent_block_size() * max_extent_multiplier;
  req.author = author;
  req.counter = 1; // FIXME: from author OR froM API?
  req.integrity_section = VESSEL_SECTION_CRC32; // FIXME we should be able to set this

  vessel::byte_sequence ext_id;

  // Create the extent, and return it.
  req.callback = [&ext_id, &extent, &data_size](vessel_error_t _err, vessel::resource::section_result & _res)
  {
    if (VESSEL_ERR_SUCCESS != _err) {
      return;
    }
    if (!_res.ext || !_res.extent_payload) {
      return;
    }

    // This is good, just return the extent at this point.
    *extent = _res.ext;
    *data_size = _res.section_data_size;
  };

  auto err = resource->resource->make_space_for_section(req);
  if (VESSEL_ERR_SUCCESS != err) {
    return err;
  }
  if (!*extent) {
    return VESSEL_ERR_IO;
  }

  return VESSEL_ERR_SUCCESS;
}



VESSEL_API vessel_error_t
vessel_resource_add_section(struct vessel_resource * resource,
    struct vessel_extent * extent,
    vessel_section_t sectype, vessel_topic_t topic,
    void * data, size_t data_size)
{
  if (!data) {
    return VESSEL_ERR_INVALID_VALUE;
  }

  struct vessel_section res{};
  return vessel_resource_add_return_section(&res,
      resource, extent, sectype, topic, data, data_size);
}



VESSEL_API vessel_error_t
vessel_resource_add_return_section(struct vessel_section * section,
    struct vessel_resource * resource,
    struct vessel_extent * extent,
    vessel_section_t sectype, vessel_topic_t topic,
    void * data,
    size_t data_size)
{
  if (!section || !resource || !extent) {
    return VESSEL_ERR_INVALID_VALUE;
  }

  // Create payload abstraction from extent
  vessel::payload payload{resource->resource->m_ctx->sections->section_sizes,
    extent->ext->opts,
    static_cast<char *>(extent->ext->payload()),
    extent->ext->payload_size()};

  // Add section to payload
  auto [res, sec] = payload.add_section(sectype, topic, data_size);
  if (!res) {
    LIBLOG_ERROR("Could not add section!");
    return VESSEL_ERR_IO;
  }

  // Copy payload, if we have any.
  if (data && data_size) {
    std::memcpy(sec.data, data, data_size);
  }

  // Return section.
  section->type = sec.type;
  section->topic = sec.topic;
  section->section_size = sec.section_size;
  section->payload_size = sec.data_size;
  section->payload = sec.data;

  // All good
  return VESSEL_ERR_SUCCESS;
}




VESSEL_API vessel_error_t
vessel_resource_add_content_type(struct vessel_resource * resource,
    struct vessel_extent ** extent,
    struct vessel_algorithm_choices const * algos,
    vessel_topic_t topic,
    struct vessel_content_type const * ct,
    size_t max_extent_multiplier,
    struct vessel_author * author,
    int flags)
{
  if (!resource || !algos || !ct || !author) {
    return VESSEL_ERR_INVALID_VALUE;
  }

  // Serialize the content type. This will take two attempts; one to find the
  // appropriate buffer size.
  std::vector<char> ct_buf;
  ct_buf.resize(1); // Need at least one byte.
  size_t required = ct_buf.size();
  auto err = vessel_content_type_to_utf8(&ct_buf[0], &required, ct);
  if (VESSEL_ERR_SUCCESS == err) {
    // The content type was tiny and fit into the buffer. If it
    // was zero sized (high probability!), then exit. We shouldn't
    // write that.
    if (!required) {
      LIBLOG_ERROR("Empty content type, aborting.");
      return VESSEL_ERR_INVALID_VALUE;
    }

    // If we reached here, we had a single byte content type. This is weird,
    // so should be logged.
    LIBLOG_DEBUG("Content type is a single Byte; this is likely not correct, "
        "but it's possible to proceed.");
  }
  else if (VESSEL_ERR_OUT_OF_MEMORY == err) {
    // Resize buffer and try again.
    ct_buf.resize(required);
    err = vessel_content_type_to_utf8(&ct_buf[0], &required, ct);
    if (VESSEL_ERR_SUCCESS != err) {
      // This is weird and we shouldn't deal with it.
      return err;
    }
    ct_buf.resize(required);
  }
  else {
    // We have some error, abort!
    return err;
  }

  // Make space for a content type section (fixed size; we know how much we
  // need).
  size_t bufsize = ct_buf.size();
  struct vessel_extent * ext = nullptr;
  if (extent && *extent) {
    ext = *extent;
  }
  err = vessel_resource_make_space_for_section(resource, &ext,
      algos, VESSEL_SECTION_CONTENT_TYPE, topic,
      bufsize, &bufsize,
      max_extent_multiplier, author, flags);
  if (VESSEL_ERR_SUCCESS != err) {
    return err;
  }

  // With the extent known, create a section and copy data into it.
  err = vessel_resource_add_section(resource, ext,
      VESSEL_SECTION_CONTENT_TYPE, topic,
      ct_buf.data(), bufsize);
  if (VESSEL_ERR_SUCCESS != err) {
    if (!extent) {
      vessel_resource_release(resource, &ext);
    }
    return err;
  }

  // If an extent was supposed to be returned, do so.
  if (extent) {
    // Return the extent
    *extent = ext;
  }
  else {
    // Commit extent, otherwise the change is lost
    err = vessel_resource_commit(resource, ext);
    if (VESSEL_ERR_SUCCESS != err) {
      return err;
    }

    err = vessel_resource_release(resource, &ext);
    if (VESSEL_ERR_SUCCESS != err) {
      return err;
    }
  }

  return VESSEL_ERR_SUCCESS;
}



struct vessel_extent_iterator
{
  std::shared_ptr<vessel::resource>     resource;
  vessel::extent_id_dag::const_iterator iter;

  inline vessel_extent_iterator(vessel_resource * res)
    : resource{res->resource}
    , iter{res->resource->m_dag.begin()}
  {
  }

  inline ~vessel_extent_iterator()
  {
  }
};



VESSEL_API vessel_error_t
vessel_extent_iterator_create(struct vessel_extent_iterator ** iterator,
    struct vessel_resource * resource)
{
  if (!iterator || !resource) {
    return VESSEL_ERR_INVALID_VALUE;
  }

  if (*iterator) {
    auto err = vessel_extent_iterator_free(iterator);
    if (VESSEL_ERR_SUCCESS != err) {
      return err;
    }
  }

  *iterator = new vessel_extent_iterator{resource};

  return VESSEL_ERR_SUCCESS;
}



VESSEL_API vessel_error_t
vessel_extent_iterator_free(struct vessel_extent_iterator ** iterator)
{
  if (!iterator) {
    return VESSEL_ERR_INVALID_VALUE;
  }

  if (*iterator) {
    delete *iterator;
    *iterator = nullptr;
  }

  return VESSEL_ERR_SUCCESS;
}



VESSEL_API vessel_error_t
vessel_extent_iterator_next(struct vessel_extent ** extent,
    struct vessel_extent_iterator * iterator)
{
  if (!extent || !iterator || !iterator->resource) {
    return VESSEL_ERR_INVALID_VALUE;
  }

  // If there is an extent still, clear it.
  if (*extent) {
    auto err = iterator->resource->release(extent);
    if (VESSEL_ERR_SUCCESS != err) {
      return err;
    }
  }

  // We're returning the current extent and advancing the iterator (in that
  // order). But only if we haven't reached the end yet.
  if (iterator->iter == iterator->resource->m_dag.end()) {
    return VESSEL_ERR_END_ITERATION;
  }

  vessel_extent_id id{};
  id.extent_id = iterator->iter->extent_id;
  auto err = iterator->resource->fetch_latest_blocking(id, extent);
  if (VESSEL_ERR_SUCCESS != err) {
    return err;
  }

  // After the fetch, increment.
  ++(iterator->iter);

  return VESSEL_ERR_SUCCESS;
}



} // extern "C"
