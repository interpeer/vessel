/*
 * This file is part of vessel.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt).
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef VESSEL_LIB_CONTENT_TYPE_H
#define VESSEL_LIB_CONTENT_TYPE_H

#include <build-config.h>

#include <cstring>
#include <string>
#include <map>
#include <vector>

#include <unicode/unistr.h>
#include <unicode/ucsdet.h>
#include <unicode/ucnv.h>
#include <unicode/ustring.h>

#include <vessel/error.h>

#include <liberate/logging.h>


namespace vessel {

/**
 * The content type is, in the abstract, a mapping of keys to values, where
 * both are unicode (utf-8) strings.
 *
 * We'd also like to preserve order in parsing and re-assembling, so in addition
 * to the value mapping, we also provide a position mapping for those keys that
 * have been parsed from a string.
 */
struct content_type
{
  using kv_map = std::map<icu::UnicodeString, icu::UnicodeString>;
  using kv_pos = std::map<size_t, icu::UnicodeString>;

  kv_map  values;
  kv_pos  positions;
};

namespace util {

inline std::string
to_utf8(icu::UnicodeString const & in)
{
  std::string s;
  in.toUTF8String(s);
  return s;
}


inline vessel_error_t
from_utf8(icu::UnicodeString & out, char const * input, ssize_t input_size)
{
  out.remove();

  if (input_size < 0) {
    input_size = std::strlen(input);
  }

  // We should try a few times to convert, growing the output each time.
  static constexpr auto RESIZE_FACTOR = 2;
  static constexpr auto MAX_ATTEMPTS = 3;

  std::vector<UChar> result;
  result.resize(input_size * RESIZE_FACTOR);
  bool success = false;
  for (auto i = 0 ; i < MAX_ATTEMPTS ; ++i) {
    LIBLOG_DEBUG("Attempt " << i << " with buffer " << result.size()
        << " (from " << input_size << ")");
    UErrorCode status = U_ZERO_ERROR;

    int32_t used = 0;
    u_strFromUTF8(
        &result[0], result.size(),
        &used,
        input, input_size,
        &status);
    if (!U_FAILURE(status)) {
      result.resize(used);
      success = true;
      break;
    }

    result.resize(result.size() * RESIZE_FACTOR);
  }

  if (!success) {
    LIBLOG_ERROR("Could not convert input from UTF-8, aborting!");
    return VESSEL_ERR_INVALID_VALUE;
  }

  // With the buffer converted, we can create a string
  out = icu::UnicodeString{result.data()};
  return VESSEL_ERR_SUCCESS;
}

} // namespace util


// All our search patterns are encoded in a single code unit (BMP), which
// means operating at the code unit level should work just fine.
static constexpr char16_t CT_PAIR_SEPARATOR{0x003b};
static constexpr char16_t CT_KV_SEPARATOR{0x003d};
static constexpr char16_t CT_ESCAPE{0x005c};



inline std::tuple<int32_t, int32_t>
find_escaped_range(char16_t separator, int32_t start, icu::UnicodeString const & input)
{
  int32_t end = -1;
  auto offset = start;

  while (true) {
    end = input.indexOf(separator, offset);
    if (end < 0) {
      // We're done, everything until end of input
      end = input.length();
      break;
    }

    // Check if this separator is escaped - this is not the case if the end is
    // at the offset, which is the only risky situation.
    if (end != offset) {
      if (CT_ESCAPE == input.charAt(end - 1)) {
        // This separtor is escaped. We need to continue, with the offset moved
        // to one past the separator.
        offset = end + 1;
        continue;
      }
    }

    // If we have not found an escaped separator, the separator is valid.
    break;
  }

  return {start, end};
}


inline vessel_error_t
unescape_value(icu::UnicodeString & value)
{
  LIBLOG_DEBUG("Value (raw) is >" << util::to_utf8(value) << "<");

  // We have to check the value for validity, which involves escape characters,
  // and remove escape characters. It seems best to do this in one loop. However,
  // we have to look ahead at times...
  int32_t offset = 0;
  while (offset < value.length()) {
    switch (value.charAt(offset)) {
      case CT_ESCAPE:
        // Escape characters need to be removed. This means the offset will now
        // point at the escaped character.
        // If that character is one of the special ones above, great, we
        // successfully escaped it. If it's a regular character, also great. In
        // either case, we should *not* process this next character, as to avoid
        // the next cases in this switch.
        // That means removing the escapce character *and* increment the offset.
        value = value.remove(offset, 1);
        offset += 1;
        break;

      case CT_KV_SEPARATOR:
      case CT_PAIR_SEPARATOR:
        // Due to the above skipping of escapced versions of this, valid values
        // can never enter this case.
        LIBLOG_ERROR("Value contains unescaped separators, aborting.");
        return VESSEL_ERR_INVALID_VALUE;
        break;


      default:
        // Regular code units get no special treatment.
        offset += 1;
        break;
    }
  }

  LIBLOG_DEBUG("Value (cleaned) is >" << util::to_utf8(value) << "<");
  return VESSEL_ERR_SUCCESS;
}


inline void
escape_value(icu::UnicodeString & value)
{
  int32_t offset = 0;
  while (offset < value.length()) {
    switch (value.charAt(offset)) {
      case CT_KV_SEPARATOR:
      case CT_PAIR_SEPARATOR:
        // We need to insert an escape marker at this offset. Then
        // we have to move the offset past this marker, and then also
        // past the escaped character.
        value.insert(offset, CT_ESCAPE);
        offset += 2;
        break;

      default:
        // Ignore all other code units
        offset += 1;
        break;
    }
  }
  LIBLOG_DEBUG("Escaped value >" << util::to_utf8(value) << "<");
}




/**
 * Parse a KV pair. In here, we can expect to find only escaped CT_PAIR_SEPARATOR,
 * but we *may* find unescaped CT_KV_SEPARATORs in the value part - that would be an
 * error.
 */
inline vessel_error_t
parse_kv_pair(content_type & output, size_t position, icu::UnicodeString const & pair)
{
  // Find separator. This is the easy part.
  auto offset = pair.indexOf(CT_KV_SEPARATOR);
  if (offset < 0) {
    LIBLOG_DEBUG("KV-Pair without value; this is invalid.");
    return VESSEL_ERR_INVALID_VALUE;
  }

  // So the part up to the separator is the key, which we'll have to trim.
  auto key = pair.tempSubString(0, offset);
  key.trim();

  LIBLOG_DEBUG("Key is >" << util::to_utf8(key) << "<");

  auto iter = output.values.find(key);
  if (iter != output.values.end()) {
    LIBLOG_ERROR("Duplicate key found, aborting: " << util::to_utf8(key));
    return VESSEL_ERR_INVALID_VALUE;
  }

  // The value is everything *after* the separator, but it does not get trimmed.
  auto value = pair.tempSubString(offset + 1, pair.length());

  auto err = unescape_value(value);
  if (VESSEL_ERR_SUCCESS != err) {
    return err;
  }

  // The cleaned up value can now be stored in the output
  output.values[key] = value;
  output.positions[position] = key;

  return VESSEL_ERR_SUCCESS;
}




/**
 * Parse a string into a content_type.
 */
inline vessel_error_t
parse_content_type(content_type & output, icu::UnicodeString const & input)
{
  // Outer loop: find pairs
  size_t position = 0;
  int32_t end = -1;
  while (end < input.length()) {
    auto start = end + 1;
    auto [rstart, rend] = find_escaped_range(CT_PAIR_SEPARATOR, start, input);
    end = rend;

    auto length = end - start;
    if (!length) {
      LIBLOG_DEBUG("Skipping empty KV-pair");
      position += 1;
      continue;
    }

    // We have a pair, pass it to the pair parsing function.
    auto pair = input.tempSubString(start, length);

    LIBLOG_DEBUG("Found pair >" << util::to_utf8(pair) << "<");

    auto err = parse_kv_pair(output, position, pair);
    if (VESSEL_ERR_SUCCESS != err) {
      LIBLOG_DEBUG("Pair parsing failed: " << vessel_error_name(err));
      return err;
    }

    position += 1;
  }

  return VESSEL_ERR_SUCCESS;
}


/**
 * Same, but expect UTF-8 encoded input.
 */
inline vessel_error_t
parse_content_type(content_type & output, char const * input, ssize_t input_size = -1)
{
  icu::UnicodeString str;
  auto err = util::from_utf8(str, input, input_size);
  if (VESSEL_ERR_SUCCESS != err) {
    return err;
  }

  // With the buffer converted, we can pass this as a UnicodeString to the actual
  // parsing function.
  return parse_content_type(output, str);
}



inline vessel_error_t
parse_content_type(content_type & output, std::string const & input)
{
  return parse_content_type(output, input.c_str(), input.size());
}



/**
 * Serialize a parsed content type into a UnicodeString
 */
inline vessel_error_t
serialize_content_type(icu::UnicodeString & out, content_type const & ct)
{
  // Clear output
  out.remove();

  // The positions are sorted, so we can just first process all pairs for which
  // we have a position.
  // We either have to remember those or modify the pair map. It's easier to
  // modify the pair map, so let's make a copy of that.
  auto values = ct.values;
  for (auto & [_, key] : ct.positions) {
    auto iter = values.find(key);
    if (iter == values.end()) {
      LIBLOG_ERROR("Found a key position, but no pair.");
      return VESSEL_ERR_INVALID_VALUE;
    }

    out += key;
    out.append(CT_KV_SEPARATOR);

    // We need to re-escape the value.
    auto value = iter->second;
    escape_value(value);

    out += value;
    out.append(CT_PAIR_SEPARATOR);

    // Erase the iterator; this way we have only non-positioned values
    // left after the loop.
    values.erase(iter);
  }
  LIBLOG_DEBUG("Escaped with positions >" << util::to_utf8(out) << "<");
  LIBLOG_DEBUG("Remaining: " << values.size());

  if (!values.empty()) {
    // Process remaining values in order
    for (auto & [key, value] : values) {
      out += key;
      out.append(CT_KV_SEPARATOR);

      escape_value(value);
      out += value;
      out.append(CT_PAIR_SEPARATOR);
    }
  }

  // The last CT_PAIR_SEPARATOR does not need to be there, remove it.
  if (out.length() > 0) {
    out.truncate(out.length() - 1);
  }

  return VESSEL_ERR_SUCCESS;
}


/**
 * Same, but return UTF-8 string
 */
inline vessel_error_t
serialize_content_type(std::string & out, content_type const & ct)
{
  icu::UnicodeString tmp;
  auto err = serialize_content_type(tmp, ct);
  if (VESSEL_ERR_SUCCESS != err) {
    LIBLOG_ERROR("Could not serialize content type, aborting");
    return err;
  }

  out = util::to_utf8(tmp);
  return VESSEL_ERR_SUCCESS;
}




} // namespace vessel

#endif // guard
