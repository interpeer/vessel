# Vessel
Vessel is a generic container file format optimized for storage and (streaming)
transmission.

[![status-badge](https://ci.codeberg.org/api/badges/interpeer/vessel/status.svg)](https://ci.codeberg.org/interpeer/vessel)
[![Build status](https://ci.appveyor.com/api/projects/status/0v7jj3j1690kg6k7?svg=true)](https://ci.appveyor.com/project/jfinkhaeuser/vessel)
[![Donate now](https://badgen.net/static/donate/now?color=green)](https://interpeer.io/donations/)
[![Merch store](https://badgen.net/static/merch/store?color=orange)](https://www.freewear.org/InterpeerProject)

The [full specification](https://specs.interpeer.io/draft-jfinkhaeuser-vessel-container-format/)
lives at [specs.interpeer.io](https://specs.interpeer.io). This is the reference
implementation.

There exists an older, partially outdated [design document](./docs/design.md).
It may provide some additonal rationale the specifications are missing.

## Overview

The purpose of Vessel is to provide a generic container file format suitable for
authoring content collaboratively, both in real-time or with eventual merging.
It is suitable for encapsulating an authoritative view of the resource, or
managing multiple diverging versions. Confidentiality and authentication of
content are both supported.

In Vessel, *resources* are subdivided into standalone *extents*; a single extent
can form a complete resource. Extents relate to their ancestors in a way that
permits deterministic ordering at any time, even with partially synchronized
resources. In this way, eventual merging is supported. Each extent is created
by a single author, but extents from multiple authors can be mixed freely in
a single resource.

Note that this functionality is similar, but not the same as a conflict-free
replicated data type. It is rather aimed at both providing a good encapsulation
format for such functionality, but simultaneously allowing for arbitrary other
content to be multiplexed into the resource. In this way, it is possible to
encapsulate any, even multiple existing files into a single vessel resource,
such as the audio and video sections of media such as DVDs/Blu-Rays.

Vessel achieves this by further subdividing extent payloads into *sections*,
that are assigned to *topics*. Each topic may carry a content type describing
the remainder of the topic's sections.

## CLI Demo

[![asciicast](https://asciinema.org/a/625867.svg)](https://asciinema.org/a/625867)

## API

The library is developed in C++, but for ease of integration with other projects
provides a C API.

# Building

## Requirements

- [meson](https://mesonbuild.com/) for the build system. Meson is most
  easily installed via [python](https://www.python.org/)'s `pip` tool.
  Installing Python als means you can use the `android.py` script for Android
  builds.
- [ninja](https://ninja-build.org/) or platform-specific tools for build
  execution.
- Vessel is implemented in C++, and requires some compiler support for
  the C++17 standard.
- Depending on which scheduler implementation you want to use, packeteer may
  require specific OS and kernel versions, e.g. Linux 2.6.9+ for the epoll
  scheduler, etc.
- For convenience, a `Pipfile` is provided that installs meson and other
  development tools.

If you're running a recent-ish UNIX-derivative, it's probably best to just
download Vessel and try to compile it.

# Instructions

1. `pip install pipenv`
1. `pipenv install`
1. `pipenv shell # the following occurs in the launched shell`
1. `mkdir build && cd build`
1. `meson ..`

# License & Acknowledgements

For the license, see the [LICENSE](./LICENSE) file.

Vessel was initially developed under a grant from the
[NLNet foundation](https://nlnet.nl), as part of the European Commission's
[Next Generation Internet](https://ngi.eu) initiative.
