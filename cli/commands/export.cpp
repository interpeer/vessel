/*
 * This file is part of vessel.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2023 Interpeer gUG (haftungsbeschränkt).
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "export.h"
#include <build-config.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <errno.h>

#if defined(VESSEL_HAVE_STAT)
#  define vessel_stat stat
#endif
#if defined(VESSEL_HAVE__STAT)
#  define vessel_stat _stat
#endif

#include <vessel/store.h>
#include <vessel/resource.h>

#include <iostream>
#include <fstream>

#include <liberate/random/unsafe_bits.h>
#include <liberate/string/util.h>
#include <liberate/string/hexencode.h>

#include "../errors.h"

namespace vessel::cli {


// FIXME this belongs in liberate
template<typename... argsT>
inline std::string
strformat(std::string const & format, argsT... args)
{
   auto fsize = 1 + std::snprintf(nullptr, 0, format.c_str(), args...);
   if (fsize <= 0) {
     return {};
   }
   std::vector<char> buf;
   buf.resize(fsize);

   std::snprintf(&buf[0], fsize, format.c_str(), args...);
   return {buf.begin(), buf.end()};
}

// FIXME this belongs in liberate
#if 1
inline bool
file_exists(std::string const & filename)
{
  struct vessel_stat buf;
  int err = vessel_stat(filename.c_str(), &buf);
  if (err < 0) {
#if defined(DEBUG)
    switch (errno) {
      case ENOENT:
        std::cerr << "File at path '" << filename << "' cannot be found." << std::endl;
        break;

      case EACCES:
        std::cerr << "Access permissions missing for file at path '" << filename << "'." << std::endl;
        break;

      default:
        std::cerr << "Unknown error accessing file at path '" << filename << "'." << std::endl;
        break;
    }
#endif
    return false;
  }
  return true;
}
#endif


struct export_context
{
  context const &   ctx;

  vessel_extent *           extent = nullptr;

  vessel_backing_store *    store = nullptr;
  vessel_resource *         res = nullptr;
  vessel_section_context *  sec_ctx = nullptr;

  struct map_entry
  {
    bool done = false;
    std::ostream * stream = nullptr;
    bool stream_owned = false;

    inline map_entry(std::ostream * _stream, bool _owned)
      : stream{_stream}
      , stream_owned{_owned}
    {
    }

    inline ~map_entry()
    {
      if (stream_owned && stream) {
        delete stream;
      }
    }

  };

  using stream_map = std::map<
    vessel_topic_t,
    std::shared_ptr<map_entry>
  >;
  stream_map                output_files = {};

  inline export_context(context const & _ctx)
    : ctx{_ctx}
  {
  }

  inline ~export_context()
  {
    vessel_section_context_free(&sec_ctx);
    vessel_resource_free(&res);
    vessel_backing_store_free(&store);
  }

  inline vessel_error_t
  initialize_resource()
  {
    // Create section context
    auto err = vessel_section_context_create(&sec_ctx, ctx.algo_ctx);
    if (VESSEL_ERR_SUCCESS != err) {
      return err;
    }

    // Create a file based store.
    err = vessel_backing_store_create_file(&store, ctx.algo_ctx,
        ctx.opts.resource_file.c_str());
    if (VESSEL_ERR_SUCCESS != err) {
      return err;
    }

    // Create a resource with this store. Note that if the file already represents
    // a resrouce, resource exportrmation will be loaded at this point.
    err = vessel_resource_create(&res, ctx.ctx, store, nullptr);
    if (VESSEL_ERR_SUCCESS != err) {
      return err;
    }

    return VESSEL_ERR_SUCCESS;
  }
};



inline bool
export_health_check(export_context & ectx [[maybe_unused]], context const & ctx)
{
  // XXX Be aware that there are races possible between checking a file exists and
  //     using the file. That'll pop up errors later, though. This should is just
  //     a quick health check.

  // Check input file exists (only if append flag is given)
  if (!file_exists(ctx.opts.resource_file)) {
    std::cerr << "Resource at path '" << ctx.opts.resource_file << "' does not exist." << std::endl;
    return false;
  }

  // All good
  return true;
}



inline void
create_file(export_context & ectx, vessel_section const & section, bool export_all)
{
  // Try to see if there is already a file for this topic. If so, we close it.
  auto iter = ectx.output_files.find(section.topic);
  if (iter != ectx.output_files.end()) {
    if (!iter->second->done) {
      if (iter->second->stream_owned) {
        delete iter->second->stream;
      }
      iter->second->stream = nullptr;
    }

    if (!export_all) {
      iter->second->done = true;
      return;
    }
  }

  // If we reached here, we should parse the content type field to understand
  // a file name.
  bool use_stdout = false;
  std::string fname;
  vessel_content_type * ct = nullptr;
  if (!ectx.ctx.opts.export_filename.empty()) {
    if (ectx.ctx.opts.export_filename == "-") {
      use_stdout = true;
    }
    else {
      // We want to use the given file name
      fname = ectx.ctx.opts.export_filename;
    }
  }
  else {
    char filename[200];
    size_t filename_size = sizeof(filename);

    auto ptr = static_cast<char *>(section.payload);
    auto err = vessel_content_type_from_utf8(&ct, ptr, section.payload_size);
    if (VESSEL_ERR_SUCCESS != err) {
      std::cerr << "Could not parse content type, ignoring." << std::endl;
      goto cleanup;
    }

    err = vessel_content_type_get(filename, &filename_size, ct,
      "filename", 8);
    if (VESSEL_ERR_SUCCESS != err) {
      std::cerr << "File name not in content type, ignoring." << std::endl;
      goto cleanup;
    }

    fname = std::string{filename, filename + filename_size};
  }

  // Create stream
  if (use_stdout) {
    ectx.output_files[section.topic] = std::make_shared<export_context::map_entry>(
        &std::cout, false
    );
  }
  else {
    ectx.output_files[section.topic] = std::make_shared<export_context::map_entry>(
        new std::ofstream{fname}, true
    );
  }

cleanup:
  vessel_content_type_free(&ct);
}



inline void
append_to_file(export_context & ectx, vessel_section const & section)
{
  auto iter = ectx.output_files.find(section.topic);
  if (iter == ectx.output_files.end()) {
    std::cerr << "Ignoring blob; we don't have a file yet" << std::endl;
    return;
  }

  if (iter->second->done) {
    std::cerr << "Ignoring blob; we're done with the topic" << std::endl;
    return;
  }

  if (!iter->second->stream) {
    std::cerr << "Internal error: we should have a stream to write to, but it's not there." << std::endl;
    return;
  }

  auto ptr = static_cast<char *>(section.payload);
  iter->second->stream->write(ptr, section.payload_size);
}



inline void
process_section(export_context & ectx, vessel_section const & section, int32_t topic, bool export_all)
{
  if (topic > 0 && section.topic != topic) {
    // Skip topic as not, err, topical.
    return;
  }

  if (section.type == VESSEL_SECTION_CONTENT_TYPE) {
    create_file(ectx, section, export_all);
  }
  else if (section.type == VESSEL_SECTION_BLOB) {
    append_to_file(ectx, section);
  }
}



inline void
process_extent(export_context & ectx, vessel_extent * ext, int32_t topic, bool export_all)
{
  vessel_section_iterator * iter = nullptr;
  vessel_section section;

  auto err = vessel_section_iterator_create(&iter, ectx.sec_ctx, ext);
  if (VESSEL_ERR_SUCCESS != err) {
    std::cerr << "Could not create section iterator: "
      << vessel_error_name(err) << " // " << vessel_error_message(err)
      << std::endl;
    goto cleanup;
  }

  err = vessel_section_iterator_next(&section, iter);
  while (VESSEL_ERR_SUCCESS == err ) {
    process_section(ectx, section, topic, export_all);
    err = vessel_section_iterator_next(&section, iter);
  }
  if (VESSEL_ERR_END_ITERATION != err) {
    std::cerr << "Unexpected error: " << vessel_error_name(err) << " // "
      << vessel_error_message(err) << std::endl;
    goto cleanup;
  }

cleanup:
  vessel_section_iterator_free(&iter);
}



VESSEL_PRIVATE int
do_export(context const & ctx)
{
  // Health check
  export_context ectx{ctx};
  if (!export_health_check(ectx, ctx)) {
    exit_log(VEXIT_CMD_HEALTH, "export") << "Invalid option(s).";
  }

  // Create resource
  auto err = ectx.initialize_resource();
  if (VESSEL_ERR_SUCCESS != err) {
    exit_log(VEXIT_CMD_INIT, "export", err);
  }

  // The logic is fairly straightforward:
  // 1. We iterate through all extents.
  // 2. We iterate through all sections of the extents.
  // 3. If the section topic does not match our topic filter, we skip the
  //    section.
  // 4. Whenever we encounter a content-type section, that's a new file
  //    for this topic. We close any existing file in the topic.
  // 5. Whenever we encounter a blob section, if we have an open file for
  //    the same topic, we write the blob to that file.

  vessel_extent_iterator * iter = nullptr;
  err = vessel_extent_iterator_create(&iter, ectx.res);
  if (VESSEL_ERR_SUCCESS != err) {
    exit_log(VEXIT_CMD_EXTENT, "export", err);
  }

  vessel_extent * ext = nullptr;
  err = vessel_extent_iterator_next(&ext, iter);
  while (VESSEL_ERR_SUCCESS == err) {
    process_extent(ectx, ext, ctx.opts.topic, ctx.opts.export_all);
    err = vessel_extent_iterator_next(&ext, iter);
  }
  if (VESSEL_ERR_END_ITERATION != err) {
    vessel_extent_iterator_free(&iter);
    exit_log(VEXIT_CMD_EXTENT, "export", err);
  }

  err = vessel_extent_iterator_free(&iter);
  if (VESSEL_ERR_SUCCESS != err) {
    exit_log(VEXIT_CMD_EXTENT, "export", err);
  }

  return 0;
}



VESSEL_PRIVATE command_func
export_command()
{
  return do_export;
}

} // namespace vessel::cli
