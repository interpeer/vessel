/*
 * This file is part of vessel.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2023 Interpeer gUG (haftungsbeschränkt).
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "info.h"
#include <build-config.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <errno.h>

#if defined(VESSEL_HAVE_STAT)
#  define vessel_stat stat
#endif
#if defined(VESSEL_HAVE__STAT)
#  define vessel_stat _stat
#endif

#include <vessel/store.h>
#include <vessel/resource.h>

#include <iostream>
#include <fstream>

#include <liberate/random/unsafe_bits.h>
#include <liberate/string/util.h>
#include <liberate/string/hexencode.h>

#include "../errors.h"

namespace vessel::cli {


// FIXME this belongs in liberate
template<typename... argsT>
inline std::string
strformat(std::string const & format, argsT... args)
{
   auto fsize = 1 + std::snprintf(nullptr, 0, format.c_str(), args...);
   if (fsize <= 0) {
     return {};
   }
   std::vector<char> buf;
   buf.resize(fsize);

   std::snprintf(&buf[0], fsize, format.c_str(), args...);
   return {buf.begin(), buf.end()};
}

// FIXME this belongs in liberate
#if 1
inline bool
file_exists(std::string const & filename)
{
  struct vessel_stat buf;
  int err = vessel_stat(filename.c_str(), &buf);
  if (err < 0) {
#if defined(DEBUG)
    switch (errno) {
      case ENOENT:
        std::cerr << "File at path '" << filename << "' cannot be found." << std::endl;
        break;

      case EACCES:
        std::cerr << "Access permissions missing for file at path '" << filename << "'." << std::endl;
        break;

      default:
        std::cerr << "Unknown error accessing file at path '" << filename << "'." << std::endl;
        break;
    }
#endif
    return false;
  }
  return true;
}
#endif


struct info_context
{
  context const &   ctx;

  vessel_extent *           extent = nullptr;

  vessel_backing_store *    store = nullptr;
  vessel_resource *         res = nullptr;
  vessel_section_context *  sec_ctx = nullptr;

  inline info_context(context const & _ctx)
    : ctx{_ctx}
  {
  }

  inline ~info_context()
  {
    vessel_section_context_free(&sec_ctx);
    vessel_resource_free(&res);
    vessel_backing_store_free(&store);
  }

  inline vessel_error_t
  initialize_resource()
  {
    // Create section context
    auto err = vessel_section_context_create(&sec_ctx, ctx.algo_ctx);
    if (VESSEL_ERR_SUCCESS != err) {
      return err;
    }

    // Create a file based store.
    err = vessel_backing_store_create_file(&store, ctx.algo_ctx,
        ctx.opts.resource_file.c_str());
    if (VESSEL_ERR_SUCCESS != err) {
      return err;
    }

    // Create a resource with this store. Note that if the file already represents
    // a resrouce, resource information will be loaded at this point.
    err = vessel_resource_create(&res, ctx.ctx, store, nullptr);
    if (VESSEL_ERR_SUCCESS != err) {
      return err;
    }

    return VESSEL_ERR_SUCCESS;
  }
};



inline bool
info_health_check(info_context & ictx [[maybe_unused]], context const & ctx)
{
  // XXX Be aware that there are races possible between checking a file exists and
  //     using the file. That'll pop up errors later, though. This should is just
  //     a quick health check.

  // Check output file exists (only if append flag is given)
  if (!file_exists(ctx.opts.resource_file)) {
    std::cerr << "Resource at path '" << ctx.opts.resource_file << "' does not exist." << std::endl;
    return false;
  }

  // All good
  return true;
}



inline std::string
truncate_id(std::string const & input, ssize_t short_part)
{
  if (short_part < 0) {
    return input;
  }

  if (input.size() < static_cast<size_t>(short_part * 2)) {
    return input;
  }

  std::string res;
  res += std::string{input.begin(), input.begin() + short_part};
  res += "...";
  res += std::string{input.begin() + input.size() - short_part, input.end()};
  return res;
}



inline std::string
maybe_truncate_id(std::string const & input, bool full_id)
{
  if (full_id) {
    return input;
  }
  return truncate_id(input, 8);
}


inline std::string
section_to_string(vessel_section_t type)
{
  switch (type) {
    case VESSEL_SECTION_CRC32:
      return "CRC32";
    case VESSEL_SECTION_MAC:
      return "MAC";
    case VESSEL_SECTION_SIGNATURE:
      return "SIGNATURE";
    case VESSEL_SECTION_CONTENT_TYPE:
      return "CONTENT_TYPE";
    case VESSEL_SECTION_BLOB:
      return "BLOB";

    default:
      break;
  }
  return "<unknown section type>";
}

inline std::string
topic_to_string(vessel_topic_t type)
{
  switch (type) {
    case VESSEL_TOPIC_AAA:
      return "AAA";
    case VESSEL_TOPIC_METADATA:
      return "METADATA";

    default:
      break;
  }
  return "<unknown topic type>";
}


inline void
dump_section(info_context const & ictx, vessel_section const & section)
{
  std::cout << strformat("\tSection %d, topic %d", section.type, section.topic) << std::endl;
  std::cout << strformat("\t\ttype:         %s", section_to_string(section.type).c_str()) << std::endl;
  std::cout << strformat("\t\ttopic:        %s", topic_to_string(section.topic).c_str()) << std::endl;
  std::cout << strformat("\t\tpayload size: %zd", section.payload_size) << std::endl;

  if (ictx.ctx.opts.verbose) {
    liberate::string::canonical_hexdump hd;
    std::cout << hd(section.payload, section.payload_size) << std::endl;
  }
}



inline void
dump_sections(info_context const & ictx, vessel_extent * ext)
{
  vessel_section_iterator * iter = nullptr;
  vessel_section section;

  auto err = vessel_section_iterator_create(&iter, ictx.sec_ctx, ext);
  if (VESSEL_ERR_SUCCESS != err) {
    std::cerr << "Could not create section iterator: "
      << vessel_error_name(err) << " // " << vessel_error_message(err)
      << std::endl;
    goto cleanup;
  }

  err = vessel_section_iterator_next(&section, iter);
  while (VESSEL_ERR_SUCCESS == err) {
    dump_section(ictx, section);
    err = vessel_section_iterator_next(&section, iter);
  }
  if (VESSEL_ERR_END_ITERATION != err) {
    std::cerr << "Unexpected error: " << vessel_error_name(err) << " // "
      << vessel_error_message(err) << std::endl;
    goto cleanup;
  }

cleanup:
  vessel_section_iterator_free(&iter);
}



inline void
dump_info(info_context const & ictx, vessel_extent * ext, bool sections = false, bool full_ids = false)
{
  vessel_extent_id * id = nullptr;
  vessel_extent_id * parent_id = nullptr;
  vessel_author_id * author_id = nullptr;
  size_t blocksize = 0;
  size_t payload_size = 0;
  uint32_t tag = 0; // Envelope version 00
  vessel_algorithm_choices choices;
  char tmp_buf[200] = {};
  size_t tmp_size = sizeof(tmp_buf);
  std::string hex_eid;
  std::string hex_parent_eid;
  std::string hex_aid;
  std::string choices_str;

  // Extract choices first; this gives info on how to format other parts
  auto err = vessel_extent_algorithm_choices(&choices,
      ictx.ctx.algo_ctx,
      ext);
  if (VESSEL_ERR_SUCCESS != err) {
    std::cerr << "Could not extract algorithm choices from extent." << std::endl;
    goto cleanup;
  }

  tmp_size = sizeof(tmp_buf);
  err = vessel_algorithm_choices_serialize(tmp_buf, &tmp_size, &choices);
  if (VESSEL_ERR_SUCCESS != err) {
    std::cerr << "Could not serialize extracted algorithm choices." << std::endl;
    goto cleanup;
  }
  choices_str = std::string{tmp_buf, tmp_buf + tmp_size};

  // Extent ID
  err = vessel_extent_identifier(&id, ext);
  if (VESSEL_ERR_SUCCESS != err) {
    std::cerr << "Could not extract extent identifier." << std::endl;
    goto cleanup;
  }
  tmp_size = sizeof(tmp_buf);
  err = vessel_extent_id_to_buffer(tmp_buf, &tmp_size, &choices, id);
  if (VESSEL_ERR_SUCCESS != err) {
    std::cerr << "Could not serialize extracted extent identifier." << std::endl;
    goto cleanup;
  }
  hex_eid = liberate::string::hexencode(tmp_buf, tmp_size);

  // Previous extent ID
  err = vessel_extent_previous_identifier(&parent_id, ext);
  if (VESSEL_ERR_SUCCESS != err) {
    std::cerr << "Could not extract extent previous identifier." << std::endl;
    goto cleanup;
  }
  tmp_size = sizeof(tmp_buf);
  err = vessel_extent_id_to_buffer(tmp_buf, &tmp_size, &choices, parent_id);
  if (VESSEL_ERR_SUCCESS != err) {
    std::cerr << "Could not serialize extracted extent previous identifier." << std::endl;
    goto cleanup;
  }
  hex_parent_eid = liberate::string::hexencode(tmp_buf, tmp_size);

  // Author ID
  err = vessel_extent_author_identifier(&author_id, ext);
  if (VESSEL_ERR_SUCCESS != err) {
    std::cerr << "Could not extract author identifier." << std::endl;
    goto cleanup;
  }
  tmp_size = sizeof(tmp_buf);
  err = vessel_author_id_to_buffer(tmp_buf, &tmp_size, &choices, author_id);
  if (VESSEL_ERR_SUCCESS != err) {
    std::cerr << "Could not serialize extracted author identifier." << std::endl;
    goto cleanup;
  }
  hex_aid = liberate::string::hexencode(tmp_buf, tmp_size);

  // Sizes
  err = vessel_extent_size_in_blocks(&blocksize, ext);
  if (VESSEL_ERR_SUCCESS != err) {
    std::cerr << "Could not extract extent size." << std::endl;
    goto cleanup;
  }

  err = vessel_extent_payload_size(&payload_size, ext);
  if (VESSEL_ERR_SUCCESS != err) {
    std::cerr << "Could not extract extent payload size." << std::endl;
    goto cleanup;
  }

  // Version tag
  err = vessel_extent_version_tag(&tag, sizeof(tag), ext);
  if (VESSEL_ERR_SUCCESS != err) {
    std::cerr << "Could not extract extent version tag." << std::endl;
    goto cleanup;
  }

  // Start outputting the main bits
  std::cout << strformat("Extent %s:", maybe_truncate_id(hex_eid, full_ids).c_str()) << std::endl;
  std::cout << strformat("\tprevious:    %s", maybe_truncate_id(hex_parent_eid, full_ids).c_str()) << std::endl;
  std::cout << strformat("\tauthor:      %s", maybe_truncate_id(hex_aid, full_ids).c_str()) << std::endl;
  std::cout << strformat("\tversion tag: %zx", tag) << std::endl;
  std::cout << strformat("\talgorithms:  %s", choices_str.c_str()) << std::endl;
  std::cout << strformat("\tsize:        %zd", blocksize * vessel_extent_block_size()) << std::endl;
  std::cout << strformat("\tpayload:     %zd", payload_size) << std::endl;

  if (sections) {
    dump_sections(ictx, ext);
  }

cleanup:
  vessel_extent_id_free(&id);
  vessel_extent_id_free(&parent_id);
  vessel_author_id_free(&author_id);
}



VESSEL_PRIVATE int
do_info(context const & ctx)
{
  // Health check
  info_context ictx{ctx};
  if (!info_health_check(ictx, ctx)) {
    exit_log(VEXIT_CMD_HEALTH, "info") << "Invalid option(s).";
  }

  // Create resource
  auto err = ictx.initialize_resource();
  if (VESSEL_ERR_SUCCESS != err) {
    exit_log(VEXIT_CMD_INIT, "info", err);
  }

  vessel_extent_iterator * iter = nullptr;
  err = vessel_extent_iterator_create(&iter, ictx.res);
  if (VESSEL_ERR_SUCCESS != err) {
    exit_log(VEXIT_CMD_EXTENT, "info", err);
  }

  vessel_extent * ext = nullptr;
  err = vessel_extent_iterator_next(&ext, iter);
  while (VESSEL_ERR_SUCCESS == err) {
    dump_info(ictx, ext, ctx.opts.sections, ctx.opts.full_ids);
    err = vessel_extent_iterator_next(&ext, iter);
  }
  if (VESSEL_ERR_END_ITERATION != err) {
    vessel_extent_iterator_free(&iter);
    exit_log(VEXIT_CMD_EXTENT, "info", err);
  }

  err = vessel_extent_iterator_free(&iter);
  if (VESSEL_ERR_SUCCESS != err) {
    exit_log(VEXIT_CMD_EXTENT, "info", err);
  }

  return 0;
}



VESSEL_PRIVATE command_func
info_command()
{
  return do_info;
}

} // namespace vessel::cli
