/*
 * This file is part of vessel.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2023 Interpeer gUG (haftungsbeschränkt).
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "import.h"
#include <build-config.h>

#if defined(VESSEL_HAVE_LIBMAGIC)
#include <magic.h>
#endif // VESSEL_HAVE_LIBMAGIC

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <errno.h>

#if defined(VESSEL_HAVE_STAT)
#  define vessel_stat stat
#endif
#if defined(VESSEL_HAVE__STAT)
#  define vessel_stat _stat
#endif

#include <vessel/store.h>
#include <vessel/resource.h>

#include <iostream>
#include <fstream>

#include <liberate/random/unsafe_bits.h>
#include <liberate/string/util.h>

#include "../errors.h"

namespace vessel::cli {

static constexpr char const CT_FILENAME[] = "filename";
static constexpr char const CT_MEDIA_TYPE[] = "media-type";
static constexpr char const CT_CHARSET[] = "charset";

static constexpr size_t MIN_BLOCK_SIZE = 1024;
static constexpr size_t READ_BLOCK_SIZE = 16 * 1024;

inline std::string
get_mimetype(std::vector<char> const & buffer)
{
#if defined(VESSEL_HAVE_LIBMAGIC)
  int flags = MAGIC_MIME | MAGIC_PRESERVE_ATIME;
  auto cookie = magic_open(flags);
  auto err = magic_load(cookie, nullptr);
  if (err) {
    std::cerr << "Could not load system magic file!" << std::endl;
    magic_close(cookie);
    return {};
  }

  auto type = magic_buffer(cookie, buffer.data(), buffer.size());
  std::string ret;
  if (type) {
    // The copy needs to be made before magic_close, or the pointer is
    // garbage. If it is non-NULL, at any rate.
    ret = type;
  }

  magic_close(cookie);
  return ret;
#else // VESSEL_HAVE_LIBMAGIC
  return {};
#endif // VESSEL_HAVE_LIBMAGIC
}


inline bool
file_exists(std::string const & filename)
{
  struct vessel_stat buf;
  int err = vessel_stat(filename.c_str(), &buf);
  if (err < 0) {
#if defined(DEBUG)
    switch (errno) {
      case ENOENT:
        std::cerr << "File at path '" << filename << "' cannot be found." << std::endl;
        break;

      case EACCES:
        std::cerr << "Access permissions missing for file at path '" << filename << "'." << std::endl;
        break;

      default:
        std::cerr << "Unknown error accessing file at path '" << filename << "'." << std::endl;
        break;
    }
#endif
    return false;
  }
  return true;
}


struct import_context
{
  context const &   ctx;

  bool                      resource_exists;
  std::vector<char>         input_buf{};
  size_t                    input_offset = 0;
  uint16_t                  starting_topic;

  vessel_extent *           extent = nullptr;

  vessel_backing_store *    store = nullptr;
  vessel_resource *         res = nullptr;
  vessel_section_context *  sec_ctx = nullptr;

  inline import_context(context const & _ctx)
    : ctx{_ctx}
  {
  }

  inline ~import_context()
  {
    vessel_section_context_free(&sec_ctx);
    vessel_resource_free(&res);
    vessel_backing_store_free(&store);
  }

  inline vessel_error_t
  initialize_resource()
  {
    // Create section context
    auto err = vessel_section_context_create(&sec_ctx, ctx.algo_ctx);
    if (VESSEL_ERR_SUCCESS != err) {
      return err;
    }

    // Create a file based store.
    err = vessel_backing_store_create_file(&store, ctx.algo_ctx,
        ctx.opts.resource_file.c_str());
    if (VESSEL_ERR_SUCCESS != err) {
      return err;
    }

    // Create a resource with this store. Note that if the file already represents
    // a resrouce, resource information will be loaded at this point.
    err = vessel_resource_create(&res, ctx.ctx, store, nullptr);
    if (VESSEL_ERR_SUCCESS != err) {
      return err;
    }

    return VESSEL_ERR_SUCCESS;
  }

};



struct input
{
  import_context &          ictx;
  std::filesystem::path     fname;

  uint16_t                  topic = 0;
  std::string               mime_type = {};
  std::istream *            stream = nullptr;

  vessel_content_type *     ct = nullptr;

  inline input(import_context & _ictx, std::filesystem::path const & _fname)
    : ictx{_ictx}
    , fname{_fname}
    , topic{_ictx.starting_topic++}
  {
  }

  inline ~input()
  {
    if (stream != &std::cin) {
      delete stream;
      stream = nullptr;
    }

    vessel_content_type_free(&ct);
  }

  inline vessel_error_t
  add_content_type()
  {
    // We first need to check the input, and create a stream.
    if (fname.empty() || fname == "-") {
      // Use std::cin
      stream = &std::cin;
    }
    else {
      // It's a file; try to open it.
      auto file = new std::ifstream{fname, std::ios::binary};
      if (!file->is_open()) {
        delete file;
        std::cerr << "Cannot import non-existent input: " << fname << std::endl;
        return VESSEL_ERR_IO;
      }
      stream = file;
    }

    // Read some input
    auto read = read_some();
    if (!read) {
      std::cerr << "Unable to read from input file: " << fname << std::endl;
      return VESSEL_ERR_IO;
    }

    // Detect mime type
    if (ictx.ctx.opts.mime_type == "application/octet-stream") {
      // Try to detect mime type
      mime_type = get_mimetype(ictx.input_buf);
      if (mime_type.empty()) {
        mime_type = "application/octet-stream";
      }
    }
    else {
      mime_type = ictx.ctx.opts.mime_type;
    }

    // Create vessel content type from this
    auto err = vessel_content_type_create(&ct);
    if (VESSEL_ERR_SUCCESS != err) {
      return err;
    }

    if (!fname.empty()) {
      auto fname_u8 = fname.filename().generic_u8string();
      err = vessel_content_type_set(ct,
          CT_FILENAME, sizeof(CT_FILENAME),
          fname_u8.c_str(), fname_u8.size());
      if (VESSEL_ERR_SUCCESS != err) {
        return err;
      }
    }

    // The mime type may need to be split according to delimiters. Technically,
    // this should only be a space and semicolon, if you believe RFC2045
    auto split = liberate::string::split(mime_type, "; ");
    err = vessel_content_type_set(ct,
        CT_MEDIA_TYPE, sizeof(CT_MEDIA_TYPE),
        split[0].c_str(), split[0].size());
    if (VESSEL_ERR_SUCCESS != err) {
      return err;
    }
    split.erase(split.begin());

    for (auto & candidate : split) {
      auto kv = liberate::string::split(candidate, "=");
      if (kv.size() < 2) {
        continue;
      }
      if (liberate::string::to_lower(kv[0]) == "charset") {
        // Got a charset, so we can set it.
        err = vessel_content_type_set(ct,
            CT_CHARSET, sizeof(CT_CHARSET),
            kv[1].c_str(), kv[1].size());
        if (VESSEL_ERR_SUCCESS != err) {
          return err;
        }
      }
    }

    // Write content type
    err = vessel_resource_add_content_type(ictx.res,
        &ictx.extent,
        &ictx.ctx.algos,
        topic,
        ct,
        ictx.ctx.opts.extent_size_multiplier,
        ictx.ctx.author, VESSEL_MF_NONE);
    if (VESSEL_ERR_SUCCESS != err) {
      return err;
    }

    return VESSEL_ERR_SUCCESS;
  }


  inline size_t
  read_some()
  {
    // Read some of input. We can freely resize this because vector does not
    // shrink the actually allocated size, just how much its API thinks is
    // available.
    ictx.input_buf.resize(READ_BLOCK_SIZE);
    stream->read(&(ictx.input_buf[0]), READ_BLOCK_SIZE);
    ictx.input_buf.resize(stream->gcount());
    return ictx.input_buf.size();
  }


  inline vessel_error_t
  import_file_contents()
  {
    ictx.input_offset = 0;

    // We read data in fixed sized chunks. We then iteratively try to add
    // BLOB sections to the current extent until that fails.
    while (true) {
      // Drain buffer before reading more
      size_t to_write = ictx.input_buf.size() - ictx.input_offset;
      while (to_write) {
        // Make space for the buffer
        auto err = vessel_resource_make_space_for_section(ictx.res,
            &ictx.extent, &ictx.ctx.algos,
            VESSEL_SECTION_BLOB, topic,
            MIN_BLOCK_SIZE, &to_write,
            ictx.ctx.opts.extent_size_multiplier,
            ictx.ctx.author, VESSEL_MF_NONE);
        if (VESSEL_ERR_SUCCESS != err) {
          return err;
        }

        // Add the amount we can write as a BLOB section
        err = vessel_resource_add_section(ictx.res, ictx.extent,
            VESSEL_SECTION_BLOB, topic,
            ictx.input_buf.data() + ictx.input_offset,
            to_write);
        if (VESSEL_ERR_SUCCESS != err) {
          return err;
        }

        // Once this is written, adjust the offset
        ictx.input_offset += to_write;
        to_write = ictx.input_buf.size() - ictx.input_offset;
      }

      // Read from the input stream again.
      auto read = read_some();
      if (!read) {
        break;
      }
      to_write = ictx.input_buf.size();
      ictx.input_offset = 0;
    }

    // If we exited this loop, then our extent is the last one. We should commit
    // and release it.
    auto err = vessel_resource_commit(ictx.res, ictx.extent);
    if (VESSEL_ERR_SUCCESS != err) {
      return err;
    }

    err = vessel_resource_release(ictx.res, &ictx.extent);
    if (VESSEL_ERR_SUCCESS != err) {
      return err;
    }

    // All done
    return VESSEL_ERR_SUCCESS;
  }
};



inline bool
import_health_check(import_context & ictx, context const & ctx)
{
  // XXX Be aware that there are races possible between checking a file exists and
  //     using the file. That'll pop up errors later, though. This should is just
  //     a quick health check.

  // Check output file exists (only if append flag is given)
  ictx.resource_exists = file_exists(ctx.opts.resource_file);
  if (ictx.resource_exists && !ctx.opts.import_append) {
    std::cerr << "Resource at path '" << ctx.opts.resource_file << "' already exists, use the -a flag to append or choose a different path." << std::endl;
    return false;
  }

  // Select topic
  if (ctx.opts.topic < 1024) {
    liberate::random::unsafe_bits<uint16_t> rng{1024};
    ictx.starting_topic = rng.get();
  }
  else {
    ictx.starting_topic = ctx.opts.topic;
  }

  // Check input file(s). None of them should be a flag.
  for (auto & fname : ctx.opts.input_files) {
    if (fname != "-" && fname.c_str()[0] == '-') {
      std::cerr << "Input file seems to be a flag: " << fname << std::endl;
      return false;
    }
  }

  // All good
  return true;
}


inline void
process_input(input & in)
{
  auto err = in.add_content_type();
  if (VESSEL_ERR_SUCCESS != err) {
    exit_log(VEXIT_CMD_CT, "import", err);
  }

  err = in.import_file_contents();
  if (VESSEL_ERR_SUCCESS != err) {
    exit_log(VEXIT_CMD_DATA, "import", err);
  }
}



VESSEL_PRIVATE int
do_import(context const & ctx)
{
  // Health check
  import_context ictx{ctx};
  if (!import_health_check(ictx, ctx)) {
    exit_log(VEXIT_CMD_HEALTH, "import") << "Invalid option(s).";
  }

  // Create resource
  auto err = ictx.initialize_resource();
  if (VESSEL_ERR_SUCCESS != err) {
    exit_log(VEXIT_CMD_INIT, "import", err);
  }

  // Import file(s)
  if (ctx.opts.input_files.empty()) {
    // Use std::cin as an input
    input in{ictx, {}};
    process_input(in);
  }
  else {
    for (auto & fname : ctx.opts.input_files) {
      input in{ictx, fname};
      process_input(in);
    }
  }

  return 0;
}



VESSEL_PRIVATE command_func
import_command()
{
  return do_import;
}

} // namespace vessel::cli
