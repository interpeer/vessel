/*
 * This file is part of vessel.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2023 Interpeer gUG (haftungsbeschränkt).
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "ls.h"
#include <build-config.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <errno.h>

#if defined(VESSEL_HAVE_STAT)
#  define vessel_stat stat
#endif
#if defined(VESSEL_HAVE__STAT)
#  define vessel_stat _stat
#endif

#include <vessel/store.h>
#include <vessel/resource.h>

#include <iostream>
#include <fstream>

#include <liberate/random/unsafe_bits.h>
#include <liberate/string/util.h>
#include <liberate/string/hexencode.h>

#include "../errors.h"

namespace vessel::cli {


// FIXME this belongs in liberate
template<typename... argsT>
inline std::string
strformat(std::string const & format, argsT... args)
{
   auto fsize = 1 + std::snprintf(nullptr, 0, format.c_str(), args...);
   if (fsize <= 0) {
     return {};
   }
   std::vector<char> buf;
   buf.resize(fsize);

   std::snprintf(&buf[0], fsize, format.c_str(), args...);
   return {buf.begin(), buf.end()};
}

// FIXME this belongs in liberate
#if 1
inline bool
file_exists(std::string const & filename)
{
  struct vessel_stat buf;
  int err = vessel_stat(filename.c_str(), &buf);
  if (err < 0) {
#if defined(DEBUG)
    switch (errno) {
      case ENOENT:
        std::cerr << "File at path '" << filename << "' cannot be found." << std::endl;
        break;

      case EACCES:
        std::cerr << "Access permissions missing for file at path '" << filename << "'." << std::endl;
        break;

      default:
        std::cerr << "Unknown error accessing file at path '" << filename << "'." << std::endl;
        break;
    }
#endif
    return false;
  }
  return true;
}
#endif

// FIXME also in import command
static constexpr char const CT_FILENAME[] = "filename";
static constexpr char const CT_MEDIA_TYPE[] = "media-type";
static constexpr char const CT_CHARSET[] = "charset";

struct ls_context
{
  context const &   ctx;

  vessel_extent *           extent = nullptr;

  vessel_backing_store *    store = nullptr;
  vessel_resource *         res = nullptr;
  vessel_section_context *  sec_ctx = nullptr;

  struct entry
  {
    std::string     name{};
    std::string     mime{};
    std::string     charset{};
    size_t          size = 0;
    vessel_topic_t  topic = 0;
  };

  std::unique_ptr<entry> current_entry;
  std::multimap<std::string, std::unique_ptr<entry>> entries;

  inline ls_context(context const & _ctx)
    : ctx{_ctx}
  {
  }

  inline ~ls_context()
  {
    vessel_section_context_free(&sec_ctx);
    vessel_resource_free(&res);
    vessel_backing_store_free(&store);
  }

  inline vessel_error_t
  initialize_resource()
  {
    // Create section context
    auto err = vessel_section_context_create(&sec_ctx, ctx.algo_ctx);
    if (VESSEL_ERR_SUCCESS != err) {
      return err;
    }

    // Create a file based store.
    err = vessel_backing_store_create_file(&store, ctx.algo_ctx,
        ctx.opts.resource_file.c_str());
    if (VESSEL_ERR_SUCCESS != err) {
      return err;
    }

    // Create a resource with this store. Note that if the file already represents
    // a resrouce, resource information will be loaded at this point.
    err = vessel_resource_create(&res, ctx.ctx, store, nullptr);
    if (VESSEL_ERR_SUCCESS != err) {
      return err;
    }

    return VESSEL_ERR_SUCCESS;
  }


  inline void print_results()
  {
    if (current_entry) {
      entries.insert({current_entry->name, std::move(current_entry)});
    }

    // We limit the media type to 30, charset to 10
    // TODO CLI option for unlimited/unaligned output

    size_t total = 0;
    for (auto & e : entries) {
      auto & f = e.second;

      std::cout << strformat("%-30s  %-10s  %12zd  %s",
          f->mime.c_str(), f->charset.c_str(), f->size, f->name.c_str())
        << std::endl;
      total += f->size;
    }

    std::cout << strformat("total: %zd entries with %zd bytes.",
        entries.size(), total) << std::endl;
  }
};



inline bool
info_health_check(ls_context & lctx [[maybe_unused]], context const & ctx)
{
  // XXX Be aware that there are races possible between checking a file exists and
  //     using the file. That'll pop up errors later, though. This should is just
  //     a quick health check.

  // Check output file exists (only if append flag is given)
  if (!file_exists(ctx.opts.resource_file)) {
    std::cerr << "Resource at path '" << ctx.opts.resource_file << "' does not exist." << std::endl;
    return false;
  }

  // All good
  return true;
}



inline void
process_content_type(ls_context & lctx, vessel_section const & section)
{
  vessel_content_type * ct = nullptr;
  char buf[200];
  size_t bufsize = sizeof(buf);

  // Store previous entry
  if (lctx.current_entry) {
    lctx.entries.insert({lctx.current_entry->name, std::move(lctx.current_entry)});
  }

  // Start new entry
  lctx.current_entry = std::make_unique<ls_context::entry>();
  lctx.current_entry->topic = section.topic;

  // Grab content type and fill entry
  auto ptr = static_cast<char *>(section.payload);
  auto err = vessel_content_type_from_utf8(&ct, ptr, section.payload_size);
  if (VESSEL_ERR_SUCCESS != err) {
    std::cerr << "Could not parse content type, ignoring." << std::endl;
    goto cleanup;
  }

  bufsize = sizeof(buf);
  err = vessel_content_type_get(buf, &bufsize, ct,
    CT_FILENAME, sizeof(CT_FILENAME));
  if (VESSEL_ERR_SUCCESS == err) {
    lctx.current_entry->name = std::string{buf, buf + bufsize};
  }

  bufsize = sizeof(buf);
  err = vessel_content_type_get(buf, &bufsize, ct,
    CT_CHARSET, sizeof(CT_CHARSET));
  if (VESSEL_ERR_SUCCESS == err) {
    lctx.current_entry->charset = std::string{buf, buf + bufsize};
  }

  bufsize = sizeof(buf);
  err = vessel_content_type_get(buf, &bufsize, ct,
    CT_MEDIA_TYPE, sizeof(CT_MEDIA_TYPE));
  if (VESSEL_ERR_SUCCESS == err) {
    lctx.current_entry->mime = std::string{buf, buf + bufsize};
  }

cleanup:
  vessel_content_type_free(&ct);
}



inline void
process_blob(ls_context & lctx, vessel_section const & section)
{
  if (!lctx.current_entry) {
    // std::cerr << "ignoring without ct" << std::endl;
    return;
  }
  if (lctx.current_entry->topic != section.topic) {
    // std::cerr << "Ignoring due to differing topic" << std::endl;
    return;
  }

  lctx.current_entry->size += section.payload_size;
}




inline void
process_extent(ls_context & lctx, vessel_extent * ext)
{
  vessel_section_iterator * iter = nullptr;
  vessel_section section;

  auto err = vessel_section_iterator_create(&iter, lctx.sec_ctx, ext);
  if (VESSEL_ERR_SUCCESS != err) {
    std::cerr << "Could not create section iterator: "
      << vessel_error_name(err) << " // " << vessel_error_message(err)
      << std::endl;
    goto cleanup;
  }

  err = vessel_section_iterator_next(&section, iter);
  while (VESSEL_ERR_SUCCESS == err) {
    if (VESSEL_SECTION_CONTENT_TYPE == section.type) {
      process_content_type(lctx, section);
    }
    else if (VESSEL_SECTION_BLOB == section.type) {
      process_blob(lctx, section);
    }
    err = vessel_section_iterator_next(&section, iter);
  }
  if (VESSEL_ERR_END_ITERATION != err) {
    std::cerr << "Unexpected error: " << vessel_error_name(err) << " // "
      << vessel_error_message(err) << std::endl;
    goto cleanup;
  }

cleanup:
  vessel_section_iterator_free(&iter);
}



VESSEL_PRIVATE int
do_ls(context const & ctx)
{
  // Health check
  ls_context lctx{ctx};
  if (!info_health_check(lctx, ctx)) {
    exit_log(VEXIT_CMD_HEALTH, "info") << "Invalid option(s).";
  }

  // Create resource
  auto err = lctx.initialize_resource();
  if (VESSEL_ERR_SUCCESS != err) {
    exit_log(VEXIT_CMD_INIT, "info", err);
  }

  vessel_extent_iterator * iter = nullptr;
  err = vessel_extent_iterator_create(&iter, lctx.res);
  if (VESSEL_ERR_SUCCESS != err) {
    exit_log(VEXIT_CMD_EXTENT, "info", err);
  }

  vessel_extent * ext = nullptr;
  err = vessel_extent_iterator_next(&ext, iter);
  while (VESSEL_ERR_SUCCESS == err) {
    process_extent(lctx, ext);
    err = vessel_extent_iterator_next(&ext, iter);
  }
  if (VESSEL_ERR_END_ITERATION != err) {
    vessel_extent_iterator_free(&iter);
    exit_log(VEXIT_CMD_EXTENT, "info", err);
  }

  err = vessel_extent_iterator_free(&iter);
  if (VESSEL_ERR_SUCCESS != err) {
    exit_log(VEXIT_CMD_EXTENT, "info", err);
  }

  // Ok, now output the entries
  lctx.print_results();

  return 0;
}



VESSEL_PRIVATE command_func
ls_command()
{
  return do_ls;
}

} // namespace vessel::cli
