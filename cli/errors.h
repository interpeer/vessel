/*
 * This file is part of vessel.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2023 Interpeer gUG (haftungsbeschränkt).
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef CLI_ERRORS_H
#define CLI_ERRORS_H

#include <string>
#include <ostream>

namespace vessel::cli {

enum vessel_exit_code : int
{
  // General
  VEXIT_OK          = 0,

  // Setup
  VEXIT_ALGOS       = 10,
  VEXIT_INIT        = 11,
  VEXIT_AUTHOR      = 12,

  // Main
  VEXIT_HELP        = 20,
  VEXIT_NO_CMD      = 21,

  // Commands (general)
  VEXIT_CMD_HEALTH  = -1,
  VEXIT_CMD_INIT    = -2,

  // Commands: import
  VEXIT_CMD_CT      = -10,
  VEXIT_CMD_DATA    = -11,

  // Commands: info
  VEXIT_CMD_EXTENT  = -20,
};


// Convenience structure for well-defined exits
struct exit_log_impl
{
  std::string       fname{};
  size_t            line;

  vessel_exit_code  code;
  std::ostream &    os;
  std::string       scope{"main"};
  bool              err_logged = false;

  inline exit_log_impl(std::string _fname, size_t _line,
      vessel_exit_code _code,
      std::ostream & _os = std::cerr)
    : fname{_fname}
    , line{_line}
    , code{_code}
    , os{_os}
  {
    log_scope();
    log_file();
  }

  inline exit_log_impl(std::string _fname, size_t _line,
      vessel_exit_code _code, vessel_error_t err,
      std::ostream & _os = std::cerr)
    : fname{_fname}
    , line{_line}
    , code{_code}
    , os{_os}
  {
    log_scope();
    log_file();
    log_err(err);
  }

  inline exit_log_impl(std::string _fname, size_t _line,
      vessel_exit_code _code, std::string const & _scope,
      std::ostream & _os = std::cerr)
    : fname{_fname}
    , line{_line}
    , code{_code}
    , os{_os}
    , scope{_scope}
  {
    log_scope();
    log_file();
  }

  inline exit_log_impl(std::string _fname, size_t _line,
      vessel_exit_code _code, std::string const & _scope,
      vessel_error_t err,
      std::ostream & _os = std::cerr)
    : fname{_fname}
    , line{_line}
    , code{_code}
    , os{_os}
    , scope{_scope}
  {
    log_scope();
    log_file();
    log_err(err);
  }


  inline ~exit_log_impl()
  {
    os << std::endl;
    exit(code);
  }

  template <typename T>
  inline exit_log_impl &
  operator<<(T && t)
  {
    if (err_logged) {
      os << " // ";
      err_logged = false;
    }
    os << t;
    return *this;
  }

  inline void log_scope()
  {
    os << "[" << scope << "] ";
  }

  inline void log_file()
  {
#if defined(DEBUG) && !defined(NDEBUG)
  os << "(" << fname << ":" << line << ") ";
#endif
  }

  inline void log_err(vessel_error_t err)
  {
    os << vessel_error_name(err) << ": " << vessel_error_message(err);
    err_logged = true;
  }
};


#define exit_log(...) exit_log_impl(__FILE__, __LINE__, __VA_ARGS__)


} // namespace vessel::cli

#endif // guard
