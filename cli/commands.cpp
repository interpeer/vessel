/*
 * This file is part of vessel.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2023 Interpeer gUG (haftungsbeschränkt).
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "commands.h"

#include "commands/import.h"
#include "commands/info.h"
#include "commands/export.h"
#include "commands/ls.h"

namespace vessel::cli {

command_map command_registry = {
  { CMD_IMPORT, import_command() },
  { CMD_INFO, info_command() },
  { CMD_EXPORT, export_command() },
  { CMD_LS, ls_command() },
  // TODO add other commands
};

} // namespace vessel::cli
