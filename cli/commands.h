/*
 * This file is part of vessel.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2023 Interpeer gUG (haftungsbeschränkt).
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef CLI_COMMANDS_H
#define CLI_COMMANDS_H

#include <map>
#include <functional>

#include "context.h"

namespace vessel::cli {

/**
 * Commands are functions that receive a context and return an error
 * code suitable for the CLI.
 */
using command_func = std::function<int (context const & ctx)>;

/**
 * We maintain a global command map.
 */
using command_map = std::map<commands, command_func>;
extern command_map command_registry;


} // namespace vessel::cli

#endif // guard
