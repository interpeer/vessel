/*
 * This file is part of vessel.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2023 Interpeer gUG (haftungsbeschränkt).
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef CLI_OPTIONS_H
#define CLI_OPTIONS_H

#include <string>
#include <filesystem>
#include <vector>

#include <vessel/extent.h>

namespace vessel::cli {

enum VESSEL_PRIVATE commands : uint8_t
{
  CMD_IMPORT  = 0,
  CMD_INFO    = 1,
  CMD_EXPORT  = 2,
  CMD_LS      = 3,

  // TODO more?
};


struct VESSEL_PRIVATE options
{
  commands    command;

  // Shared options
  std::filesystem::path resource_file;
  std::string           algorithms = "sha3-512;sha3-512;none;sha3-512;eddsa;ed25519;kmac128;chacha20;aead;ph=1";
  std::filesystem::path author_filename;
  bool                  verbose = false;

  int32_t               topic = -1;

  // Import options
  bool                                import_append = false;
  size_t                              extent_size_multiplier = vessel_extent_default_size_in_blocks();
  std::string                         mime_type = "application/octet-stream";
  std::vector<std::filesystem::path>  input_files;

  // Info options
  bool                                sections = false;
  bool                                full_ids = false;

  // Export options
  bool                                export_all = false;
  std::string                         export_filename = {};
};

} // namespace vessel::cli

#endif // guard
