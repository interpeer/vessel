/*
 * This file is part of vessel.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2023 Interpeer gUG (haftungsbeschränkt).
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef CLI_CONTEXT_H
#define CLI_CONTEXT_H

#include <vessel/algorithms.h>
#include <vessel/author.h>

#include "options.h"

namespace vessel::cli {

struct VESSEL_PRIVATE context
{
  options     opts;

  // Algorithms
  struct vessel_algorithm_choices   algos;
  struct vessel_algorithm_context * algo_ctx = nullptr;

  // Context
  struct vessel_context *           ctx = nullptr;

  // Author
  struct vessel_author *            author = nullptr;
  struct vessel_author_id *         author_id = nullptr;

  inline ~context()
  {
    vessel_author_id_free(&author_id);
    vessel_author_free(&author);
    vessel_context_free(&ctx);
    vessel_algorithm_context_free(&algo_ctx);
    // TODO free other resources
  }
};

} // namespace vessel::cli

#endif // guard
