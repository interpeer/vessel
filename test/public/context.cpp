/**
 * This file is part of vessel.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt).
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#include <gtest/gtest.h>

#include <vessel/context.h>

namespace {

static char const * TEST_ALGOS = "sha3-512;sha3-512;none;sha3-512;eddsa;ed25519;kmac128;chacha20;aead;ph=1";

} // anonymous namespace


TEST(Context, creation)
{
  struct vessel_algorithm_context * algo_ctx = nullptr;
  auto err = vessel_algorithm_context_create_from_specs(&algo_ctx,
      &TEST_ALGOS, 1);
  ASSERT_EQ(VESSEL_ERR_SUCCESS, err);

  struct vessel_context * ctx = nullptr;
  err = vessel_context_create(&ctx, algo_ctx);
  ASSERT_EQ(VESSEL_ERR_SUCCESS, err);
  ASSERT_TRUE(ctx);

  // Ensure there's a section context, too
  ASSERT_TRUE(vessel_context_get_section_ctx(ctx));

  // Cleanup
  vessel_algorithm_context_free(&algo_ctx);
  vessel_context_free(&ctx);
}
