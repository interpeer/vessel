/**
 * This file is part of vessel.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt).
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#include <gtest/gtest.h>

#include <vessel/algorithms.h>

#include <string>
#include <regex>

namespace {

struct test_data
{
  char const * const  spec;
  bool                valid;
  uint32_t            version_tag;
} test_specs[] = {
  // First example is from specs
  { "sha3-512;sha3-512;none;sha3-512;eddsa;ed25519;kmac128;chacha20;aead;ph=1", true, 0xf8059ab6 },
  // -> yields f8059ab6

  // Other examples
  { "sha3-224;sha3-224;none;sha3-384;sha3-224;ed448;poly1305;chacha20;aead;ph=1", true, 0x330a499b },

  // Failure examples
  { "foo", false, 0x0 },
};


template <typename T>
std::string generate_name(testing::TestParamInfo<T> const & info)
{
  std::string s{info.param.spec};
  auto ret = std::regex_replace(s, std::regex("[;=-]+"), "_");
  return ret;
}

} // anonymous namespace

class Algorithms : public testing::TestWithParam<test_data>
{
};


TEST_P(Algorithms, create_from_spec)
{
  auto td = GetParam();

  auto algos = vessel_algorithm_choices_create(td.spec);
  if (td.valid) {
    ASSERT_NE(algos.version_tag, 0x0);
    ASSERT_EQ(algos.version_tag, td.version_tag);
  }
  else {
    ASSERT_EQ(algos.version_tag, 0x0);
  }
}

INSTANTIATE_TEST_SUITE_P(basics, Algorithms, testing::ValuesIn(test_specs),
    generate_name<test_data>);


TEST(Algorithms, create_and_validate)
{
  auto algos = vessel_algorithm_choices_create(test_specs[0].spec);
  ASSERT_EQ(algos.version_tag, test_specs[0].version_tag);

  // Check all algorithms
  ASSERT_EQ(algos.version_tag_hash, VESSEL_VTH_SHA3_512);
  ASSERT_EQ(algos.extent_identifier_hash, VESSEL_EIH_SHA3_512);
  ASSERT_EQ(algos.author_identifier_hash, VESSEL_AIH_NONE);
  ASSERT_EQ(algos.nonce_hash, VESSEL_NH_SHA3_512);
  ASSERT_EQ(algos.signature_hash, VESSEL_SH_EDDSA);
  ASSERT_EQ(algos.asymmetric_signature, VESSEL_AS_ED25519);
  ASSERT_EQ(algos.message_authentication, VESSEL_MA_KMAC128);
  ASSERT_EQ(algos.symmetric_encryption, VESSEL_SE_CHACHA20);
  ASSERT_EQ(algos.signature_algorithm, VESSEL_SA_AEAD);
  ASSERT_EQ(algos.private_header_flag, VESSEL_PHF_PH_EQ_1);
}



TEST(Algorithms, create_context_from_specs)
{
  struct vessel_algorithm_context * ctx = nullptr;

  char const * specs[2];
  specs[0] = test_specs[0].spec;
  specs[1] = test_specs[1].spec;

  auto err = vessel_algorithm_context_create_from_specs(&ctx,
      specs, sizeof(specs) / sizeof(specs[0]));
  ASSERT_EQ(VESSEL_ERR_SUCCESS, err);

  ASSERT_EQ(2, vessel_algorithm_context_choices(ctx));

  vessel_algorithm_context_free(&ctx);
}
