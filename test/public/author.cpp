/**
 * This file is part of vessel.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt).
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#include <gtest/gtest.h>

#include <vessel/author.h>

#include "../test_keys.h"


TEST(Author, construct_and_free)
{
  vessel_algorithm_choices choices{};
  choices.author_identifier_hash = VESSEL_AIH_SHA3_256;
  // TODO

  vessel_author * author = nullptr;
  ASSERT_EQ(VESSEL_ERR_SUCCESS, vessel_author_generate(&author, &choices));
  ASSERT_TRUE(author);

  ASSERT_EQ(VESSEL_ERR_SUCCESS, vessel_author_free(&author));
  ASSERT_FALSE(author);
}


TEST(Author, compare_generated)
{
  vessel_algorithm_choices choices{};
  choices.author_identifier_hash = VESSEL_AIH_SHA3_256;
  // TODO

  vessel_author * author1 = nullptr;
  ASSERT_EQ(VESSEL_ERR_SUCCESS, vessel_author_generate(&author1, &choices));
  ASSERT_TRUE(author1);

  vessel_author * author2 = nullptr;
  ASSERT_EQ(VESSEL_ERR_SUCCESS, vessel_author_generate(&author2, &choices));
  ASSERT_TRUE(author2);

  int res = vessel_author_compare(author1, author2);
  ASSERT_NE(0, res);

  ASSERT_EQ(VESSEL_ERR_SUCCESS, vessel_author_free(&author1));
  ASSERT_FALSE(author1);

  ASSERT_EQ(VESSEL_ERR_SUCCESS, vessel_author_free(&author2));
  ASSERT_FALSE(author2);
}


TEST(Author, compare_generated_with_test_key)
{
  vessel_algorithm_choices choices{};
  choices.author_identifier_hash = VESSEL_AIH_SHA3_256;
  // TODO

  vessel_author * author1 = nullptr;
  ASSERT_EQ(VESSEL_ERR_SUCCESS,
      vessel_author_from_buffer(&author1,
        &choices,
        test_keys::ed_private_key.c_str(), test_keys::ed_private_key.size()
  ));
  ASSERT_TRUE(author1);

  vessel_author * author2 = nullptr;
  ASSERT_EQ(VESSEL_ERR_SUCCESS, vessel_author_generate(&author2, &choices));
  ASSERT_TRUE(author2);

  int res = vessel_author_compare(author1, author2);
  ASSERT_NE(0, res);

  ASSERT_EQ(VESSEL_ERR_SUCCESS, vessel_author_free(&author1));
  ASSERT_FALSE(author1);

  ASSERT_EQ(VESSEL_ERR_SUCCESS, vessel_author_free(&author2));
  ASSERT_FALSE(author2);
}



TEST(Author, compare_same_test_key)
{
  vessel_algorithm_choices choices{};
  choices.author_identifier_hash = VESSEL_AIH_SHA3_256;
  // TODO

  vessel_author * author1 = nullptr;
  ASSERT_EQ(VESSEL_ERR_SUCCESS,
      vessel_author_from_buffer(&author1,
        &choices,
        test_keys::ed_private_key.c_str(), test_keys::ed_private_key.size()
  ));
  ASSERT_TRUE(author1);

  vessel_author * author2 = nullptr;
  ASSERT_EQ(VESSEL_ERR_SUCCESS,
      vessel_author_from_buffer(&author2,
        &choices,
        test_keys::ed_private_key.c_str(), test_keys::ed_private_key.size()
  ));
  ASSERT_TRUE(author2);

  int res = vessel_author_compare(author1, author2);
  ASSERT_EQ(0, res);

  ASSERT_EQ(VESSEL_ERR_SUCCESS, vessel_author_free(&author1));
  ASSERT_FALSE(author1);

  ASSERT_EQ(VESSEL_ERR_SUCCESS, vessel_author_free(&author2));
  ASSERT_FALSE(author2);
}




TEST(AuthorId, construct_and_free)
{
  vessel_algorithm_choices choices{};
  choices.author_identifier_hash = VESSEL_AIH_SHA3_256;

  vessel_author_id * author_id = nullptr;
  ASSERT_EQ(VESSEL_ERR_SUCCESS, vessel_author_id_generate(&author_id, &choices));
  ASSERT_TRUE(author_id);

  ASSERT_EQ(VESSEL_ERR_SUCCESS, vessel_author_id_free(&author_id));
  ASSERT_FALSE(author_id);
}



TEST(AuthorId, construct_from_author)
{
  vessel_algorithm_choices choices{};
  choices.author_identifier_hash = VESSEL_AIH_SHA3_256;

  vessel_author * author = nullptr;
  ASSERT_EQ(VESSEL_ERR_SUCCESS, vessel_author_generate(&author, &choices));
  ASSERT_TRUE(author);

  vessel_author_id * author_id = nullptr;
  ASSERT_EQ(VESSEL_ERR_SUCCESS, vessel_author_id_from_author(&author_id, &choices, author));
  ASSERT_TRUE(author_id);

  ASSERT_EQ(VESSEL_ERR_SUCCESS, vessel_author_id_free(&author_id));
  ASSERT_FALSE(author_id);

  ASSERT_EQ(VESSEL_ERR_SUCCESS, vessel_author_free(&author));
  ASSERT_FALSE(author);
}

// TODO
// - compare, also to generated
// - serialize and deserialize
// - add test keys
