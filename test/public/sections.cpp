/**
 * This file is part of vessel.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt).
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#include <gtest/gtest.h>

#include <vessel/section.h>

namespace {

static char const * TEST_ALGOS = "sha3-512;sha3-512;none;sha3-512;eddsa;ed25519;kmac128;chacha20;aead;ph=1";

} // anonymous namespace


TEST(Section, predefined_section_sizes)
{
  auto algos = vessel_algorithm_choices_create(TEST_ALGOS);
  ASSERT_NE(algos.version_tag, 0);

  struct vessel_algorithm_context * algo_ctx = nullptr;
  auto err = vessel_algorithm_context_create_from_choices(&algo_ctx,
      &algos, 1);
  ASSERT_EQ(VESSEL_ERR_SUCCESS, err);

  struct vessel_section_context * ctx = nullptr;
  err = vessel_section_context_create(&ctx, algo_ctx);
  ASSERT_EQ(VESSEL_ERR_SUCCESS, err);

  // Given these algorithm selections, determine section sizes.
  ASSERT_EQ(8, vessel_section_size(ctx, &algos, VESSEL_SECTION_CRC32, 0));
  ASSERT_EQ(20, vessel_section_size(ctx, &algos, VESSEL_SECTION_MAC, 0));
  ASSERT_EQ(68, vessel_section_size(ctx, &algos, VESSEL_SECTION_SIGNATURE, 0));
  ASSERT_EQ(68, vessel_section_size(ctx, &algos, VESSEL_SECTION_SIGNATURE, 123));

  ASSERT_EQ(0, vessel_section_size(ctx, &algos, VESSEL_SECTION_CONTENT_TYPE, 0));
  ASSERT_EQ(7, vessel_section_size(ctx, &algos, VESSEL_SECTION_CONTENT_TYPE, 1));

  vessel_section_context_free(&ctx);
  vessel_algorithm_context_free(&algo_ctx);
}


TEST(Section, custom_section_sizes)
{
  auto algos = vessel_algorithm_choices_create(TEST_ALGOS);
  ASSERT_NE(algos.version_tag, 0);

  struct vessel_algorithm_context * algo_ctx = nullptr;
  auto err = vessel_algorithm_context_create_from_choices(&algo_ctx,
      &algos, 1);
  ASSERT_EQ(VESSEL_ERR_SUCCESS, err);

  struct vessel_section_context * ctx = nullptr;
  err = vessel_section_context_create(&ctx, algo_ctx);
  ASSERT_EQ(VESSEL_ERR_SUCCESS, err);

  static constexpr vessel_section_t TEST_SECTION = 4321;

  // Given the above configuration, the API should not be able to return
  // a section size.
  ASSERT_EQ(-1, vessel_section_size(ctx, &algos, TEST_SECTION, 0));

  // We also cannot register reserved section sizes, or otherwise mess up
  // registration.
  ASSERT_EQ(VESSEL_ERR_INVALID_VALUE, vessel_set_section(nullptr, TEST_SECTION, 42));
  ASSERT_EQ(VESSEL_ERR_BAD_SECTION, vessel_set_section(ctx, VESSEL_SECTION_MAC, 42));
  ASSERT_EQ(VESSEL_ERR_INVALID_VALUE, vessel_set_section(ctx, TEST_SECTION, -1));

  // However, if everthing is good with the registation, we should be able to
  // retrieve the section size afterwards.
  ASSERT_EQ(VESSEL_ERR_SUCCESS, vessel_set_section(ctx, TEST_SECTION, 42));
  ASSERT_EQ(42, vessel_section_size(ctx, &algos, TEST_SECTION, 0));

  // Variable sized section
  ASSERT_EQ(VESSEL_ERR_SUCCESS, vessel_set_section(ctx, TEST_SECTION + 1, 0));
  ASSERT_EQ(0, vessel_section_size(ctx, &algos, TEST_SECTION + 1, 0));
  ASSERT_EQ(7, vessel_section_size(ctx, &algos, TEST_SECTION + 1, 1));

  vessel_section_context_free(&ctx);
  vessel_algorithm_context_free(&algo_ctx);
}



TEST(Section, create_manipulate_content_type)
{
  struct vessel_content_type * ct = nullptr;

  ASSERT_EQ(VESSEL_ERR_SUCCESS, vessel_content_type_from_utf8(&ct, "foo=bar;baz=quux", 0));
  ASSERT_EQ(VESSEL_ERR_SUCCESS, vessel_content_type_set(ct, "foo", 0, "baz", 0));
  ASSERT_EQ(VESSEL_ERR_SUCCESS, vessel_content_type_set(ct, "asdf", 0, "1234", 0));
  ASSERT_EQ(VESSEL_ERR_SUCCESS, vessel_content_type_remove(ct, "baz", 0));

  char buf[100];
  size_t size = sizeof(buf);
  ASSERT_NE(VESSEL_ERR_SUCCESS, vessel_content_type_to_utf8(nullptr, &size, ct));
  ASSERT_NE(VESSEL_ERR_SUCCESS, vessel_content_type_to_utf8(buf, nullptr, ct));
  ASSERT_NE(VESSEL_ERR_SUCCESS, vessel_content_type_to_utf8(buf, &size, nullptr));

  ASSERT_EQ(VESSEL_ERR_SUCCESS, vessel_content_type_to_utf8(buf, &size, ct));
  ASSERT_GT(size, 0);
  ASSERT_LT(size, sizeof(buf));

  std::string tmp{buf, buf + size};
  ASSERT_EQ(tmp, "foo=baz;asdf=1234");

  ASSERT_EQ(VESSEL_ERR_SUCCESS, vessel_content_type_free(&ct));
  ASSERT_EQ(nullptr, ct);
}

