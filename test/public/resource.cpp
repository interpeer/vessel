/**
 * This file is part of vessel.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt).
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#include <gtest/gtest.h>

#include <vessel/resource.h>

#include <liberate/fs/tmp.h>

namespace {

static char const * TEST_ALGOS = "sha3-512;sha3-512;none;sha3-512;eddsa;ed25519;kmac128;chacha20;aead;ph=1";

} // anonymous namespace


TEST(Resource, create_new)
{
  GTEST_SKIP() << "Needs revamp; this API is not up-to-date.";

  // Setup
  struct vessel_algorithm_context * algo_ctx = nullptr;
  auto err = vessel_algorithm_context_create_from_specs(&algo_ctx,
      &TEST_ALGOS, 1);
  ASSERT_EQ(VESSEL_ERR_SUCCESS, err);
  uint32_t version_tag = 0xf8059ab6;

  struct vessel_context * ctx = nullptr;
  err = vessel_context_create(&ctx, algo_ctx);
  ASSERT_EQ(VESSEL_ERR_SUCCESS, err);
  ASSERT_TRUE(ctx);

  auto tmpname = liberate::fs::temp_name("vessel-test-");

  vessel_backing_store * store = nullptr;
  err = vessel_backing_store_create_file(&store, algo_ctx, tmpname.c_str());
  ASSERT_EQ(VESSEL_ERR_SUCCESS, err);
  ASSERT_TRUE(store);
  ASSERT_TRUE(store->data);

  vessel_algorithm_choices choices{};
  choices.author_identifier_hash = VESSEL_AIH_NONE;
  choices.extent_identifier_hash = VESSEL_EIH_SHA3_512;
  choices.asymmetric_signature = VESSEL_AS_ED25519;

  vessel_author_id * author_id = nullptr;
  vessel_author_id_generate(&author_id, &choices);

  // Create a new resource (no origin extent)
  struct vessel_resource * res = nullptr;
  err = vessel_resource_create(&res, ctx, store, nullptr);
  ASSERT_EQ(VESSEL_ERR_SUCCESS, err);

  // Add an extent. Since this is the first one, a new origin will be created.
  auto opid = vessel_resource_add_extent(res,
      version_tag, 17,
      author_id, 42);
  ASSERT_GT(opid, 0);

  struct vessel_extent * ext = nullptr;
  err = vessel_resource_result(res, opid, &ext);
  ASSERT_TRUE(ext);

  vessel_resource_commit(res, ext);
  vessel_resource_release(res, &ext);

  // Add a second extent. This will use the origin as its parent.
  opid = vessel_resource_add_extent(res,
      version_tag, 1,
      author_id, 43);
  ASSERT_GT(opid, 0);

  ext = nullptr;
  err = vessel_resource_result(res, opid, &ext);
  ASSERT_TRUE(ext);

  // Cleanup
  vessel_resource_release(res, &ext);
  vessel_resource_free(&res);
  vessel_backing_store_free(&store);
  vessel_context_free(&ctx);
  vessel_algorithm_context_free(&algo_ctx);
  vessel_author_id_free(&author_id);

  unlink(tmpname.c_str());
}
