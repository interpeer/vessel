/**
 * This file is part of vessel.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt).
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#include <gtest/gtest.h>

#include <vessel/store.h>

#include <cstring>

#include <liberate/fs/tmp.h>


TEST(Store, create_memory_backed_store)
{
  // XXX as long as we don't use it, the algorithm context can be unset.
  vessel_backing_store * store = nullptr;
  auto err = vessel_backing_store_create_memory(&store, nullptr);
  ASSERT_EQ(VESSEL_ERR_SUCCESS, err);
  ASSERT_TRUE(store);
  ASSERT_TRUE(store->data);

  err = vessel_backing_store_free(&store);
  ASSERT_EQ(VESSEL_ERR_SUCCESS, err);
  ASSERT_FALSE(store);
}


TEST(Store, create_file_backed_store)
{
  // XXX as long as we don't use it, the algorithm context can be unset.
  auto tmpname = liberate::fs::temp_name("vessel-test-");

  vessel_backing_store * store = nullptr;
  auto err = vessel_backing_store_create_file(&store, nullptr, tmpname.c_str());
  ASSERT_EQ(VESSEL_ERR_SUCCESS, err);
  ASSERT_TRUE(store);
  ASSERT_TRUE(store->data);

  err = vessel_backing_store_free(&store);
  ASSERT_EQ(VESSEL_ERR_SUCCESS, err);
  ASSERT_FALSE(store);

  // Cleanup
  unlink(tmpname.c_str());
}



TEST(Store, create_discard_extent)
{
  // Preparation
  auto tmpname = liberate::fs::temp_name("vessel-test-");

  struct vessel_algorithm_context * algo_ctx = nullptr;
  char const * specs = "sha3-512;sha3-512;none;sha3-512;eddsa;ed25519;kmac128;chacha20;aead;ph=1";
  auto err = vessel_algorithm_context_create_from_specs(&algo_ctx, &specs, 1);
  ASSERT_EQ(VESSEL_ERR_SUCCESS, err);
  uint32_t version_tag = 0xf8059ab6;
  vessel_algorithm_choices choices{};
  choices.author_identifier_hash = VESSEL_AIH_SHA3_512;

  vessel_author_id * author_id = nullptr;
  vessel_author_id_generate(&author_id, &choices);

  // Create a file based store
  vessel_backing_store * store = nullptr;
  err = vessel_backing_store_create_file(&store, algo_ctx, tmpname.c_str());
  ASSERT_EQ(VESSEL_ERR_SUCCESS, err);
  ASSERT_TRUE(store);
  ASSERT_TRUE(store->data);

  // Create an extent. Note that this *store* barely cares about the extent,
  // other than that it has some identifier. Most other extent data is for
  // higher level functionality.
  auto opid = store->create_extent_origin(store,
      version_tag, 2,
      author_id, 42, nullptr, nullptr);
  ASSERT_NE(0, opid);

  // We don't want to wait for the callback here - just fetch the results
  // immediately.
  struct vessel_extent * ext = nullptr;
  err = store->extent_result(store, &ext, opid);
  ASSERT_EQ(VESSEL_ERR_SUCCESS, err);
  ASSERT_TRUE(ext);

  // Release the extent
  opid = store->release_extent(store, &ext, nullptr, nullptr);
  ASSERT_NE(0, opid);
  ASSERT_FALSE(ext);

  err = store->extent_result(store, nullptr, opid);
  ASSERT_EQ(VESSEL_ERR_SUCCESS, err);

  // Cleanup
  err = vessel_backing_store_free(&store);
  ASSERT_EQ(VESSEL_ERR_SUCCESS, err);
  ASSERT_FALSE(store);
  unlink(tmpname.c_str());

  vessel_author_id_free(&author_id);

  vessel_algorithm_context_free(&algo_ctx);
}


namespace {

struct test_baton
{
  struct vessel_extent *  extent = nullptr;
  int                     called = 0;
};

vessel_error_t
test_callback(struct vessel_backing_store const * store,
    vessel_operation_id opid, void * baton)
{
  EXPECT_TRUE(store);
  EXPECT_TRUE(baton);
  if (!store || !baton) {
    return VESSEL_ERR_UNEXPECTED;
  }

  auto bt = static_cast<struct test_baton *>(baton);

  // Fetch extent.
  auto err = store->extent_result(store, &(bt->extent), opid);
  EXPECT_EQ(VESSEL_ERR_SUCCESS, err);
  if (VESSEL_ERR_SUCCESS != err) {
    return err;
  }

  ++(bt->called);

  return VESSEL_ERR_SUCCESS;
}


} // anonymous namespace



TEST(Store, create_discard_extent_callback)
{
  // Preparation
  auto tmpname = liberate::fs::temp_name("vessel-test-");

  struct vessel_algorithm_context * algo_ctx = nullptr;
  char const * specs = "sha3-512;sha3-512;none;sha3-512;eddsa;ed25519;kmac128;chacha20;aead;ph=1";
  auto err = vessel_algorithm_context_create_from_specs(&algo_ctx, &specs, 1);
  ASSERT_EQ(VESSEL_ERR_SUCCESS, err);
  uint32_t version_tag = 0xf8059ab6;
  vessel_algorithm_choices choices{};
  choices.author_identifier_hash = VESSEL_AIH_SHA3_512;

  vessel_author_id * author_id = nullptr;
  vessel_author_id_generate(&author_id, &choices);

  // Create a file based store
  vessel_backing_store * store = nullptr;
  err = vessel_backing_store_create_file(&store, algo_ctx, tmpname.c_str());
  ASSERT_EQ(VESSEL_ERR_SUCCESS, err);
  ASSERT_TRUE(store);
  ASSERT_TRUE(store->data);

  // Creat a callback baton.
  test_baton baton{};

  // Create an extent. Note that this *store* barely cares about the extent,
  // other than that it has some identifier. Most other extent data is for
  // higher level functionality.
  auto opid = store->create_extent_origin(store,
      version_tag, 2,
      author_id, 42, test_callback, &baton);
  ASSERT_NE(0, opid);

  ASSERT_EQ(1, baton.called);
  ASSERT_NE(nullptr, baton.extent);


  // Release the extent
  opid = store->release_extent(store, &(baton.extent),
      test_callback, &baton);
  ASSERT_NE(0, opid);

  ASSERT_EQ(2, baton.called);
  ASSERT_EQ(nullptr, baton.extent);

  // Cleanup
  err = vessel_backing_store_free(&store);
  ASSERT_EQ(VESSEL_ERR_SUCCESS, err);
  ASSERT_FALSE(store);
  unlink(tmpname.c_str());

  vessel_author_id_free(&author_id);

  vessel_algorithm_context_free(&algo_ctx);
}



TEST(Store, create_sync_read_extents)
{
  // Preparation
  auto tmpname = liberate::fs::temp_name("vessel-test-");

  struct vessel_algorithm_context * algo_ctx = nullptr;
  char const * specs = "sha3-512;sha3-512;none;sha3-512;eddsa;ed25519;kmac128;chacha20;aead;ph=1";
  auto err = vessel_algorithm_context_create_from_specs(&algo_ctx, &specs, 1);
  ASSERT_EQ(VESSEL_ERR_SUCCESS, err);
  uint32_t version_tag = 0xf8059ab6;
  vessel_algorithm_choices choices{};
  choices.author_identifier_hash = VESSEL_AIH_NONE;
  choices.extent_identifier_hash = VESSEL_EIH_SHA3_512;
  choices.asymmetric_signature = VESSEL_AS_ED25519;

  vessel_author_id * author_id = nullptr;
  vessel_author_id_generate(&author_id, &choices);

  // Create a file based store
  vessel_backing_store * store = nullptr;
  err = vessel_backing_store_create_file(&store, algo_ctx, tmpname.c_str());
  ASSERT_EQ(VESSEL_ERR_SUCCESS, err);
  ASSERT_TRUE(store);
  ASSERT_TRUE(store->data);

  // Create an extent. Note that this *store* barely cares about the extent,
  // other than that it has some identifier. Most other extent data is for
  // higher level functionality.
  auto opid = store->create_extent_origin(store,
      version_tag, 2,
      author_id, 42, nullptr, nullptr);
  ASSERT_NE(0, opid);

  // We don't want to wait for the callback here - just fetch the results
  // immediately.
  struct vessel_extent * ext = nullptr;
  err = store->extent_result(store, &ext, opid);
  ASSERT_EQ(VESSEL_ERR_SUCCESS, err);
  ASSERT_TRUE(ext);

  // Having an extent means we have information about it.
  struct vessel_extent_id * id = nullptr;
  ASSERT_EQ(VESSEL_ERR_SUCCESS, vessel_extent_identifier(&id, ext));
  ASSERT_TRUE(id);

  size_t size = 0;
  ASSERT_EQ(VESSEL_ERR_SUCCESS, vessel_extent_size_in_blocks(&size, ext));
  ASSERT_EQ(size, 2);

  size_t psize = 0;
  ASSERT_EQ(VESSEL_ERR_SUCCESS, vessel_extent_payload_size(&psize, ext));
  ASSERT_GT(psize, 0);
  ASSERT_LT(psize, size * 4096);

  void * payload = nullptr;
  ASSERT_EQ(VESSEL_ERR_SUCCESS, vessel_extent_payload(&payload, ext));
  ASSERT_NE(nullptr, payload);

  // Write something to the payload
  char msg[] = "Hello, world!";
  std::memcpy(payload, msg, sizeof(msg));

  // Commit the extent - that should flush it to the file.
  opid = store->commit_extent(store, ext, nullptr, nullptr);
  ASSERT_NE(0, opid);
  ASSERT_TRUE(ext);

  err = store->extent_result(store, nullptr, opid);
  ASSERT_EQ(VESSEL_ERR_SUCCESS, err);

  // Release the extent
  opid = store->release_extent(store, &ext, nullptr, nullptr);
  ASSERT_NE(0, opid);
  ASSERT_FALSE(ext);

  err = store->extent_result(store, nullptr, opid);
  ASSERT_EQ(VESSEL_ERR_SUCCESS, err);

  // Close the store
  err = vessel_backing_store_free(&store);
  ASSERT_EQ(VESSEL_ERR_SUCCESS, err);
  ASSERT_FALSE(store);

  // Re-open the store
  err = vessel_backing_store_create_file(&store, algo_ctx, tmpname.c_str());
  ASSERT_EQ(VESSEL_ERR_SUCCESS, err);
  ASSERT_TRUE(store);
  ASSERT_TRUE(store->data);

  // Find the same extent again (we know version 0).
  opid = store->fetch_extent_latest(store, id, 0, nullptr, nullptr);
  ASSERT_NE(0, opid);
  ASSERT_FALSE(ext);

  err = store->extent_result(store, &ext, opid);
  ASSERT_EQ(VESSEL_ERR_SUCCESS, err);

  // Ensure the ID is the same
  struct vessel_extent_id * id2 = nullptr;
  ASSERT_EQ(VESSEL_ERR_SUCCESS, vessel_extent_identifier(&id2, ext));
  ASSERT_TRUE(id2);
  ASSERT_EQ(0, vessel_extent_id_cmp(id, id2));

  // Ensure the payload is correct
  payload = nullptr;
  ASSERT_EQ(VESSEL_ERR_SUCCESS, vessel_extent_payload(&payload, ext));
  ASSERT_NE(nullptr, payload);

  ASSERT_EQ(0, std::strncmp(static_cast<char const *>(payload),
        "Hello, world!", 13));

  // Release
  opid = store->release_extent(store, &ext, nullptr, nullptr);
  ASSERT_NE(0, opid);
  ASSERT_FALSE(ext);

  err = store->extent_result(store, nullptr, opid);
  ASSERT_EQ(VESSEL_ERR_SUCCESS, err);

  // Cleanup
  err = vessel_backing_store_free(&store);
  ASSERT_EQ(VESSEL_ERR_SUCCESS, err);
  ASSERT_FALSE(store);
  unlink(tmpname.c_str());

  vessel_extent_id_free(&id);
  vessel_extent_id_free(&id2);
  vessel_author_id_free(&author_id);

  vessel_algorithm_context_free(&algo_ctx);
}


TEST(Store, create_multiple)
{
  // Preparation
  auto tmpname = liberate::fs::temp_name("vessel-test-");

  struct vessel_algorithm_context * algo_ctx = nullptr;
  char const * specs = "sha3-512;sha3-512;none;sha3-512;eddsa;ed25519;kmac128;chacha20;aead;ph=1";
  auto err = vessel_algorithm_context_create_from_specs(&algo_ctx, &specs, 1);
  ASSERT_EQ(VESSEL_ERR_SUCCESS, err);
  uint32_t version_tag = 0xf8059ab6;
  vessel_algorithm_choices choices{};
  choices.author_identifier_hash = VESSEL_AIH_NONE;
  choices.extent_identifier_hash = VESSEL_EIH_SHA3_512;
  choices.asymmetric_signature = VESSEL_AS_ED25519;

  vessel_author_id * author_id = nullptr;
  vessel_author_id_generate(&author_id, &choices);

  // Create a file based store
  vessel_backing_store * store = nullptr;
  err = vessel_backing_store_create_file(&store, algo_ctx, tmpname.c_str());
  ASSERT_EQ(VESSEL_ERR_SUCCESS, err);
  ASSERT_TRUE(store);
  ASSERT_TRUE(store->data);

  // Create an extent. Note that this *store* barely cares about the extent,
  // other than that it has some identifier. Most other extent data is for
  // higher level functionality.
  auto opid = store->create_extent_origin(store,
      version_tag, 2,
      author_id, 42, nullptr, nullptr);
  ASSERT_NE(0, opid);

  // We don't want to wait for the callback here - just fetch the results
  // immediately.
  struct vessel_extent * ext = nullptr;
  err = store->extent_result(store, &ext, opid);
  ASSERT_EQ(VESSEL_ERR_SUCCESS, err);
  ASSERT_TRUE(ext);

  // Having an extent means we have information about it.
  struct vessel_extent_id * id = nullptr;
  ASSERT_EQ(VESSEL_ERR_SUCCESS, vessel_extent_identifier(&id, ext));
  ASSERT_TRUE(id);

  // Commit the extent - that should flush it to the file.
  opid = store->commit_extent(store, ext, nullptr, nullptr);
  ASSERT_NE(0, opid);
  ASSERT_TRUE(ext);

  err = store->extent_result(store, nullptr, opid);
  ASSERT_EQ(VESSEL_ERR_SUCCESS, err);

  // Release the extent
  opid = store->release_extent(store, &ext, nullptr, nullptr);
  ASSERT_NE(0, opid);
  ASSERT_FALSE(ext);

  err = store->extent_result(store, nullptr, opid);
  ASSERT_EQ(VESSEL_ERR_SUCCESS, err);

  // Create a next extent
  opid = store->create_extent(store,
      version_tag, 3, // Different size
      author_id, 99,
      id, nullptr, nullptr);
  ASSERT_NE(0, opid);

  err = store->extent_result(store, &ext, opid);
  ASSERT_EQ(VESSEL_ERR_SUCCESS, err);
  ASSERT_TRUE(ext);

  // Fetch second identifier
  struct vessel_extent_id * id2 = nullptr;
  ASSERT_EQ(VESSEL_ERR_SUCCESS, vessel_extent_identifier(&id2, ext));
  ASSERT_TRUE(id2);

  // ID must be different!
  ASSERT_NE(0, vessel_extent_id_cmp(id, id2));

  // Release
  opid = store->release_extent(store, &ext, nullptr, nullptr);
  ASSERT_NE(0, opid);
  ASSERT_FALSE(ext);

  err = store->extent_result(store, nullptr, opid);
  ASSERT_EQ(VESSEL_ERR_SUCCESS, err);

  // Cleanup
  err = vessel_backing_store_free(&store);
  ASSERT_EQ(VESSEL_ERR_SUCCESS, err);
  ASSERT_FALSE(store);
  unlink(tmpname.c_str());

  vessel_extent_id_free(&id);
  vessel_extent_id_free(&id2);
  vessel_author_id_free(&author_id);

  vessel_algorithm_context_free(&algo_ctx);
}


TEST(Store, create_sync_index)
{
  // Preparation
  auto tmpname = liberate::fs::temp_name("vessel-test-");

  struct vessel_algorithm_context * algo_ctx = nullptr;
  char const * specs = "sha3-512;sha3-512;none;sha3-512;eddsa;ed25519;kmac128;chacha20;aead;ph=1";
  auto err = vessel_algorithm_context_create_from_specs(&algo_ctx, &specs, 1);
  ASSERT_EQ(VESSEL_ERR_SUCCESS, err);
  uint32_t version_tag = 0xf8059ab6;
  vessel_algorithm_choices choices{};
  choices.author_identifier_hash = VESSEL_AIH_NONE;
  choices.extent_identifier_hash = VESSEL_EIH_SHA3_512;
  choices.asymmetric_signature = VESSEL_AS_ED25519;

  vessel_author_id * author_id = nullptr;
  vessel_author_id_generate(&author_id, &choices);

  // Create a file based store
  vessel_backing_store * store = nullptr;
  err = vessel_backing_store_create_file(&store, algo_ctx, tmpname.c_str());
  ASSERT_EQ(VESSEL_ERR_SUCCESS, err);
  ASSERT_TRUE(store);
  ASSERT_TRUE(store->data);

  // Create an extent. Note that this *store* barely cares about the extent,
  // other than that it has some identifier. Most other extent data is for
  // higher level functionality.
  auto opid = store->create_extent_origin(store,
      version_tag, 2,
      author_id, 42, nullptr, nullptr);
  ASSERT_NE(0, opid);

  // We don't want to wait for the callback here - just fetch the results
  // immediately.
  struct vessel_extent * ext = nullptr;
  err = store->extent_result(store, &ext, opid);
  ASSERT_EQ(VESSEL_ERR_SUCCESS, err);
  ASSERT_TRUE(ext);

  // Having an extent means we have information about it.
  struct vessel_extent_id * id = nullptr;
  ASSERT_EQ(VESSEL_ERR_SUCCESS, vessel_extent_identifier(&id, ext));
  ASSERT_TRUE(id);

  size_t size = 0;
  ASSERT_EQ(VESSEL_ERR_SUCCESS, vessel_extent_size_in_blocks(&size, ext));
  ASSERT_EQ(size, 2);

  size_t psize = 0;
  ASSERT_EQ(VESSEL_ERR_SUCCESS, vessel_extent_payload_size(&psize, ext));
  ASSERT_GT(psize, 0);
  ASSERT_LT(psize, size * 4096);

  void * payload = nullptr;
  ASSERT_EQ(VESSEL_ERR_SUCCESS, vessel_extent_payload(&payload, ext));
  ASSERT_NE(nullptr, payload);

  // Write something to the payload
  char msg[] = "Hello, world!";
  std::memcpy(payload, msg, sizeof(msg));

  // Commit the extent - that should flush it to the file.
  opid = store->commit_extent(store, ext, nullptr, nullptr);
  ASSERT_NE(0, opid);
  ASSERT_TRUE(ext);

  err = store->extent_result(store, nullptr, opid);
  ASSERT_EQ(VESSEL_ERR_SUCCESS, err);

  // Release the extent
  opid = store->release_extent(store, &ext, nullptr, nullptr);
  ASSERT_NE(0, opid);
  ASSERT_FALSE(ext);

  err = store->extent_result(store, nullptr, opid);
  ASSERT_EQ(VESSEL_ERR_SUCCESS, err);

  // Close the store
  err = vessel_backing_store_free(&store);
  ASSERT_EQ(VESSEL_ERR_SUCCESS, err);
  ASSERT_FALSE(store);

  // Re-open the store
  err = vessel_backing_store_create_file(&store, algo_ctx, tmpname.c_str());
  ASSERT_EQ(VESSEL_ERR_SUCCESS, err);
  ASSERT_TRUE(store);
  ASSERT_TRUE(store->data);

  // Try and build an index - without callback
  opid = store->try_build_index(store, nullptr, nullptr);
  ASSERT_NE(0, opid);

  // Fetch index result
  struct vessel_store_index_iterator * iter = nullptr;
  err = store->index_result(store, &iter, opid);
  ASSERT_EQ(VESSEL_ERR_SUCCESS, err);
  ASSERT_TRUE(iter);

  // Iterate...
  struct vessel_extent_id * id2 = nullptr;
  size_t found = 0;
  while (VESSEL_ERR_SUCCESS == vessel_store_index_iterator_forward(store, iter, &id2, nullptr, nullptr)) {
    ++found;
  }

  err = store->index_iterator_free(&iter);
  ASSERT_EQ(VESSEL_ERR_SUCCESS, err);
  ASSERT_FALSE(iter);

  ASSERT_EQ(1, found);
  ASSERT_TRUE(id2);
  ASSERT_EQ(0, vessel_extent_id_cmp(id, id2));

  // Cleanup
  err = vessel_backing_store_free(&store);
  ASSERT_EQ(VESSEL_ERR_SUCCESS, err);
  ASSERT_FALSE(store);
  unlink(tmpname.c_str());

  vessel_extent_id_free(&id);
  vessel_extent_id_free(&id2);
  vessel_author_id_free(&author_id);

  vessel_algorithm_context_free(&algo_ctx);
}

