/**
 * This file is part of vessel.
 *
 * Authors(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt).
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#include <gtest/gtest.h>

#include <vessel/extent.h>

#include "../test_keys.h"


TEST(ExtentId, size)
{
  vessel_algorithm_choices choices{};

  // The size depends on the algorithm choice
  choices.extent_identifier_hash = VESSEL_EIH_SHA2_224;
  ASSERT_EQ(28, vessel_extent_id_size(&choices));

  choices.extent_identifier_hash = VESSEL_EIH_SHA3_512;
  ASSERT_EQ(64, vessel_extent_id_size(&choices));
}


TEST(ExtentId, construct_and_free)
{
  vessel_extent_id * extent_id = nullptr;
  ASSERT_EQ(VESSEL_ERR_SUCCESS, vessel_extent_id_new(&extent_id));
  ASSERT_TRUE(extent_id);

  ASSERT_EQ(VESSEL_ERR_SUCCESS, vessel_extent_id_free(&extent_id));
  ASSERT_FALSE(extent_id);
}


TEST(ExtentId, compare_new)
{
  vessel_extent_id * id1 = nullptr;
  ASSERT_EQ(VESSEL_ERR_SUCCESS, vessel_extent_id_new(&id1));
  ASSERT_TRUE(id1);

  vessel_extent_id * id2 = nullptr;
  ASSERT_EQ(VESSEL_ERR_SUCCESS, vessel_extent_id_new(&id2));
  ASSERT_TRUE(id2);


  int result = vessel_extent_id_cmp(id1, id2);
  ASSERT_EQ(0, result);

  result = vessel_extent_id_cmp(id2, id1);
  ASSERT_EQ(0, result);


  ASSERT_EQ(VESSEL_ERR_SUCCESS, vessel_extent_id_free(&id1));
  ASSERT_FALSE(id1);

  ASSERT_EQ(VESSEL_ERR_SUCCESS, vessel_extent_id_free(&id2));
  ASSERT_FALSE(id2);
}


TEST(ExtentId, compare_bad_inputs)
{
  vessel_extent_id * id = nullptr;
  int result = 0;

  ASSERT_EQ(VESSEL_ERR_INVALID_VALUE,
      vessel_extent_id_compare(nullptr, id, id));

  ASSERT_EQ(VESSEL_ERR_INVALID_VALUE,
      vessel_extent_id_compare(&result, nullptr, id));

  ASSERT_EQ(VESSEL_ERR_INVALID_VALUE,
      vessel_extent_id_compare(&result, id, nullptr));
}


TEST(ExtentId, generate_new)
{
  vessel_algorithm_choices choices{};
  choices.extent_identifier_hash = VESSEL_EIH_SHA3_224;
  choices.author_identifier_hash = VESSEL_AIH_SHA3_224;

  vessel_author_id * author_id = nullptr;
  ASSERT_EQ(VESSEL_ERR_SUCCESS, vessel_author_id_generate(&author_id, &choices));

  vessel_extent_id * extent_id = nullptr;
  ASSERT_EQ(VESSEL_ERR_SUCCESS, vessel_extent_id_generate(&extent_id, &choices, author_id, nullptr));

  // Verify created ID
  ASSERT_EQ(VESSEL_ERR_SUCCESS, vessel_extent_id_verify(extent_id, &choices, author_id, nullptr));

  // The generated ID should not be equal to a newly created one.
  vessel_extent_id * new_id = nullptr;
  ASSERT_EQ(VESSEL_ERR_SUCCESS, vessel_extent_id_new(&new_id));
  ASSERT_TRUE(new_id);

  int result = vessel_extent_id_cmp(extent_id, new_id);
  ASSERT_NE(0, result);

  result = vessel_extent_id_cmp(new_id, extent_id);
  ASSERT_NE(0, result);


  ASSERT_EQ(VESSEL_ERR_SUCCESS, vessel_extent_id_free(&new_id));
  ASSERT_FALSE(new_id);

  ASSERT_EQ(VESSEL_ERR_SUCCESS, vessel_extent_id_free(&extent_id));
  ASSERT_FALSE(extent_id);

  ASSERT_EQ(VESSEL_ERR_SUCCESS, vessel_author_id_free(&author_id));
  ASSERT_FALSE(author_id);
}


TEST(ExtentId, generate_derived)
{
  vessel_algorithm_choices choices{};
  choices.extent_identifier_hash = VESSEL_EIH_SHA3_384;
  choices.author_identifier_hash = VESSEL_AIH_SHA3_384;

  vessel_author_id * author_id = nullptr;
  ASSERT_EQ(VESSEL_ERR_SUCCESS, vessel_author_id_generate(&author_id, &choices));

  vessel_extent_id * extent_id = nullptr;
  ASSERT_EQ(VESSEL_ERR_SUCCESS, vessel_extent_id_generate(&extent_id, &choices, author_id, nullptr));

  vessel_extent_id * derived_id = nullptr;
  ASSERT_EQ(VESSEL_ERR_SUCCESS, vessel_extent_id_generate(&derived_id, &choices, author_id, extent_id));

  // Verify created ID
  ASSERT_EQ(VESSEL_ERR_SUCCESS, vessel_extent_id_verify(derived_id, &choices, author_id, extent_id));

  // The two IDs should not compare
  int result = vessel_extent_id_cmp(extent_id, derived_id);
  ASSERT_NE(0, result);

  result = vessel_extent_id_cmp(derived_id, extent_id);
  ASSERT_NE(0, result);


  ASSERT_EQ(VESSEL_ERR_SUCCESS, vessel_extent_id_free(&derived_id));
  ASSERT_FALSE(derived_id);

  ASSERT_EQ(VESSEL_ERR_SUCCESS, vessel_extent_id_free(&extent_id));
  ASSERT_FALSE(extent_id);

  ASSERT_EQ(VESSEL_ERR_SUCCESS, vessel_author_id_free(&author_id));
  ASSERT_FALSE(author_id);
}


TEST(ExtentId, serialization)
{
  vessel_algorithm_choices choices{};
  choices.extent_identifier_hash = VESSEL_EIH_SHA3_384;
  choices.author_identifier_hash = VESSEL_AIH_SHA3_384;

  vessel_author_id * author_id = nullptr;
  ASSERT_EQ(VESSEL_ERR_SUCCESS, vessel_author_id_generate(&author_id, &choices));

  vessel_extent_id * extent_id = nullptr;
  ASSERT_EQ(VESSEL_ERR_SUCCESS, vessel_extent_id_generate(&extent_id, &choices, author_id, nullptr));

  // Serialize to a buffer.
  std::vector<char> buf;
  buf.resize(vessel_extent_id_size(&choices));

  auto id_size = buf.size();
  ASSERT_EQ(VESSEL_ERR_SUCCESS, vessel_extent_id_to_buffer(&buf[0], &id_size, &choices, extent_id));

  // Create new ID from this buffer.
  vessel_extent_id * new_id = nullptr;
  ASSERT_EQ(VESSEL_ERR_SUCCESS, vessel_extent_id_create_from_buffer(&new_id,
        &choices,
        &buf[0], buf.size()));

  // The two IDs should be identical
  int result = vessel_extent_id_cmp(extent_id, new_id);
  ASSERT_EQ(0, result);

  result = vessel_extent_id_cmp(new_id, extent_id);
  ASSERT_EQ(0, result);


  ASSERT_EQ(VESSEL_ERR_SUCCESS, vessel_extent_id_free(&new_id));
  ASSERT_FALSE(new_id);

  ASSERT_EQ(VESSEL_ERR_SUCCESS, vessel_extent_id_free(&extent_id));
  ASSERT_FALSE(extent_id);

  ASSERT_EQ(VESSEL_ERR_SUCCESS, vessel_author_id_free(&author_id));
  ASSERT_FALSE(author_id);
}
