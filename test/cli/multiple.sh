echo ">>> Create resource (multiple files)..."
TOPIC=$(expr $RANDOM + 1024)

FULLPATHS=
for file in ${FILES} ; do
  FULLPATH="${FULLPATH} ${SRC_DIR}/${file}"
done

"${BUILD_DIR}/cli/vessel" -k "${TEST_KEY}" import "${TMP_RESOURCE}" ${FULLPATH} -t "${TOPIC}"

"${BUILD_DIR}/cli/vessel" info "${TMP_RESOURCE}" -s

"${BUILD_DIR}/cli/vessel" export "${TMP_RESOURCE}" -a

for file in ${FILES} ; do
  diff -q "${TMP_DIR}/${file}" "${SRC_DIR}/${file}"
done
