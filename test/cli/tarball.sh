echo ">>> Create resource (varying extents)..."

# Create tarball and pipe into resource
cd "${SRC_DIR}"
tar cvj -f- ${FILES} |\
  "${BUILD_DIR}/cli/vessel" -k "${TEST_KEY}" import "${TMP_RESOURCE}"
cd "${TMP_DIR}"

"${BUILD_DIR}/cli/vessel" info "${TMP_RESOURCE}" -s --verbose

"${BUILD_DIR}/cli/vessel" export "${TMP_RESOURCE}" -o- |\
  tar vxj

for file in ${FILES} ; do
  diff -q "${TMP_DIR}/${file}" "${SRC_DIR}/${file}"
done
