echo ">>> Create resource (shuffle extents)..."
TOPIC=$(expr $RANDOM + 1024)

for file in ${FILES} ; do
  "${BUILD_DIR}/cli/vessel" -k "${TEST_KEY}" import "${TMP_RESOURCE}" "${SRC_DIR}/${file}" -t "${TOPIC}" -a -s 1
done

TMP_RESOURCE_SHUFFLED="${TMP_RESOURCE}.shuffled"
"${SRC_DIR}/scripts/shuffle_resource.sh" "${TMP_RESOURCE}" "${TMP_RESOURCE_SHUFFLED}"

"${BUILD_DIR}/cli/vessel" info "${TMP_RESOURCE_SHUFFLED}" -s

"${BUILD_DIR}/cli/vessel" export "${TMP_RESOURCE_SHUFFLED}" -t "${TOPIC}" -a

for file in ${FILES} ; do
  diff -q "${TMP_DIR}/${file}" "${SRC_DIR}/${file}"
done
