echo ">>> Create resource (varying extents)..."
TOPIC=$(expr $RANDOM + 1024)

for file in ${FILES} ; do
  SIZE=$(expr '(' $RANDOM '%' 4 ')' + 1)
  "${BUILD_DIR}/cli/vessel" -k "${TEST_KEY}" import "${TMP_RESOURCE}" "${SRC_DIR}/${file}" -t "${TOPIC}" -a -s "${SIZE}"
done

"${BUILD_DIR}/cli/vessel" info "${TMP_RESOURCE}" -s

"${BUILD_DIR}/cli/vessel" export "${TMP_RESOURCE}" -t "${TOPIC}" -a

for file in ${FILES} ; do
  diff -q "${TMP_DIR}/${file}" "${SRC_DIR}/${file}"
done
