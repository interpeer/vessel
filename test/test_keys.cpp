/**
 * This file is part of vessel.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt).
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

#include "test_keys.h"

namespace test_keys
{
  std::string ed_private_key = R"(-----BEGIN PRIVATE KEY-----
MC4CAQAwBQYDK2VwBCIEIE6V3BprgqVmv18GD75hBkxAUMLZM8KTy7JntEA1abN6
-----END PRIVATE KEY-----
  )";

  std::string ed_public_key = R"(-----BEGIN PUBLIC KEY-----
MCowBQYDK2VwAyEA/LHBym63DYPoVAUWl1QWJWprDP7QWPDZEdUPVsb2ynw=
-----END PUBLIC KEY-----
  )";
} // namespace test_keys

