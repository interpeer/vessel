/**
 * This file is part of vessel.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2023 Interpeer gUG (haftungsbeschränkt).
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#include <gtest/gtest.h>

#include <vessel/resource.h>
#include "../../lib/resource.h"

#include <s3kr1t/kdfs.h>

#include "../test_keys.h"

namespace {

static char const * TEST_ALGOS = "sha3-512;sha3-512;none;sha3-512;eddsa;ed25519;kmac128;chacha20;aead;ph=1";
static constexpr uint32_t VERSION_TAG = 0xf8059ab6;

struct test_context
{
  struct vessel_algorithm_context * algo_ctx = nullptr;
  struct vessel_context * ctx = nullptr;
  struct vessel_backing_store * store = nullptr;
  struct vessel_resource * res = nullptr;
  struct vessel_author * author = nullptr;
  struct vessel_author_id * author_id = nullptr;

  inline test_context()
  {
    // Setup
    auto err = vessel_algorithm_context_create_from_specs(&algo_ctx,
        &TEST_ALGOS, 1);
    EXPECT_EQ(VESSEL_ERR_SUCCESS, err);

    err = vessel_context_create(&ctx, algo_ctx);
    EXPECT_EQ(VESSEL_ERR_SUCCESS, err);
    EXPECT_TRUE(ctx);

    err = vessel_backing_store_create_memory(&store, algo_ctx);
    EXPECT_EQ(VESSEL_ERR_SUCCESS, err);
    EXPECT_TRUE(store);
    EXPECT_TRUE(store->data);

    vessel_algorithm_choices choices{};
    choices.author_identifier_hash = VESSEL_AIH_NONE;
    choices.extent_identifier_hash = VESSEL_EIH_SHA3_512;
    choices.asymmetric_signature = VESSEL_AS_ED25519;

    err = vessel_author_generate(&author, &choices);
    EXPECT_EQ(VESSEL_ERR_SUCCESS, err);

    err = vessel_author_id_from_author(&author_id, &choices, author);
    EXPECT_EQ(VESSEL_ERR_SUCCESS, err);

    // Create a new resource (no origin extent)
    err = vessel_resource_create(&res, ctx, store, nullptr);
    EXPECT_EQ(VESSEL_ERR_SUCCESS, err);
  }


  inline ~test_context()
  {
    // Cleanup
    vessel_resource_free(&res);
    vessel_backing_store_free(&store);
    vessel_context_free(&ctx);
    vessel_algorithm_context_free(&algo_ctx);
    vessel_author_id_free(&author_id);
    vessel_author_free(&author);
  }
};


inline void
test_write_section(
    vessel::resource const & _res,
    struct vessel_extent * ext [[maybe_unused]],
    vessel::payload & payload, vessel::resource::section_request & req)
{
  // Write the section into the payload. Writing the actual data is not
  // necessary, so we skip the data pointer.
  auto [res, section] = payload.add_section(
      req.section, req.topic,
      req.min_data_size);
  ASSERT_TRUE(res);

  // Finalize; pretend we'll write the integrity section
  payload.finalize(
      _res, *(ext->ext),
      [](vessel::resource const &, vessel::extent const &, vessel::section &, vessel::payload const &){ return true; }
  );
}


inline void
test_extent_creation_for_section(
    test_context & ctx,
    size_t requested_size,
    vessel_section_t const & integrity_section)
{
  ASSERT_TRUE(ctx.res->resource);

  // Construct section request
  vessel::resource::section_request req{};
  req.version_tag = VERSION_TAG;
  req.section = VESSEL_SECTION_BLOB;
  req.topic = 0x1312;
  req.min_data_size = requested_size;
  req.max_extent_size = vessel_extent_block_size();
  req.author = ctx.author;
  req.counter = 1;
  req.integrity_section = integrity_section;

  vessel::byte_sequence ext_id;
  size_t called = 0;

  // First section should result in a new extent being delivered out.
  req.callback = [&called, &req, &ctx, &ext_id](vessel_error_t _err, vessel::resource::section_result & _res)
  {
    ASSERT_EQ(VESSEL_ERR_SUCCESS, _err);
    ASSERT_TRUE(_res.ext);
    ASSERT_TRUE(_res.extent_payload);

    test_write_section(*(ctx.res->resource.get()), _res.ext, *(_res.extent_payload), req);

    ext_id = _res.ext->ext->header.current_extent_identifier;

    ASSERT_EQ(VESSEL_ERR_SUCCESS, ctx.res->resource->commit(_res.ext));
    ASSERT_EQ(VESSEL_ERR_SUCCESS, ctx.res->resource->release(&(_res.ext)));

    ++called;
  };

  ASSERT_EQ(0, called);
  auto err = ctx.res->resource->make_space_for_section(req);
  ASSERT_EQ(VESSEL_ERR_SUCCESS, err);
  ASSERT_EQ(1, called);

  std::cerr << "=====================" << std::endl;

  // For the second section, we should receive the same extent; it's still
  // got enough space.
  req.callback = [&called, &ctx, &ext_id](vessel_error_t _err, vessel::resource::section_result & _res)
  {
    ASSERT_EQ(VESSEL_ERR_SUCCESS, _err);
    ASSERT_TRUE(_res.ext);
    ASSERT_TRUE(_res.extent_payload);

    ASSERT_EQ(ext_id, _res.ext->ext->header.current_extent_identifier);
    ASSERT_EQ(VESSEL_ERR_SUCCESS, ctx.res->resource->release(&(_res.ext)));

    ++called;
  };

  ASSERT_EQ(1, called);
  err = ctx.res->resource->make_space_for_section(req);
  ASSERT_EQ(VESSEL_ERR_SUCCESS, err);
  ASSERT_EQ(2, called);
}


} // anonymous namespace


TEST(Resource, extent_creation_for_section_without_integrity)
{
  test_context ctx{};
  test_extent_creation_for_section(ctx, 20, 0);
}



TEST(Resource, extent_creation_for_section_with_mac)
{
  test_context ctx{};

  std::string key{"super secret key"};
  std::string salt{"I'm pepper"};
  char keybuf[33] = {};
  size_t required = 0;
  auto err = s3kr1t::kdf(keybuf, sizeof(keybuf) - 1, required,
        key.c_str(), key.size(),
        salt.c_str(), salt.size(),
        s3kr1t::KDFT_SCRYPT);
  ASSERT_EQ(s3kr1t::ERR_SUCCESS, err);

  ctx.res->resource->set_symmetric_key(keybuf);
  test_extent_creation_for_section(ctx, 20, VESSEL_SECTION_MAC);
}



TEST(Resource, extent_creation_for_section_with_crc32)
{
  test_context ctx{};
  test_extent_creation_for_section(ctx, 20, VESSEL_SECTION_CRC32);
}



TEST(Resource, extent_creation_for_section_with_signature)
{
  // This works "out of the box", because we specify an author key for
  // the extent creation.
  test_context ctx{};
  test_extent_creation_for_section(ctx, 20, VESSEL_SECTION_SIGNATURE);
}


// TODO same test with:
// - ct
// - both ct and integrity
// - too large a section
// - encryption (eventually)

