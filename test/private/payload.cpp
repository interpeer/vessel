/**
 * This file is part of vessel.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt).
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#include <gtest/gtest.h>

#include "../../lib/payload.h"

#include <liberate/string/hexencode.h>

namespace {

static char const * TEST_ALGOS = "sha3-512;sha3-512;none;sha3-512;eddsa;ed25519;kmac128;chacha20;aead;ph=1";

static constexpr char TEST_PAYLOAD[] = {
  // First section: CRC32 in METADAT topic
  '\x00', '\x01', '\x00', '\x0a',
  '\xac', '\xab', '\x13', '\x12',

  // Second section: a MAC - this needs to have 128 bits payload
  '\x00', '\x02', '\x00', '\x0a',
  '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00',
  '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00',

  // Third section: content type
  '\x00', '\xa0', '\xac', '\xab',
  '\x00', '\x13',
  'H', 'e', 'l', 'l', 'o', ',', ' ', 'w', 'o', 'r', 'l', 'd', '!',

  // Last section: blob
  '\x00', '\xa1', '\xac', '\xab',
  '\x00', '\x09',
  'f', 'o', 'o',
};


struct test_context
{
  struct vessel_algorithm_context * algo_ctx = nullptr;
  struct vessel_section_context *   section_ctx = nullptr;
  vessel::algorithm_options         algo_opts;

  inline test_context()
  {
    auto algos = vessel_algorithm_choices_create(TEST_ALGOS);
    EXPECT_NE(algos.version_tag, 0);

    algo_opts = vessel::options_from_choices(algos);

    auto err = vessel_algorithm_context_create_from_choices(&algo_ctx,
        &algos, 1);
    EXPECT_EQ(VESSEL_ERR_SUCCESS, err);

    err = vessel_section_context_create(&section_ctx, algo_ctx);
    EXPECT_EQ(VESSEL_ERR_SUCCESS, err);
  }

  inline ~test_context()
  {
    vessel_section_context_free(&section_ctx);
    vessel_algorithm_context_free(&algo_ctx);
  }
};

} // anonymous namespace

namespace {


} // anonymous namespace



TEST(Payload, parse)
{
  test_context ctx{};

  vessel::payload pl{ctx.section_ctx->section_sizes, ctx.algo_opts,
    const_cast<char *>(TEST_PAYLOAD), sizeof(TEST_PAYLOAD)};

  // Four sections
  ASSERT_EQ(4, pl.size());

  // Size of all sections combined should be the size of the payload
  ASSERT_EQ(sizeof(TEST_PAYLOAD), pl.used());
  ASSERT_EQ(0, pl.available());
}



TEST(Payload, parse_empty)
{
  test_context ctx{};

  std::vector<char> payload;
  payload.resize(42);

  vessel::payload pl{ctx.section_ctx->section_sizes, ctx.algo_opts,
    &payload[0], payload.size()};

  ASSERT_EQ(0, pl.size());
  ASSERT_EQ(0, pl.used());
  ASSERT_EQ(payload.size(), pl.available());
}



TEST(Payload, construct)
{
  test_context ctx{};

  std::vector<char> payload;
  payload.resize(200);

  vessel::payload pl{ctx.section_ctx->section_sizes, ctx.algo_opts,
    &payload[0], payload.size()};

  // CRC32
  {
    auto [res, sec] = pl.add_section(VESSEL_SECTION_CRC32, VESSEL_TOPIC_METADATA);
    ASSERT_TRUE(res);
    ASSERT_EQ(sec.type, VESSEL_SECTION_CRC32);
    ASSERT_EQ(sec.topic, VESSEL_TOPIC_METADATA);
    ASSERT_EQ(sec.section_size, 8);
  }

  // With immediate data
  {
    char content_type[] = "foo=bar";
    auto [res, sec] = pl.add_section(VESSEL_SECTION_CONTENT_TYPE,
      42, sizeof(content_type), content_type);
    ASSERT_TRUE(res);
    ASSERT_EQ(sec.type, VESSEL_SECTION_CONTENT_TYPE);
    ASSERT_EQ(sec.topic, 42);
    ASSERT_EQ(sec.section_size, sizeof(content_type) + 6);
    ASSERT_EQ(sec.data_size, sizeof(content_type));

    ASSERT_EQ(0, memcmp(content_type, sec.data, sizeof(content_type)));
  }

  // With later data
  {
    char data[] = "well hello!";
    auto [res, sec] = pl.add_section(VESSEL_SECTION_BLOB,
      42, sizeof(data));
    ASSERT_TRUE(res);
    ASSERT_EQ(sec.type, VESSEL_SECTION_BLOB);
    ASSERT_EQ(sec.topic, 42);
    ASSERT_EQ(sec.section_size, sizeof(data) + 6);
    ASSERT_EQ(sec.data_size, sizeof(data));

    ASSERT_NE(0, memcmp(data, sec.data, sizeof(data)));
  }

  ASSERT_EQ(3, pl.size());
}


TEST(Payload, construct_finalize)
{
  test_context ctx{};

  std::vector<char> payload;
  payload.resize(200);

  vessel::payload pl{ctx.section_ctx->section_sizes, ctx.algo_opts,
    &payload[0], payload.size()};

  uint32_t * crc_offset = nullptr;
  // CRC32
  {
    auto [res, sec] = pl.add_section(VESSEL_SECTION_CRC32, VESSEL_TOPIC_METADATA);
    ASSERT_TRUE(res);
    ASSERT_EQ(sec.type, VESSEL_SECTION_CRC32);
    ASSERT_EQ(sec.topic, VESSEL_TOPIC_METADATA);
    ASSERT_EQ(sec.section_size, 8);
    crc_offset = static_cast<uint32_t *>(sec.data);
  }

  // With immediate data
  {
    char content_type[] = "foo=bar";
    auto [res, sec] = pl.add_section(VESSEL_SECTION_CONTENT_TYPE,
      42, sizeof(content_type), content_type);
    ASSERT_TRUE(res);
    ASSERT_EQ(sec.type, VESSEL_SECTION_CONTENT_TYPE);
    ASSERT_EQ(sec.topic, 42);
    ASSERT_EQ(sec.section_size, sizeof(content_type) + 6);
    ASSERT_EQ(sec.data_size, sizeof(content_type));

    ASSERT_EQ(0, memcmp(content_type, sec.data, sizeof(content_type)));
  }

  // This isn't used, but we need it - making it NULL and dereferencing it is
  // kind of evil, but it'll also lead to very obvious bugs when the code under
  // tests does *not* behave as expected.
  vessel::resource * dummy_res = nullptr;
  vessel::extent * dummy_ext = nullptr;

  pl.finalize(*dummy_res, *dummy_ext,
      [](vessel::resource const &, vessel::extent const &, vessel::section & sec,
        vessel::payload const &) -> bool
      {
        // We don't have to write a real checksum; we just have
        // to check later that it's the same value.
        uint32_t checksum = 0xf007b411;
        EXPECT_EQ(sizeof(checksum), sec.data_size);
        std::memcpy(sec.data, &checksum, sizeof(checksum));
        return true;
      });

  liberate::string::canonical_hexdump hd;
  LIBLOG_DEBUG("Payload:\n" << hd(pl.buffer, pl.buffer_size));

  // Check CRC32 has been written
  ASSERT_EQ(*crc_offset, 0xf007b411);

  // Check padding has been added
  size_t offset = pl.used();
  char val = *(pl.buffer + offset);
  while (++offset < pl.buffer_size) {
    ASSERT_EQ(val, *(pl.buffer + offset));
  }
}
