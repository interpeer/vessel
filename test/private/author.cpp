/**
 * This file is part of vessel.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt).
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#include <gtest/gtest.h>

#include "../../lib/author.h"

#include "../test_keys.h"

namespace {



} // anonymous namespace


TEST(AuthorId, created)
{
  vessel_algorithm_choices choices{};
  choices.author_identifier_hash = VESSEL_AIH_SHA3_224;
  auto opts = vessel::options_from_choices(choices);

  auto authid = vessel::create_default_author_id(opts);
  ASSERT_TRUE(vessel::is_valid_author_id(opts, authid));

  auto authid2 = vessel::create_default_author_id(opts);
  ASSERT_TRUE(vessel::is_valid_author_id(opts, authid2));

  ASSERT_EQ(authid, authid2);
}


TEST(AuthorId, extract_from_key)
{
  vessel_algorithm_choices choices{};
  choices.author_identifier_hash = VESSEL_AIH_SHA3_224;
  choices.asymmetric_signature = VESSEL_AS_ED25519;
  auto opts = vessel::options_from_choices(choices);

  auto kpair = s3kr1t::key::read_keys(test_keys::ed_private_key.c_str(),
      test_keys::ed_private_key.size());
  ASSERT_TRUE(kpair.is_valid());

  auto authid0 = vessel::extract_author_id(opts, *(kpair.pubkey));
  ASSERT_TRUE(vessel::is_valid_author_id(opts, authid0));

  auto pubkey = s3kr1t::key::read_pubkey(test_keys::ed_public_key.c_str(),
      test_keys::ed_public_key.size());

  auto authid1 = vessel::extract_author_id(opts, pubkey);
  ASSERT_TRUE(vessel::is_valid_author_id(opts, authid1));

  ASSERT_EQ(authid0, authid1);
}



TEST(AuthorId, extract_from_key_no_hash)
{
  vessel_algorithm_choices choices{};
  choices.author_identifier_hash = VESSEL_AIH_NONE;
  choices.asymmetric_signature = VESSEL_AS_ED25519;
  auto opts = vessel::options_from_choices(choices);

  auto kpair = s3kr1t::key::read_keys(test_keys::ed_private_key.c_str(),
      test_keys::ed_private_key.size());
  ASSERT_TRUE(kpair.is_valid());

  auto authid0 = vessel::extract_author_id(opts, *(kpair.pubkey));
  ASSERT_TRUE(vessel::is_valid_author_id(opts, authid0));

  auto pubkey = s3kr1t::key::read_pubkey(test_keys::ed_public_key.c_str(),
      test_keys::ed_public_key.size());

  auto authid1 = vessel::extract_author_id(opts, pubkey);
  ASSERT_TRUE(vessel::is_valid_author_id(opts, authid1));

  ASSERT_EQ(authid0, authid1);
}



TEST(Author, from_key)
{
  vessel_algorithm_choices choices{};
  choices.author_identifier_hash = VESSEL_AIH_SHA3_224;
  auto opts = vessel::options_from_choices(choices);

  auto kpair = s3kr1t::key::read_keys(test_keys::ed_private_key.c_str(),
      test_keys::ed_private_key.size());
  ASSERT_TRUE(kpair.is_valid());

  auto pubkey = s3kr1t::key::read_pubkey(test_keys::ed_public_key.c_str(),
      test_keys::ed_public_key.size());

  using author_t = vessel::author<int>;

  auto auth0 = author_t{opts, *(kpair.privkey)};
  auto auth1 = author_t{opts, pubkey};

  ASSERT_EQ(auth0, auth1);

  // The main difference is that auth0 can create, while both can verify.
  ASSERT_TRUE(auth0.can_create());
  ASSERT_TRUE(auth0.can_verify());
  ASSERT_FALSE(auth1.can_create());
  ASSERT_TRUE(auth1.can_verify());
}


TEST(Author, from_id)
{
  vessel_algorithm_choices choices{};
  choices.author_identifier_hash = VESSEL_AIH_SHA3_224;
  auto opts = vessel::options_from_choices(choices);

  auto kpair = s3kr1t::key::read_keys(test_keys::ed_private_key.c_str(),
      test_keys::ed_private_key.size());
  ASSERT_TRUE(kpair.is_valid());

  using author_t = vessel::author<int>;

  auto auth0 = author_t{opts, *(kpair.privkey)};
  auto auth1 = author_t{opts, auth0.author_id()};

  ASSERT_EQ(auth0, auth1);

  // The main difference is that auth0 can create and verify, while auth1 can do
  // neither; it just holds an id.
  ASSERT_TRUE(auth0.can_create());
  ASSERT_TRUE(auth0.can_verify());
  ASSERT_FALSE(auth1.can_create());
  ASSERT_FALSE(auth1.can_verify());
}


TEST(Author, counter)
{
  vessel_algorithm_choices choices{};
  choices.author_identifier_hash = VESSEL_AIH_SHA3_224;
  auto opts = vessel::options_from_choices(choices);

  auto pubkey = s3kr1t::key::read_pubkey(test_keys::ed_public_key.c_str(),
      test_keys::ed_public_key.size());

  using author_t = vessel::author<int>;

  auto auth = author_t{opts, pubkey, 42};

  // Reserve a counter; it should be one past the initial counter above. This should
  // work several times in a row.
  ASSERT_EQ(43, auth.reserve_counter());
  ASSERT_EQ(43, auth.current_counter());
  ASSERT_EQ(44, auth.reserve_counter());
  ASSERT_EQ(44, auth.current_counter());
  ASSERT_EQ(45, auth.reserve_counter());
  ASSERT_EQ(45, auth.current_counter());

  // Encountering a low counter should not change anything
  ASSERT_FALSE(auth.update_counter(3));
  ASSERT_EQ(46, auth.reserve_counter());
  ASSERT_EQ(46, auth.current_counter());

  // Encountering a higher counter should, however, update the internal
  // state.
  ASSERT_TRUE(auth.update_counter(99));
  ASSERT_EQ(100, auth.reserve_counter());
  ASSERT_EQ(100, auth.current_counter());
}


TEST(Author, sign_verify)
{
  vessel_algorithm_choices choices{};
  choices.author_identifier_hash = VESSEL_AIH_SHA3_224;
  auto opts = vessel::options_from_choices(choices);

  auto kpair = s3kr1t::key::read_keys(test_keys::ed_private_key.c_str(),
      test_keys::ed_private_key.size());
  ASSERT_TRUE(kpair.is_valid());

  using author_t = vessel::author<int>;

  auto auth0 = author_t{opts, *(kpair.privkey)};

  char text[] = "Hello, world!";
  char sigbuf[200];
  size_t size = sizeof(sigbuf);

  auto err = auth0.sign(sigbuf, size, text, sizeof(text));
  ASSERT_EQ(VESSEL_ERR_SUCCESS, err);
  ASSERT_LT(size, sizeof(sigbuf));

  err = auth0.verify(text, sizeof(text), sigbuf, size);
  ASSERT_EQ(VESSEL_ERR_SUCCESS, err);
}
