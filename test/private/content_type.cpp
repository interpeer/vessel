/*
 * This file is part of vessel.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt).
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <gtest/gtest.h>

#include <regex>

#include "../../lib/content_type.h"

namespace {

struct test_data
{
  std::string                   input;
  bool                          expect_success;

  vessel::content_type::kv_map  expected_values = {};
  vessel::content_type::kv_pos  expected_positions = {};

  std::string                   serialized = {};
} tests[] = {
  // Bad inputs
  { "foo", false },
  { "foo=bar\\;baz=quux", false },
  { "foo=42;foo=asdf", false },

  // Successs
  { {}, true },

  { "foo=bar", true,
    {
      { icu::UnicodeString{"foo"}, icu::UnicodeString{"bar"} },
    },
    {
      { 0, icu::UnicodeString{"foo"}, },
    },
    "foo=bar",
  },

  { " foo=bar", true,
    {
      { icu::UnicodeString{"foo"}, icu::UnicodeString{"bar"} },
    },
    {
      { 0, icu::UnicodeString{"foo"}, },
    },
    "foo=bar",
  },

  { "foo =bar", true,
    {
      { icu::UnicodeString{"foo"}, icu::UnicodeString{"bar"} },
    },
    {
      { 0, icu::UnicodeString{"foo"}, },
    },
    "foo=bar",
  },

  { "foo=bar; baz=quux", true,
    {
      { icu::UnicodeString{"foo"}, icu::UnicodeString{"bar"} },
      { icu::UnicodeString{"baz"}, icu::UnicodeString{"quux"} },
    },
    {
      { 0, icu::UnicodeString{"foo"}, },
      { 1, icu::UnicodeString{"baz"}, },
    },
    "foo=bar;baz=quux",
  },

  { "foo=bar ;baz=quux", true,
    {
      { icu::UnicodeString{"foo"}, icu::UnicodeString{"bar "} },
      { icu::UnicodeString{"baz"}, icu::UnicodeString{"quux"} },
    },
    {
      { 0, icu::UnicodeString{"foo"}, },
      { 1, icu::UnicodeString{"baz"}, },
    },
    "foo=bar ;baz=quux",
  },

  { "foo=bar;baz=\tquux", true,
    {
      { icu::UnicodeString{"foo"}, icu::UnicodeString{"bar"} },
      { icu::UnicodeString{"baz"}, icu::UnicodeString{"\tquux"} },
    },
    {
      { 0, icu::UnicodeString{"foo"}, },
      { 1, icu::UnicodeString{"baz"}, },
    },
    "foo=bar;baz=\tquux",
  },

  { "foo=bar; baz=quux; p=a", true,
    {
      { icu::UnicodeString{"foo"}, icu::UnicodeString{"bar"} },
      { icu::UnicodeString{"baz"}, icu::UnicodeString{"quux"} },
      { icu::UnicodeString{"p"}, icu::UnicodeString{"a"} },
    },
    {
      { 0, icu::UnicodeString{"foo"}, },
      { 1, icu::UnicodeString{"baz"}, },
      { 2, icu::UnicodeString{"p"}, },
    },
    "foo=bar;baz=quux;p=a",
  },

  { "foo=bar\\;baz\\=quux", true,
    {
      { icu::UnicodeString{"foo"}, icu::UnicodeString{"bar;baz=quux"} },
    },
    {
      { 0, icu::UnicodeString{"foo"}, },
    },
    "foo=bar\\;baz\\=quux",
  },
};



inline std::string
generate_name_value(testing::TestParamInfo<test_data> const & info)
{
  if (info.param.input.empty()) {
    return "__empty";
  }

  std::string ret = info.param.input;
  ret = std::regex_replace(ret, std::regex("[;]+"), "_sc_");
  ret = std::regex_replace(ret, std::regex("[=]+"), "_eq_");
  ret = std::regex_replace(ret, std::regex("[\\\\]+"), "_esc_");
  ret = std::regex_replace(ret, std::regex("[\\s]+"), "_space_");
  return ret;
}


class ContentType
  : public testing::TestWithParam<test_data>
{
};

} // anonymous namespace



TEST(ContentType, find_escaped_range)
{
  // Unescaped
  {
    auto [start, end] = vessel::find_escaped_range(0x003b, 0, icu::UnicodeString{"foo;bar;baz"});
    ASSERT_EQ(0, start);
    ASSERT_EQ(3, end);
  }

  {
    auto [start, end] = vessel::find_escaped_range(0x003b, 4, icu::UnicodeString{"foo;bar;baz"});
    ASSERT_EQ(4, start);
    ASSERT_EQ(7, end);
  }

  {
    auto [start, end] = vessel::find_escaped_range(0x003b, 8, icu::UnicodeString{"foo;bar;baz"});
    ASSERT_EQ(8, start);
    ASSERT_EQ(11, end);
  }

  // Escaped
  {
    auto [start, end] = vessel::find_escaped_range(0x003b, 0, icu::UnicodeString{"foo;bar\\;baz"});
    ASSERT_EQ(0, start);
    ASSERT_EQ(3, end);
  }

  {
    auto [start, end] = vessel::find_escaped_range(0x003b, 4, icu::UnicodeString{"foo;bar\\;baz"});
    ASSERT_EQ(4, start);
    ASSERT_EQ(12, end);
  }
}



TEST_P(ContentType, parse)
{
  auto td = GetParam();

  vessel::content_type ct{};
  auto err = vessel::parse_content_type(ct, td.input);
  if (td.expect_success) {
    ASSERT_EQ(VESSEL_ERR_SUCCESS, err);
  }
  else {
    ASSERT_NE(VESSEL_ERR_SUCCESS, err);
    return;
  }

  // Check values
  if (!td.expected_values.empty()) {
    ASSERT_EQ(td.expected_values.size(), ct.values.size());

    for (auto & [k, v] : td.expected_values) {
      ASSERT_EQ(ct.values[k], v);
    }
  }

  // Check positions
  if (!td.expected_positions.empty()) {
    ASSERT_EQ(td.expected_positions.size(), ct.positions.size());

    for (auto & [k, v] : td.expected_positions) {
      ASSERT_EQ(ct.positions[k], v);
    }
  }
}


TEST_P(ContentType, assemble)
{
  auto td = GetParam();
  if (td.expected_values.empty() || td.expected_positions.empty()) {
    GTEST_SKIP() << "Not applicable for this test data.";
    return;
  }

  vessel::content_type ct{};
  ct.values = td.expected_values;
  ct.positions = td.expected_positions;


  std::string serialized;
  auto err = vessel::serialize_content_type(serialized, ct);
  ASSERT_EQ(VESSEL_ERR_SUCCESS, err);

  ASSERT_EQ(td.serialized, serialized);
}



INSTANTIATE_TEST_SUITE_P(util, ContentType,
    testing::ValuesIn(tests),
    generate_name_value);



TEST(ContentType, assemble_empty)
{
  vessel::content_type ct{};

  std::string serialized;
  auto err = vessel::serialize_content_type(serialized, ct);

  // We expect success, but an empty result
  ASSERT_EQ(VESSEL_ERR_SUCCESS, err);
  ASSERT_TRUE(serialized.empty());
}


TEST(ContentType, assemble_with_new_pairs)
{
  vessel::content_type ct{};

  // Add some values at *positions*
  ct.values["foo"] = "bar";
  ct.positions[0] = "foo";

  ct.values["baz"] = "quux";
  ct.positions[1] = "baz";

  // Also add some unpositioned values. These will have to be appended.
  // Values are sorted, though, so key order matters
  ct.values["life"] = "42";
  ct.values["acab"] = "1312";

  std::string serialized;
  auto err = vessel::serialize_content_type(serialized, ct);
  ASSERT_EQ(VESSEL_ERR_SUCCESS, err);

  ASSERT_EQ("foo=bar;baz=quux;acab=1312;life=42", serialized);
}
