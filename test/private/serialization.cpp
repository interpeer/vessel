/**
 * This file is part of vessel.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt).
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#include <gtest/gtest.h>

#include "../../lib/serialization.h"

#include <liberate/string/hexencode.h>

// *** Envelope

TEST(Serialization, envelope_prefix_ok)
{
  using namespace vessel::serialization;

  char buf[5];
  auto used = serialize_envelope_prefix(buf, sizeof(buf), vessel::EV_DRAFT_00);
  ASSERT_EQ(used, 4);
  ASSERT_EQ('X',  buf[0]);
  ASSERT_EQ('E',  buf[1]);
  ASSERT_EQ('V',  buf[2]);
  ASSERT_EQ('\0', buf[3]);
}


TEST(Serialization, envelope_prefix_err)
{
  using namespace vessel::serialization;

  char buf[5];
  ASSERT_EQ(0, serialize_envelope_prefix(nullptr, sizeof(buf), vessel::EV_DRAFT_00));
  ASSERT_EQ(0, serialize_envelope_prefix(buf, 3, vessel::EV_DRAFT_00));
  ASSERT_EQ(0, serialize_envelope_prefix(buf, sizeof(buf), static_cast<vessel::ev_version>(42)));
}


TEST(Deserialization, envelope_prefix_ok)
{
  using namespace vessel::serialization;

  char buf[] = "XEV\0";
  vessel::ev_version version;
  auto used = deserialize_envelope_prefix(version, buf, sizeof(buf));
  ASSERT_EQ(used, 4);
  ASSERT_EQ(version, vessel::EV_DRAFT_00);
}



TEST(Deserialization, envelope_prefix_err)
{
  using namespace vessel::serialization;

  vessel::ev_version version;
  ASSERT_EQ(0, deserialize_envelope_prefix(version, nullptr, 6));
  ASSERT_EQ(0, deserialize_envelope_prefix(version, "XEV\0", 3));
  ASSERT_EQ(0, deserialize_envelope_prefix(version, "foobar", 6));
  ASSERT_EQ(0, deserialize_envelope_prefix(version, "XEV\1", 3));
}


TEST(Serialization, envelope_00_ok)
{
  using namespace vessel::serialization;

  vessel::extent_envelope_00 env;
  env.envelope_version = vessel::EV_DRAFT_00;
  env.version_tag = 0xdeadd00d;

  char buf[8];
  auto used = serialize_envelope_00(buf, sizeof(buf), env);
  ASSERT_EQ(used, 8);
  ASSERT_EQ('X',  buf[0]);
  ASSERT_EQ('E',  buf[1]);
  ASSERT_EQ('V',  buf[2]);
  ASSERT_EQ('\0', buf[3]);

  ASSERT_EQ('\xde', buf[4]);
  ASSERT_EQ('\xad', buf[5]);
  ASSERT_EQ('\xd0', buf[6]);
  ASSERT_EQ('\x0d', buf[7]);
}


TEST(Serialization, envelope_00_err)
{
  using namespace vessel::serialization;

  vessel::extent_envelope_00 env;
  env.envelope_version = vessel::EV_DRAFT_00;
  env.version_tag = 0xdeadd00d;

  char buf[8];
  ASSERT_EQ(0, serialize_envelope_00(nullptr, sizeof(buf), env));
  ASSERT_EQ(0, serialize_envelope_00(buf, 3, env));

  env.envelope_version = static_cast<vessel::ev_version>(42);
  ASSERT_EQ(0, serialize_envelope_00(buf, sizeof(buf), env));
}


TEST(Deserialization, envelope_00_ok)
{
  using namespace vessel::serialization;

  char buf[] = "XEV\0\xde\xad\xd0\x0d";

  vessel::extent_envelope_00 env;
  auto used = deserialize_envelope_00(env, buf, sizeof(buf));
  ASSERT_EQ(used, 8);
  ASSERT_EQ(env.envelope_version, vessel::EV_DRAFT_00);
  ASSERT_EQ(env.version_tag, 0xdeadd00d);
}



TEST(Deserialization, envelope_00_err)
{
  using namespace vessel::serialization;

  vessel::extent_envelope_00 env;
  ASSERT_EQ(0, deserialize_envelope_00(env, nullptr, 8));
  ASSERT_EQ(0, deserialize_envelope_00(env, "XEV\0abcd", 7));
  ASSERT_EQ(0, deserialize_envelope_00(env, "xEV\0abcd", 7));
  ASSERT_EQ(0, deserialize_envelope_00(env, "foobar12", 8));
  ASSERT_EQ(0, deserialize_envelope_00(env, "XEV\1abcd", 8));
}

// *** Header

TEST(Serialization, header_00_ok)
{
  using namespace liberate::types::literals;
  using namespace vessel::serialization;

  vessel::algorithm_options opts;
  opts.extent_identifier_size = 3;
  opts.author_identifier_size = 5;

  vessel::extent_header_00 header;
  header.extent_size_multiplier = 7;
  header.current_extent_identifier = {0xda_b, 0x11_b, 0xa5_b};
  header.authoring_counter = 42;
  header.previous_extent_identifier = {0x1a_b, 0x2a_b, 0x3a_b};
  header.author_identifier = {0xaa_b, 0xbb_b, 0xcc_b, 0xdd_b, 0xee_b};

  char buf[100];
  auto used = serialize_header_00(opts, buf, sizeof(buf), header);
  ASSERT_EQ(used, 16);

  char expected[] = "\x00\x07\xda\x11\xa5\x00\x00\x2a\x1a\x2a\x3a\xaa\xbb\xcc\xdd\xee";
  ASSERT_EQ(0, std::memcmp(buf, expected, used));
}


TEST(Serialization, header_00_err)
{
  using namespace liberate::types::literals;
  using namespace vessel::serialization;

  vessel::algorithm_options opts;
  opts.extent_identifier_size = 3;
  opts.author_identifier_size = 5;

  // This header is OK; we'll copy & modify it in tests
  vessel::extent_header_00 header;
  header.extent_size_multiplier = 7;
  header.current_extent_identifier = {0xda_b, 0x11_b, 0xa5_b};
  header.authoring_counter = 42;
  header.previous_extent_identifier = {0x1a_b, 0x2a_b, 0x3a_b};
  header.author_identifier = {0xaa_b, 0xbb_b, 0xcc_b, 0xdd_b, 0xee_b};

  char buf[100];

  // Bad buffers
  ASSERT_EQ(0, serialize_header_00(opts, nullptr, sizeof(buf), header));
  ASSERT_EQ(0, serialize_header_00(opts, buf, 0, header));
  ASSERT_EQ(0, serialize_header_00(opts, buf, 3, header));

  // Bad header
  {
    auto h = header;
    h.current_extent_identifier.resize(2);
    ASSERT_EQ(0, serialize_header_00(opts, buf, sizeof(buf), h));
  }
  {
    auto h = header;
    h.previous_extent_identifier.resize(9);
    ASSERT_EQ(0, serialize_header_00(opts, buf, sizeof(buf), h));
  }
  {
    auto h = header;
    h.author_identifier.clear();
    ASSERT_EQ(0, serialize_header_00(opts, buf, sizeof(buf), h));
  }
}


TEST(Deserialization, header_00_ok)
{
  using namespace liberate::types::literals;
  using namespace vessel::serialization;

  vessel::algorithm_options opts;
  opts.extent_identifier_size = 3;
  opts.author_identifier_size = 5;

  char serialized[] = "\x00\x07\xda\x11\xa5\x00\x00\x2a\x1a\x2a\x3a\xaa\xbb\xcc\xdd\xee";

  vessel::extent_header_00 header;

  auto used = deserialize_header_00(opts, header, serialized, sizeof(serialized));
  ASSERT_EQ(used, sizeof(serialized) - 1);

  ASSERT_EQ(header.extent_size_multiplier, 7);
  ASSERT_EQ(header.current_extent_identifier, (vessel::byte_sequence{0xda_b, 0x11_b, 0xa5_b}));
  ASSERT_EQ(header.authoring_counter, 42);
  ASSERT_EQ(header.previous_extent_identifier, (vessel::byte_sequence{0x1a_b, 0x2a_b, 0x3a_b}));
  ASSERT_EQ(header.author_identifier, (vessel::byte_sequence{0xaa_b, 0xbb_b, 0xcc_b, 0xdd_b, 0xee_b}));
}


TEST(Deserialization, header_00_err)
{
  using namespace liberate::types::literals;
  using namespace vessel::serialization;

  vessel::algorithm_options opts;
  opts.extent_identifier_size = 3;
  opts.author_identifier_size = 5;

  char serialized[] = "\x00\x07\xda\x11\xa5\x00\x00\x2a\x1a\x2a\x3a\xaa\xbb\xcc\xdd\xee";

  vessel::extent_header_00 header;

  // Bad buffer
  ASSERT_EQ(0, deserialize_header_00(opts, header, nullptr, sizeof(serialized)));
  ASSERT_EQ(0, deserialize_header_00(opts, header, serialized, 0));
  ASSERT_EQ(0, deserialize_header_00(opts, header, serialized, 3));

  // Bad input
  ASSERT_EQ(0, deserialize_header_00(opts, header, "foo", 3));
}


// *** Footer

TEST(Serialization, footer_00_ok)
{
  using namespace liberate::types::literals;
  using namespace vessel::serialization;

  vessel::algorithm_options opts;
  opts.signature_size = 13;

  vessel::extent_footer_00 footer;
  footer.signature = {0x00_b, 0x11_b, 0x22_b, 0x33_b,
                      0x44_b, 0x55_b, 0x66_b, 0x77_b,
                      0x88_b, 0x99_b, 0xaa_b, 0xbb_b,
                      0xcc_b};

  char buf[100];
  auto used = serialize_footer_00(opts, buf, sizeof(buf), footer);
  ASSERT_EQ(used, opts.signature_size);


  char expected[] = "\x00\x11\x22\x33\x44\x55\x66\x77\x88\x99\xaa\xbb\xcc";
  ASSERT_EQ(0, std::memcmp(buf, expected, used));
}


TEST(Serialization, footer_00_err)
{
  using namespace liberate::types::literals;
  using namespace vessel::serialization;

  vessel::algorithm_options opts;
  opts.signature_size = 13;

  vessel::extent_footer_00 footer;
  footer.signature = {0x00_b, 0x11_b, 0x22_b, 0x33_b,
                      0x44_b, 0x55_b, 0x66_b, 0x77_b,
                      0x88_b, 0x99_b, 0xaa_b, 0xbb_b,
                      0xcc_b};

  char buf[100];

  // Bad buffers
  ASSERT_EQ(0, serialize_footer_00(opts, nullptr, sizeof(buf), footer));
  ASSERT_EQ(0, serialize_footer_00(opts, buf, 0, footer));
  ASSERT_EQ(0, serialize_footer_00(opts, buf, 3, footer));

  // Bad footer
  {
    auto f = footer;
    f.signature.resize(2);
    ASSERT_EQ(0, serialize_footer_00(opts, buf, sizeof(buf), f));
  }
}


TEST(Deserialization, footer_00_ok)
{
  using namespace liberate::types::literals;
  using namespace vessel::serialization;

  vessel::algorithm_options opts;
  opts.signature_size = 13;

  char serialized[] = "\x00\x11\x22\x33\x44\x55\x66\x77\x88\x99\xaa\xbb\xcc";

  vessel::extent_footer_00 footer;
  auto used = deserialize_footer_00(opts, footer, serialized, sizeof(serialized));
  ASSERT_EQ(used, sizeof(serialized) - 1);

  ASSERT_EQ(footer.signature, (
        vessel::byte_sequence{0x00_b, 0x11_b, 0x22_b, 0x33_b,
                              0x44_b, 0x55_b, 0x66_b, 0x77_b,
                              0x88_b, 0x99_b, 0xaa_b, 0xbb_b,
                              0xcc_b}
        ));
}


TEST(Deserialization, footer_00_err)
{
  using namespace liberate::types::literals;
  using namespace vessel::serialization;

  vessel::algorithm_options opts;
  opts.signature_size = 13;

  char serialized[] = "\x00\x11\x22\x33\x44\x55\x66\x77\x88\x99\xaa\xbb\xcc";

  vessel::extent_footer_00 footer;

  // Bad buffer
  ASSERT_EQ(0, deserialize_footer_00(opts, footer, nullptr, sizeof(serialized)));
  ASSERT_EQ(0, deserialize_footer_00(opts, footer, serialized, 0));
  ASSERT_EQ(0, deserialize_footer_00(opts, footer, serialized, 3));

  // Bad input
  ASSERT_EQ(0, deserialize_footer_00(opts, footer, "foo", 3));
}


// *** Sections

namespace {

inline void test_serialize_section_ok(
    vessel_section_t type,
    char * data,
    size_t data_size,
    char const * expected,
    size_t expected_size)
{
  using namespace liberate::types::literals;
  using namespace vessel::serialization;

  vessel_section_context::section_size_map section_sizes;
  vessel::algorithm_options opts;
  opts.message_authentication_size = 3;
  opts.signature_size = 5;

  vessel::section section;
  section.type = type;
  section.topic = VESSEL_TOPIC_CUSTOM_START + 42;
  section.data = data;
  section.data_size = data_size;

  char buf[100];
  auto used = serialize_section(section_sizes, opts, buf, sizeof(buf), section);
  ASSERT_EQ(used, expected_size);

  liberate::string::hexdump hd;
  ASSERT_EQ(0, std::memcmp(buf, expected, used))
    << "Got:" << std::endl << hd(buf, used) << std::endl
    << "Expected:" << std::endl << hd(expected, expected_size);
}


inline void test_serialize_big_variable_section_ok(
    vessel_section_t type,
    char * data,
    size_t data_size,
    size_t expected_size,
    char const * header,
    size_t header_size)
{
  using namespace liberate::types::literals;
  using namespace vessel::serialization;

  vessel_section_context::section_size_map section_sizes;
  vessel::algorithm_options opts;
  opts.message_authentication_size = 3;
  opts.signature_size = 5;

  vessel::section section;
  section.type = type;
  section.topic = VESSEL_TOPIC_CUSTOM_START + 42;
  section.data = data;
  section.data_size = data_size;

  std::vector<char> buf;
  buf.resize(expected_size);
  auto used = serialize_section(section_sizes, opts, &buf[0], buf.size(), section);
  ASSERT_EQ(used, expected_size);

  liberate::string::hexdump hd;
  ASSERT_EQ(0, std::memcmp(&buf[0], header, header_size))
    << "Got:" << std::endl << hd(&buf[0], header_size) << std::endl
    << "Expected:" << std::endl << hd(header, header_size);
}


inline void test_serialize_section_err(
    vessel_section_t type,
    char * data,
    size_t data_size)
{
  using namespace liberate::types::literals;
  using namespace vessel::serialization;

  vessel_section_context::section_size_map section_sizes;
  vessel::algorithm_options opts;
  opts.message_authentication_size = 3;
  opts.signature_size = 5;

  vessel::section section;
  section.type = type;
  section.topic = VESSEL_TOPIC_CUSTOM_START + 42;
  section.data = data;
  section.data_size = data_size;

  char buf[100];

  // Bad buffer
  ASSERT_EQ(0, serialize_section(section_sizes, opts, nullptr, sizeof(buf), section));
  ASSERT_EQ(0, serialize_section(section_sizes, opts, buf, 0, section));
  ASSERT_EQ(0, serialize_section(section_sizes, opts, buf, 3, section));
}


inline void test_deserialize_section_ok(
    vessel_section_t type,
    char * input,
    size_t input_size,
    char * data,
    size_t data_size)
{
  using namespace liberate::types::literals;
  using namespace vessel::serialization;

  vessel_section_context::section_size_map section_sizes;
  vessel::algorithm_options opts;
  opts.message_authentication_size = 3;
  opts.signature_size = 5;

  vessel::section section;

  auto used = deserialize_section(section_sizes, opts, section,
      input, input_size);
  ASSERT_EQ(used, input_size);

  ASSERT_EQ(type, section.type);
  ASSERT_EQ(VESSEL_TOPIC_CUSTOM_START + 42, section.topic);
  ASSERT_EQ(input_size, section.section_size);
  ASSERT_TRUE(section.data);
  ASSERT_EQ(data_size, section.data_size);

  liberate::string::hexdump hd;
  ASSERT_EQ(0, std::memcmp(section.data, data, data_size))
    << "Got:" << std::endl << hd(section.data, data_size) << std::endl
    << "Expected:" << std::endl << hd(data, data_size);
}


inline void test_deserialize_big_variable_section_ok(
    vessel_section_t type,
    size_t expected_data_size,
    char * header,
    size_t header_size)
{
  using namespace liberate::types::literals;
  using namespace vessel::serialization;

  vessel_section_context::section_size_map section_sizes;
  vessel::algorithm_options opts;
  opts.message_authentication_size = 3;
  opts.signature_size = 5;

  vessel::section section;

  // We may read out of bounds here, but it should really be ok.
  auto used = deserialize_section(section_sizes, opts, section,
      header, header_size + expected_data_size);
  ASSERT_GE(used, expected_data_size);

  ASSERT_EQ(type, section.type);
  ASSERT_EQ(VESSEL_TOPIC_CUSTOM_START + 42, section.topic);
  ASSERT_EQ(used, section.section_size);
  ASSERT_TRUE(section.data);
  ASSERT_EQ(expected_data_size, section.data_size);
}


inline void test_deserialize_section_err(
    char * input,
    size_t input_size)
{
  using namespace liberate::types::literals;
  using namespace vessel::serialization;

  vessel_section_context::section_size_map section_sizes;
  vessel::algorithm_options opts;
  opts.message_authentication_size = 3;
  opts.signature_size = 5;

  vessel::section section;

  // Bad buffer
  ASSERT_EQ(0, deserialize_section(section_sizes, opts, section,
        nullptr, input_size));
  ASSERT_EQ(0, deserialize_section(section_sizes, opts, section,
        input, 0));
  ASSERT_EQ(0, deserialize_section(section_sizes, opts, section,
        input, 3));

  // Bad input
  char tmp[] = "foo";
  ASSERT_EQ(0, deserialize_section(section_sizes, opts, section,
        tmp, 3));
}



} // anonymous namespace


TEST(Serialization, section_crc32_ok)
{
  // CRC32 is always 4
  char data[4] = { '\xaa', '\xbb', '\xcc', '\xdd' };
  char expected[] = "\x00\x01\x04\x2a\xaa\xbb\xcc\xdd";

  test_serialize_section_ok(VESSEL_SECTION_CRC32,
      data, sizeof(data),
      expected, sizeof(expected) - 1);
}


TEST(Serialization, section_crc32_err)
{
  // CRC32 is always 4
  char data[3] = { '\xaa', '\xbb', '\xcc' };

  test_serialize_section_err(VESSEL_SECTION_CRC32,
      data, sizeof(data));
}


TEST(Deserialization, section_crc32_ok)
{
  // CRC32 is always 4
  char data[4] = { '\xaa', '\xbb', '\xcc', '\xdd' };
  char input[] = "\x00\x01\x04\x2a\xaa\xbb\xcc\xdd";

  test_deserialize_section_ok(VESSEL_SECTION_CRC32,
      input, sizeof(input) - 1,
      data, sizeof(data));
}


TEST(Deserialization, section_crc32_err)
{
  // CRC32 is always 4
  char input[] = "\x00\x01\x04\x2a\xaa\xbb\xcc\xdd";

  test_deserialize_section_err(input, sizeof(input) - 1);
}


TEST(Serialization, section_mac_ok)
{
  // MAC size is 3 in these tests
  char data[3] = { '\xaa', '\xbb', '\xcc' };
  char expected[] = "\x00\x02\x04\x2a\xaa\xbb\xcc";

  test_serialize_section_ok(VESSEL_SECTION_MAC,
      data, sizeof(data),
      expected, sizeof(expected) - 1);
}


TEST(Serialization, section_mac_err)
{
  // MAC size is 3 in these tests
  char data[2] = { '\xaa', '\xbb' };

  test_serialize_section_err(VESSEL_SECTION_MAC,
      data, sizeof(data));
}


TEST(Deserialization, section_mac_ok)
{
  // MAC size is 3 in these tests
  char data[3] = { '\xaa', '\xbb', '\xcc' };
  char input[] = "\x00\x02\x04\x2a\xaa\xbb\xcc";

  test_deserialize_section_ok(VESSEL_SECTION_MAC,
      input, sizeof(input) - 1,
      data, sizeof(data));
}


TEST(Deserialization, section_mac_err)
{
  // MAC size is 3 in these tests
  char input[] = "\x00\x02\x04\x2a\xaa\xbb\xcc";

  test_deserialize_section_err(input, sizeof(input) - 1);
}


TEST(Serialization, section_signature_ok)
{
  // signature size is 5 in these tests
  char data[5] = { '\xaa', '\xbb', '\xcc', '\xdd', '\xee' };
  char expected[] = "\x00\x03\x04\x2a\xaa\xbb\xcc\xdd\xee";

  test_serialize_section_ok(VESSEL_SECTION_SIGNATURE,
      data, sizeof(data),
      expected, sizeof(expected) - 1);
}


TEST(Serialization, section_signature_err)
{
  // signature size is 5 in these tests
  char data[2] = { '\xaa', '\xbb' };

  test_serialize_section_err(VESSEL_SECTION_SIGNATURE,
      data, sizeof(data));
}


TEST(Deserialization, section_signature_ok)
{
  // signature size is 5 in these tests
  char data[5] = { '\xaa', '\xbb', '\xcc', '\xdd', '\xee' };
  char input[] = "\x00\x03\x04\x2a\xaa\xbb\xcc\xdd\xee";

  test_deserialize_section_ok(VESSEL_SECTION_SIGNATURE,
      input, sizeof(input) - 1,
      data, sizeof(data));
}


TEST(Deserialization, section_signature_err)
{
  // signature size is 5 in these tests
  char input[] = "\x00\x03\x04\x2a\xaa\xbb\xcc\xdd\xee";
  test_deserialize_section_err(input, sizeof(input) - 1);
}


TEST(Serialization, section_content_type_ok)
{
  // Content type section is variably sized.
  char data[5] = { '\xaa', '\xbb', '\xcc', '\xdd', '\xee' };
  char expected[] = "\x00\xa0\x04\x2a\x00\x0b\xaa\xbb\xcc\xdd\xee";

  test_serialize_section_ok(VESSEL_SECTION_CONTENT_TYPE,
      data, sizeof(data),
      expected, sizeof(expected) - 1);

  std::vector<char> bigdata;
  bigdata.resize(100'000);
  char bigexpected[] = "\x00\xa0\x04\x2a\x80\x01\x86\xa8";
  test_serialize_big_variable_section_ok(VESSEL_SECTION_CONTENT_TYPE,
      bigdata.data(), bigdata.size(),
      bigdata.size() + 8,
      bigexpected, sizeof(bigexpected));
}


TEST(Serialization, section_content_type_err)
{
  char data[5] = { '\xaa', '\xbb', '\xcc', '\xdd', '\xee' };
  test_serialize_section_err(VESSEL_SECTION_SIGNATURE,
      data, sizeof(data));
}


TEST(Deserialization, section_content_type_ok)
{
  // Content type section is variably sized.
  char data[5] = { '\xaa', '\xbb', '\xcc', '\xdd', '\xee' };
  char input[] = "\x00\xa0\x04\x2a\x00\x0b\xaa\xbb\xcc\xdd\xee";

  test_deserialize_section_ok(VESSEL_SECTION_CONTENT_TYPE,
      input, sizeof(input) - 1,
      data, sizeof(data));

  char bigexpected[] = "\x00\xa0\x04\x2a\x80\x01\x86\xa8";
  test_deserialize_big_variable_section_ok(VESSEL_SECTION_CONTENT_TYPE,
      100'000,
      bigexpected, sizeof(bigexpected));
}


TEST(Deserialization, section_content_type_err)
{
  // Content type section is variably sized.
  char input[] = "\x00\xa0\x04\x2a\x00\x0b\xaa\xbb\xcc\xdd\xee";
  test_deserialize_section_err(input, sizeof(input) - 1);
}
