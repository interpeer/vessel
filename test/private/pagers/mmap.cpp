/**
 * This file is part of vessel.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt).
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#include <gtest/gtest.h>

#include "../../../lib/pagers/mmap.h"

#include "../../file_deleter.h"

namespace {

struct test_data
{
  std::string filename;
} tests[] = {
  { "mmap_test_file" },
  { {} }, // anonumous mapping
};


inline std::string
generate_name_value(testing::TestParamInfo<test_data> const & info)
{
  if (info.param.filename.empty()) {
    return "anonymous_mapping";
  }
  return "file_mapping";
}

} // anonymous namespace


class MMapPager
  : public testing::TestWithParam<test_data>
{
};


TEST_P(MMapPager, single_page)
{
  auto td = GetParam();
  auto pager = std::make_shared<vessel::pagers::mmap_pager>(td.filename);
  test::file_deleter guard{td.filename};

  ASSERT_EQ(vessel::PK_MMAP, pager->kind());

  ASSERT_NE(0, pager->page_size());
  ASSERT_EQ(0, pager->pages_available());
  ASSERT_EQ(0, pager->pages_in_use());

  vessel::paged_memory mem;
  bzero(&mem, sizeof(mem));

  ASSERT_EQ(VESSEL_ERR_SUCCESS, pager->fetch(mem, 0, 1));
  ASSERT_TRUE(mem.data);
  ASSERT_EQ(1, mem.count);
  ASSERT_EQ(pager->page_size(), mem.size);
  ASSERT_TRUE(mem.responsible_pager.lock());

  ASSERT_EQ(1, pager->pages_in_use());
  ASSERT_EQ(1, pager->pages_available());

  // Write something into the page.
  strncpy(reinterpret_cast<char *>(mem.data), "Test", mem.size);

  // Free page
  ASSERT_EQ(VESSEL_ERR_SUCCESS, pager->release(mem));
  ASSERT_EQ(nullptr, mem.data);
  ASSERT_EQ(0, mem.count);
  ASSERT_EQ(0, mem.size);
  ASSERT_FALSE(mem.responsible_pager.lock());

  ASSERT_EQ(0, pager->pages_in_use());
  ASSERT_EQ(1, pager->pages_available());
}


TEST_P(MMapPager, multiple_pages)
{
  auto td = GetParam();
  auto pager = std::make_shared<vessel::pagers::mmap_pager>(td.filename);
  test::file_deleter guard{td.filename};

  ASSERT_EQ(vessel::PK_MMAP, pager->kind());

  ASSERT_NE(0, pager->page_size());
  ASSERT_EQ(0, pager->pages_available());
  ASSERT_EQ(0, pager->pages_in_use());

  vessel::paged_memory mem;
  bzero(&mem, sizeof(mem));

  ASSERT_EQ(VESSEL_ERR_SUCCESS, pager->fetch(mem, 0, 3));
  ASSERT_TRUE(mem.data);
  ASSERT_EQ(3, mem.count);
  ASSERT_EQ(3 * pager->page_size(), mem.size);
  ASSERT_TRUE(mem.responsible_pager.lock());

  ASSERT_EQ(3, pager->pages_in_use());
  ASSERT_EQ(3, pager->pages_available());

  // Fetch another page, this one at offset 1 - this overlaps, but should
  // succeed.
  vessel::paged_memory overlapped_mem;
  bzero(&overlapped_mem, sizeof(overlapped_mem));

  ASSERT_EQ(VESSEL_ERR_SUCCESS, pager->fetch(overlapped_mem, 1, 1));
  ASSERT_TRUE(overlapped_mem.data);
  ASSERT_EQ(1, overlapped_mem.count);
  ASSERT_EQ(pager->page_size(), overlapped_mem.size);
  ASSERT_TRUE(overlapped_mem.responsible_pager.lock());

  ASSERT_EQ(4, pager->pages_in_use());
  ASSERT_EQ(3, pager->pages_available());

  // Ensure the offset is as expected
  ASSERT_EQ(overlapped_mem.data,
      static_cast<char *>(mem.data) + pager->page_size());

  // Write something into the page.
  strncpy(static_cast<char *>(overlapped_mem.data), "Test", overlapped_mem.size);

  // Free the three pages
  ASSERT_EQ(VESSEL_ERR_SUCCESS, pager->release(mem));
  ASSERT_EQ(nullptr, mem.data);
  ASSERT_EQ(0, mem.count);
  ASSERT_EQ(0, mem.size);
  ASSERT_FALSE(mem.responsible_pager.lock());

  ASSERT_EQ(1, pager->pages_in_use());
  ASSERT_EQ(3, pager->pages_available());

  // Free the other page
  ASSERT_EQ(VESSEL_ERR_SUCCESS, pager->release(overlapped_mem));
  ASSERT_EQ(nullptr, overlapped_mem.data);
  ASSERT_EQ(0, overlapped_mem.count);
  ASSERT_EQ(0, overlapped_mem.size);
  ASSERT_FALSE(overlapped_mem.responsible_pager.lock());

  ASSERT_EQ(0, pager->pages_in_use());
  ASSERT_EQ(3, pager->pages_available());
}


TEST_P(MMapPager, fail_grow_while_pages_in_use)
{
  auto td = GetParam();
  auto pager = std::make_shared<vessel::pagers::mmap_pager>(td.filename);
  test::file_deleter guard{td.filename};

  ASSERT_EQ(vessel::PK_MMAP, pager->kind());

  ASSERT_NE(0, pager->page_size());
  ASSERT_EQ(0, pager->pages_available());
  ASSERT_EQ(0, pager->pages_in_use());

  // Fetch page at offset 0
  vessel::paged_memory mem;
  bzero(&mem, sizeof(mem));

  ASSERT_EQ(VESSEL_ERR_SUCCESS, pager->fetch(mem, 0, 1));
  ASSERT_TRUE(mem.data);
  ASSERT_EQ(1, mem.count);
  ASSERT_EQ(pager->page_size(), mem.size);
  ASSERT_TRUE(mem.responsible_pager.lock());

  ASSERT_EQ(1, pager->pages_in_use());
  ASSERT_EQ(1, pager->pages_available());

  // Fetch another page, this one at offset 1 - this fails
  vessel::paged_memory mem1;
  bzero(&mem1, sizeof(mem1));

  ASSERT_NE(VESSEL_ERR_SUCCESS, pager->fetch(mem1, 1, 1));

  // If we release the first page, it works again.
  ASSERT_EQ(VESSEL_ERR_SUCCESS, pager->release(mem));
  ASSERT_EQ(nullptr, mem.data);
  ASSERT_EQ(0, mem.count);
  ASSERT_EQ(0, mem.size);
  ASSERT_FALSE(mem.responsible_pager.lock());

  ASSERT_EQ(0, pager->pages_in_use());
  ASSERT_EQ(1, pager->pages_available());

  ASSERT_EQ(VESSEL_ERR_SUCCESS, pager->fetch(mem1, 1, 1));

  ASSERT_EQ(1, pager->pages_in_use());
  ASSERT_EQ(2, pager->pages_available());

  // Don't release anything; the pager should ake care of it on exit
}



INSTANTIATE_TEST_SUITE_P(pagers, MMapPager,
    testing::ValuesIn(tests),
    generate_name_value);
