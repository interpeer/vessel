/**
 * This file is part of vessel.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt).
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#include <gtest/gtest.h>

#include <set>
#include <unordered_set>

#include <liberate/random/unsafe_bits.h>
#include <liberate/serialization/integer.h>

#include "../../lib/extent.h"
#include "../../lib/author.h"

#include "../test_keys.h"

namespace {

template <typename author_idT>
inline author_idT
generate_author_id()
{
  liberate::random::unsafe_bits<liberate::types::byte> generator;
  author_idT ret;
  auto ptr = reinterpret_cast<liberate::types::byte *>(&ret);

  for (size_t i = 0 ; i < sizeof(author_idT) ; ++i, ++ptr) {
    *ptr = generator.get();
  }

  return ret;
}


template <typename author_idT>
inline std::vector<author_idT>
generate_author_ids(size_t amount)
{
  std::set<author_idT> ids;
  while (ids.size() < amount) {
    ids.insert(generate_author_id<author_idT>());
  }

  std::vector<author_idT> ret;
  for (auto & id : ids) {
    ret.push_back(id);
  }
  return ret;
}



template <typename dagT>
inline void
add_children(dagT & dag,
    typename dagT::extent_id_type const & parent_id,
    std::vector<typename dagT::author_id_type> const & authors,
    size_t min_children, size_t max_depth)
{
  liberate::random::unsafe_bits<size_t> generator{min_children, authors.size()};
  size_t num_children = generator.get();

  for (size_t i = 0 ; i < num_children ; ++i) {
    auto & author_id = authors[i];
    typename dagT::extent_id_type extent_id;
    vessel::extent_id_create_derived(extent_id, author_id, parent_id);

    auto success = dag.add(extent_id, author_id, parent_id);
    ASSERT_TRUE(success);

    if (max_depth > 0) {
      add_children(dag, extent_id, authors, min_children, max_depth - 1);
    }
  }
}



template <typename dagT>
inline void
generate_dag(dagT & dag, size_t min_children, size_t max_children, size_t max_depth)
{
  // Add root
  typename dagT::extent_id_type root_id;
  auto err = vessel::extent_id_create_new(root_id);

  auto success = dag.add(root_id);
  ASSERT_TRUE(success);

  // Generate authors. We need one unique author per sibling.
  auto authors = generate_author_ids<typename dagT::author_id_type>(max_children);
  ASSERT_EQ(max_children, authors.size());

  // Add children to the root
  add_children(dag, root_id, authors, min_children, max_depth);
}


struct test_data
{
  uint32_t extent_id;
  uint32_t parent_id;
  uint32_t author_id;
};

static const test_data test_dag_complete[] = {
  { 0x517ce179, 0x0, 0x0 }, // root
  { 0x5545f950, 0x517ce179, 0x8bb6f708 }, { 0x8581aa0a, 0x5545f950, 0x8bb6f708 },
  { 0xa65c751d, 0x8581aa0a, 0x90370b86 }, { 0x30dc1711, 0xa65c751d, 0x8eeb923d },
  { 0x7a55ce0d, 0x30dc1711, 0x8eeb923d }, { 0xae769639, 0x30dc1711, 0x90370b86 },
  { 0x4686808b, 0x30dc1711, 0x8fde9a8d }, { 0xd8fadb90, 0x30dc1711, 0x8bb6f708 },
  { 0xc782d361, 0xa65c751d, 0x90370b86 }, { 0xf1686113, 0xc782d361, 0x91b956ee },
  { 0xcd083655, 0xc782d361, 0x8bb6f708 }, { 0x8d2c7157, 0xc782d361, 0x90370b86 },
  { 0xac2a8595, 0xc782d361, 0x8eeb923d }, { 0x240539ea, 0xc782d361, 0x8fde9a8d },
  { 0x39430866, 0xa65c751d, 0x8fde9a8d }, { 0x203bab05, 0x39430866, 0x8fde9a8d },
  { 0x96e0e031, 0x39430866, 0x8eeb923d }, { 0xcec36eaa, 0x39430866, 0x91b956ee },
  { 0x9043e3ca, 0x39430866, 0x90370b86 }, { 0x90cff7fe, 0x39430866, 0x8bb6f708 },
  { 0x8a10bac5, 0xa65c751d, 0x8bb6f708 }, { 0x5b6c3624, 0x8a10bac5, 0x8eeb923d },
  { 0x4c98464c, 0x8a10bac5, 0x8bb6f708 }, { 0xedc15b93, 0x8a10bac5, 0x90370b86 },
  { 0xd5c7b4a3, 0x8a10bac5, 0x8fde9a8d }, { 0xeb50b166, 0x8581aa0a, 0x8bb6f708 },
  { 0x5988fc14, 0xeb50b166, 0x8bb6f708 }, { 0x13771c1c, 0x5988fc14, 0x8eeb923d },
  { 0x6a0c1525, 0x5988fc14, 0x91b956ee }, { 0x55807c58, 0x5988fc14, 0x8bb6f708 },
  { 0x566d0c71, 0x5988fc14, 0x90370b86 }, { 0x5321b5b0, 0x5988fc14, 0x8fde9a8d },
  { 0x7f48a143, 0xeb50b166, 0x8fde9a8d }, { 0x99f8d107, 0x7f48a143, 0x8eeb923d },
  { 0x30edb331, 0x7f48a143, 0x8bb6f708 }, { 0xbd974165, 0xeb50b166, 0x90370b86 },
  { 0x2ee94d4c, 0xbd974165, 0x8bb6f708 }, { 0x3c5830dc, 0xbd974165, 0x8eeb923d },
  { 0x0368e577, 0xeb50b166, 0x91b956ee }, { 0xf1ad3c6b, 0x0368e577, 0x8bb6f708 },
  { 0xc26791bd, 0x0368e577, 0x8eeb923d }, { 0xab0ab993, 0xeb50b166, 0x8eeb923d },
  { 0xbd2fb443, 0xab0ab993, 0x8eeb923d }, { 0x27fca97c, 0xab0ab993, 0x8bb6f708 },
  { 0xe02f8299, 0xab0ab993, 0x8fde9a8d }, { 0x0f412fc5, 0xab0ab993, 0x91b956ee },
  { 0xa68c7afc, 0xab0ab993, 0x90370b86 }, { 0x61a5629e, 0x8581aa0a, 0x8eeb923d },
  { 0xef4dc141, 0x61a5629e, 0x8bb6f708 }, { 0x95b76355, 0xef4dc141, 0x8bb6f708 },
  { 0x0f5e7a95, 0xef4dc141, 0x90370b86 }, { 0x1e61a3a5, 0xef4dc141, 0x8eeb923d },
  { 0xe5d265d3, 0x61a5629e, 0x8eeb923d }, { 0xa8978b57, 0xe5d265d3, 0x90370b86 },
  { 0x0e0b3f77, 0xe5d265d3, 0x8bb6f708 }, { 0x22ea7aae, 0xe5d265d3, 0x8eeb923d },
  { 0xb0995df5, 0x61a5629e, 0x90370b86 }, { 0xdb715e57, 0xb0995df5, 0x8bb6f708 },
  { 0xeecd9dab, 0xb0995df5, 0x90370b86 }, { 0xce19cffa, 0xb0995df5, 0x8eeb923d },
  { 0x4d5d2ce4, 0x8581aa0a, 0x91b956ee }, { 0x46440f31, 0x4d5d2ce4, 0x8eeb923d },
  { 0xd606df29, 0x46440f31, 0x8eeb923d }, { 0xeca38558, 0x46440f31, 0x8bb6f708 },
  { 0x509aa056, 0x4d5d2ce4, 0x8bb6f708 }, { 0xf9064218, 0x509aa056, 0x8bb6f708 },
  { 0xb1bd7c80, 0x509aa056, 0x8eeb923d }, { 0x6f6444fa, 0x8581aa0a, 0x8fde9a8d },
  { 0x23efce58, 0x6f6444fa, 0x8eeb923d }, { 0x0e821032, 0x23efce58, 0x8bb6f708 },
  { 0x58b0cd51, 0x23efce58, 0x8eeb923d }, { 0x6b5fd3a4, 0x6f6444fa, 0x8bb6f708 },
  { 0xa0ba1634, 0x6b5fd3a4, 0x8eeb923d }, { 0xf6e21367, 0x6b5fd3a4, 0x8bb6f708 },
  { 0x729e0136, 0x5545f950, 0x90370b86 }, { 0x74273d54, 0x729e0136, 0x8eeb923d },
  { 0x47a31d4c, 0x74273d54, 0x8eeb923d }, { 0xc0684d03, 0x47a31d4c, 0x90370b86 },
  { 0x7afeef8b, 0x47a31d4c, 0x8fde9a8d }, { 0x1fe827c7, 0x47a31d4c, 0x8eeb923d },
  { 0xf63b77d8, 0x47a31d4c, 0x8bb6f708 }, { 0x4c736584, 0x74273d54, 0x8bb6f708 },
  { 0xddfd677a, 0x4c736584, 0x8eeb923d }, { 0xada3688a, 0x4c736584, 0x8bb6f708 },
  { 0x508dfee7, 0x4c736584, 0x90370b86 }, { 0xe8aeb4a5, 0x74273d54, 0x90370b86 },
  { 0xa501fd41, 0xe8aeb4a5, 0x8bb6f708 }, { 0x0866245c, 0xe8aeb4a5, 0x90370b86 },
  { 0x90200990, 0xe8aeb4a5, 0x8fde9a8d }, { 0xe54d5ad5, 0xe8aeb4a5, 0x8eeb923d },
  { 0xe7a518ca, 0x729e0136, 0x8bb6f708 }, { 0x37840547, 0xe7a518ca, 0x8eeb923d },
  { 0xc2171d2e, 0x37840547, 0x8bb6f708 }, { 0x14bbbb6e, 0x37840547, 0x90370b86 },
  { 0xca04d5ee, 0x37840547, 0x8eeb923d }, { 0x114fca9e, 0xe7a518ca, 0x8bb6f708 },
  { 0x6e7d0906, 0x114fca9e, 0x8eeb923d }, { 0x24409ca0, 0x114fca9e, 0x8bb6f708 },
  { 0xa5e24d62, 0x5545f950, 0x8eeb923d }, { 0x76f50517, 0xa5e24d62, 0x90370b86 },
  { 0x23b6c442, 0x76f50517, 0x8eeb923d }, { 0xa559d34b, 0x23b6c442, 0x8bb6f708 },
  { 0x76a40b50, 0x23b6c442, 0x8eeb923d }, { 0xe1e1deca, 0x76f50517, 0x8bb6f708 },
  { 0xed77f568, 0xe1e1deca, 0x8bb6f708 }, { 0x19bd4acc, 0xe1e1deca, 0x8eeb923d },
  { 0x83a73c7d, 0xa5e24d62, 0x8bb6f708 }, { 0xb1f75c6b, 0x83a73c7d, 0x90370b86 },
  { 0x7744b29f, 0xb1f75c6b, 0x8eeb923d }, { 0xc5d408a4, 0xb1f75c6b, 0x90370b86 },
  { 0x1c46fbc3, 0xb1f75c6b, 0x8bb6f708 }, { 0xb6366dfb, 0xb1f75c6b, 0x8fde9a8d },
  { 0x3e86336e, 0x83a73c7d, 0x8eeb923d }, { 0x57b24428, 0x3e86336e, 0x8bb6f708 },
  { 0xd15f0189, 0x3e86336e, 0x90370b86 }, { 0x42622adf, 0x3e86336e, 0x8eeb923d },
  { 0xed03d5ca, 0x83a73c7d, 0x8bb6f708 }, { 0x3043e63c, 0xed03d5ca, 0x8eeb923d },
  { 0x8a93528a, 0xed03d5ca, 0x8bb6f708 }, { 0x2752baf0, 0xed03d5ca, 0x90370b86 },
  { 0xdbe732c3, 0xa5e24d62, 0x8eeb923d }, { 0x569f544a, 0xdbe732c3, 0x8bb6f708 },
  { 0x6936fb37, 0x569f544a, 0x8eeb923d }, { 0x57b49368, 0x569f544a, 0x8fde9a8d },
  { 0x063426a0, 0x569f544a, 0x8bb6f708 }, { 0x11f0b6d0, 0x569f544a, 0x90370b86 },
  { 0xf4c07472, 0xdbe732c3, 0x90370b86 }, { 0x95b2b21c, 0xf4c07472, 0x91b956ee },
  { 0xf6dc462d, 0xf4c07472, 0x8fde9a8d }, { 0x362ff760, 0xf4c07472, 0x8bb6f708 },
  { 0x83968b83, 0xf4c07472, 0x90370b86 }, { 0x2d379cd6, 0xf4c07472, 0x8eeb923d },
  { 0x90a85dd3, 0xdbe732c3, 0x8eeb923d }, { 0xeff0db56, 0x90a85dd3, 0x8eeb923d },
  { 0x3095aeb9, 0x90a85dd3, 0x8bb6f708 }, { 0x3ca3dab9, 0x90a85dd3, 0x8fde9a8d },
  { 0x83db34c5, 0x90a85dd3, 0x90370b86 }, { 0x222615dd, 0xdbe732c3, 0x8fde9a8d },
  { 0xeeb3ec18, 0x222615dd, 0x91b956ee }, { 0x96707242, 0x222615dd, 0x90370b86 },
  { 0x6386dd97, 0x222615dd, 0x8eeb923d }, { 0xf6fa9fa2, 0x222615dd, 0x8bb6f708 },
  { 0xd5d5d9b6, 0x222615dd, 0x8fde9a8d }, { 0xfbc83584, 0x5545f950, 0x91b956ee },
  { 0x3327bd17, 0xfbc83584, 0x8eeb923d }, { 0xa23a6282, 0x3327bd17, 0x8fde9a8d },
  { 0x3a056e64, 0xa23a6282, 0x8bb6f708 }, { 0x3471cecc, 0xa23a6282, 0x8eeb923d },
  { 0xef496890, 0x3327bd17, 0x8eeb923d }, { 0xa5576d29, 0xef496890, 0x8eeb923d },
  { 0x9f187e30, 0xef496890, 0x91b956ee }, { 0x6d7dcc74, 0xef496890, 0x8bb6f708 },
  { 0x01b64187, 0xef496890, 0x90370b86 }, { 0x0c301db4, 0xef496890, 0x8fde9a8d },
  { 0x821793b2, 0x3327bd17, 0x8bb6f708 }, { 0xceadff51, 0x821793b2, 0x90370b86 },
  { 0xa7f6a685, 0x821793b2, 0x8bb6f708 }, { 0xcbd65d93, 0x821793b2, 0x8fde9a8d },
  { 0xa3905fb8, 0x821793b2, 0x8eeb923d }, { 0x392203eb, 0x3327bd17, 0x90370b86 },
  { 0xe419379b, 0x392203eb, 0x8eeb923d }, { 0x6202f2ef, 0x392203eb, 0x8bb6f708 },
  { 0x24a0c3c7, 0xfbc83584, 0x8bb6f708 }, { 0xf6c2f0ab, 0x24a0c3c7, 0x8eeb923d },
  { 0x28037339, 0xf6c2f0ab, 0x90370b86 }, { 0xf99be966, 0xf6c2f0ab, 0x8eeb923d },
  { 0x643fbfab, 0xf6c2f0ab, 0x8bb6f708 }, { 0x7fbe5ed0, 0x24a0c3c7, 0x8bb6f708 },
  { 0x9c8c342e, 0x7fbe5ed0, 0x8eeb923d }, { 0x6868de61, 0x7fbe5ed0, 0x8bb6f708 },
  { 0x5d7b837f, 0x7fbe5ed0, 0x90370b86 }, { 0x97897cb4, 0x5545f950, 0x8fde9a8d },
  { 0x9972bf08, 0x97897cb4, 0x8eeb923d }, { 0xfd626e42, 0x9972bf08, 0x8bb6f708 },
  { 0x0941714e, 0xfd626e42, 0x8eeb923d }, { 0xc48f5ccf, 0xfd626e42, 0x8bb6f708 },
  { 0x9a83a495, 0x9972bf08, 0x8eeb923d }, { 0x5645bd15, 0x9a83a495, 0x8eeb923d },
  { 0x92033c95, 0x9a83a495, 0x90370b86 }, { 0x726995d1, 0x9a83a495, 0x8bb6f708 },
  { 0x0b668b28, 0x97897cb4, 0x90370b86 }, { 0x06d40203, 0x0b668b28, 0x8eeb923d },
  { 0xe973102b, 0x06d40203, 0x90370b86 }, { 0xafd6e274, 0x06d40203, 0x8bb6f708 },
  { 0x85cf54e9, 0x06d40203, 0x8eeb923d }, { 0x627f17a1, 0x0b668b28, 0x8bb6f708 },
  { 0xf3021f2b, 0x627f17a1, 0x90370b86 }, { 0x840db288, 0x627f17a1, 0x8eeb923d },
  { 0x31378fbf, 0x627f17a1, 0x8bb6f708 }, { 0x186c23d7, 0x0b668b28, 0x90370b86 },
  { 0xf8314226, 0x186c23d7, 0x8bb6f708 }, { 0x69cc0c57, 0x186c23d7, 0x90370b86 },
  { 0x61352b70, 0x186c23d7, 0x8fde9a8d }, { 0x86b9a6af, 0x186c23d7, 0x8eeb923d },
  { 0xf2968d38, 0x97897cb4, 0x8fde9a8d }, { 0xdf8e5016, 0xf2968d38, 0x8fde9a8d },
  { 0x81a1ae62, 0xdf8e5016, 0x90370b86 }, { 0x39d54878, 0xdf8e5016, 0x91b956ee },
  { 0xfcffa386, 0xdf8e5016, 0x8bb6f708 }, { 0x44339ec6, 0xdf8e5016, 0x8fde9a8d },
  { 0x1e502ce3, 0xdf8e5016, 0x8eeb923d }, { 0xfb860125, 0xf2968d38, 0x8bb6f708 },
  { 0x3760d442, 0xfb860125, 0x90370b86 }, { 0x78b947ad, 0xfb860125, 0x8eeb923d },
  { 0x876f0dcb, 0xfb860125, 0x8bb6f708 }, { 0x03b557d1, 0xfb860125, 0x8fde9a8d },
  { 0xec2efebe, 0xf2968d38, 0x90370b86 }, { 0x57a4572a, 0xec2efebe, 0x91b956ee },
  { 0xd951565d, 0xec2efebe, 0x8fde9a8d }, { 0xa35403ac, 0xec2efebe, 0x8eeb923d },
  { 0xf33d80b8, 0xec2efebe, 0x8bb6f708 }, { 0x180dbce0, 0xec2efebe, 0x90370b86 },
  { 0xda3a2adc, 0xf2968d38, 0x8eeb923d }, { 0x03b4430e, 0xda3a2adc, 0x8fde9a8d },
  { 0x19cbdb44, 0xda3a2adc, 0x8bb6f708 }, { 0xceb33548, 0xda3a2adc, 0x8eeb923d },
  { 0xa8e610d9, 0xda3a2adc, 0x90370b86 }, { 0x128eceee, 0x97897cb4, 0x8bb6f708 },
  { 0xc19d7431, 0x128eceee, 0x8eeb923d }, { 0x08b97f88, 0xc19d7431, 0x8bb6f708 },
  { 0x8352c289, 0xc19d7431, 0x8fde9a8d }, { 0xc947fa9b, 0xc19d7431, 0x90370b86 },
  { 0x7f6249a8, 0xc19d7431, 0x8eeb923d }, { 0xa9d9bec7, 0xc19d7431, 0x91b956ee },
  { 0x8afa5a38, 0x128eceee, 0x8fde9a8d }, { 0x7679d64b, 0x8afa5a38, 0x8eeb923d },
  { 0xf35771ab, 0x8afa5a38, 0x8bb6f708 }, { 0x56dd1c81, 0x128eceee, 0x90370b86 },
  { 0x5bbe487c, 0x56dd1c81, 0x8eeb923d }, { 0x0325a2b4, 0x56dd1c81, 0x8bb6f708 },
  { 0x16b58ea8, 0x128eceee, 0x91b956ee }, { 0x1e7f7d06, 0x16b58ea8, 0x8eeb923d },
  { 0x7eae1251, 0x16b58ea8, 0x8bb6f708 }, { 0xb86e6cd6, 0x128eceee, 0x8bb6f708 },
  { 0x0bb7c71b, 0xb86e6cd6, 0x90370b86 }, { 0x16f0ed1b, 0xb86e6cd6, 0x8bb6f708 },
  { 0x44f2e664, 0xb86e6cd6, 0x8fde9a8d }, { 0x4c994874, 0xb86e6cd6, 0x91b956ee },
  { 0x080d22ad, 0xb86e6cd6, 0x8eeb923d }, { 0x4b4a4c5a, 0x517ce179, 0x90370b86 },
  { 0x3d096b12, 0x4b4a4c5a, 0x8bb6f708 }, { 0x84566713, 0x3d096b12, 0x90370b86 },
  { 0x5951d61c, 0x84566713, 0x8fde9a8d }, { 0x536c5243, 0x5951d61c, 0x8eeb923d },
  { 0x7814b487, 0x5951d61c, 0x8bb6f708 }, { 0x13579c27, 0x84566713, 0x90370b86 },
  { 0xfae2ef26, 0x13579c27, 0x8bb6f708 }, { 0x2a190845, 0x13579c27, 0x8eeb923d },
  { 0x7cf7a57f, 0x84566713, 0x91b956ee }, { 0x7581cf70, 0x7cf7a57f, 0x90370b86 },
  { 0xc1f70883, 0x7cf7a57f, 0x8bb6f708 }, { 0xddd722e1, 0x7cf7a57f, 0x8eeb923d },
  { 0xfd2105eb, 0x84566713, 0x8bb6f708 }, { 0x869e5c21, 0xfd2105eb, 0x8bb6f708 },
  { 0xfb2a245a, 0xfd2105eb, 0x8fde9a8d }, { 0x016a487d, 0xfd2105eb, 0x90370b86 },
  { 0xf89f89d4, 0xfd2105eb, 0x91b956ee }, { 0xecae3fd6, 0xfd2105eb, 0x8eeb923d },
  { 0x7f1486fd, 0x84566713, 0x8eeb923d }, { 0xf0c5f811, 0x7f1486fd, 0x8bb6f708 },
  { 0xa0c639b2, 0x7f1486fd, 0x8eeb923d }, { 0xbbaeb6a9, 0x3d096b12, 0x8bb6f708 },
  { 0xb4101014, 0xbbaeb6a9, 0x8eeb923d }, { 0x47a37343, 0xb4101014, 0x8bb6f708 },
  { 0x93e132c1, 0xb4101014, 0x8eeb923d }, { 0x6b7a97d5, 0xb4101014, 0x90370b86 },
  { 0xbb659073, 0xbbaeb6a9, 0x90370b86 }, { 0x93a55725, 0xbb659073, 0x90370b86 },
  { 0x26fe074c, 0xbb659073, 0x8eeb923d }, { 0x4cb6bdb6, 0xbb659073, 0x8bb6f708 },
  { 0xb1a1cdfa, 0xbbaeb6a9, 0x8bb6f708 }, { 0x4070680e, 0xb1a1cdfa, 0x8eeb923d },
  { 0xcd092212, 0xb1a1cdfa, 0x90370b86 }, { 0xd5e4fbee, 0xb1a1cdfa, 0x8bb6f708 },
  { 0x119b82ec, 0x3d096b12, 0x8eeb923d }, { 0x868fa77f, 0x119b82ec, 0x8fde9a8d },
  { 0x78b2b203, 0x868fa77f, 0x90370b86 }, { 0xbcb4c405, 0x868fa77f, 0x8bb6f708 },
  { 0x5f3b1385, 0x868fa77f, 0x8eeb923d }, { 0x9b0330a8, 0x868fa77f, 0x91b956ee },
  { 0xabc23cff, 0x868fa77f, 0x8fde9a8d }, { 0xdf8b89b2, 0x119b82ec, 0x90370b86 },
  { 0xd2d7d094, 0xdf8b89b2, 0x8fde9a8d }, { 0xabb757a0, 0xdf8b89b2, 0x8eeb923d },
  { 0x5b7362a3, 0xdf8b89b2, 0x90370b86 }, { 0xa8c019e9, 0xdf8b89b2, 0x8bb6f708 },
  { 0x694990e8, 0x119b82ec, 0x8eeb923d }, { 0xc8b08303, 0x694990e8, 0x90370b86 },
  { 0xc7bcca5e, 0x694990e8, 0x8fde9a8d }, { 0x1e399c7d, 0x694990e8, 0x8bb6f708 },
  { 0x316519ca, 0x694990e8, 0x8eeb923d }, { 0x46d0d9f6, 0x119b82ec, 0x8bb6f708 },
  { 0x4afd613a, 0x46d0d9f6, 0x8fde9a8d }, { 0x615acd49, 0x46d0d9f6, 0x90370b86 },
  { 0xa06aed68, 0x46d0d9f6, 0x8eeb923d }, { 0xbbedda73, 0x46d0d9f6, 0x8bb6f708 },
  { 0x8f8a8515, 0x4b4a4c5a, 0x8eeb923d }, { 0x55643723, 0x8f8a8515, 0x8bb6f708 },
  { 0xf8ebca24, 0x55643723, 0x8eeb923d }, { 0x118dfb3d, 0xf8ebca24, 0x8bb6f708 },
  { 0x61726c67, 0xf8ebca24, 0x90370b86 }, { 0x2d65688f, 0xf8ebca24, 0x8eeb923d },
  { 0xe5ca1a27, 0x55643723, 0x8bb6f708 }, { 0xae768e94, 0xe5ca1a27, 0x8bb6f708 },
  { 0x765cf7d9, 0xe5ca1a27, 0x90370b86 }, { 0x1636efe9, 0xe5ca1a27, 0x8eeb923d },
  { 0x7868fcf5, 0x55643723, 0x90370b86 }, { 0xdb14e028, 0x7868fcf5, 0x8eeb923d },
  { 0x801c1b87, 0x7868fcf5, 0x90370b86 }, { 0x8bb3c9c9, 0x7868fcf5, 0x8bb6f708 },
  { 0xf9758ceb, 0x7868fcf5, 0x8fde9a8d }, { 0x779d8346, 0x8f8a8515, 0x8eeb923d },
  { 0xa4375800, 0x779d8346, 0x8fde9a8d }, { 0x2db3c730, 0xa4375800, 0x90370b86 },
  { 0xb4163e5d, 0xa4375800, 0x8eeb923d }, { 0xd06e2e87, 0xa4375800, 0x8fde9a8d },
  { 0xde6f63af, 0xa4375800, 0x8bb6f708 }, { 0xf17e33ba, 0xa4375800, 0x91b956ee },
  { 0x63f00010, 0x779d8346, 0x8bb6f708 }, { 0xa53c4758, 0x63f00010, 0x8bb6f708 },
  { 0xb5c8935e, 0x63f00010, 0x90370b86 }, { 0x1ceb2e9e, 0x63f00010, 0x8eeb923d },
  { 0x746f60d6, 0x63f00010, 0x8fde9a8d }, { 0x9b18b458, 0x779d8346, 0x8eeb923d },
  { 0xadbfe336, 0x9b18b458, 0x8eeb923d }, { 0xb858e1ac, 0x9b18b458, 0x91b956ee },
  { 0xee58b7b3, 0x9b18b458, 0x8bb6f708 }, { 0xc404a0cd, 0x9b18b458, 0x90370b86 },
  { 0x6ec85ad9, 0x9b18b458, 0x8fde9a8d }, { 0x2c2b77ab, 0x779d8346, 0x90370b86 },
  { 0x5127e52a, 0x2c2b77ab, 0x8eeb923d }, { 0xaed5dc43, 0x2c2b77ab, 0x8bb6f708 },
  { 0x243d3e4b, 0x2c2b77ab, 0x91b956ee }, { 0x07cc076d, 0x2c2b77ab, 0x90370b86 },
  { 0x2dda04df, 0x2c2b77ab, 0x8fde9a8d }, { 0x3db129e9, 0x8f8a8515, 0x90370b86 },
  { 0xb460d069, 0x3db129e9, 0x8bb6f708 }, { 0x82b4405e, 0xb460d069, 0x8eeb923d },
  { 0xf8026cf2, 0xb460d069, 0x8bb6f708 }, { 0x847718d5, 0x3db129e9, 0x8eeb923d },
  { 0x71ba1280, 0x847718d5, 0x8bb6f708 }, { 0xd74fcacf, 0x847718d5, 0x8eeb923d },
  { 0xa26f22be, 0x517ce179, 0x8fde9a8d }, { 0xadb56938, 0xa26f22be, 0x8bb6f708 },
  { 0xa8830ab7, 0xadb56938, 0x8eeb923d }, { 0x3be23813, 0xa8830ab7, 0x8fde9a8d },
  { 0x78241c00, 0x3be23813, 0x91b956ee }, { 0x4c672c83, 0x3be23813, 0x8fde9a8d },
  { 0x0536eb87, 0x3be23813, 0x90370b86 }, { 0x3831788d, 0x3be23813, 0x8bb6f708 },
  { 0x8439e19a, 0x3be23813, 0x8eeb923d }, { 0x500bb22b, 0xa8830ab7, 0x8eeb923d },
  { 0x2c32c00f, 0x500bb22b, 0x8fde9a8d }, { 0x33b15d86, 0x500bb22b, 0x8eeb923d },
  { 0xfc3d50ba, 0x500bb22b, 0x8bb6f708 }, { 0x7420b8c3, 0x500bb22b, 0x90370b86 },
  { 0x6fe27768, 0xa8830ab7, 0x8bb6f708 }, { 0x7e4a980f, 0x6fe27768, 0x90370b86 },
  { 0x1d61f317, 0x6fe27768, 0x8fde9a8d }, { 0xff21808f, 0x6fe27768, 0x8eeb923d },
  { 0xd18f36e6, 0x6fe27768, 0x8bb6f708 }, { 0x6f066574, 0xa8830ab7, 0x90370b86 },
  { 0xb1b95285, 0x6f066574, 0x8fde9a8d }, { 0xa3f6bf86, 0x6f066574, 0x8bb6f708 },
  { 0x8f4c4fa7, 0x6f066574, 0x8eeb923d }, { 0x8a17d0ad, 0x6f066574, 0x90370b86 },
  { 0xa23e76c9, 0x6f066574, 0x91b956ee }, { 0xff33abf0, 0xadb56938, 0x8bb6f708 },
  { 0x0735430e, 0xff33abf0, 0x8bb6f708 }, { 0x42304f36, 0x0735430e, 0x90370b86 },
  { 0x2e9b868a, 0x0735430e, 0x8bb6f708 }, { 0x1188e4f8, 0x0735430e, 0x8eeb923d },
  { 0x1a4dda9a, 0xff33abf0, 0x8eeb923d }, { 0x0879a2b7, 0x1a4dda9a, 0x8bb6f708 },
  { 0xc3061abf, 0x1a4dda9a, 0x90370b86 }, { 0x510714ef, 0x1a4dda9a, 0x8eeb923d },
  { 0xfb7002c9, 0xff33abf0, 0x90370b86 }, { 0x27a3a6a4, 0xfb7002c9, 0x8eeb923d },
  { 0x813665a6, 0xfb7002c9, 0x8bb6f708 }, { 0x8b94cad2, 0xfb7002c9, 0x90370b86 },
  { 0xb07430ff, 0xadb56938, 0x90370b86 }, { 0x30e0e3bb, 0xb07430ff, 0x8eeb923d },
  { 0xdd5cfc64, 0x30e0e3bb, 0x8eeb923d }, { 0x88ed96d4, 0x30e0e3bb, 0x8bb6f708 },
  { 0x87a7a1ce, 0xb07430ff, 0x8bb6f708 }, { 0x4296c013, 0x87a7a1ce, 0x8bb6f708 },
  { 0x074b61d8, 0x87a7a1ce, 0x8eeb923d }, { 0xd62f5c60, 0xa26f22be, 0x8eeb923d },
  { 0x0bc0c2bd, 0xd62f5c60, 0x8bb6f708 }, { 0xef43383d, 0x0bc0c2bd, 0x8bb6f708 },
  { 0x60457a7c, 0xef43383d, 0x8bb6f708 }, { 0xf430d581, 0xef43383d, 0x8eeb923d },
  { 0x3577439e, 0x0bc0c2bd, 0x8eeb923d }, { 0x54e6193e, 0x3577439e, 0x8bb6f708 },
  { 0x2f14305d, 0x3577439e, 0x8eeb923d }, { 0x4ad4ab5e, 0x3577439e, 0x90370b86 },
  { 0x400d96e4, 0xd62f5c60, 0x8eeb923d }, { 0x3e15b67b, 0x400d96e4, 0x8eeb923d },
  { 0x3a5efe24, 0x3e15b67b, 0x8bb6f708 }, { 0x1a21319f, 0x3e15b67b, 0x90370b86 },
  { 0x827aceef, 0x3e15b67b, 0x8eeb923d }, { 0xb78d1599, 0x400d96e4, 0x90370b86 },
  { 0x3bbbb224, 0xb78d1599, 0x8bb6f708 }, { 0xcbb08658, 0xb78d1599, 0x90370b86 },
  { 0xdae10a5e, 0xb78d1599, 0x8fde9a8d }, { 0xc466fb9f, 0xb78d1599, 0x8eeb923d },
  { 0x96f6ccfa, 0x400d96e4, 0x8bb6f708 }, { 0x506db8ce, 0x96f6ccfa, 0x8bb6f708 },
  { 0xe3795dd2, 0x96f6ccfa, 0x90370b86 }, { 0x7c2cc4f4, 0x96f6ccfa, 0x8eeb923d },
  { 0x6825bb75, 0xa26f22be, 0x90370b86 }, { 0xe88ec08b, 0x6825bb75, 0x8fde9a8d },
  { 0x461b111d, 0xe88ec08b, 0x8eeb923d }, { 0xa082cd3a, 0x461b111d, 0x8fde9a8d },
  { 0x90afc674, 0x461b111d, 0x8eeb923d }, { 0x6b6a92a3, 0x461b111d, 0x91b956ee },
  { 0x03fbddc4, 0x461b111d, 0x8bb6f708 }, { 0xe9dab5e5, 0x461b111d, 0x90370b86 },
  { 0x9a11dc48, 0xe88ec08b, 0x8fde9a8d }, { 0xe171d06a, 0x9a11dc48, 0x8bb6f708 },
  { 0xa467a07c, 0x9a11dc48, 0x8eeb923d }, { 0x7341a5f8, 0xe88ec08b, 0x90370b86 },
  { 0x73b70455, 0x7341a5f8, 0x8eeb923d }, { 0xf58c809f, 0x7341a5f8, 0x91b956ee },
  { 0xf75c32a0, 0x7341a5f8, 0x90370b86 }, { 0xea0892ca, 0x7341a5f8, 0x8bb6f708 },
  { 0x8a175ecf, 0x7341a5f8, 0x8fde9a8d }, { 0x8284b1ff, 0xe88ec08b, 0x8bb6f708 },
  { 0x909c5351, 0x8284b1ff, 0x90370b86 }, { 0x27da6a89, 0x8284b1ff, 0x8bb6f708 },
  { 0xb5ebfb8c, 0x8284b1ff, 0x8eeb923d }, { 0x4dab3ce5, 0x8284b1ff, 0x8fde9a8d },
  { 0xaffc8098, 0x6825bb75, 0x8eeb923d }, { 0x7b4e184e, 0xaffc8098, 0x8bb6f708 },
  { 0x00932801, 0x7b4e184e, 0x8eeb923d }, { 0x83171a42, 0x7b4e184e, 0x8bb6f708 },
  { 0x468b797d, 0xaffc8098, 0x8eeb923d }, { 0x625bd774, 0x468b797d, 0x8bb6f708 },
  { 0xb008f6e9, 0x468b797d, 0x8eeb923d }, { 0xf12896a4, 0x6825bb75, 0x90370b86 },
  { 0x4e526b6b, 0xf12896a4, 0x8bb6f708 }, { 0xde6d663d, 0x4e526b6b, 0x8eeb923d },
  { 0x06ecdcbc, 0x4e526b6b, 0x8bb6f708 }, { 0xa02fe979, 0xf12896a4, 0x8eeb923d },
  { 0x2d808a06, 0xa02fe979, 0x8bb6f708 }, { 0xc258912a, 0xa02fe979, 0x90370b86 },
  { 0x27d13844, 0xa02fe979, 0x8fde9a8d }, { 0x9c4b2ade, 0xa02fe979, 0x8eeb923d },
  { 0xd639f6d5, 0x6825bb75, 0x8bb6f708 }, { 0xb84a780b, 0xd639f6d5, 0x8fde9a8d },
  { 0xc58661e5, 0xb84a780b, 0x8eeb923d }, { 0x04eb8ce9, 0xb84a780b, 0x8bb6f708 },
  { 0x6958971e, 0xd639f6d5, 0x8bb6f708 }, { 0x5b09d30d, 0x6958971e, 0x90370b86 },
  { 0x5f0e6d3d, 0x6958971e, 0x8fde9a8d }, { 0xf557d5bd, 0x6958971e, 0x8bb6f708 },
  { 0x2bfc9ef0, 0x6958971e, 0x8eeb923d }, { 0x76a48171, 0xd639f6d5, 0x8eeb923d },
  { 0x9032b75a, 0x76a48171, 0x8bb6f708 }, { 0x25158c5d, 0x76a48171, 0x8eeb923d },
  { 0x24bba4a5, 0x76a48171, 0x91b956ee }, { 0x30d11eed, 0x76a48171, 0x90370b86 },
  { 0x09a59ef7, 0x76a48171, 0x8fde9a8d }, { 0xfa25e3e5, 0xd639f6d5, 0x90370b86 },
  { 0x8cb2d602, 0xfa25e3e5, 0x90370b86 }, { 0x36e5d410, 0xfa25e3e5, 0x8fde9a8d },
  { 0xb81a192c, 0xfa25e3e5, 0x91b956ee }, { 0x2f81d463, 0xfa25e3e5, 0x8bb6f708 },
  { 0x832c87fa, 0xfa25e3e5, 0x8eeb923d }, { 0xd324c7f3, 0x517ce179, 0x8eeb923d },
  { 0xb513f34b, 0xd324c7f3, 0x8eeb923d }, { 0xba711d01, 0xb513f34b, 0x8fde9a8d },
  { 0xd58c0540, 0xba711d01, 0x90370b86 }, { 0xf7e82142, 0xd58c0540, 0x8eeb923d },
  { 0x2717c049, 0xd58c0540, 0x90370b86 }, { 0x8f9f9954, 0xd58c0540, 0x91b956ee },
  { 0x6701a9a0, 0xd58c0540, 0x8fde9a8d }, { 0x51e43fcf, 0xd58c0540, 0x8bb6f708 },
  { 0x27dc9b62, 0xba711d01, 0x8fde9a8d }, { 0x2d7d3264, 0x27dc9b62, 0x90370b86 },
  { 0x7096177b, 0x27dc9b62, 0x8fde9a8d }, { 0xe05292a2, 0x27dc9b62, 0x91b956ee },
  { 0xc462c1e8, 0x27dc9b62, 0x8eeb923d }, { 0x7a0a9ceb, 0x27dc9b62, 0x8bb6f708 },
  { 0x23c175b0, 0xba711d01, 0x8eeb923d }, { 0xa6930723, 0x23c175b0, 0x90370b86 },
  { 0x03d74933, 0x23c175b0, 0x91b956ee }, { 0xd1b2e568, 0x23c175b0, 0x8fde9a8d },
  { 0x7e41607d, 0x23c175b0, 0x8bb6f708 }, { 0x327b3691, 0x23c175b0, 0x8eeb923d },
  { 0x80587fc1, 0xba711d01, 0x8bb6f708 }, { 0x37264174, 0x80587fc1, 0x8eeb923d },
  { 0x9561f895, 0x80587fc1, 0x8bb6f708 }, { 0x806474a0, 0x80587fc1, 0x90370b86 },
  { 0x0f4ffcd3, 0x80587fc1, 0x8fde9a8d }, { 0xfb5e0f02, 0xb513f34b, 0x91b956ee },
  { 0x8da78e36, 0xfb5e0f02, 0x8bb6f708 }, { 0x38f735c4, 0x8da78e36, 0x8eeb923d },
  { 0xeb82eae2, 0x8da78e36, 0x8bb6f708 }, { 0x914cc4fc, 0xfb5e0f02, 0x8eeb923d },
  { 0x03a6b2b8, 0x914cc4fc, 0x8bb6f708 }, { 0x6d7605c7, 0x914cc4fc, 0x8eeb923d },
  { 0xefd09c30, 0xb513f34b, 0x8bb6f708 }, { 0x3481c53c, 0xefd09c30, 0x8eeb923d },
  { 0x59bd5e9b, 0x3481c53c, 0x90370b86 }, { 0x8dbd11a5, 0x3481c53c, 0x91b956ee },
  { 0x8b193ab5, 0x3481c53c, 0x8eeb923d }, { 0xc27263bc, 0x3481c53c, 0x8fde9a8d },
  { 0x2e0bb8cb, 0x3481c53c, 0x8bb6f708 }, { 0x00f0f245, 0xefd09c30, 0x8bb6f708 },
  { 0xb4bb5f11, 0x00f0f245, 0x90370b86 }, { 0xc1447d78, 0x00f0f245, 0x8bb6f708 },
  { 0x4eee9f83, 0x00f0f245, 0x91b956ee }, { 0xfa645d9d, 0x00f0f245, 0x8eeb923d },
  { 0xb442f6f5, 0x00f0f245, 0x8fde9a8d }, { 0x95968680, 0xefd09c30, 0x90370b86 },
  { 0x4343fa48, 0x95968680, 0x8bb6f708 }, { 0x01451c74, 0x95968680, 0x8eeb923d },
  { 0xbe208484, 0xefd09c30, 0x91b956ee }, { 0x0363e920, 0xbe208484, 0x8eeb923d },
  { 0x743b9d63, 0xbe208484, 0x8bb6f708 }, { 0x7d5261c1, 0xefd09c30, 0x8fde9a8d },
  { 0xfe65dfa8, 0x7d5261c1, 0x8eeb923d }, { 0x5e798df6, 0x7d5261c1, 0x8bb6f708 },
  { 0x3ca5f27e, 0xb513f34b, 0x8eeb923d }, { 0x68c85513, 0x3ca5f27e, 0x8bb6f708 },
  { 0x7c140e73, 0x68c85513, 0x8bb6f708 }, { 0xbd6db69b, 0x68c85513, 0x8eeb923d },
  { 0xe5bd81b8, 0x3ca5f27e, 0x8eeb923d }, { 0x34ce1a16, 0xe5bd81b8, 0x8bb6f708 },
  { 0x42e14093, 0xe5bd81b8, 0x90370b86 }, { 0x27788fc4, 0xe5bd81b8, 0x8eeb923d },
  { 0x3f121faa, 0xb513f34b, 0x90370b86 }, { 0xfb628a19, 0x3f121faa, 0x8bb6f708 },
  { 0x8620cc86, 0xfb628a19, 0x8eeb923d }, { 0xa025eec3, 0xfb628a19, 0x8bb6f708 },
  { 0x7c4d87f2, 0xfb628a19, 0x90370b86 }, { 0x0a5ec51c, 0x3f121faa, 0x90370b86 },
  { 0xc25a5e2e, 0x0a5ec51c, 0x8eeb923d }, { 0x9bebc662, 0x0a5ec51c, 0x90370b86 },
  { 0xa58b2eca, 0x0a5ec51c, 0x8fde9a8d }, { 0x7f6e4ed1, 0x0a5ec51c, 0x8bb6f708 },
  { 0x679cab82, 0x3f121faa, 0x8eeb923d }, { 0xd7754112, 0x679cab82, 0x8bb6f708 },
  { 0x26c261ab, 0x679cab82, 0x8eeb923d }, { 0x7e7d6ada, 0x679cab82, 0x90370b86 },
  { 0x102e0b8e, 0xd324c7f3, 0x8bb6f708 }, { 0x4e87bccf, 0x102e0b8e, 0x8bb6f708 },
  { 0x524d0dc2, 0x4e87bccf, 0x8bb6f708 }, { 0x71a54b0c, 0x524d0dc2, 0x8eeb923d },
  { 0x9c4fe476, 0x524d0dc2, 0x8bb6f708 }, { 0x5a7a70d4, 0x4e87bccf, 0x8eeb923d },
  { 0xbbd34d24, 0x5a7a70d4, 0x8bb6f708 }, { 0xee455057, 0x5a7a70d4, 0x8eeb923d },
  { 0xbab5a0c0, 0x5a7a70d4, 0x90370b86 }, { 0x2fcf2cef, 0x102e0b8e, 0x8eeb923d },
  { 0x362a9f83, 0x2fcf2cef, 0x8bb6f708 }, { 0xb870966c, 0x362a9f83, 0x90370b86 },
  { 0x15df19a9, 0x362a9f83, 0x8eeb923d }, { 0x169220ae, 0x362a9f83, 0x8bb6f708 },
  { 0x7e3404ac, 0x2fcf2cef, 0x90370b86 }, { 0xc3d117e3, 0x7e3404ac, 0x8bb6f708 },
  { 0x1536f0e5, 0x7e3404ac, 0x8eeb923d }, { 0xf5b172eb, 0x7e3404ac, 0x8fde9a8d },
  { 0x6393dbf5, 0x7e3404ac, 0x90370b86 }, { 0x609b12b2, 0x2fcf2cef, 0x8eeb923d },
  { 0x460c0107, 0x609b12b2, 0x90370b86 }, { 0x7a941289, 0x609b12b2, 0x8eeb923d },
  { 0xbda06dee, 0x609b12b2, 0x8bb6f708 },
};


static const test_data test_dag_complete_simple[] = {
  { 0xd4add00d, 0x0, 0x0 }, // root
  { 0x00010000, 0xd4add00d, 0x12345678 }, // First direct child
  { 0x00010001, 0x00010000, 0x12345678 }, // Child 1.1
  { 0x00010002, 0x00010000, 0x12345678 }, // Child 1.2
  { 0x00020000, 0xd4add00d, 0x12345678 }, // Second direct child
  { 0x00020001, 0x00020000, 0x12345678 }, // Child 2.1
  { 0x00020002, 0x00020000, 0x12345678 }, // Child 2.2
};


static const test_data test_dag_complete_orphans[] = {
  { 0xd4add00d, 0x0, 0x0 }, // root
  { 0x00010000, 0xd4add00d, 0x12345678 }, // First direct child
  { 0x00010001, 0x00010000, 0x12345678 }, // Child 1.1
  { 0x00010002, 0x00010000, 0x12345678 }, // Child 1.2
//  { 0x00020000, 0xd4add00d, 0x12345678 }, // Second direct child
  { 0x00020001, 0x00020000, 0x12345678 }, // Child 2.1
  { 0x00020002, 0x00020000, 0x12345678 }, // Child 2.2
  { 0x00200201, 0x00020002, 0x12345678 }, // Child 2.2.1
};


inline vessel::byte_sequence
i2id(uint32_t input)
{
  vessel::byte_sequence ret;
  ret.resize(sizeof(input));
  liberate::serialization::serialize_int(&ret[0], ret.size(), input);
  return ret;
}


template <typename dagT>
inline void
construct_dag_from(dagT & dag, test_data const * data, size_t elements)
{
  vessel_algorithm_choices choices{};
  choices.extent_identifier_hash = VESSEL_EIH_SHA3_512;
  auto opts = vessel::options_from_choices(choices);

  // The first entry is the root node.
  typename dagT::extent_id_type root_id = i2id(data[0].extent_id);
  auto success = dag.add(root_id);
  ASSERT_TRUE(success);

  // All following entries are added as additional nodes
  for (size_t i = 1 ; i < elements ; ++i) {
    success = dag.add(i2id(data[i].extent_id), i2id(data[i].author_id),
        i2id(data[i].parent_id));
    ASSERT_TRUE(success);
  }
}


} // anonymous namespace


TEST(ExtentIdDAG, extent_id_hashing)
{
  vessel_algorithm_choices choices{};
  choices.extent_identifier_hash = VESSEL_EIH_SHA3_512;
  auto opts = vessel::options_from_choices(choices);

  using dag_type = vessel::extent_id_dag;

  // Compare IDs
  vessel::byte_sequence extent_id1;
  vessel::extent_id_create_new(opts, extent_id1);

  vessel::byte_sequence extent_id2;
  vessel::extent_id_create_new(opts, extent_id2);

  ASSERT_NE(extent_id1, extent_id2);

  // Compare hashes
  dag_type::extent_id_hasher hasher;

  auto hash1 = hasher(extent_id1);
  auto hash2 = hasher(extent_id2);
  ASSERT_NE(hash1, hash2);

  // Use a set
  {
    std::set<vessel::byte_sequence> s;
    s.insert(extent_id1);
    ASSERT_EQ(1, s.size());
    s.insert(extent_id1);
    ASSERT_EQ(1, s.size());

    s.insert(extent_id2);
    ASSERT_EQ(2, s.size());

    s.insert(extent_id1);
    ASSERT_EQ(2, s.size());
  }

  // Use unordered set
  {
    std::unordered_set<vessel::byte_sequence, dag_type::extent_id_hasher> s;
    s.insert(extent_id1);
    ASSERT_EQ(1, s.size());
    s.insert(extent_id1);
    ASSERT_EQ(1, s.size());

    s.insert(extent_id2);
    ASSERT_EQ(2, s.size());

    s.insert(extent_id1);
    ASSERT_EQ(2, s.size());
  }
}


TEST(ExtentIdDAG, add_root_extent_id_no_author)
{
  vessel_algorithm_choices choices{};
  choices.extent_identifier_hash = VESSEL_EIH_SHA3_512;
  auto opts = vessel::options_from_choices(choices);

  using dag_type = vessel::extent_id_dag;

  dag_type dag;

  // Create and add a root (without author)
  vessel::byte_sequence extent_id1;
  vessel::extent_id_create_new(opts, extent_id1);

  auto success = dag.add(extent_id1);
  ASSERT_TRUE(success);
  ASSERT_TRUE(dag.contains(extent_id1));
  ASSERT_EQ(dag[extent_id1].extent_id, extent_id1);
  ASSERT_FALSE(dag[extent_id1].author_id.has_value());
  ASSERT_FALSE(dag[extent_id1].parent_id.has_value());

  // Adding another root extent ID must fail
  vessel::byte_sequence extent_id2;
  vessel::extent_id_create_new(opts, extent_id2);

  success = dag.add(extent_id2);
  ASSERT_FALSE(success);
  ASSERT_FALSE(dag.contains(extent_id2));
}



TEST(ExtentIdDAG, add_root_extent_id_with_author)
{
  vessel_algorithm_choices choices{};
  choices.extent_identifier_hash = VESSEL_EIH_SHA3_512;
  auto opts = vessel::options_from_choices(choices);

  using dag_type = vessel::extent_id_dag;

  dag_type dag;

  // We use the same author ID for both, just to make things a bit simpler for
  // us.
  vessel::byte_sequence author_id;

  // Create and add a root (without author)
  vessel::byte_sequence extent_id1;
  vessel::extent_id_create_new(opts, extent_id1);

  auto success = dag.add(extent_id1, author_id);
  ASSERT_TRUE(success);
  ASSERT_TRUE(dag.contains(extent_id1));
  ASSERT_EQ(dag[extent_id1].extent_id, extent_id1);
  ASSERT_TRUE(dag[extent_id1].author_id.has_value());
  ASSERT_FALSE(dag[extent_id1].parent_id.has_value());

  // Adding another root extent ID must fail
  vessel::byte_sequence extent_id2;
  vessel::extent_id_create_new(opts, extent_id2);

  success = dag.add(extent_id2, author_id);
  ASSERT_FALSE(success);
  ASSERT_FALSE(dag.contains(extent_id2));
}



TEST(ExtentIdDAG, add_dangling_extent_id)
{
  vessel_algorithm_choices choices{};
  choices.extent_identifier_hash = VESSEL_EIH_SHA3_512;
  choices.author_identifier_hash = VESSEL_AIH_SHA2_224;
  auto opts = vessel::options_from_choices(choices);

  using dag_type = vessel::extent_id_dag;

  dag_type dag;
  ASSERT_EQ(0, dag.num_orphans());

  // We use the same author ID for both, just to make things a bit simpler for
  // us.
  auto author_id = vessel::create_default_author_id(opts);

  // Create a root
  vessel::byte_sequence root_id;
  vessel::extent_id_create_new(opts, root_id);

  auto success = dag.add(root_id);
  ASSERT_TRUE(success);
  ASSERT_TRUE(dag.contains(root_id));
  ASSERT_EQ(0, dag.num_orphans());

  // Create a dangling extent ID. This requires a parent, which is not
  // itself added.
  vessel::byte_sequence parent_id;
  vessel::extent_id_create_derived(opts, parent_id, author_id, root_id);

  vessel::byte_sequence dangling_id;
  vessel::extent_id_create_derived(opts, dangling_id, author_id, parent_id);

  success = dag.add(dangling_id, author_id, parent_id);
  ASSERT_TRUE(success);
  ASSERT_TRUE(dag.contains(dangling_id));
  ASSERT_EQ(dag[dangling_id].extent_id, dangling_id);
  ASSERT_TRUE(dag[dangling_id].author_id.has_value());
  ASSERT_TRUE(dag[dangling_id].parent_id.has_value());

  ASSERT_EQ(1, dag.num_orphans());
}


TEST(ExtentIdDAG, unorphan_dangling_extent_id)
{
  vessel_algorithm_choices choices{};
  choices.extent_identifier_hash = VESSEL_EIH_SHA3_512;
  choices.author_identifier_hash = VESSEL_AIH_SHA2_224;
  auto opts = vessel::options_from_choices(choices);

  using dag_type = vessel::extent_id_dag;

  dag_type dag;
  EXPECT_EQ(0, dag.num_orphans());

  // We use the same author ID for both, just to make things a bit simpler for
  // us.
  auto author_id = vessel::create_default_author_id(opts);

  // Create a root
  vessel::byte_sequence root_id;
  vessel::extent_id_create_new(opts, root_id);

  auto success = dag.add(root_id);
  EXPECT_TRUE(success);
  EXPECT_TRUE(dag.contains(root_id));
  EXPECT_EQ(0, dag.num_orphans());

  // Create a dangling extent ID. This requires a parent, which is not
  // itself added.
  vessel::byte_sequence parent_id;
  vessel::extent_id_create_derived(opts, parent_id, author_id, root_id);

  vessel::byte_sequence dangling_id;
  vessel::extent_id_create_derived(opts, dangling_id, author_id, parent_id);

  success = dag.add(dangling_id, author_id, parent_id);
  EXPECT_TRUE(success);
  EXPECT_TRUE(dag.contains(dangling_id));
  EXPECT_EQ(dag[dangling_id].extent_id, dangling_id);
  EXPECT_TRUE(dag[dangling_id].author_id.has_value());
  EXPECT_TRUE(dag[dangling_id].parent_id.has_value());

  EXPECT_EQ(1, dag.num_orphans());

  // std::cerr << "BEFORE ADDING PARENT" << std::endl;
  // std::cerr << dag << std::endl;

  // If we add the parent ID now, this should un-orphan the dangling node.
  success = dag.add(parent_id, author_id, root_id);
  ASSERT_TRUE(success);

  // std::cerr << "AFTER ADDING PARENT" << std::endl;
  // std::cerr << dag << std::endl;

  ASSERT_TRUE(dag.contains(parent_id));
  ASSERT_EQ(dag[dangling_id].extent_id, dangling_id);
  ASSERT_TRUE(dag[dangling_id].author_id.has_value());
  ASSERT_TRUE(dag[dangling_id].parent_id.has_value());

  ASSERT_EQ(0, dag.num_orphans());
}


TEST(ExtentIdDAG, stable_iteration_simple)
{
  using dag_type = vessel::extent_id_dag;

  dag_type dag1;
  construct_dag_from(dag1, test_dag_complete_simple, sizeof(test_dag_complete_simple) / sizeof(test_data));

  dag_type dag2;
  construct_dag_from(dag2, test_dag_complete_simple, sizeof(test_dag_complete_simple) / sizeof(test_data));

  // Essentially test that iterating over two complete DAGs is always going
  // to yield the same elements in the same order.
  auto dag1_iter = dag1.begin();
  ASSERT_NE(dag1_iter, dag1.end());
  auto dag2_iter = dag2.begin();
  ASSERT_NE(dag2_iter, dag2.end());

  // Roots must be the same
  ASSERT_EQ(*dag1_iter, *dag2_iter);

  // Every iteration must have the same value.
  for ( ; dag1_iter != dag1.end() ; ++dag1_iter, ++dag2_iter) {
    ASSERT_NE(dag2_iter, dag2.end());
    ASSERT_EQ(*dag1_iter, *dag2_iter);
  }
}


TEST(ExtentIdDAG, stable_iteration)
{
  using dag_type = vessel::extent_id_dag;

  dag_type dag1;
  construct_dag_from(dag1, test_dag_complete, sizeof(test_dag_complete) / sizeof(test_data));

  dag_type dag2;
  construct_dag_from(dag2, test_dag_complete, sizeof(test_dag_complete) / sizeof(test_data));

  // Essentially test that iterating over two complete DAGs is always going
  // to yield the same elements in the same order.
  auto dag1_iter = dag1.begin();
  ASSERT_NE(dag1_iter, dag1.end());
  auto dag2_iter = dag2.begin();
  ASSERT_NE(dag2_iter, dag2.end());

  // Roots must be the same
  ASSERT_EQ(*dag1_iter, *dag2_iter);

  // Every iteration must have the same value.
  for ( ; dag1_iter != dag1.end() ; ++dag1_iter, ++dag2_iter) {
    ASSERT_NE(dag2_iter, dag2.end());
    ASSERT_EQ(*dag1_iter, *dag2_iter);
  }
}


TEST(ExtentIdDAG, stable_iteration_with_orphans)
{
  using dag_type = vessel::extent_id_dag;

  dag_type dag1;
  construct_dag_from(dag1, test_dag_complete_orphans, sizeof(test_dag_complete_orphans) / sizeof(test_data));

  dag_type dag2;
  construct_dag_from(dag2, test_dag_complete_orphans, sizeof(test_dag_complete_orphans) / sizeof(test_data));

  std::set<vessel::byte_sequence> found;

  // Essentially test that iterating over two complete DAGs is always going
  // to yield the same elements in the same order.
  auto dag1_iter = dag1.begin();
  ASSERT_NE(dag1_iter, dag1.end());
  auto dag2_iter = dag2.begin();
  ASSERT_NE(dag2_iter, dag2.end());

  // Roots must be the same
  ASSERT_EQ(*dag1_iter, *dag2_iter);

  // Every iteration must have the same value.
  for ( ; dag1_iter != dag1.end() ; ++dag1_iter, ++dag2_iter) {
    ASSERT_NE(dag2_iter, dag2.end());
    ASSERT_EQ(*dag1_iter, *dag2_iter);
    found.insert(dag1_iter->extent_id);
  }

  // Find an orphaned node to ensure all the tree has been iterated
  // over.
  auto iter = found.find(i2id(0x00020001));
  ASSERT_NE(iter, found.end());
}


TEST(ExtentIdDAG, find_nodes)
{
  using dag_type = vessel::extent_id_dag;

  dag_type dag;
  construct_dag_from(dag, test_dag_complete_orphans, sizeof(test_dag_complete_orphans) / sizeof(test_data));

  // Cannot find
  {
    auto iter = dag.find(i2id(0x0));
    ASSERT_EQ(iter, dag.end());
  }

  {
    auto iter = dag.find(i2id(0xdeadd00d));
    ASSERT_EQ(iter, dag.end());
  }

  // Should find
  {
    auto iter = dag.find(i2id(0xd4add00d));
    ASSERT_NE(iter, dag.end());
  }

  {
    auto iter = dag.find(i2id(0x00010000));
    ASSERT_NE(iter, dag.end());
  }

  {
    auto iter = dag.find(i2id(0x00200201));
    ASSERT_NE(iter, dag.end());
  }
}


TEST(ExtentIdDAG, siblings)
{
  using dag_type = vessel::extent_id_dag;

  dag_type dag;
  construct_dag_from(dag, test_dag_complete_orphans, sizeof(test_dag_complete_orphans) / sizeof(test_data));


  // Not found
  ASSERT_EQ(0, dag.num_siblings(i2id(0x0)));
  ASSERT_EQ(0, dag.num_siblings(i2id(0xdeadd00d)));
  ASSERT_EQ(0, dag.num_siblings(i2id(0x00020000)));

  // Root
  ASSERT_EQ(1, dag.num_siblings(i2id(0xd4add00d)));

  // Non-orphaned
  ASSERT_EQ(1, dag.num_siblings(i2id(0x00010000)));

  // Orphaned
  ASSERT_EQ(2, dag.num_siblings(i2id(0x00020001)));
  ASSERT_EQ(2, dag.num_siblings(i2id(0x00020002)));
}



TEST(ExtentIdDAG, iterate_orphan_subtree)
{
  using dag_type = vessel::extent_id_dag;

  dag_type dag;
  construct_dag_from(dag, test_dag_complete_orphans, sizeof(test_dag_complete_orphans) / sizeof(test_data));

  auto orphans = dag.orphans();
  ASSERT_FALSE(orphans.empty());
  ASSERT_EQ(2, orphans.size());

  std::set<vessel::byte_sequence> found;

  for (auto & orphan : orphans) {
    auto iter = dag.find(orphan);
    ASSERT_NE(iter, dag.end());

    for ( ; iter != dag.end() ; ++iter) {
      found.insert(iter->extent_id);
    }
  }

  // Two orphaned sub-trees with 3 nodes total
  ASSERT_EQ(3, found.size());
}


TEST(ExtentIdDAG, find_create_parent)
{
  using dag_type = vessel::extent_id_dag;

  dag_type dag;
  construct_dag_from(dag, test_dag_complete, sizeof(test_dag_complete) / sizeof(test_data));

  auto parent_id = dag.best_parent();

  // Best from the above DAG
  ASSERT_EQ(i2id(0x9c4fe476), parent_id);
}
