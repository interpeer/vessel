/**
 * This file is part of vessel.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt).
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>

#include <vessel.h>
#include <vessel/author.h>
#include <vessel/extent.h>

int main(int argc, char **argv)
{
  // Create options
  struct vessel_algorithm_choices choices;
  memset(&choices, 0, sizeof(choices));

  // Create a new author and serialize it to a buffer.
  struct vessel_author * author = NULL;
  struct vessel_author * author2 = NULL;
  vessel_error_t err = VESSEL_ERR_SUCCESS;
  char buf[200];
  memset(buf, 0, sizeof(buf));

  // Create author
  err = vessel_author_generate(&author, &choices);
  assert(VESSEL_ERR_SUCCESS == err);
  assert(author);

  // Serialize author to buffer.
  err = vessel_author_to_buffer(buf, sizeof(buf),
      &choices,
      author, VESSEL_KP_BOTH);
  assert(VESSEL_ERR_SUCCESS == err);

  // Deserailize from buffer
  err = vessel_author_from_buffer(&author2,
      &choices,
      buf, sizeof(buf));
  assert(VESSEL_ERR_SUCCESS == err);
  assert(author2);

  int res = vessel_author_compare(author, author2);
  assert(0 == res);

  // All good, free author(s)
  vessel_author_free(&author);
  assert(!author);

  vessel_author_free(&author2);
  assert(!author2);

  exit(0);
}
