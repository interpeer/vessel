/**
 * This file is part of vessel.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt).
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#ifndef VESSEL_VERSION_H
#define VESSEL_VERSION_H

#include <vessel.h>

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

struct vessel_version_data
{
  uint16_t major;
  uint16_t minor;
  uint16_t patch;
};

VESSEL_API struct vessel_version_data vessel_version(void);

VESSEL_API char const * vessel_copyright_string(void);
VESSEL_API char const * vessel_license_string(void);

#ifdef __cplusplus
} // extern "C"
#endif // __cplusplus

#endif // guard
