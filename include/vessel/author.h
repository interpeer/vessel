/**
 * This file is part of vessel.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt).
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#ifndef VESSEL_AUTHOR_H
#define VESSEL_AUTHOR_H

#include <vessel.h>

#include <vessel/algorithms.h>

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

/**
 * An authoring counter is a 24 bit counter, here represented by the nearest
 * larger integer.
 */
typedef uint32_t vessel_authoring_counter;


/**
 * An author similarly is an opaque structure, but it can be created from and
 * serialized to a cryptographic key (pair). It's also possible to extract
 * an author ID from it.
 * TODO swap some language with author_id below
 */
struct vessel_author;


/**
 * Generate a new author (key pair).
 */
VESSEL_API vessel_error_t
vessel_author_generate(struct vessel_author ** author,
    struct vessel_algorithm_choices const * choices);

/**
 * Free the author structure.
 */
VESSEL_API vessel_error_t
vessel_author_free(struct vessel_author ** author);


/**
 * Copy an author ID to/from a buffer. An author ID always requires exactly
 * vessel_author_size() of buffer space.
 *
 * Note that for compatibility reasons, vessel_author_from_buffer() may be
 * able to import various key serialization formats (PEM, etc.), but the
 * vessel_author_to_buffer() function always provides a DER encoded key.
 *
 * You can specify whether to write an entire author key pair or only the
 * public key.
 */
VESSEL_API vessel_error_t
vessel_author_from_buffer(struct vessel_author ** author,
    struct vessel_algorithm_choices const * choices,
    void const * buffer, size_t bufsize);

typedef enum
{
  VESSEL_KP_PUBLIC = 0,
  VESSEL_KP_PRIVATE = 1, // A private key encompasses the public key.
  VESSEL_KP_BOTH = VESSEL_KP_PRIVATE,
} vessel_key_part;


VESSEL_API vessel_error_t
vessel_author_to_buffer(void * buffer, size_t bufsize,
    struct vessel_algorithm_choices const * choices,
    struct vessel_author const * author,
    int key_part);

/**
 * Return the size of an author ID.
 */
VESSEL_API size_t
vessel_author_size(int key_part);



/**
 * Compare two authors; ordering authors makes no semantic sense, it's just
 * a bitwise order.
 */
VESSEL_API int
vessel_author_compare(struct vessel_author const * author1,
    struct vessel_author const * author2);





/**
 * An author ID is an opaque structure with the functions below defined for it.
 */
struct vessel_author_id;


/**
 * Generate a new author ID. The returned author ID is a default author ID that
 * works fine for the purposes of generating extent identifiers, but is not and
 * cannot be linked to a full author - and therefore cannot be used to sign
 * extents.
 */
VESSEL_API vessel_error_t
vessel_author_id_generate(struct vessel_author_id ** author_id,
    struct vessel_algorithm_choices const * choices);

/**
 * Extract an author ID from an author. This is produces a copy, which has to
 * be freed as a generated author ID.
 */
VESSEL_API vessel_error_t
vessel_author_id_from_author(struct vessel_author_id ** author_id,
    struct vessel_algorithm_choices const * choices,
    struct vessel_author const * author);

/**
 * Free the author ID structure.
 */
VESSEL_API vessel_error_t
vessel_author_id_free(struct vessel_author_id ** author_id);

/**
 * Return the size of an author ID.
 */
VESSEL_API size_t
vessel_author_id_size(struct vessel_algorithm_choices const * choices);


/**
 * Copy an author ID to/from a buffer. An author ID always requires exactly
 * vessel_author_id_size() of buffer space.
 */
VESSEL_API vessel_error_t
vessel_author_id_from_buffer(struct vessel_author_id ** author_id,
    struct vessel_algorithm_choices const * choices,
    void const * buffer, size_t bufsize);

VESSEL_API vessel_error_t
vessel_author_id_to_buffer(void * buffer, size_t * bufsize,
    struct vessel_algorithm_choices const * choices,
    struct vessel_author_id const * author_id);


/**
 * Compare two author ID; see comparing authors above.
 */
VESSEL_API int
vessel_author_id_compare(struct vessel_author_id const * author_id1,
    struct vessel_author_id const * author_id2);



#ifdef __cplusplus
} // extern "C"
#endif // __cplusplus


#endif // guard
