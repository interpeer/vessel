/**
 * This file is part of vessel.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt).
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#ifndef VESSEL_RESOURCE_H
#define VESSEL_RESOURCE_H

#include <vessel.h>

#include <vessel/context.h>
#include <vessel/store.h>

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

/**
 * A resource is a sequence of extents that belong together. You can think
 * of it as a file/file handle.
 */
struct vessel_resource;

/**
 * Create a resource.
 *
 * Resources are backed by a store. Whether the handle represents a new
 * or an existing resource depends on the store.
 *
 * - If an origin extent identifier is provided, and the store contains
 *   the corresponding extent, a handle for the existing resource matching
 *   that origin extent is returned.
 * - If no origin extent identifier is provided, a handle for a new resource
 *   is returned.
 * - If an origin extent identfifier is provided, but the store does not
 *   contain it, VESSEL_ERR_EXTENT_NOT_FOUND is returned.
 */
VESSEL_API vessel_error_t
vessel_resource_create(struct vessel_resource ** resource,
    struct vessel_context const * context,
    struct vessel_backing_store const * store,
    struct vessel_extent_id const * origin_extent_id);


/**
 * Free the resource. This flushes all pending extents to the store.
 */
VESSEL_API vessel_error_t
vessel_resource_free(struct vessel_resource ** resource);


/**
 * An iterator for extents in a resource.
 */
struct vessel_extent_iterator;

/**
 * Given a resource, create (read only) extent iterator.
 *
 * Using this iterator allows you to retrieve extents in logical order from
 * a resource.
 */
VESSEL_API vessel_error_t
vessel_extent_iterator_create(struct vessel_extent_iterator ** iterator,
    struct vessel_resource * resource);

VESSEL_API vessel_error_t
vessel_extent_iterator_free(struct vessel_extent_iterator ** iterator);

/**
 * Retrieve the next extent in the resource.
 *
 * If an extent is passed in the first parameter, it is released before a new
 * extent is loaded.
 *
 * Returns VESSEL_ERR_SUCCESS on success. If the resource ends,
 * VESSEL_ERR_EXTENT_NOT_FOUND is returned. Other return values indicate errors.
 *
 * Note: this performs blocking I/O if the extent has to be fetched from a remote
 *       store.
 */
VESSEL_API vessel_error_t
vessel_extent_iterator_next(struct vessel_extent ** extent,
    struct vessel_extent_iterator * iterator);


/**
 * Add an extent to the resource.
 *
 * This may create a new origin extent for a new resource, or add an extent
 * with its prior extent chosen according to the specs.
 *
 * Since this uses the store API in the background, the function returns
 * an operation id or 0 on errors.
 *
 * Fetch the result to retrieve the added extent.
 */
VESSEL_API vessel_operation_id
vessel_resource_add_extent(struct vessel_resource * resource,
    uint32_t version_tag, size_t size_in_blocks,
    struct vessel_author_id const * author,
    vessel_authoring_counter counter);


/**
 * Fetch an operation result.
 */
VESSEL_API vessel_error_t
vessel_resource_result(struct vessel_resource * resource,
    vessel_operation_id opid,
    struct vessel_extent ** extent);


/**
 * Commit a resource extent. Does not wait for the operation to finish,
 * but returns immediately.
 */
VESSEL_API vessel_error_t
vessel_resource_commit(struct vessel_resource * resource,
    struct vessel_extent * extent);


/**
 * Release a resource extent. Does not wait for the operation to finish,
 * but returns immediately.
 */
VESSEL_API vessel_error_t
vessel_resource_release(struct vessel_resource * resource,
    struct vessel_extent ** extent);


/**
 * make space flags
 * FIXME
 */
typedef enum
{
  VESSEL_MF_NONE              = 0,
  VESSEL_MF_ERROR_ON_UNUSABLE = 1,
  VESSEL_MF_NO_COMMIT         = 2,
} vessel_make_flag;

/**
 * Make space for a section. On success, the extent parameter contains
 * an extent with payload space for the given section. The data_size parameter
 * contains the data size available for this section.
 *
 * If an extent is already provided prior to the call, the function first
 * checks in this extent for available space. If no space is found, the extent
 * is released. It is committed as well if the appropriate flag is provided.
 *
 * If no extent is provided or the provided extent is too small, a new extent
 * is found and/or created until an extent with enough space can be returned.
 *
 * This function is meant to be called iteratively, adding section after section
 * to a resource, without having to worry too much about extent management.
 */
VESSEL_API vessel_error_t
vessel_resource_make_space_for_section(struct vessel_resource * resource,
    struct vessel_extent ** extent,
    struct vessel_algorithm_choices const * algos,
    vessel_section_t section, vessel_topic_t topic,
    size_t min_data_size,
    size_t * data_size,
    size_t max_extent_multiplier,
    struct vessel_author * author,
    int flags);


/**
 * Given an extent, add a section with the parameters provided to its payload.
 */
VESSEL_API vessel_error_t
vessel_resource_add_section(struct vessel_resource * resource,
    struct vessel_extent * extent,
    vessel_section_t sectype, vessel_topic_t topic,
    void * data,
    size_t data_size);

VESSEL_API vessel_error_t
vessel_resource_add_return_section(struct vessel_section * section,
    struct vessel_resource * resource,
    struct vessel_extent * extent,
    vessel_section_t sectype, vessel_topic_t topic,
    void * data,
    size_t data_size);



/**
 * This function simplifies the usage of vessel_resource_make_space_for_section()
 * and vessel_resource_add_section() for the specific use-case of adding a content
 * type section.
 */
VESSEL_API vessel_error_t
vessel_resource_add_content_type(struct vessel_resource * resource,
    struct vessel_extent ** extent,
    struct vessel_algorithm_choices const * algos,
    vessel_topic_t topic,
    struct vessel_content_type const * ct,
    size_t max_extent_multiplier,
    struct vessel_author * author, 
    int flags);


#ifdef __cplusplus
} // extern "C"
#endif // __cplusplus


#endif // guard
