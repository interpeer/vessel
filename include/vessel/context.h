/**
 * This file is part of vessel.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt).
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#ifndef VESSEL_CONTEXT_H
#define VESSEL_CONTEXT_H

#include <vessel.h>

#include <vessel/algorithms.h>
#include <vessel/section.h>

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

/**
 * The vessel_context functions like a library handle; it combines more
 * topic specific contexts into one.
 */
struct vessel_context;

/**
 * Create a vessel_context.
 *
 * Because the vessel_algorithm_context determines the scope of the
 * implementation choices, it'll be passed during creation.
 *
 * By contrast, the vessel_section_context is created internally, and
 * can be returned.
 */
VESSEL_API vessel_error_t
vessel_context_create(struct vessel_context ** context,
    struct vessel_algorithm_context const * algo_context);


/**
 * Retrieve the internal section context; this function can only fail if
 * the passed context is invalid, in which case it will return a NULL
 * pointer.
 */
VESSEL_API struct vessel_section_context *
vessel_context_get_section_ctx(struct vessel_context const * context);


/**
 * Free context with all internal contexts, except for the algorithm
 * context passed during creation.
 */
VESSEL_API vessel_error_t
vessel_context_free(struct vessel_context ** context);


#ifdef __cplusplus
} // extern "C"
#endif // __cplusplus


#endif // guard
