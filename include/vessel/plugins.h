/**
 * This file is part of vessel.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2023 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#ifndef VESSEL_PLUGINS_H
#define VESSEL_PLUGINS_H

#include <vessel.h>
#include <vessel/algorithms.h>

#define VESSEL_PLUGIN_TYPE_PROFILE  42
#define VESSEL_PLUGIN_PROFILE_V1    1

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

/**
 * The profile plugin type (in v1) defines only a single context member
 * initialized with a number of algorithm choices.
 */
struct vessel_plugin_profile_v1
{
  struct vessel_algorithm_context * algorithm_context;
};

typedef struct vessel_plugin_profile_v1 vessel_plugin_profile_v1;

#ifdef __cplusplus
} // extern "C"
#endif // __cplusplus

#endif // guard
