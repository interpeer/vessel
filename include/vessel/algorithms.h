/**
 * This file is part of vessel.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt).
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#ifndef VESSEL_ALGORITHMS_H
#define VESSEL_ALGORITHMS_H

#include <vessel.h>

/**
 * The header included here is generated via tools/version_tag_hash.py
 * - it includes enums for all algorithms, plus the definition of an
 *   vessel_algorithm_choices structure.
 */
#include <vessel/generated/enums.h>

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

/**
 * Given a specification of algorithm choices, this function creates
 * a matching vessel_algorithm_choices structure complete with version_tag.
 *
 * If the specificationis valid, the vessel_algorithm_choices struct will
 * contain a version_tag that is set to a value other than 0x0. This is the
 * tag to use in extent envelopes.
 */
VESSEL_API struct vessel_algorithm_choices
vessel_algorithm_choices_create(char const * spec);

/**
 * Inversely, given algorithm choices, create a spec from it.
 */
VESSEL_API vessel_error_t
vessel_algorithm_choices_serialize(char * out, size_t * out_size,
    struct vessel_algorithm_choices const * choices);



/**
 * The vessel_algorithm_context is an opaque structure that contains references
 * to one or more vessel_algorithm_choices; it can may be used in some API calls
 * to provide information about which combinations of algorithms an application
 * is interested in.
 */
struct vessel_algorithm_context;


/**
 * Given a set of vessel_algorithm_choices, create a vessel_algorithm_context
 * from them. The latter version of the function parses all spec strings.
 *
 * If any algorithm choice is invalid, ERR_BAD_ALGORITHMS is returned, and
 * the context is not set.
 */
VESSEL_API vessel_error_t
vessel_algorithm_context_create_from_choices(
    struct vessel_algorithm_context ** context,
    struct vessel_algorithm_choices const * choices,
    size_t amount);

VESSEL_API vessel_error_t
vessel_algorithm_context_create_from_specs(
    struct vessel_algorithm_context ** context,
    char const ** specs,
    size_t amount);


/**
 * Free a vessel_algorithm_context again.
 */
VESSEL_API vessel_error_t
vessel_algorithm_context_free(struct vessel_algorithm_context ** context);


/**
 * Return the number of unique algorithm choices in a context.
 */
VESSEL_API size_t
vessel_algorithm_context_choices(struct vessel_algorithm_context * context);

#ifdef __cplusplus
} // extern "C"
#endif // __cplusplus

#endif // guard
