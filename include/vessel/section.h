/**
 * This file is part of vessel.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt).
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#ifndef VESSEL_SECTION_H
#define VESSEL_SECTION_H

#include <vessel.h>

#include <vessel/algorithms.h>

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

#if defined(__cplusplus)
#  define VESSEL_SECTION_BASE_TYPE : vessel_section_t
#  define VESSEL_TOPIC_BASE_TYPE : vessel_topic_t
#else
#  define VESSEL_SECTION_BASE_TYPE
#  define VESSEL_TOPIC_BASE_TYPE
#endif

/**
 * Forward declarations
 */
struct vessel_context;

/**
 * Typedefs for section and topic identifier types.
 */
typedef uint16_t vessel_section_t;
typedef uint16_t vessel_topic_t;

/**
 * A section and its payload.
 */
struct vessel_section
{
  vessel_section_t  type;
  vessel_topic_t    topic;
  size_t            section_size;
  size_t            payload_size;
  void *            payload;
};


/**
 * The vessel_section_context is an opaque structure for use when registering
 * new section types. It requires a vessel_algorithm_context for creation.
 */
struct vessel_section_context;



/**
 * Some topics are pre-defined by the specs; this enum provides them.
 */
typedef enum VESSEL_TOPIC_BASE_TYPE {
  VESSEL_TOPIC_AAA            = 0x0000, // Authentication, ... (AAA)
  VESSEL_TOPIC_METADATA       = 0x00a0, // Metadata
  VESSEL_TOPIC_CUSTOM_START   = 0x0400, // Lowest non-reserved
} vessel_predefined_topics;


/**
 * Similarly, some section types are defined by the specs.
 */
typedef enum VESSEL_SECTION_BASE_TYPE {
  VESSEL_SECTION_CRC32        = 0x0001,
  VESSEL_SECTION_MAC          = 0x0002,
  VESSEL_SECTION_SIGNATURE    = 0x0003,
  VESSEL_SECTION_CONTENT_TYPE = 0x00a0,
  VESSEL_SECTION_BLOB         = 0x00a1,
  VESSEL_SECTION_CUSTOM_START = 0x0400,
} vessel_predefined_sections;


/**
 * Create and free a section context for use in other functions.
 *
 * TODO mainly for testing; this exists in vessel_context
 */
VESSEL_API vessel_error_t
vessel_section_context_create(
    struct vessel_section_context ** context,
    struct vessel_algorithm_context const * algorithm_context);

VESSEL_API vessel_error_t
vessel_section_context_free(struct vessel_section_context ** context);



/**
 * Given a section, return the section size. If the section is of a variable
 * size, 2 is returned. If the section size is unknown because the section
 * itself is not known, -1 is returned. On other errors, -2 is returned.
 *
 * Some section sizes depend on the choice of algorithms made. The first
 * parameter therefore is the algorithm choice to apply here.
 *
 * TODO if data_size is given and it's a variable sized section, we can calculate
 * the size, otherwise 0 may be returned. See vessel::section_size()
 */
VESSEL_API size_t
vessel_section_size(struct vessel_section_context const * context,
    struct vessel_algorithm_choices const * choices,
    vessel_section_t section, size_t data_size);



/**
 * Applications can register custom sections with either a fixed or variable
 * size. If the section is to have a variable size, pass 0 as the size.
 * Negative sizes are not accepted.
 *
 * Setting the same section multiple times will overwrite the section size
 * from previous invocations.
 */
VESSEL_API vessel_error_t
vessel_set_section(struct vessel_section_context * context,
    vessel_section_t section, int section_size);


/**
 * Given a buffer, write a section header into it, and return a data offset
 * and size. This takes into account the offsets for the header, section size
 * (if necessary), etc.
 *
 * For fixed-sized sections, the last parameter specifying a data size is not
 * necessary; it will be ignored. For variable-sized sections, you should
 * specify the amount of data you need to write. In that case, unless the
 * buffer is too small, the returned data size should be equal to the requested
 * data size.
 */
VESSEL_API vessel_error_t
vessel_section_write_header(
    void ** data, size_t * data_size,
    struct vessel_context const * context,
    uint32_t version_tag,
    void * buffer, size_t buffer_size,
    vessel_section_t section, vessel_topic_t topic,
    size_t requested_size);

// TODO write a section to a buffer.
// -> different sections have different requirements, so we'll need several
//    functions for known sections. Also one for writing a section *header*
//    only, returning a byte range for the data.
// - Also: delete a section. There are *a lot* of follow-on functions from
//   that.

/**
 * Holds a content type section in a way that can be manipulated.
 */
struct vessel_content_type;

// TODO docs

VESSEL_API vessel_error_t
vessel_content_type_create(struct vessel_content_type ** ct);

VESSEL_API vessel_error_t
vessel_content_type_free(struct vessel_content_type ** ct);


VESSEL_API vessel_error_t
vessel_content_type_from_utf8(struct vessel_content_type ** ct,
    char const * input, size_t input_size);

VESSEL_API vessel_error_t
vessel_content_type_set(struct vessel_content_type * ct,
    char const * key, size_t key_size,
    char const * value, size_t value_size);

VESSEL_API vessel_error_t
vessel_content_type_get(char * value, size_t * value_size,
    struct vessel_content_type const * ct,
    char const * key, size_t key_size);

VESSEL_API vessel_error_t
vessel_content_type_remove(struct vessel_content_type * ct,
    char const * key, size_t key_size);

VESSEL_API vessel_error_t
vessel_content_type_to_utf8(char * output, size_t * output_size,
    struct vessel_content_type const * ct);


#ifdef __cplusplus
} // extern "C"
#endif // __cplusplus


#endif // guard
