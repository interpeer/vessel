/**
 * This file is part of vessel.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt).
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#ifndef VESSEL_STORE_H
#define VESSEL_STORE_H

#include <vessel.h>

#include <vessel/extent.h>
#include <vessel/algorithms.h>

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

/**
 * Vessel accesses extent data via a backing store. Applications may supply
 * their own backing store by providing the functions contained within, or
 * use one of the predefined backing stores.
 *
 * Forward declaration.
 */
struct vessel_backing_store;


/**
 * Operations may be asynchronous. The pattern used here is that an operation
 * identifier is returned for most operations. It is possible to register a
 * callback that is invoked when an operation completes. Finally, each operation
 * also defines a function for fetching the operation result.
 */
typedef uint32_t vessel_operation_id;

typedef vessel_error_t (*vessel_store_operation_completed)(
    struct vessel_backing_store const * store,
    vessel_operation_id opid, void * baton);


/**
 * Free the opaque backing store data. This will be invoked by
 * vessel_backing_store_free() as required.
 */
typedef vessel_error_t (*vessel_store_data_free)(struct vessel_backing_store * store);


/**
 * Retrieve an extent by identifier. The version specifies the latest
 * version you know. The fetched extent may contain a newer version, but
 * must not contain an older version.
 */
typedef vessel_operation_id (*vessel_store_fetch_extent_latest)(
    struct vessel_backing_store const * store,
    struct vessel_extent_id const * id,
    vessel_authoring_counter version,
    vessel_store_operation_completed callback,
    void * baton
);


/**
 * Retrieve an extent by identifier with a specific version. If that
 * version is not found, nothing is returned.
 */
typedef vessel_operation_id (*vessel_store_fetch_extent_specific)(
    struct vessel_backing_store const * store,
    struct vessel_extent_id const * id,
    vessel_authoring_counter version,
    vessel_store_operation_completed callback,
    void * baton
);



/**
 * Create a new extent. The function accepts all the required parameters in
 * order to generate a valid extent envelope and header; this is all the
 * function is expected to do.
 */
typedef vessel_operation_id (*vessel_store_create_extent)(
    struct vessel_backing_store const * store,
    uint32_t version_tag, size_t size_in_blocks,
    struct vessel_author_id const * author,
    vessel_authoring_counter counter,
    struct vessel_extent_id const * previous_extent,
    vessel_store_operation_completed callback,
    void * baton
);


/**
 * Create an origin extent. This does not require a previous extent, and will
 * generate a random origin extent identifier.
 */
typedef vessel_operation_id (*vessel_store_create_extent_origin)(
    struct vessel_backing_store const * store,
    uint32_t version_tag, size_t size_in_blocks,
    struct vessel_author_id const * author,
    vessel_authoring_counter counter,
    vessel_store_operation_completed callback,
    void * baton
);



/**
 * Release an extent reference. This discards any changes you may have made.
 * The passed reference is immediately invalid, even if the release occurs
 * in the background.
 */
typedef vessel_operation_id (*vessel_store_release_extent)(
    struct vessel_backing_store const * store,
    struct vessel_extent ** extent,
    vessel_store_operation_completed callback,
    void * baton
);


/**
 * Commit an extent reference. This writes any changes you may have made to
 * the backing store. The passed reference remains valid; you still need to
 * release it when you're done.
 */
typedef vessel_operation_id (*vessel_store_commit_extent)(
    struct vessel_backing_store const * store,
    struct vessel_extent * extent,
    vessel_store_operation_completed callback,
    void * baton
);


/**
 * Retrieve the extent fetched or created in one of the prior calls.
 */
typedef vessel_error_t (*vessel_store_extent_result)(
    struct vessel_backing_store const * store,
    struct vessel_extent ** extent,
    vessel_operation_id opid
);


/**
 * Try and build an internal index of extents if possible. This may not
 * be possible for all store types, in which case VESSEL_ERR_NOT_IMPLEMENTED
 * is returned.
 *
 * Note that you cannot use the vessel_store_extent_result function for fetching
 * results for this. Instead, you have to use vessel_store_index_result, which
 * produces a vessel_store_index_iterator.
 */
typedef vessel_operation_id (*vessel_store_try_build_index)(
    struct vessel_backing_store const * store,
    vessel_store_operation_completed callback,
    void * baton
);


/**
 * An iterator for a store index.
 */
struct vessel_store_index_iterator;


/**
 * Retrieve a store index result.
 */
typedef vessel_error_t (*vessel_store_index_result)(
    struct vessel_backing_store const * store,
    struct vessel_store_index_iterator ** iter,
    vessel_operation_id opid
);


/**
 * Store indices are indices of extent identifiers; this function lets
 * you receive the identifier an index iterator refers to.
 *
 * It is ok to omit any of the values (i.e. set them to NULL); they will
 * then not be retrieved.
 */
typedef vessel_error_t (*vessel_store_index_iterator_value)(
    struct vessel_store_index_iterator const * iter,
    struct vessel_extent_id ** current_id,
    struct vessel_author_id ** author_id,
    struct vessel_extent_id ** previous_id
);


/**
 * Advance a store index iterator.
 */
typedef vessel_error_t (*vessel_store_index_iterator_next)(
    struct vessel_store_index_iterator * iter
);


/**
 * Free a store index iterator.
 */
typedef vessel_error_t (*vessel_store_index_iterator_free)(
    struct vessel_store_index_iterator ** iter
);




/**
 * Backing store definition
 */
struct vessel_backing_store
{
  // The data is opaque to vessel, and each backing store implementation must
  // cast it to its desired type, if any.
  void *                          data;

  // Fetch
  vessel_store_fetch_extent_latest              fetch_extent_latest;
  vessel_store_fetch_extent_specific            fetch_extent_specific;

  // Create
  vessel_store_create_extent                    create_extent;
  vessel_store_create_extent_origin             create_extent_origin;

  // Release/commit
  vessel_store_release_extent                   release_extent;
  vessel_store_commit_extent                    commit_extent;

  // Results of fetch or create
  vessel_store_extent_result                    extent_result;

  // Maintenance functions
  vessel_store_data_free                        data_free;

  // Index functions
  vessel_store_try_build_index                  try_build_index;
  vessel_store_index_result                     index_result;

  vessel_store_index_iterator_value             index_iterator_value;
  vessel_store_index_iterator_next              index_iterator_next;
  vessel_store_index_iterator_free              index_iterator_free;
};

typedef struct vessel_backing_store vessel_backing_store;



/**
 * Create backing stores in memory, or in a file.
 */
VESSEL_API vessel_error_t
vessel_backing_store_create_memory(struct vessel_backing_store ** store,
    struct vessel_algorithm_context * algo_ctx);


VESSEL_API vessel_error_t
vessel_backing_store_create_file(struct vessel_backing_store ** store,
    struct vessel_algorithm_context * algo_ctx,
    char const * filename);

/**
 * Free any backing store created by one of the above functions; you can also
 * free custom backing stores, of course, but MUST then provide the data_free()
 * function or risk memory leaks.
 */
VESSEL_API vessel_error_t
vessel_backing_store_free(struct vessel_backing_store ** store);


/**
 * Simplify a common operation; for a store index iterator, extract its value
 * and then advance it.
 *
 * Any of the values can be NULL; if so, they are not filled in.
 */
VESSEL_API vessel_error_t
vessel_store_index_iterator_forward(
    struct vessel_backing_store const * store,
    struct vessel_store_index_iterator * iter,
    struct vessel_extent_id ** current_id,
    struct vessel_author_id ** author_id,
    struct vessel_extent_id ** previous_id
);


#ifdef __cplusplus
} // extern "C"
#endif // __cplusplus


#endif // guard
