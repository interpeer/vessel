/**
 * Enumerations for all algorithms.
 */

#if defined(__cplusplus)
#  define VESSEL_ALGO_BASE_TYPE : uint8_t
#else
#  define VESSEL_ALGO_BASE_TYPE
#endif

typedef enum VESSEL_ALGO_BASE_TYPE {
  VESSEL_HA_SHA3_512 = 1,
  VESSEL_HA_SHA3_384 = 2,
  VESSEL_HA_SHA3_256 = 3,
  VESSEL_HA_SHA3_224 = 4,
  VESSEL_HA_SHA2_512 = 5,
  VESSEL_HA_SHA2_384 = 6,
  VESSEL_HA_SHA2_256 = 7,
  VESSEL_HA_SHA2_224 = 8,
  VESSEL_HA_NEXT = 9,
} vessel_hash_algorithms;

typedef enum VESSEL_ALGO_BASE_TYPE
{
  VESSEL_VTH_UNSUPPORTED = 0,
  VESSEL_VTH_SHA2_224 = VESSEL_HA_SHA2_224,
  VESSEL_VTH_SHA2_256 = VESSEL_HA_SHA2_256,
  VESSEL_VTH_SHA2_384 = VESSEL_HA_SHA2_384,
  VESSEL_VTH_SHA2_512 = VESSEL_HA_SHA2_512,
  VESSEL_VTH_SHA3_224 = VESSEL_HA_SHA3_224,
  VESSEL_VTH_SHA3_256 = VESSEL_HA_SHA3_256,
  VESSEL_VTH_SHA3_384 = VESSEL_HA_SHA3_384,
  VESSEL_VTH_SHA3_512 = VESSEL_HA_SHA3_512,
} vessel_version_tag_hash;

typedef enum VESSEL_ALGO_BASE_TYPE
{
  VESSEL_EIH_UNSUPPORTED = 0,
  VESSEL_EIH_SHA2_224 = VESSEL_HA_SHA2_224,
  VESSEL_EIH_SHA2_256 = VESSEL_HA_SHA2_256,
  VESSEL_EIH_SHA2_384 = VESSEL_HA_SHA2_384,
  VESSEL_EIH_SHA2_512 = VESSEL_HA_SHA2_512,
  VESSEL_EIH_SHA3_224 = VESSEL_HA_SHA3_224,
  VESSEL_EIH_SHA3_256 = VESSEL_HA_SHA3_256,
  VESSEL_EIH_SHA3_384 = VESSEL_HA_SHA3_384,
  VESSEL_EIH_SHA3_512 = VESSEL_HA_SHA3_512,
} vessel_extent_identifier_hash;

typedef enum VESSEL_ALGO_BASE_TYPE
{
  VESSEL_AIH_UNSUPPORTED = 0,
  VESSEL_AIH_NONE = VESSEL_HA_NEXT + 0,
  VESSEL_AIH_SHA2_224 = VESSEL_HA_SHA2_224,
  VESSEL_AIH_SHA2_256 = VESSEL_HA_SHA2_256,
  VESSEL_AIH_SHA2_384 = VESSEL_HA_SHA2_384,
  VESSEL_AIH_SHA2_512 = VESSEL_HA_SHA2_512,
  VESSEL_AIH_SHA3_224 = VESSEL_HA_SHA3_224,
  VESSEL_AIH_SHA3_256 = VESSEL_HA_SHA3_256,
  VESSEL_AIH_SHA3_384 = VESSEL_HA_SHA3_384,
  VESSEL_AIH_SHA3_512 = VESSEL_HA_SHA3_512,
} vessel_author_identifier_hash;

typedef enum VESSEL_ALGO_BASE_TYPE
{
  VESSEL_NH_UNSUPPORTED = 0,
  VESSEL_NH_SHA2_224 = VESSEL_HA_SHA2_224,
  VESSEL_NH_SHA2_256 = VESSEL_HA_SHA2_256,
  VESSEL_NH_SHA2_384 = VESSEL_HA_SHA2_384,
  VESSEL_NH_SHA2_512 = VESSEL_HA_SHA2_512,
  VESSEL_NH_SHA3_224 = VESSEL_HA_SHA3_224,
  VESSEL_NH_SHA3_256 = VESSEL_HA_SHA3_256,
  VESSEL_NH_SHA3_384 = VESSEL_HA_SHA3_384,
  VESSEL_NH_SHA3_512 = VESSEL_HA_SHA3_512,
} vessel_nonce_hash;

typedef enum VESSEL_ALGO_BASE_TYPE
{
  VESSEL_SH_UNSUPPORTED = 0,
  VESSEL_SH_EDDSA = VESSEL_HA_NEXT + 0,
  VESSEL_SH_SHA2_224 = VESSEL_HA_SHA2_224,
  VESSEL_SH_SHA2_256 = VESSEL_HA_SHA2_256,
  VESSEL_SH_SHA2_384 = VESSEL_HA_SHA2_384,
  VESSEL_SH_SHA2_512 = VESSEL_HA_SHA2_512,
  VESSEL_SH_SHA3_224 = VESSEL_HA_SHA3_224,
  VESSEL_SH_SHA3_256 = VESSEL_HA_SHA3_256,
  VESSEL_SH_SHA3_384 = VESSEL_HA_SHA3_384,
  VESSEL_SH_SHA3_512 = VESSEL_HA_SHA3_512,
} vessel_signature_hash;

typedef enum VESSEL_ALGO_BASE_TYPE
{
  VESSEL_AS_UNSUPPORTED = 0,
  VESSEL_AS_DSA = 1,
  VESSEL_AS_ED25519 = 2,
  VESSEL_AS_ED448 = 3,
  VESSEL_AS_RSA = 4,
} vessel_asymmetric_signature;

typedef enum VESSEL_ALGO_BASE_TYPE
{
  VESSEL_MA_UNSUPPORTED = 0,
  VESSEL_MA_KMAC128 = 1,
  VESSEL_MA_KMAC256 = 2,
  VESSEL_MA_POLY1305 = 3,
} vessel_message_authentication;

typedef enum VESSEL_ALGO_BASE_TYPE
{
  VESSEL_SE_UNSUPPORTED = 0,
  VESSEL_SE_AEAD_AES_128_CCM = 1,
  VESSEL_SE_AEAD_AES_128_GCM = 2,
  VESSEL_SE_AEAD_AES_256_CCM = 3,
  VESSEL_SE_AEAD_AES_256_GCM = 4,
  VESSEL_SE_CHACHA20 = 5,
  VESSEL_SE_NONE = 6,
} vessel_symmetric_encryption;

typedef enum VESSEL_ALGO_BASE_TYPE
{
  VESSEL_SA_UNSUPPORTED = 0,
  VESSEL_SA_AEAD = 1,
  VESSEL_SA_KEYPAIR = 2,
  VESSEL_SA_MAC = 3,
} vessel_signature_algorithm;

typedef enum VESSEL_ALGO_BASE_TYPE
{
  VESSEL_PHF_UNSUPPORTED = 0,
  VESSEL_PHF_PH_EQ_0 = 1,
  VESSEL_PHF_PH_EQ_1 = 2,
} vessel_private_header_flag;

/**
 * Algorithm choices structure.
 */
struct vessel_algorithm_choices
{
    uint32_t                        version_tag;
    vessel_version_tag_hash         version_tag_hash;
    vessel_extent_identifier_hash   extent_identifier_hash;
    vessel_author_identifier_hash   author_identifier_hash;
    vessel_nonce_hash               nonce_hash;
    vessel_signature_hash           signature_hash;
    vessel_asymmetric_signature     asymmetric_signature;
    vessel_message_authentication   message_authentication;
    vessel_symmetric_encryption     symmetric_encryption;
    vessel_signature_algorithm      signature_algorithm;
    vessel_private_header_flag      private_header_flag;
};

typedef struct vessel_algorithm_choices vessel_algorithm_choices;

