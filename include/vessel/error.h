/**
 * This file is part of vessel.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt).
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#ifndef VESSEL_ERROR_H
#define VESSEL_ERROR_H

#include <vessel.h>

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

/**
 * Macros for building a table of known error codes and associated messages.
 **/
#if !defined(VESSEL_START_ERRORS)
#define VESSEL_START_ERRORS \
  typedef uint32_t vessel_error_t; \
  enum vessel_error_code {
#define VESSEL_ERRDEF(name, code, desc) VESSEL_ ## name = code,
#define VESSEL_END_ERRORS \
    VESSEL_ERROR_LAST, \
    VESSEL_START_USER_RANGE = 1000 };
#define VESSEL_ERROR_FUNCTIONS
#endif


/*****************************************************************************
 * Error definitions
 **/
VESSEL_START_ERRORS

VESSEL_ERRDEF(ERR_SUCCESS,
    0,
    "No error")

VESSEL_ERRDEF(ERR_UNEXPECTED,
    1,
    "Nobody expects the Spanish Inquisition!")

VESSEL_ERRDEF(ERR_INVALID_VALUE,
    2,
    "An invalid parameter value was provided.")

VESSEL_ERRDEF(ERR_NOT_IMPLEMENTED,
    3,
    "The requested functionality was not implemented (yet).")

VESSEL_ERRDEF(ERR_FUNC_NOT_AVAILABLE,
    4,
    "The current build does not contain this functionality.")

VESSEL_ERRDEF(ERR_OPERATION_NOT_FOUND,
    5,
    "An asynchronous operation could not be retrieved.")

VESSEL_ERRDEF(ERR_INTERNAL_LOGIC,
    6,
    "An internal logic error was detected.")

VESSEL_ERRDEF(ERR_END_ITERATION,
    7,
    "Iteration of a sequence/container ended.")

VESSEL_ERRDEF(ERR_OUT_OF_MEMORY,
    10,
    "Could not allocate/map memory as requested.")

VESSEL_ERRDEF(ERR_MEMORY_ACCESS,
    11,
    "Invalid memory access/page fault.")

VESSEL_ERRDEF(ERR_IO,
    12,
    "I/O error.")

VESSEL_ERRDEF(ERR_INTERRUPTED,
    13,
    "An operation was interrupted.")

VESSEL_ERRDEF(ERR_BAD_ALGORITHMS,
    14,
    "Invalid set of algorithm choices encountered.")

VESSEL_ERRDEF(ERR_BAD_SECTION,
    15,
    "A bad section was passed.")

VESSEL_ERRDEF(ERR_EXTENT_NOT_FOUND,
    20,
    "Could not fetch the requested extent; it cannot be found.")

VESSEL_ERRDEF(ERR_KEY_GENERATION,
    30,
    "Could not generate necessary cryptographic keys.")

VESSEL_ERRDEF(ERR_KEY_SERIALIZATION,
    31,
    "Could not serialize or deserialize cryptographic keys for an author.")

VESSEL_ERRDEF(ERR_HASH_GENERATION,
    32,
    "Could not generate a necessary cryptographic hash.")

VESSEL_ERRDEF(ERR_AUTHORING,
    40,
    "Could not author/sign data!")

VESSEL_ERRDEF(ERR_VERIFICATION,
    41,
    "Could not verify author signature!")

VESSEL_ERRDEF(ERR_KEY_NOT_FOUND,
    50,
    "Could not find key in associative map.")

// TODO add errors as required

VESSEL_END_ERRORS


#if defined(VESSEL_ERROR_FUNCTIONS)

/*****************************************************************************
 * Functions
 **/

/**
 * Return the error message associated with the given error code. Never returns
 * nullptr; if an unknown error code is given, an "unidentified error" string is
 * returned. Not that this should happen, given that error_t is an enum...
 **/
VESSEL_API char const * vessel_error_message(vessel_error_t code);

/**
 * Return a string representation of the given error code. Also never returns
 * nullptr, see error_message() above.
 **/
VESSEL_API char const * vessel_error_name(vessel_error_t code);


#endif // VESSEL_ERROR_FUNCTIONS


/**
 * Undefine macros again
 **/
#undef VESSEL_START_ERRORS
#undef VESSEL_ERRDEF
#undef VESSEL_END_ERRORS

#undef VESSEL_ERROR_FUNCTIONS

#ifdef __cplusplus
} // extern "C"
#endif // __cplusplus


#endif // guard
