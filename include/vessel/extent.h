/**
 * This file is part of vessel.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt).
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#ifndef VESSEL_EXTENT_H
#define VESSEL_EXTENT_H

#include <vessel.h>

#include <vessel/author.h>
#include <vessel/algorithms.h>
#include <vessel/context.h>

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

/**
 * An extent ID is an opaque structure with the functions below defined for it.
 */
struct vessel_extent_id;


/**
 * Generate a new extent ID.
 *
 * Extents do not come out of nowhere, so in order to generate one, you must
 * provide an author ID that the extent belongs to. If the extent is logically
 * connected to a preceeding extent (see specs), you can optionally provide
 * this previous extent ID as well.
 *
 * The ID generation algorithm depends on hash functions defined in
 * vessel_algorithm_choices.
 */
VESSEL_API vessel_error_t
vessel_extent_id_generate(struct vessel_extent_id ** extent_id,
    struct vessel_algorithm_choices const * choices,
    struct vessel_author_id const * author_id,
    struct vessel_extent_id const * previous_extent_id);


/**
 * Create a new (invalid) extent ID.
 */
VESSEL_API vessel_error_t
vessel_extent_id_new(struct vessel_extent_id ** extent_id);


/**
 * Create a new extent ID from a buffer. This is the same as calling
 * vessel_extent_id_new() and vessel_extent_id_from_buffer() in sequence.
 */
VESSEL_API vessel_error_t
vessel_extent_id_create_from_buffer(struct vessel_extent_id ** extent_id,
    struct vessel_algorithm_choices const * choices,
    void const * buffer, size_t bufsize);


/**
 * Free the extent ID structure.
 */
VESSEL_API vessel_error_t
vessel_extent_id_free(struct vessel_extent_id ** extent_id);


/**
 * Return the size of an extent ID, in Bytes. The size depends on the algorithm
 * choices.
 */
VESSEL_API size_t
vessel_extent_id_size(struct vessel_algorithm_choices const * choices);


/**
 * Copy an extent ID to/from a buffer. An extent ID always requires exactly
 * vessel_extent_id_size() of buffer space.
 *
 * Neither function strictly needs to understand algorithm choices, but they
 * help check for valid value ranges.
 */
VESSEL_API vessel_error_t
vessel_extent_id_from_buffer(struct vessel_extent_id * extent_id,
    struct vessel_algorithm_choices const * choices,
    void const * buffer, size_t bufsize);

VESSEL_API vessel_error_t
vessel_extent_id_to_buffer(void * buffer, size_t * bufsize,
    struct vessel_algorithm_choices const * choices,
    struct vessel_extent_id const * extent_id);


/**
 * Compares two extent IDs.
 *
 * Returns an integer less than, equal to, or greater than zero if the
 * first extent ID is found, respectively, to be less than, to match, or be
 * greater than the second.
 *
 * A more POSIX-ish function that returns MAX_INT on errors is also available.
 */
VESSEL_API vessel_error_t
vessel_extent_id_compare(int * result,
    struct vessel_extent_id const * extent_id1,
    struct vessel_extent_id const * extent_id2);

VESSEL_API int
vessel_extent_id_cmp(
    struct vessel_extent_id const * extent_id1,
    struct vessel_extent_id const * extent_id2);


/**
 * Verify that an extent ID is created by the given author, and derived from
 * the given previous extent ID (which is optional). This lets callers establish
 * previous relationships (the previous extent ID) as well as sibling relationships
 * (multiple extent IDs/author IDs with the same previous extent ID).
 *
 * Note that if no previous extent ID is provided, the function cannot verify
 * anything, as root extent IDs are random by design. The function will merely
 * check parameters are provided, and return success.
 */
VESSEL_API vessel_error_t
vessel_extent_id_verify(struct vessel_extent_id const * extent_id,
    struct vessel_algorithm_choices const * choices,
    struct vessel_author_id const * author_id,
    struct vessel_extent_id const * previous_extent_id);



/**
 * Similarly, extents are opaque reference. They are created by and released
 * by a vessel_backing_store implementation, but most users will not need to
 * know this, and instead focus on the vessel_resource abstraction. Suffice to
 * say that you cannot create or free a vessel_extent without some context.
 */
struct vessel_extent;


/**
 * Given an extent, return its identifier, size (in blocks), payload size
 * (in bytes) and/or payload pointer.
 *
 * Note that directly writing to an extent payload is discouraged and not
 * specs conforming, use sections instead. This is a largely internal API
 * that may be useful for debugging purposes.
 */
VESSEL_API vessel_error_t
vessel_extent_identifier(struct vessel_extent_id ** id,
    struct vessel_extent const * extent);

/**
 * Return the default extent size, as expressed in blocks.
 */
VESSEL_API size_t
vessel_extent_default_size_in_blocks(void);

/**
 * Return the block size of extents. Extents are always multiple of this block
 * size.
 */
VESSEL_API size_t
vessel_extent_block_size(void);


VESSEL_API vessel_error_t
vessel_extent_size_in_blocks(size_t * size,
    struct vessel_extent const * extent);

VESSEL_API vessel_error_t
vessel_extent_payload_size(size_t * size,
    struct vessel_extent const * extent);

VESSEL_API vessel_error_t
vessel_extent_payload(void ** payload,
    struct vessel_extent const * extent);

VESSEL_API vessel_error_t
vessel_extent_payload_unused_offset(void ** offset,
    size_t * available,
    struct vessel_context const * ctx,
    struct vessel_extent const * extent);

VESSEL_API vessel_error_t
vessel_extent_version_tag(void * version_tag, size_t tag_size,
    struct vessel_extent const * extent);

VESSEL_API vessel_error_t
vessel_extent_algorithm_choices(struct vessel_algorithm_choices * choices,
    struct vessel_algorithm_context const * ctx,
    struct vessel_extent const * extent);


VESSEL_API vessel_error_t
vessel_extent_previous_identifier(struct vessel_extent_id ** id,
    struct vessel_extent const * extent);

VESSEL_API vessel_error_t
vessel_extent_author_identifier(struct vessel_author_id ** id,
    struct vessel_extent const * extent);


// TODO document this all

/**
 * An iterator for sections in an extent.
 */
struct vessel_section_iterator;

/**
 * Given a resource, create (read only) extent iterator.
 *
 * Using this iterator allows you to retrieve extents in logical order from
 * a resource.
 */
VESSEL_API vessel_error_t
vessel_section_iterator_create(struct vessel_section_iterator ** iterator,
    struct vessel_section_context * ctx,
    struct vessel_extent * extent);

VESSEL_API vessel_error_t
vessel_section_iterator_free(struct vessel_section_iterator ** iterator);

/**
 * Retrieve the next section in the extent.
 */
VESSEL_API vessel_error_t
vessel_section_iterator_next(struct vessel_section * section,
    struct vessel_section_iterator * iterator);


#ifdef __cplusplus
} // extern "C"
#endif // __cplusplus


#endif // guard
