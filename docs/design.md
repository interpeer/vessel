# vessel
Vessel is a generic container file format optimized for secure multi-authoring,
high I/O and (streaming) transmission.

These usages induce a number of requirements:

1. The streaming focus requires that content is subdivided into blocks which
   are well delineated, such that reading content is easily picked up at a block
   boundary.
1. The I/O focus requires that blocks of content are aligned well with file
   system block boundaries. It also requires that blocks of content can be
   updated efficiently.
1. The security focus requires that blocks of content are a) ideally encrypted,
   and b) leak little to no metadata, but c) are easily verifiable.
1. The multi-authoring focus requires that each block of content may be created
   independently by a different author.

**Note**: The approach taken here has similarities to but should not be confused
with [blockchain](https://en.wikipedia.org/wiki/Blockchain). It is consistent
with simplest definition of blockchain as a cryptographically linked chain of
blocks, but does not (need to) make use of cryptocurrencies. In order to avoid
confusion, we'll use the term *extent* for subdivision of content, and *extent
list* for the cryptographic linkage thereof.

## Security & Multi-Authoring

The obvious and simple scheme for secure multi-authoring is to distribute a
single authoring key to any party authorized to create extents. This approach
is brittle, however, in that it does not permit revoking access.

The scheme employed here is therefore more complex, and essentially equivalent
to a PKI. We distinguish between a single *ownership key pair* and multiple
*authoring key pairs*. Transfer of ownership can be conducted by transferring
the *ownership private key*, but that is (somewhat) outside of the scope of
this project. Authorizing and revoking *authoring key pairs* is in scope, and
uses [CAProck](https://codeberg.org/interpeer/caprock) tokens.

CAProck tokens are transferrable, which means read access to encrypted extents
can be granted outside of the content stream. At the same time, additional
tokens can also be embedded in the content stream.

To facilitate this, we define that each extent must be fully authored by a
single entity (key). In this fashion, the owner can author an extent with only
tokens and thus manage a distributed authorization database, while other
authors provide the resource itself.

The implication is that it is not sufficient to verify an extent via e.g. a
hash or a [MAC](https://en.wikipedia.org/wiki/Message_authentication_code), but
must instead be provided via a public key signature.

## Extents, Sections & Sub-Streams

From the above, one can treat all extents created by the same author as a sub-
stream of the overall resource stream. An alternative view is to sub-divide
a resource stream into specific types of content, such as CAProck tokens. Then
again, a logical sub-stream may mix different content types.

In order to facilitate all of these views, we encapsulate content within
extents into sections; each section has a type that defines its format, as well
as a topic. Vessel permits querying sub-streams by author, topic or section
type, or a combination thereof in order to provide efficiently interleaved
sub-streams.

Note however that due to the multi-authoring requirement above, it is not
guaranteed that two stream elements provided in two successive extents were
created and should be interpreted in the order they're retrieved. Such logical
ordering is outside of the scope of vessel, except for the specific resource
sub-streams described in this document.

## I/O and Mutability

In the above requirements we note that extents should be updatable. This is
an efficiency requirement in some sense, and best explored in contrast to other
technology.

First, a popular principle for verifying an entire file is to construct (a
variant of) a [Merkle tree](https://en.wikipedia.org/wiki/Merkle_tree), in
extents are individually hashed, hashes of adjacent blocks are hashed into
a parent hash, and so forth until a root hash is determined. If any block
is modified, the root hash will no longer match.

The downside of this scheme is that it effectively turns every resource into
something immutable. In order to communicate that a small change has been made,
such an extent's content has been modified, a comparatively large amount of
data needs to be synchronized: both the updated extent hash, as well as any
adjacent and parent hashes that contribute to the root changing.

This issue is annoying enough when dealing with relatively static resources,
but is acerbated in streaming. In streaming, one never modifies extents but
only adds additional ones. Here, every successive change would require an ever
larger amount of state being synchronized because the merkle tree grows with
each addition. It does not scale.

Other approaches such as blockchain furthermore rely on a particular sequence
of block (hashes) in order to determine causality. Causality determination is
an important problem to consider, but as we note above, multi-authoring may
mean that a particular sequence of extents is not the best reflection of the
order in which extent content should be processed. Blockchain enforces that
the extent (here: block) order reflects the content order via its proof
algorithms, but tends to delay block creation artificially in order to reach
consensus state. This again is unsuitable for streaming.

Vessel's key insight here is that these schemes attempt to verify multiple
properties with the same algorithm and simultaneously. This conceptual
efficiency leads to practical inefficiencies, however.

## Aspects of Verification

Above we've already outlined some aspects of verification that is often
considered as a whole rather than separately. These are:

1. The integrity of the extent's content.
1. The fact that all extents are authored by valid entities.
1. The correct sequence in which extents are to be arranged.
1. The completeness of this sequence.

### Completeness

Completeness is conceptually anathema to streaming; in a streaming situation,
it is by definition always unclear whether the stream ends, unless one
transmits an explicit end-of-stream marker. And if content follows that marker,
it is not inherently clear what this means for the stream.

The realization that data streams need not end cleanly and clearly can be
extended to any kind of resource. Is this document finished, or will it be
modified? When will modifications to this document complete?

Verifying completeness, therefore, is best treated as an ephemeral property of
the stream, and formulated thus: given a sequence of extents, can we know if is
complete at this point in time? We can, if the concept of what it means to be
complete is also encoded. This *can* happen in the form of a Merkle tree root
hash, for example. But the key point here is that such a statement of
completeness is always bound to a particular point in the resource's history.

### Extent Integrity and Authoring

As we explored previously, multi-authoring essentially requires cryptographic
signatures by the author over the extent's content. In doing so, we also
provide verification of the extent's integrity - keep in mind that a
cryptographic signature is a signature over a hash of the content; the hash
verifies integrity, while the signature itself verifies authorship.

### Sequence Correctness

The means by which a sequence of extents can be verified as the correct sequence
varies. Merkle trees rely on immutability of content, at which point a root
hash verifies the order of extents as well as the completeness of the sequence.
This root hash represents a single source of truth. Blockchain instead relies
on consensus, where multiple participants essentially vote on their view of
the correct order.

An alternative approach is to employ a deterministic tie breaker, and this is
the approach taken by vessel.

Sequence correctness revolves around a specific question, namely having received
an extent of a resource, how can a recipient verify that it is the next in
sequence? In a single author scenario, we can trust the author's signature.
Multi-authoring however means that multiple extents may be equally valid based
on their respective author's signatures alone.

For this, we need a tie breaking mechanism that is fair to multiple authors,
but also deterministic. As luck would have it, we can use cryptographic hash
functions for this.

Suppose as the author of an extent, Alice also generates a random key and embeds
it into the extent (it is therefore verified via her signature). Suppose further
that Bob and Carol both attempt to author the next extent. To do so, they both
calculate the next extent's identifier as follows: they concatenate the
preceding extent's identifer and their own public key, and produce a hash value
over the this. The result is the identifier.

The properties of the cryptographic hash functions are such that this is
unlikely to collide. It is therefore possible to treat the outputs as numbers,
and let the lower or higher number win (or perform bitwise comparison).

The mechanism is also deterministic, and anyone can replicate it. That means
that any extent identified in this fashion can be verified to either follow
its predecessor or not. Extents that cannot be verified can be rejected by
recipients.

**Parallel Creation**

While the above provides a clear signal for which of two extents created in
parallel should be accepted, it does introduce the question what should happen
to a rejected extent. Naively, the authoring party should just re-create its
identifier based on a previously accepted extent, but that implies
synchronization round-trips to check for new extents, and an still fail. We
need a better mechanism.

We solve this by accepting all extents created in parallel, and use their
identifier for ordering. That is, two (or more) extent IDs created from the
same parent will both be considered valid, but their order in the stream is
determined by the numeric value of their IDs, as per the criteria above.

In doing so, we create a [directed acyclic graph](https://en.wikipedia.org/wiki/Directed_acyclic_graph)
of extents - in this case, from a single extent to its N successors. If each
successor extent in turn yields a number of successors of its own, the graph
deepens (in fact, it remains a simple tree).


```
Extent A ----> Extent B1 ----> Extent C1
         |               \---> Extent C2
         |
         |---> Extent B2 ----> Extent D  ----> Extent F1
         |                               \---> Extent F2
         |
         \---> Extent B3 ----> Extent E1
                         |---> Extent E2 ----> Extent G
                         \---> Extent E3
```

Ideally, we want to avoid excessive branching, as branches make it harder
to determine which should be considered the "last" extent, in order to base
the next identifier off it.

There are two options immediately available here. Since we're ordering
identifers, always picking the last in the order would yield Extent E3 as
the best candidate for the last extent.

However, in the above image, Extents F1 and F2 have the least ambiguous path
from the origin - because they're the longest and have fewer branches. We can
select this least ambiguous path by giving each edge a weight of one dividided
by the number of siblings, and summing up the weight of all edges. The heaviest
edge wins.

| Path               | Weigths       | Decimal Sum   |
|--------------------|---------------|---------------|
| A -> B1            | 1/3           | 0.333         |
| A -> B2            | 1/3           | 0.333         |
| A -> B3            | 1/3           | 0.333         |
| A -> B1 -> C1      | 1/3 + 1/2     | 0.833         |
| A -> B1 -> C2      | 1/3 + 1/2     | 0.833         |
| A -> B2 -> D       | 1/3 + 1       | 1.333         |
| A -> B1 -> D -> F1 | 1/3 + 1 + 1/2 | **1.833**     |
| A -> B1 -> D -> F2 | 1/3 + 1 + 1/2 | **1.833**     |
| A -> B3 -> E1      | 1/3 + 1/3     | 0.666         |
| A -> B3 -> E2      | 1/3 + 1/3     | 0.666         |
| A -> B3 -> E2 -> G | 1/3 + 1/3 + 1 | 1.666         |
| A -> B3 -> E3      | 1/3 + 1/3     | 0.666         |

We combine both approaches:

1. For absolute ordering of extents, we go by identifier order, breadth first.
   That is, the order of extents above is:
   A, B1, B2, B3, C1, C2, D, E1, E2, E3, F1, F2, G
1. For determining the optimal extent to base new identifiers off, we follow the
   heaviest path. This yields either of F1 or F2 in the above example.
   - If multiple paths have equal weight, we order them by identifier order of
     the last identifier in each path. Assuming "F1" and "F2" are the actual
     identifiers, string-wise sorting may make F2 the best candidate for "last"
     extent.

The algorithm for the best candiate has the advantage that it is deterministic;
as long as a complete set of extents is known, the result will always be the
same. If a node (recall, we're discussing only those with authorship rights) has
an incomplete view of the extents in a resource, it may create extents on a
less than ideal branch. But once all such nodes are fully synchronized with
each other, they will reach the same conclusion again. We can therefore conclude
that the algorithm converges on the same result as synchronization completes.

A secondary property of the algorithm is that it is relatively easy to implement
in a memory friendly manner even when extents are synchronized piecemeal. As
extents arrive, paths from their parent are invalidated, which - in the absence
of a major network split - is the likeliest close to leaf extents.

**Origin Extents**

Given the above, origin extent identifiers should be randomly selected. The
identifer space must be large enough to make collisions unlikely in either
case.

### Mutability Revisited

The above methods are chosen carefully to either rely only on the content of
a single extent, or avoid using content hashes. This means that the content
of each extent can be treated as de facto mutable. This is very important for
local storage and I/O, but raises the question how to treat mutability across
multiple nodes.

Streaming and random I/O here present very different usage patterns. In
streaming, synchronization protocols would subscribe to updates for a resource.
Any additional extent can be considered such an update. In random I/O, it is
more likely that a node requests a specific extent by identifier. If we can
also provide an indicator of the version already known (if any), then the
sender can decide whether an update to the extent is available or not. The two
modes, of course, can be mixed: it is eminently possible to request updates to
a resource while randomly accessing specific extents.

Naive approaches would either provide a version for the entire resource, such
as via the root node hash provided by a Merkle tree. As described previously,
this requires synchronizing a number of hashes in order to reconstruct the tree,
and is thus less efficient with growing size of the resource.

Alternatively, each extent can be considered an individually versioned sub-
resource. While that allows for easy consideration of changes to an individual
extent, nodes would have to synchronize all versions for all extents to
understand the state of the other node, which is significantly less efficient
than synchronizing a Merkle sub-tree.

The multi-authoring approach offers a third path, however. Each author can
maintain exactly one strictly incrementing counter which is applied to any
changes they produce - either updates to existing extents or addition of new
extents. Since the subset of nodes that are authors is likely low compared to
the number of all participating nodes, and low compared to the number of
extents produced over time, this seems like a reasonable compromise in the
amount of state to synchronize. Furthermore, it is conflict-free amongst
authors.

One requirement here is that each extent contains this counter value. This seems
like a small price to pay.

## Versioning

It's worth highlighting that the above design rationale has made fairly
few specific claims. That is, there was no mention of a specific signature
or encryption algorithm to use, no mention of specific random function, etc.
This is on purpose.

We use an approach similar to that documented by [WireGuard](https://www.wireguard.com/)
for providing a unique version tag for each combination of algorithms. This
version tag is included in the extent header. Implementations reading an extent
can decide whether or not this version tag is supported, and react accordingly.

Similarly, we do not provide for a fixed extent size. While the default extent
size is chosen carefully, it is also encoded in the extent header. Again, it is
up to the implementation to decide which to support.

There is only one strict requirement: all extents belonging to a resource
MUST have the same version tag and extent size encoded in their header. Other
approaches are theoretically possible to implement, but would complicate
implementations without significant improvements to the feature set.
Implementations therefore MUST reject subsequent extents that do not match the
origin extent's versioning and size information.

## Layout

Given the above note on versioning, we do not provide strict offsets in this
design guide for most fields. Rather, we provide only the relative layout.

### Extent Layout

```
+-----+----+-------------+-------------+--------------------+------------+
| XEV | EV | Version Tag | Extent Size | Previous Extent ID | Current... |
+-----+----+----+--------+--+----------+--------------------+------------+
| ... extent ID | Author ID | Counter  | Payload ...                     |
+---------------+-----------+----------+                                 |
| ... payload ...                                                        |
| ...                                                        +-----------+
| ...                                                        | Signature |
+------------------------------------------------------------+-----------+
```

1. The literal 3-Byte ASCII string `XEV` marks the start of an extent as a
   boundary marker in Byte streams. `XEV` in reverse becomes `VEX`, which can
   be read as "VEssel EXtent.".
1. The single-Byte EV field provides an envelope version. This determines the
   layout of this Extent envelope/layout, and is currently a NUL Byte. It
   permits for future modifications. Note that the separate version tag is
   deliberately chosen to make such modifications unlikely, as most variations
   will appear in the algorithms chosen.
1. The version tag is a four-Byte field. Its value is a truncation of a hash
   of algorithm inputs; the precise algorithm is specified in the code. The
   size is chosen to provide sufficient collision resistance for the very
   limited number of valid inputs.
1. The Extent Size is a 16-bit Integer value in big-endian mode. It specifies
   the size of the extent from the beginning of the `XEV` marker to the end
   of the signature. It is to be interpreted as a multiplier for a nominal page
   size of 4096 Bytes, yielding possible extent sizes from 4096 Bytes to
   256 MiB.
1. We need to encode the identifier of the previous extent. The size depends
   on the cryptographic functions chosen, and is determined by the Version Tag.
   It is used to calculate the path weight (see above).
1. The Identifier for the extent follows (see discussion above). This size,
   too, depends on the Version Tag.
1. We'll need an identifier for the author as well, by which the system can
   look up key material for verifying the extent signature.
1. As the identifier is stable, we also provide a counter for mutability (see
   above). We'll make the size of the counter dependent on the version tag as
   well for keeping this envelope format stable. However, a counter of at least
   24 bits is recommended.
1. Next up is the extent Payload. It's size is the Extent Size minus the 10
   Bytes taken up by the header, minus the Bytes taken up by the identifier and
   signature.
1. Finally, the signature ends the extent; it's size is also determined by the
   version tag. The signature must be taken over the entirety of the extent's
   verifiable data, i.e.  from the beginning of the `XEV` marker to the end of
   the payload.

Reasonably secure cryptographic algorithms would likely yield a signature size
of 64 Bytes and above, and a similar assumption can be made for identifiers.
Further assuming a generous counter of 32 Bytes, the envelope may take up to
142 Bytes for typical configurations.

**A Note on Extent Sizes**

Extents need to be multiples of typical memory pages in size in order to be
efficiently mapped into memory - but memory page sizes are CPU and OS
dependent, and therefore not portable. The choice of specifying extent sizes
as multiples of 4096 Bytes is derived from Raymond Chen's
[article on page sizes used by Windows on various CPUs](https://devblogs.microsoft.com/oldnewthing/20210510-00/?p=105200)
and is also a sufficiently reasonable choice on modern UNIX derivates.

Smaller extent sizes waste space because the extent envelope must be repeated.
Larger extent sizes may e.g. block streaming applications, as the extent is
the atomic unit that needs to be transferred in order to be verified and
processed.

A good default can be derived from e.g.  video streaming applications. For
example, 4K video (state of the art at the time of writing) would, depending
on compression algorithm used, require transfer rates of up to ca. 32 MiB per
second. To permit for better recovery of lost data, smaller extent sizes than
this should be used for such applications, e.g. in the low single digit MiB
range.

**Effective Extent Sizes with Encryption**

Extent payloads should ideally be encrypted. While the container format does
not provide for any specific scheme that *must* be used - and, in fact, it is
possible to have unencrypted extents - the above notes on envelope and extent
sizes make clear that the extent payload is not typically cleanly divisible by
a cipher block size.

We therefore require that encryption modes use some form of
[cipertext stealing](https://en.wikipedia.org/wiki/Ciphertext_stealing) to avoid
unused extent payload bytes.

### Section Layout

As previously mentioned, extent payloads are subdivided into sections, each
of which may belong to an individual sub-stream. We also discussed how mutliple
authors per extent are not ideal; it follows that sections must then only carry
additional distinguishers for sub-streams.

A section type determines the layout and size of the section. Similarly, a
section topic can be used to combine sections of different types into a single
sub-stream.

**Fixed-Sized Section**

Some section types imply a fixed section size. For these sections, it is not
necessary to carry additional size information.

```
+------+-------+-------------+
| Type | Topic | Payload ... |
+------+-------+-------------+
```

**Variable-Sized Sections**

Other sections are variable sized. Sections can never be larger than the
extent in which they're encapsulated. However, sections need not be a multiple
of 4096 Bytes in size. A 28-bit value is sufficient, but hard to encode. We use
a simple, but variable-sized encoding scheme (see below).

To maintain compatibility with the fixed-size section header, the section size
follows the topic.

```
+------+-------+------+-------------+
| Type | Topic | Size | Payload ... |
+------+-------+------+-------------+
```

**Fields**

1. The section type is a 2-Byte, big-endian, unsigned integer value. Values
   below 1024 are reserved; applications must specifify higher values. In
   order for applications to determine compatibility with the section scheme,
   a special content type section is used (see below).
1. The topic is also a 2-Byte, big-endian, unsigned integer value. Similarly,
   values below 1024 are reserved. The same content type section can be used
   to determine application compatibility.
1. If present, the size is either 2 or 4 Bytes in size. See below for details
   on the encoding scheme.
1. The remainder of the section is the payload. Note that the section size -
   whether implied by the type, or specified explicitly - specifies the size of
   the section, not the size of the section payload. The payload size is easily
   derived from the section size.

**Size Encoding**

As mentioned above, the size should ideally be encoded as a 28-bit value. As a
compromise between ease of encoding and space efficiency, we define the size to
consist of either one or two 2-Byte, big-endian encoded unsigned integer words.

1. Read the first word. If the most signifcant bit is unset, you're done.
1. If the most significant bit is set,
    1. unset it.
    1. Shift the word into the most significant word of a 32-bit unsigned
       integer variable.
    1. Read the next word, and assign it to the least significant word of the
       32-bit value.

The result is the section size. The next Byte will be part of the section
payload.

Note that this scheme yields up to 31 bit sizes; any size larger than the extent
size minus the extent envelope must be rejected. Since it is difficult to
continue parsing the extent payload without precise offsets, it is a valid
implementation decision to reject the entire extent as corrupted.

**Padding**

As section sizes and extent sizes are not strictly aligned, it is very likely
that the extent contains unused payload bytes. We use a simple variation of the
[PKCS#7](https://datatracker.ietf.org/doc/html/rfc5652#section-6.3) padding
scheme to fill the remainder of the extent payload.

Note that above we mandated the use of ciphertext stealing. This is required
due to the uneven extent payload size. The extent payload may still be only
partially filled, and we wish to pad the unused Bytes here.

First, sections are encoded with a size. We therefore do not need the padding
to signal the end of plain text input. It is therefore feasible for us to use
zero-length padding (i.e. not pad at all) if the sections happen to fill the
extent payload area completely.

If padding is to be used, however, we follow the PKCS#7 scheme as above. Since
the padding can - in principle - be significantly larger than 256 Bytes, the
value we use for the padding Bytes is the remaining payload size mod 256.

## Encryption

This specification mentions extent payload encryption a few times, but does
not provide much in the ways of specifics. For the purposes of this
specification, encryption is symmetric encryption of (some of) the extent.

Specially, we have to distinguish between envelope information that is necessary
in order to proceed with parsing and processing. That is *most*, but not all
of the extent header, as well as the signature.

There are [authenticated encryption](https://en.wikipedia.org/wiki/Authenticated_encryption)
modes available that provide some of the same guarantees as we discuss above.
Specifically, the relatively easy to implement encrypt-then-MAC construct would
permit authenticated encryption. Furthermore, the MAC can be verified
independently of decryption, since different keys are used for both operations.

As noted previously, however, this requires sharing of the MAC key, possibly
independently of the encryption key. Public key based signatures may remain
the better option. This specification treats this as one of the decisions
encoded in the version tag.

However, it is clear that of the envelope information, the following parts
must be in plain text:

1. The envelope header, up to and including the extent identifier.
1. The envelope footer with the signature (or MAC).

The first few fields are required simply for skipping ahead to the next
extent, if necessary. The previous and current identifier are used to calculate
extent path lengths and weights; these are necessary simply for ordering
extents well. At the same time, the signature is used to verify the extent.

However, the counter is not required to be known. Since it is only used to
interpret which version of the extent to interpret at any given time, it is
also only required when the extent has been successfully decrypted.

Note that authenticated encryption provides little information on whether
decryption succeeded. To know this, applications must interpret the decrypted
payload. Including the counter into the plaintext may help verify this. It is
strongly recommended that applications place easy to verify sections into the
payload at known offsets to verify that decryption succeeded.

## Standard Section Types

Since there are reserved values for section types, it stands to reason that
some sections have well-defined, standard meanings. As mentioned above, one
such standard section describes the content type of the resource.

**Section: CRC32**

The section type is `1 (0x01)`. The only permissible topic type is the default
metadata topic.

The section is only valid at the beginning of an extent payload (offset 0).
This is mostly to do with how to interpret the field; more lenient scenarios
could be considered. For the sake of simplicity, if the section is encountered
anywhere other than at offset 0 of the payload, applications MUST discard it.

A valid section provides a CRC32 checksum over the remainder of the extent
payload, including padding (if any). This section provides for a means for
applications to verify that decryption succeeded. It is strongly recommended
that applications use it unless they provide different means for such
verification.

Verification that decryption succeded is relatively simple:

1. Check that we have a CRC32 section at offset 0. If that is not the case,
   the application has to verify the extent somehow.
1. Check that the topic is the default metadata topic. If that is not the
   case, fail validation.
1. Check that the CRC32 sum in the section value matches the checksum over
   the remainder of the extent. That starts at offset 8, and runs until the
   end of the extent. If the value matches, decryption succeeded.

When an extent is intended to be encrypted, it is easy enough to initialize
the payload with an empty CRC32 section. Write the CRC32 checksum into its
value field after applying padding, just prior to the encryption step.

**Section: Content Type**

The section type is `10 (0x0A)`.

The content type section can signal to applications whether and how to consume
a resource. It can be treated as largely semantically equivalent to the
collection of [representation headers](https://datatracker.ietf.org/doc/html/rfc7231#section-3)
in HTTP, the best known of which is probably
[Content-Type](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Content-Type).

However, the section does not need to apply to the entirety of the resource.

1. If the section's topic is the *default metadata topic*, it specifies the
   default content type to assume.
1. If the section's topic is any other value, it specifies the content type
   only sections of this topic. A topic content type therefore overrides the
   default content type.
1. The content type applies only to application-defined sections (i.e. section
   types 1024 and above). Reserved sections always have specific, hard-coded
   meanings defined here.

Much like the HTTP headers, the section can carry multiple key-value pairs. The
collection of key-value pairs describes the content type in sufficient detail for
applications to interpret application defined sections in one fashion or another.

1. The section payload is interpreted as a UTF-8 string.
1. Key-value pairs are separated with a semicolon `; (U+003B)`.
1. Keys are separated from values with an equals sign `= (U+003D)`.
1. Either separator used as a key or value character must be escaped, i.e.
   preceded by a backslash `\ (U+005C)`.
1. Keys must be stripped of leading and trailing whitespace. In values,
   whitespace is considered part of the value.
   Whitespace is interpreted as [ICU's \s regexp character](https://unicode-org.github.io/icu/userguide/strings/regexp.html)

The specific keys and values used in this section can be rather arbitrary.
We define only the `media-type` and `charset` keys (ASCII equivalent characters),
to be interpreted as HTTP's `Content-Type` header's media type and charset,
respectively.

Other HTTP representation headers are not part of this specification, though a
a reasonable approach would be to treat the header name in lower case as the
key, and the header value as the value.

There is no particular requirement on the part of vessel to provide this
section. However, if present, it MUST preceed other sections for the topic
(if specified for a topic other than the default metadata topic). Furthermore,
it SHOULD be repeated periodically, such that streaming applications can join
a stream at offsets other than the beginning. Applications MAY produce this
section for every extent, but MUST NOT provide more than one per extent.
Repetitions SHOULD be equivalent to the first section produced.

This creates a uniqueness issue. Applications MUST only apply the terms of this
section to subsequent sections. If they encounter subsequent occurrences of the
section, they MUST check occurrences for equivalence. If the subsequent
occurence is not equivalent to the first they encountered, applications SHOULD
abort processing of the stream. If they choose not to, the new occurrence MUST
become leading, and subsequent sections MUST be intepreted accordingly.

The above paragraph is provided to provide a minimal safety approach in the
face of malicious applications.

## Standard Topics

Reserved topics carry information that is part of the vessel specification,
and always have specific meanings.

**Topic: Authentication, Authorization and Accounting**

The topic type is `0 (0x00)`.

The topic carries AAA information, such as sections containing CAProck tokens,
etc.

**Topic: Default Metadata**

The topic type is `10 (0x0A)`.

The default metadata topic should be used to carry resource metadata that can
be considered global to the entire resource. Such metadata may e.g. be the
content type, a resource name, [Dublin Core properties](https://www.dublincore.org/specifications/dublin-core/dcmi-terms/),
etc.
