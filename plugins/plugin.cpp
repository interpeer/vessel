/**
 * This file is part of vessel.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2023 Interpeer gUG (haftungsbeschränkt).
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <vessel/plugins.h>

#include <liberate/sys/plugin.h>
#include <liberate/logging.h>

namespace {

#if defined(VESSEL_PLUGIN_PROFILE_TABLE)
#  if VESSEL_PLUGIN_PROFILE_TABLE==full
#    include "full.cpp"
#    define VESSEL_TABLE_NAME vessel_lookup_table_full
#  elif VESSEL_PLUGIN_PROFILE_TABLE==extended
#    include "extended.cpp"
#    define VESSEL_TABLE_NAME vessel_lookup_table_extended
#  else
#    error "Bad profile table specified for plugin."
#  endif
#else
#  error "No profile table specified for plugin."
#endif // VESSEL_PLUGIN_PROFILE_TABLE

static struct vessel_plugin_profile_v1 plugin_data =
{
  nullptr,
};

static struct liberate::sys::plugin_meta meta =
{
  VESSEL_PLUGIN_TYPE_PROFILE,
  VESSEL_PLUGIN_PROFILE_V1,
  &plugin_data,
  nullptr,
};

} // anonymous namespace


extern "C" {

VESSEL_API void *
liberate_plugin_meta()
{
  if (!plugin_data.algorithm_context) {
    auto amount = sizeof(VESSEL_TABLE_NAME) / sizeof(vessel_algorithm_choices);
    LIBLOG_INFO("Creating algorithmm context with " << amount << " of algorithm combinations...");
    auto err = vessel_algorithm_context_create_from_choices(
        &plugin_data.algorithm_context,
        VESSEL_TABLE_NAME, amount);
    if (VESSEL_ERR_SUCCESS != err) {
      LIBLOG_ERROR("Could not create algorithm context, aborting!");
      return nullptr;
    }
    LIBLOG_INFO("Success.");
  }

  return &meta;
}

} // extern "C"
