#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# This file is part of vessel.
#
# Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
#
# Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt).
#
# SPDX-License-Identifier: GPL-3.0-only
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Algorithm possibilities from
# https://specs.interpeer.io/draft-jfinkhaeuser-vessel-container-format/
ALGOS_ORDER_00 = (
    'version_tag_hash',
    'extent_identifier_hash',
    'author_identifier_hash',
    'nonce_hash',
    'signature_hash',
    'asymmetric_signature',
    'message_authentication',
    'symmetric_encryption',
    'signature_algorithm',
    'private_header_flag',
)
HASH_ALGOS_00 = {
  'sha3-512': 'MUST',
  'sha3-384': 'MUST',
  'sha3-256': 'MUST',
  'sha3-224': 'MUST',
  'sha2-512': 'MAY',
  'sha2-384': 'MAY',
  'sha2-256': 'MAY',
  'sha2-224': 'MAY',
}

ALGOS_DRAFT_00 = {
    'version_tag_hash': HASH_ALGOS_00,
    'extent_identifier_hash': HASH_ALGOS_00,
    'author_identifier_hash': HASH_ALGOS_00 | { 'none': 'MUST', },
    'nonce_hash': HASH_ALGOS_00,
    'signature_hash': HASH_ALGOS_00 | { 'eddsa': 'MUST' },
    'asymmetric_signature': {
        'ed25519': 'MUST',
        'ed448': 'MUST',
        'dsa': 'SHOULD',
        'rsa': 'SHOULD',
    },
    'message_authentication': {
        'poly1305': 'MUST',
        'kmac128': 'SHOULD',
        'kmac256': 'SHOULD',
    },
    'symmetric_encryption': {
        'chacha20': 'MUST',
        'aead_aes_128_gcm': 'SHOULD',
        'aead_aes_256_gcm': 'SHOULD',
        'aead_aes_128_ccm': 'SHOULD',
        'aead_aes_256_ccm': 'SHOULD',
        'none': 'MUST',
    },
    'signature_algorithm': {
        'aead': 'MUST',
        'keypair': 'MUST',
        'mac': 'MUST',
    },
    'private_header_flag': {
        'ph=0': 'MUST',
        'ph=1': 'MUST',
    },
}

def validate_words(chosen, spec, verbose = True):
    for algo, choice in chosen.items():
        if algo not in spec:
            if verbose:
                click.echo(f'Algorithm "{algo:s}" is unknown, aborting.', err=True)
            return False
        if choice not in spec[algo]:
            if verbose:
                click.echo(f'Choice "{choice:s}" is not valid for algorithm "{algo:s}", aborting.', err=True)
            return False
    return True


def validate_combinations_00(chosen, spec, verbose = True):
    if chosen['symmetric_encryption'] == 'none':
        if chosen['signature_algorithm'] == 'aead':
            if verbose:
                click.echo('Without AEAD (symmetric_encryption), signature_algorithm MUST NOT be "aead"!', err=True)
            return False
    else:
        if chosen['signature_algorithm'] != 'aead':
            if verbose:
                click.echo('With AEAD (symmetric_encryption), signature_algorithm MUST be "aead"!', err=True)
            return False

    if chosen['signature_hash'] == 'eddsa':
        if chosen['asymmetric_signature'][0:2] != 'ed':
            if verbose:
                click.echo('The "eddsa" signature hash is only valid for edwards curve asymmetric_signature choices!', err=True)
            return False


    return True


def validate_choices(chosen, spec, verbose = True):
    if not validate_words(chosen, spec, verbose):
        return False

    # FIXME this is hardcoded for draft 00
    if not validate_combinations_00(chosen, spec, verbose):
        return False

    return True


def parse_choices(string, order):
    values = [x.strip() for x in string.split(';')]
    if len(values) != len(order):
        import sys
        click.echo(f'We need {len(order):d} inputs, but got {len(values):d}, aborting.', err=True)

    ret = {}
    for i, name in enumerate(order):
        ret[name] = values[i]
    return ret

def print_algos_ordered(algos):
    must = []
    should = []
    may = []
    for name, req in algos.items():
        if req == 'MUST':
            must.append(name)
        elif req == 'SHOULD':
            should.append(name)
        elif req == 'MAY':
            may.append(name)
        else:
            import sys
            click.echo(f'Unknown requirement word "{req:s}", aborting.', err=True)
            return False

    for name in sorted(must):
        click.echo(f'    {name:<30}  MUST')
    for name in sorted(should):
        click.echo(f'    {name:<30}  SHOULD')
    for name in sorted(may):
        click.echo(f'    {name:<30}  MAY')

    return True

def print_algorithms(order, spec):
    click.echo('Available algorithms')
    click.echo('--------------------\n')
    for name in order:
        click.echo(f'{name:s}:')
        if not print_algos_ordered(spec[name]):
            return False
    return True


def pre_validation(choices, verbose, order, specs):
    parsed = parse_choices(choices, order)
    if not parsed:
        return -1
    if verbose:
        click.echo('Parsed input as:')
        from pprint import pformat
        click.echo(pformat(parsed))

    if not validate_choices(parsed, specs):
        import sys
        sys.stderr.write('Specification does not validate!')
        return -2
    click.echo('Specification validates OK!')

    return parsed


def assemble_input(validated, order):
    entry_list = []
    for name in order:
        entry_list.append(validated[name])
    return ';'.join(entry_list)


def hash_instance(name):
    import hashlib
    if name == 'sha3-512':
        return hashlib.sha3_512()
    elif name == 'sha3-384':
        return hashlib.sha3_384()
    elif name == 'sha3-256':
        return hashlib.sha3_256()
    elif name == 'sha3-224':
        return hashlib.sha3_224()
    elif name == 'sha2-512':
        return hashlib.sha512()
    elif name == 'sha2-384':
        return hashlib.sha384()
    elif name == 'sha2-256':
        return hashlib.sha256()
    elif name == 'sha2-224':
        return hashlib.sha224()
    else:
        return None



def calculate_version_tag(validated, order, length):
    vth1 = hash_instance(validated['version_tag_hash'])
    if not vth1:
        click.echo('Version tag hash unsupported: {validated["version_tag_hash"]:s}', err=True)
        return None

    assembled = assemble_input(validated, order)
    vth1.update(assembled.encode('ascii'))
    pre_hash = vth1.digest()

    vth2 = hash_instance(validated['version_tag_hash'])
    vth2.update(b'Vessel')
    vth2.update(pre_hash)
    the_hash = vth2.digest()

    return (the_hash[0:length], assembled)


class TextFormatter(object):

    def front_matter(self, profile, order, specs):
        click.echo(f'Permutations for profile: {profile:s}')
        click.echo('=====')
        click.echo()


    def output_tag(self, tag, parsed, inputs, order, specs):
        click.echo(f'0x{tag.hex():s}: {inputs:s}')

    def back_matter(self, profile, order, specs):
        pass

class HeaderFormatter(object):
    def __init__(self, format):
        self.format = format

    def front_matter(self, profile, order, specs):
        if self.format in ('enums', 'both'):
            self.header(profile, order, specs)

        if self.format in ('table', 'both'):
            self.table(profile, order, specs)

    def header(self, profile, order, specs):
        click.echo('''/**
 * Enumerations for all algorithms.
 */

#if defined(__cplusplus)
#  define VESSEL_ALGO_BASE_TYPE : uint8_t
#else
#  define VESSEL_ALGO_BASE_TYPE
#endif

typedef enum VESSEL_ALGO_BASE_TYPE {''')

        for idx, (algo, _) in enumerate(HASH_ALGOS_00.items()):
            sym = self.symbolize('HA', algo)
            click.echo(f'  {sym:s} = {idx+1:d},')
        click.echo(f'''  VESSEL_HA_NEXT = {idx+2:d},
}} vessel_hash_algorithms;
''')

        for item in order:
            short = self.shortname(item)
            click.echo(f'typedef enum VESSEL_ALGO_BASE_TYPE\n{{')
            none = self.symbolize(short, 'unsupported')
            click.echo(f'  {none:s} = 0,')
            for idx, algo in enumerate(sorted(specs[item].keys())):
                req = specs[item][algo]
                sym = self.symbolize(short, algo)
                value = None
                if algo in HASH_ALGOS_00:
                    value = self.symbolize('HA', algo)
                else:
                    if 'hash' in item:
                        value = f'VESSEL_HA_NEXT + {idx:d}'
                    else:
                        value = str(idx + 1)
                click.echo(f'  {sym:s} = {value:s},')
            click.echo(f'}} vessel_{item:s};\n')

        click.echo(f'''/**
 * Algorithm choices structure.
 */
struct vessel_algorithm_choices
{{
    uint32_t                        version_tag;''')
        for item in order:
            click.echo(f'    vessel_{item:s} {item:>24s};')
        click.echo(f'''}};

typedef struct vessel_algorithm_choices vessel_algorithm_choices;
''')


    def table(self, profile, order, specs):
        # FIXME type depends on the tag length, so this needs work for anything
        #       other than 4 octets
        click.echo(f'''/**
 * Lookup table for profile: {profile:s}
 */
''')
        click.echo(f'struct vessel_algorithm_choices vessel_lookup_table_{profile:s}[] = {{')


    def shortname(self, name):
        parts = name.upper().split('_')
        letters = [part[0] for part in parts]
        return ''.join(letters)

    def symbolize(self, prefix, name):
        ret = name.upper()
        ret = ret.replace('-', '_')
        ret = ret.replace('=', '_EQ_')
        return f'VESSEL_{prefix:s}_{ret:s}'

    def output_tag(self, tag, parsed, inputs, order, specs):
        if self.format not in ('table', 'both'):
            return

        # click.echo(f'0x{tag.hex():s}: {inputs:s}')
        click.echo(f'  {{ 0x{tag.hex():s},')
        for item in order:
            short = self.shortname(item)
            sym = self.symbolize(short, parsed[item])
            click.echo(f'    {sym:s},')
        click.echo('  },')


    def back_matter(self, profile, order, specs):
        if self.format not in ('table', 'both'):
            return
        click.echo('};')


def generate_permutations(profile, order, specs):
    def _recursively_permutate(current, profile, order, specs):
        partial = current.copy()
        idx = len(partial)
        if idx >= len(order):
            if validate_choices(partial, specs, False):
                yield partial
            return
        name = order[idx]
        for algo, req in specs[name].items():
            if profile == 'full' and req != 'MUST':
                continue
            partial[name] = algo
            for res in _recursively_permutate(partial, profile, order, specs):
                yield res

    current = {}
    for res in _recursively_permutate(current, profile, order, specs):
        yield res


def produce_permutations(profile, order, specs, length, formatter):
    formatter.front_matter(profile, order, specs)
    for perm in generate_permutations(profile, order, specs):
        tag, assembled = calculate_version_tag(perm, order, length)
        formatter.output_tag(tag, perm, assembled, order, specs)
    formatter.back_matter(profile, order, specs)


##############################################################################
# CLI

import click

@click.command()
def algorithms():
    """List algorithms for the current draft."""
    # We don't offer selection of the draft yet, but this would become an option.
    if not print_algorithms(ALGOS_ORDER_00, ALGOS_DRAFT_00):
        return -1


@click.command()
@click.argument('choices')
@click.option('-v', '--verbose', is_flag=True, help='Enables verbose mode')
def validate(choices, verbose):
    """Validate a list of concatenated algorithm choices against the specs."""
    # We don't offer selection of the draft yet, but this would become an option.
    validated = pre_validation(choices, verbose, ALGOS_ORDER_00, ALGOS_DRAFT_00)
    if not isinstance(validated, dict):
        return validated



@click.command()
@click.argument('choices')
@click.option('-l', '--length', default=4, help='Length of the version tag, in octets/bytes')
@click.option('-v', '--verbose', is_flag=True, help='Enables verbose mode')
def versiontag(choices, length, verbose):
    """Calculates a version tag from the algorithm choices provided."""
    # We don't offer selection of the draft yet, but this would become an option.
    validated = pre_validation(choices, verbose, ALGOS_ORDER_00, ALGOS_DRAFT_00)
    if not isinstance(validated, dict):
        return validated

    # Calculate the tag.
    tag, assembled = calculate_version_tag(validated, ALGOS_ORDER_00, length)
    if not tag:
        click.echo('Version tag could not be calculated!', err=True)
        return -3

    click.echo(f'Version tag is: 0x{tag.hex():s}')


@click.command()
@click.argument('profile', type=click.Choice(['full', 'extended'], case_sensitive=False))
@click.option('-l', '--length', default=4, help='Length of the version tag, in octets/bytes')
@click.option('-f', '--format',
        type=click.Choice(['text', 'enums', 'table', 'both'], case_sensitive=False),
        default='text', help='Output format; can produce text or C/C++ headers.')
def profile(profile, length, format):
    """Calculates all version tags for a given profile.

    'text' produces human readable output. 'enums" produces enumerations for use
    in C or C++ code that represent the various algorithm choices. 'table'
    produces a lookup table from version tag to a combination of algorithm
    choices. 'both' produces first the enums, then the lookup table.
    """
    # TODO *instead* of profile, also accept a spec for limited support, but
    #      that's harder...

    formatter = None
    f = format.lower()
    if f == 'text':
        formatter = TextFormatter()
    else:
        formatter = HeaderFormatter(f)

    produce_permutations(profile, ALGOS_ORDER_00, ALGOS_DRAFT_00, length, formatter)


@click.group()
def cli():
    pass

cli.add_command(algorithms)
cli.add_command(validate)
cli.add_command(versiontag)
cli.add_command(profile)


if __name__ == '__main__':
    cli()
