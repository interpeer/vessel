Tools
=====

- `version_tag_hash.py` - Python script for generating version tag hashes.


Version Tag Hashes
------------------

You can use the `version_tag_hash.py` script for generating version tags
from a choice of algorithms. The same script can be used for creating enums
for all algorithms from a spec (only -00 supported so far); this is how
`include/vessel/generated/enums.h` is generated.

Additionally, you can create a full lookup table for the full or extended
profiles. However note that the full table creates a header of several MiB
in size, while the extended table is about a GiB and a half. Compiled, the
full table takes some 300KiB, while the extended is significantly larger.

(With C++, specifying the enum base type can reduce this somewhat.)
