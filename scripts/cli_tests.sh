#!/bin/bash

set -x
set -e

SRC_DIR="$1"
SCRIPT_NAME="$2"
BUILD_DIR="$PWD"
TEST_DIR="${SRC_DIR}/test"
TEST_KEY="${TEST_DIR}/test_cli_key.pem"

TMP_DIR=$(mktemp --tmpdir -d vessel_cli_tests.XXXXXXXXXX)
TMP_RESOURCE="${TMP_DIR}/testresource"

FILES="AUTHORS README.md LICENSE codemeta.json Pipfile.lock"

cd "${TMP_DIR}"

source "${SRC_DIR}/test/cli/${SCRIPT_NAME}.sh"

echo "!!! Cleanup"
rm -rf "${TMP_DIR}"
