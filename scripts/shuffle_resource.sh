#!/bin/bash

set +x
set +e

INPUT="$1"
OUTPUT="$2"
BLOCKS="$3"

if [ -z "${INPUT}" -o -z "${OUTPUT}" ] ; then
  echo "usage: $0 <input resource> <output resource> [size in blocks]" >&2
  exit 1
fi

if [ -z "${BLOCKS}" ] ; then
  BLOCKS=1
elif [ ! "${BLOCKS}" -ge 1 ] ; then
  BLOCKS=1
fi
SPLITSIZE=$(expr "${BLOCKS}" '*' 4096)
echo "Splitting every ${BLOCKS} blocks (${SPLITSIZE} bytes)."

TMPDIR=$(mktemp --tmpdir -d vessel-shuffle-XXXXX)

split -b "${SPLITSIZE}" "${INPUT}" "${TMPDIR}/part-"

cat $(find "${TMPDIR}" -type f -name "part-*" | sort -R) >"${OUTPUT}"

rm -rf "${TMPDIR}"
